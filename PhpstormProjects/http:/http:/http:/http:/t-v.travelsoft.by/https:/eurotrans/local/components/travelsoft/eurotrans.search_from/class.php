<?php

use travelsoft\eurotrans\Settings;

/**
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class TravelsoftEurotransSearchFrom extends CBitrixComponent {

    public function executeComponent() {

        Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

        try {

            $this->arResult['DEPARTURE_POINTS'] = array();
            $this->arResult['ARRIVAL_POINTS'] = array();

            $this->arResult['DEPARTURE_POINTS'] = \travelsoft\eurotrans\Utils::getDeparturePoints();

            if (empty($this->arResult['DEPARTURE_POINTS'])) {
                throw new Exception("departure points not found");
            }
            foreach ($this->arResult['DEPARTURE_POINTS'] as $point) {

                $cacheId = md5('eurotrans_arrival_points_' . $point["id"] . '_' . \travelsoft\eurotrans\Settings::cacheTime());

                $cache = new \travelsoft\eurotrans\adapters\Cache($cacheId);

                if (!empty($result = $cache->get())) {
                    $this->arResult['ARRIVAL_POINTS'][$point["id"]] = $result;
                    foreach ($result as $point2) {

                        $this->arResult['DATES'][$point["id"]][$point2['id']] =
                                travelsoft\eurotrans\Utils::getTripsDatesByLocations($point["id"], $point2['id']);

                        $this->arResult['DATES'][$point2["id"]][$point['id']] =
                                travelsoft\eurotrans\Utils::getTripsDatesByLocations($point2['id'], $point["id"]);
                    }
                }
            }

            if (is_array($this->arParams['REQUEST']) && !empty($this->arParams['REQUEST'])) {

                $request = &$this->arParams['REQUEST'];

                if (!isset($this->arResult['ARRIVAL_POINTS'][$request['location_from']])) {

                    $this->arResult['ARRIVAL_POINTS'][$request['location_from']] =
                            travelsoft\eurotrans\Utils::getArrivalPointsByDeparturePoint($request['location_from']);

                }

                if ($request['location_from'] > 0 && $request['location_to'] > 0) {

                    $this->arResult['DATES'][$request['location_from']][$request['location_to']] =
                                travelsoft\eurotrans\Utils::getTripsDatesByLocations($request['location_from'], $request['location_to']);

                    $this->arResult['DATES'][$request['location_to']][$request['location_from']] =
                            travelsoft\eurotrans\Utils::getTripsDatesByLocations($request['location_to'], $request['location_from']);

                    if (!strlen($request['date_from']) && !empty($this->arResult['DATES'][$request['location_from']][$request['location_to']])) {
                        $request["date_from"] = date("d.m.Y", strtotime($this->arResult['DATES'][$request['location_from']][$request['location_to']][0]));
                    }

                    /*if ($request["adults"] <= 0 && $request["social"] <= 0 && $request["children"] <=0) {
                        $request["adults"] = "1";
                    }*/

                }

            } elseif ($this->arParams['DEF_DEP_POINT'] > 0 && $this->arParams['DEF_ARR_POINT'] > 0) {

                $this->arParams['REQUEST']['location_from'] = $this->arParams['DEF_DEP_POINT'];
                $this->arParams['REQUEST']['location_to'] = $this->arParams['DEF_ARR_POINT'];

                if (!empty($this->arResult['DATES'][$this->arParams['DEF_DEP_POINT']][$this->arParams['DEF_ARR_POINT']])) {
                    $this->arParams['REQUEST']['date_from'] = date("d.m.Y", strtotime($this->calculateNearestDate($this->arResult['DATES'][$this->arParams['DEF_DEP_POINT']][$this->arParams['DEF_ARR_POINT']])));

                    //$this->arParams['REQUEST']['date_from'] = date("d.m.Y", strtotime($this->arResult['DATES'][$this->arParams['DEF_DEP_POINT']][$this->arParams['DEF_ARR_POINT']][0]));
                } else {
                    $this->arParams['REQUEST']['date_from'] = date("d.m.Y", time());
                }
                //$this->arParams['REQUEST']['adults'] = "1";

            }

            $this->IncludeComponentTemplate();

            $this->incExtFiles();

        } catch (Exception $ex) {

            (new travelsoft\eurotrans\Logger())
                    ->write("Component: " . $this->__name . "; Error: " . $ex->getMessage());
        }
    }

    private function calculateNearestDate($arrDates){
        $minDate = null;
        foreach ($arrDates as $date){
            if(strtotime($date) >= strtotime(date("d.m.Y", time()))){
                if(($minDate == null) || ($date < $minDate)){
                    $minDate = $date;
                }
            }
        }
        if($minDate == null){
            return date("d.m.Y", time());
        }
        return $minDate;
    }

    /**
     * Подключение внешних файлов
     */
    public function incExtFiles () {

        \Bitrix\Main\Page\Asset::getInstance()->addCss($this->__path . "/css/style.css", true);

        \Bitrix\Main\Page\Asset::getInstance()->addCss($this->__path . "/css/jquery.webui-popover.min.css", true);

        \Bitrix\Main\Page\Asset::getInstance()->addJs("/local/modules/travelsoft.eurotrans.booking/js/cache.min.js", true);

        \Bitrix\Main\Page\Asset::getInstance()->addJs("/local/modules/travelsoft.eurotrans.booking/js/date_format.min.js", true);

        \Bitrix\Main\Page\Asset::getInstance()->addJs($this->__path . "/js/jquery.webui-popover.min.js", true);

        \Bitrix\Main\Page\Asset::getInstance()->addJs($this->__path . "/js/script_2.js", true);
    }

}
