<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
$this->setFrameMode(true);
?>

<?if(!empty($arResult)):?>

<div class="widget section-sidebar left_block">
    <?if(!empty($arParams["TEXT_TITLE"])):?>
        <div class="widget-title ui-title-inner ui-title-inner_lg">
            <span class="ui-title-inner__inner"><?=htmlspecialchars_decode($arParams["TEXT_TITLE"])?></span>
        </div>
    <?endif?>
    <div class="widget-content">
        <div class="owl-carousel owl-theme owl-theme_control-center owl-theme_control-center_mrg enable-owl-carousel" data-pagination="false" data-navigation="true" data-single-item="true" data-auto-play="7000" data-transition-style="fade" data-main-text-animation="true" data-after-init-delay="3000" data-after-move-delay="1000" data-stop-on-hover="true">
            <?foreach($arResult as $arItem):?>
                <?$comma = false;?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                $img = getSrc($arItem["PROPERTIES"]["PHOTO"]["VALUE"], array('width' => 360, 'height' => 222), NO_PHOTO_PATH_360_222); ?>

                <div class="b-post b-post_bg-white clearfix" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="entry-media">
                        <a class="js-zoom-images img-hover-effect" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                            <img class="img-responsive" src="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>">
                            <div class="b-text__inner">
                                <?if(!empty($arItem["PROPERTIES"]["TYPE"]["VALUE"])):?>
                                    <span class="entry-label bg-second"><?=$arItem["PROPERTIES"]["TYPE"]["VALUE"][0]?></span>
                                <?endif?>
                                <?if(!empty($arItem["PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"])):?>
                                    <?$comma = true;?>
                                    <span class="entry-label"><?=$arItem["PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"]?></span>
                                <?endif?>
                                <?if(!empty($arItem["PROPERTIES"]["CITY"]["DISPLAY_VALUE"])):?>
                                    <?if($comma):?>, <?endif?>
                                    <span class="entry-label"><?=$arItem["PROPERTIES"]["CITY"]["DISPLAY_VALUE"]?></span>
                                <?endif?>
                            </div>
                        </a>
                    </div>
                    <div class="entry-main events_block">
                        <div class="entry-header">
                            <div class="entry-meta">
                                <?if(isset($arItem["DATES"]) && !empty($arItem["DATES"])):?>
                                    <span class="entry-meta__item">
                                        <i class="icon fa-calendar text-second"></i>
                                        <span class="entry-meta__link text-primary">
                                            <strong><?=implode(', ', $arItem["DATES"])?></strong>
                                        </span>
                                    </span>
                                <?endif?>
                            </div>
                            <div class="entry-title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                        </div>
                        <?if(!empty($arItem["PROPERTIES"]["TEXT_PREVIEW"]["VALUE"]["TEXT"])):?>
                            <div class="entry-content">
                                <p>
                                    <?=substr2($arItem["PROPERTIES"]["TEXT_PREVIEW"]["~VALUE"]["TEXT"],94)?>
                                </p>
                            </div>
                        <?elseif(!empty($arItem["PROPERTIES"]["ABOUT"]["VALUE"]["TEXT"])):?>
                            <div class="entry-content">
                                <p>
                                    <?=substr2($arItem["PROPERTIES"]["ABOUT"]["~VALUE"]["TEXT"],94)?>
                                </p>
                            </div>
                        <?endif?>
                        <?if(!empty($arItem["PROPERTIES"]["SITE"]["VALUE"])):?>
                            <div class="entry-footer">
                                <i class="icon pe-7s-angle-right"></i>
                                <a target="_blank" href="<?=$arItem["PROPERTIES"]["SITE"]["VALUE"]?>"><?=GetMessage('LINK')?></a>
                            </div>
                        <?endif?>
                    </div>
                </div>

            <?endforeach;?>
        </div>
    </div>
</div>

<?endif?>

