<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if(!empty($arResult["ITEMS"])):?>

    <?if(!empty($arParams["TEXT_TITLE"])):?>
        <h2 class="widget-title ui-title-inner ui-title-inner_lg">
            <span class="ui-title-inner__inner"><?=htmlspecialchars_decode($arParams["TEXT_TITLE"])?></span>
        </h2>
    <?endif?>
    <div class="widget-content">
        <div class="posts-group-4">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                $img = getSrc($arItem["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"], array('width' => 120, 'height' => 95), NO_PHOTO_PATH_120_95); ?>

                <section class="b-post b-post_img-left b-post_img-120 bg-blue clearfix" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="entry-media">
                        <a class="" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                            <img class="img-responsive hov-scal" src="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>">
                        </a>
                    </div>
                    <div class="entry-main">
                        <div class="entry-header">
                            <h2 class="entry-title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                            <div class="entry-meta">
                                <span class="entry-meta__item">
                                    <a class="entry-meta__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                        <strong><?=$arItem["DISPLAY_ACTIVE_FROM"]?></strong>
                                    </a>
                                </span>
                                <?if(in_array(array(1,7),$GLOBALS["USER"]->GetUserGroupArray())):?>
                                    <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                                        <a class="entry-meta__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arResult["SHOW_COUNTER"]?></a>
                                    </span>
                                <?endif?>
                            </div>
                        </div>
                    </div>
                </section>

            <?endforeach;?>
        </div>
    </div>

<?endif?>

