/*
| ----------------------------------------------------------------------------------
| TABLE OF CONTENT
| ----------------------------------------------------------------------------------
-SETTING
-Preloader
-Scroll Animation
-Animated Entrances
-Video player
-Loader blocks
-Accordion
-Tooltip
-Ticker
-Zoom Images
-Main slider
-OWL Sliders
*/

//import 'air-form.js';

$(document).ready(function() {

    "use strict";


/////////////////////////////////////////////////////////////////
// SETTING
/////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////
// Preloader
/////////////////////////////////////////////////////////////////


    var $preloader = $('#page-preloader'),
    $spinner   = $preloader.find('.spinner-loader');
    $spinner.fadeOut();
    $preloader.delay(50).fadeOut('slow');


/////////////////////////////////////
//  Scroll Animation
/////////////////////////////////////


if ($('.scrollreveal').length > 0) {
    window.sr = ScrollReveal({
        reset:true,
        duration: 1000,
        delay: 200
    });

    sr.reveal('.scrollreveal');
  }


/////////////////////////////////////
//  Video player
/////////////////////////////////////


if ($('.player').length > 0) {
  $(".player").flowplayer();
}


///// forms
/*
    let tabs_forms = document.querySelectorAll('.search-form-tio');
            let indecators_form = document.querySelector('#search-form-tio').children;

            [].slice.call(indecators_form).forEach((item,index)=>{
                    item.onclick=()=>{
                        [].slice.call(indecators_form).forEach((elem,i)=>{
                            elem.classList.remove('item-tabs-forms-active-TVTopForm');
                            tabs_forms[i].classList.add('d-none');
                        })
                        item.classList.toggle('item-tabs-forms-active-TVTopForm');
                        tabs_forms[index].classList.remove('d-none');
                    }
            });*/
////
/////////////////////////////////////
//  Loader blocks
/////////////////////////////////////


    $( ".js-scroll-next" ).on( "click", function() {

        var hiddenContent =  $( ".js-scroll-next + .js-scroll-content") ;

        $(".js-scroll-next").hide() ;
        hiddenContent.show() ;
        hiddenContent.addClass("animated");
        hiddenContent.addClass("animation-done");
        hiddenContent.addClass("bounceInUp");

    });



/////////////////////////////////////////////////////////////////
// Accordion
/////////////////////////////////////////////////////////////////

    $(".btn-collapse").on('click', function () {
            $(this).parents('.panel-group').children('.panel').removeClass('panel-default');
            $(this).parents('.panel').addClass('panel-default');
            if ($(this).is(".collapsed")) {
                $('.panel-title').removeClass('panel-passive');
            }
            else {$(this).next().toggleClass('panel-passive');
        };
    });




/////////////////////////////////////
//  Tooltip
/////////////////////////////////////


    $('.link-tooltip-1').tooltip({
    template: '<div class="tooltip tooltip-1" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
  });
    $('.link-tooltip-2').tooltip({
    template: '<div class="tooltip tooltip-2" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
  });





/////////////////////////////////////
//  Ticker
/////////////////////////////////////

  $('.ticker1').easyTicker({
    direction: 'up',
    easing: 'swing',
    speed: 'slow',
    interval: 2000,
    height: 20,
    visible: 0,
    mousePause: 1
  });



/////////////////////////////////////
//  Zoom Images
/////////////////////////////////////



  if ($('.js-zoom-gallery').length > 0) {
      $('.js-zoom-gallery').each(function() { // the containers for all your galleries
          $(this).magnificPopup({
              delegate: '.js-zoom-gallery__item', // the selector for gallery item
              type: 'image',
              gallery: {
                enabled:true
              },
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below

        zoom: {
          enabled: true, // By default it's false, so don't forget to enable it

          duration: 300, // duration of the effect, in milliseconds
          easing: 'ease-in-out', // CSS transition easing function

          // The "opener" function should return the element from which popup will be zoomed in
          // and to which popup will be scaled down
          // By defailt it looks for an image tag:
          opener: function(openerElement) {
            // openerElement is the element on which popup was initialized, in this case its <a> tag
            // you don't need to add "opener" option if this code matches your needs, it's defailt one.
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
          });
      });
    }

////////////////////
// Табы форм поиска
////////////////////




//////////////////
  /*if ($('.js-zoom-images').length > 0) {
      $('.js-zoom-images').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below

        zoom: {
          enabled: true, // By default it's false, so don't forget to enable it

          duration: 300, // duration of the effect, in milliseconds
          easing: 'ease-in-out', // CSS transition easing function

          // The "opener" function should return the element from which popup will be zoomed in
          // and to which popup will be scaled down
          // By defailt it looks for an image tag:
          opener: function(openerElement) {
            // openerElement is the element on which popup was initialized, in this case its <a> tag
            // you don't need to add "opener" option if this code matches your needs, it's defailt one.
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
      });

    }*/


  if ($('.popup-youtube').length > 0) {
    $('.popup-youtube').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
      });
  }


////////////////////////////////////////////
// Main slider
///////////////////////////////////////////


    if ($('#main-slider').length > 0) {

        var sliderWidth = $("#main-slider").data("slider-width");
        var sliderHeigth = $("#main-slider").data("slider-height");
        var sliderArrows = $("#main-slider").data("slider-arrows");
        var sliderButtons = $("#main-slider").data("slider-buttons");

        $( '#main-slider' ).sliderPro({
            width:  sliderWidth,
            height: sliderHeigth,
            arrows: sliderArrows,
            buttons: sliderButtons,
            fullScreen: true,
            touchSwipe: true,
            autoplay: true,
            aspectRatio: 2,
            visibleSize: '100%',
            forceSize: 'fullWidth',
            slideDistance: 0
        });
      }

      if ($('#main-slider1').length > 0) {

        var sliderWidth1 = $("#main-slider1").data("slider-width");
        var sliderHeigth1 = $("#main-slider1").data("slider-height");
        var sliderArrows1 = $("#main-slider1").data("slider-arrows");
        var sliderButtons1 = $("#main-slider1").data("slider-buttons");

        $( '#main-slider1' ).sliderPro({
            width:  sliderWidth1,
            height: sliderHeigth1,
            arrows: sliderArrows1,
            buttons: sliderButtons1,
            fullScreen: true,
            touchSwipe: true,
            autoplay: true,
            aspectRatio: 2,
            visibleSize: '100%',
            forceSize: 'none',
            slideDistance: 0,
            centerImage: true
        });
      }

    if ($('#main-slider2').length > 0) {

        var sliderWidth2 = $("#main-slider2").data("slider-width");
        var sliderHeigth2 = $("#main-slider2").data("slider-height");
        var sliderArrows2 = $("#main-slider2").data("slider-arrows");
        var sliderButtons2 = $("#main-slider2").data("slider-buttons");

        $( '#main-slider2' ).sliderPro({
            width:  sliderWidth2,
            height: sliderHeigth2,
            arrows: sliderArrows2,
            buttons: sliderButtons2,
            fullScreen: true,
            touchSwipe: true,
            autoplay: true,
            aspectRatio: 2,
            visibleSize: '100%',
            forceSize: 'fullWidth',
            slideDistance: 0
        });
    }


    if ($('#slider-thumbs').length > 0) {
        $( '#slider-thumbs' ).sliderPro({
          width: 780,
          height: 456,
          orientation: 'vertical',
          loop: false,
          arrows: false,
          buttons: false,
          thumbnailsPosition: 'right',
          thumbnailPointer: true,
          thumbnailWidth: 300,
          thumbnailHeight: 105,
          breakpoints: {
            800: {
              thumbnailsPosition: 'bottom',
              thumbnailWidth: 270
            },
            500: {
              thumbnailsPosition: 'bottom',
              thumbnailWidth: 140,
              thumbnailHeight: 95
            }
          },
            autoplay: true
        });
    }


/////////////////////////////////////////////////////////////////
// OWL Sliders
/////////////////////////////////////////////////////////////////

    var Core = {

        initialized: false,

        initialize: function() {

                if (this.initialized) return;
                this.initialized = true;

                this.build();

        },

        build: function() {

        // Owl Carousel

            this.initOwlCarousel();
        },
        initOwlCarousel: function(options) {

            $(".enable-owl-carousel").each(function(i) {
                var $owl = $(this);

                var itemsData = $owl.data('items');
                var navigationData = $owl.data('navigation');
                var paginationData = $owl.data('pagination');
                var singleItemData = $owl.data('single-item');
                var autoPlayData = $owl.data('auto-play');
                var transitionStyleData = $owl.data('transition-style');
                var mainSliderData = $owl.data('main-text-animation');
                var afterInitDelay = $owl.data('after-init-delay');
                var stopOnHoverData = $owl.data('stop-on-hover');
                var min480 = $owl.data('min480');
                var min768 = $owl.data('min768');
                var min992 = $owl.data('min992');
                var min1200 = $owl.data('min1200');
                var min1920 = $owl.data('min1920');

                $owl.owlCarousel({
                    navigation : navigationData,
                    pagination: paginationData,
                    singleItem : singleItemData,
                    autoPlay : autoPlayData,
                    transitionStyle : transitionStyleData,
                    stopOnHover: stopOnHoverData,
                    navigationText : ["<i></i>","<i></i>"],
                    items: itemsData,
                    itemsCustom:[
                                    [0, 1],
                                    [465, min480],
                                    [750, min768],
                                    [975, min992],
                                    [1185, min1200],
                                    [1900, min1920]
                    ],
                    afterInit: function(elem){
                                if(mainSliderData){
                                        setTimeout(function(){
                                                $('.main-slider_zoomIn').css('visibility','visible').removeClass('zoomIn').addClass('zoomIn');
                                                $('.main-slider_fadeInLeft').css('visibility','visible').removeClass('fadeInLeft').addClass('fadeInLeft');
                                                $('.main-slider_fadeInLeftBig').css('visibility','visible').removeClass('fadeInLeftBig').addClass('fadeInLeftBig');
                                                $('.main-slider_fadeInRightBig').css('visibility','visible').removeClass('fadeInRightBig').addClass('fadeInRightBig');
                                        }, afterInitDelay);
                                    }
                    },
                    beforeMove: function(elem){
                        if(mainSliderData){
                                $('.main-slider_zoomIn').css('visibility','hidden').removeClass('zoomIn');
                                $('.main-slider_slideInUp').css('visibility','hidden').removeClass('slideInUp');
                                $('.main-slider_fadeInLeft').css('visibility','hidden').removeClass('fadeInLeft');
                                $('.main-slider_fadeInRight').css('visibility','hidden').removeClass('fadeInRight');
                                $('.main-slider_fadeInLeftBig').css('visibility','hidden').removeClass('fadeInLeftBig');
                                $('.main-slider_fadeInRightBig').css('visibility','hidden').removeClass('fadeInRightBig');
                        }
                    },
                    afterMove: sliderContentAnimate,
                    afterUpdate: sliderContentAnimate,
                });
            });

            function sliderContentAnimate(elem){
                var $elem = elem;
                var afterMoveDelay = $elem.data('after-move-delay');
                var mainSliderData = $elem.data('main-text-animation');
                if(mainSliderData){
                    setTimeout(function(){
                        $('.main-slider_zoomIn').css('visibility','visible').addClass('zoomIn');
                        $('.main-slider_slideInUp').css('visibility','visible').addClass('slideInUp');
                        $('.main-slider_fadeInLeft').css('visibility','visible').addClass('fadeInLeft');
                        $('.main-slider_fadeInRight').css('visibility','visible').addClass('fadeInRight');
                        $('.main-slider_fadeInLeftBig').css('visibility','visible').addClass('fadeInLeftBig');
                        $('.main-slider_fadeInRightBig').css('visibility','visible').addClass('fadeInRightBig');
                    }, afterMoveDelay);
                }
            }
        },

    };

    Core.initialize();

    $('a.anchor').on("click", function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            $('html,body').stop().animate({
                scrollTop: (target.offset().top - 135) // 70px offset for navbar menu
            }, 1000);
            return false;

        }
    });

    /*$('#sidebar').theiaStickySidebar({
        additionalMarginTop: 80
    });*/

});