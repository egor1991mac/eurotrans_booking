<?php

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PUBLIC_AJAX_MODE", true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

try {

    if (!isset($_REQUEST["details"]) || empty($_REQUEST["details"])) {throw new Exception("");}

    $gateway = new travelsoft\eurotrans\Gateway();

    // prepare data
    $req = array();
    foreach ($_REQUEST["details"] as $bus_id => $details) {

        $req[$bus_id] = array(
            "bus_id" => $details["bus_id"],
            "pickup_id" => $details["pickup_id"],
            "return_id" => $details["return_id"],
            "booking_period" => array(
                $details["bus_id"] => array(
                    "departure_time" => date(travelsoft\eurotrans\Settings::EXT_DATE_FORMAT, strtotime($details["departure_date"] . " " . $details["departure_time"])),
                    "arrival_time" => date(travelsoft\eurotrans\Settings::EXT_DATE_FORMAT, strtotime($details["arrival_date"] . " " . $details["arrival_time"]))
                )
            )
        );
    }

    if (empty($req)) {throw new Exception("");}
    
    $resp["data"] = (new travelsoft\eurotrans\Gateway())->getBusesBookedSeats($req);
    $resp["error"] =  false;
    travelsoft\eurotrans\Utils::jsonResponse(json_encode($resp));
    
    
} catch (Exception $ex) {

    travelsoft\eurotrans\Utils::jsonResponse(json_encode(array("error" => true)));
}

