<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$oAsset = Bitrix\Main\Page\Asset::getInstance();
CJSCore::Init();
?><!doctype html>
<html lang="<?= LANGUAGE_ID ?>">
    <head>
        <title><?= $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->ShowHead() ?>
        <meta charset="utf-8"/>
        <meta http-equiv="x-ua-compatible" content="ie=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="shortcut icon" type="image/png" href="/favicon.ico">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">

        <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/assets/css/master.css"); ?>
        <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/assets/css/tscustom.css"); ?>
        <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/assets/css/ts-theme.css"); ?>
		<? $oAsset->addCss(SITE_TEMPLATE_PATH . "/assets/css/ts-style.css"); ?>
        <?/*
        <!--[if lt IE 9 ]>
        <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/separate-js/html5shiv-3.7.2.min.js" type="text/javascript"></script>
        <![endif]-->
        */?>

        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/libs/jquery-1.12.4.min.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/libs/jquery-migrate-1.2.1.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/libs/modernizr.custom.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/libs/bootstrap/bootstrap.min.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/js/custom.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/owl-carousel/owl.carousel.min.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/magnific-popup/jquery.magnific-popup.min.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/headers/jquery.dlmenu.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/headers/slidebar.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/headers/header.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/jqBootstrapValidation.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/contact_me.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/flowplayer/flowplayer.min.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/isotope/isotope.pkgd.min.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/isotope/imagesLoaded.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/rendro-easy-pie-chart/jquery.easypiechart.min.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/rendro-easy-pie-chart/waypoints.min.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/scrollreveal/scrollreveal.min.js") ?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/plugins/jquery-easy-ticker/jquery.easy-ticker.min.js") ?>
		<? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/js/react-form.js") ?>


		</head>
    <body>

		<?/*
        <!-- Loader-->
        <div id="page-preloader"><span class="spinner border-t_second_b border-t_prim_a"></span></div>
        <!-- Loader end-->
		*/?>
        <div class="l-theme animated-css" data-header="sticky"  data-canvas="container">

            <!-- ==========================-->
            <!-- SEARCH MODAL-->
            <!-- ==========================-->
            <div class="header-search open-search">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:search.title",
                                "search",
                                Array(
                                    "CATEGORY_0" => array("no"),
                                    "CATEGORY_0_TITLE" => "",
                                    "CATEGORY_0_main" => array(""),
                                    "CHECK_DATES" => "Y",
                                    "CONTAINER_ID" => "title-search",
                                    "INPUT_ID" => "title-search-input",
                                    "NUM_CATEGORIES" => "1",
                                    "ORDER" => "rank",
                                    "PAGE" => "#SITE_DIR#poisk-po-saytu/",
                                    "SHOW_INPUT" => "Y",
                                    "SHOW_OTHERS" => "N",
                                    "TOP_COUNT" => "5",
                                    "USE_LANGUAGE_GUESS" => "Y"
                                )
                            );?>
                        </div>
                    </div>
                </div>
                <button class="search-close close" type="button"><i class="fa fa-times"></i></button>
            </div>

            <!-- ==========================-->
            <!-- FULL SCREEN MENU-->
            <!-- ==========================-->
            <div class="wrap-fixed-menu" id="fixedMenu">
                <nav class="fullscreen-center-menu">
                    <div class="menu-main-menu-container">
                        <div class="slidebar-nav-menu">
                            <nav>
                                <div id="dl-menu" class="dl-menuwrapper">
                                    <ul class="dl-menu nav navbar-nav">
                                        <?$APPLICATION->IncludeComponent(
                                            "bitrix:menu",
                                            "mobile.top.menu",
                                            Array(
                                                "ALLOW_MULTI_SELECT" => "N",
                                                "CHILD_MENU_TYPE" => "left",
                                                "DELAY" => "N",
                                                "MAX_LEVEL" => "2",
                                                "MENU_CACHE_GET_VARS" => array(""),
                                                "MENU_CACHE_TIME" => "3600",
                                                "MENU_CACHE_TYPE" => "N",
                                                "MENU_CACHE_USE_GROUPS" => "N",
                                                "ROOT_MENU_TYPE" => "top",
                                                "USE_EXT" => "Y"
                                            )
                                        );?>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </nav>
                <button type="button" class="fullmenu-close"><i class="fa fa-times"></i></button>
            </div>
            <header style="position: fixed; width: 100%; z-index: 10000; top: 0">
                <div class="ts-wrap">
                <!-- верхняя строка -->
                <div class="ts-row ts-p-2 ts-justify-content__space-between ts-border-bottom ts-border__color__light-gray  ">
                    <div class="search">
                        <a class="btn_header_search" href="#"><i class="fa fa-search header-icon-search"></i> <span class="header-title-search">Поиск по сайту</span></a>

                    </div>
                    <div class="contact  ts-d-none ts-d-sm-flex  ts-justify-content__flex-end">
                        <i class="fa fa-phone header-icon-phone"></i>
                        <?$APPLICATION->IncludeFile("/include/header_phones.php", Array(), Array(
                            "MODE"      => "html",
                            "NAME"      => "Телефоны (шапка)",
                        ));?>
                    </div>
                </div>
                <!-- нижняя строка -->
                <div class="ts-row ts-py-2">
                    <div class="logo ts-col-auto">
                        <a class="navbar-brand scroll" <?if(defined("HOME_PAGE") && HOME_PAGE === true):?><?else:?>href="/"<?endif;?>>
                            <img class="normal-logo img-responsive" src="<?=SITE_TEMPLATE_PATH?>/assets/media/general/logo.png" alt="logo"/>
                            <img class="scroll-logo img-responsive hidden-xs" src="<?=SITE_TEMPLATE_PATH?>/assets/media/general/logo_small.png" alt="logo"/>
                        </a>
                    </div>
                    <nav class="ts-col-auto ts-justify-content__flex-end ts-align-items__flex-end">
                        <button class="menu-mobile-button ts-d-block ts-d-lg-none js-toggle-screen toggle-menu-button ts-m-0 "><i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i></button>

                        <ul class="ts-d-lg-flex ts-justify-content__space-between ts-width-100 ts-m-0 ts-d-none">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "top.menu",
                                Array(
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "CHILD_MENU_TYPE" => "left",
                                    "DELAY" => "N",
                                    "MAX_LEVEL" => "2",
                                    "MENU_CACHE_GET_VARS" => array(""),
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_USE_GROUPS" => "N",
                                    "ROOT_MENU_TYPE" => "top",
                                    "USE_EXT" => "Y"
                                )
                            );?>
                        </ul>
                    </nav>
                </div>
                </div>
            </header>
            <!-- header old
            <header class="header header-boxed-width header-logo-black header-navbar-white header-topbarbox-1-left header-topbarbox-2-right header-navibox-1-left header-navibox-3-right header-navibox-4-right">
                <div class="container container-boxed-width">
                    <div class="top-bar ts-wrap ts-py-2 ts-d-none ts-d-sm-block">
                        <div class="ts-row ts-align-items__center">
                            <div class="ts-col-24 ts-col-sm-12">
                                <div class="header-topbarbox-1 ts-py-0">
                                    <a class="btn_header_search" href="#"><i class="fa fa-search header-icon-search"></i> <span class="header-title-search">Поиск по сайту</span></a>
                                </div>
                            </div>
                            <div class="ts-col-24 ts-col-sm-12 ts-mt-2 ts-mt-sm-0 header-order-block ts-justify-content-sm__flex-end">
                                <span class="ts-mr-0 hover_dashed header-order-block-title"><div class="tv-free-button tv-moduleid-965081"></div><script type="text/javascript" src="//tourvisor.ru/module/init.js"></script></span>
                                <span class="header-phone-block"><i class="fa fa-phone header-icon-phone"></i>
                                    <?$APPLICATION->IncludeFile("/include/header_phones.php", Array(), Array(
                                        "MODE"      => "html",
                                        "NAME"      => "Телефоны (шапка)",
                                    ));?>
                                </span>
                            </div>
							<?/* div class="col-sm-2">

                                <div class="header-topbarbox-2">
                                    <ul class="social-links list-inline pull-right">
                                        <a href="#">Войти</a><li></li>
                                    </ul>
                                </div>
							</div */?>
                        </div>
                    </div>
                    <div class="ts-wrap">
                        <div class="ts-row ts-justify-content__space-between ts-align-items__center ts-py-3">


                        <div class="ts-col-6">
                            <a class="navbar-brand scroll" <?if(defined("HOME_PAGE") && HOME_PAGE === true):?><?else:?>href="/"<?endif;?>>
                                <img class="normal-logo img-responsive" src="<?=SITE_TEMPLATE_PATH?>/assets/media/general/logo.png" alt="logo"/>
                                <img class="scroll-logo img-responsive hidden-xs" src="<?=SITE_TEMPLATE_PATH?>/assets/media/general/logo_small.png" alt="logo"/>
                            </a>
                        </div>
                        <div class="ts-col-auto ts-justify-content__flex-end">
                            <button class="menu-mobile-button visible-xs-block js-toggle-screen toggle-menu-button ts-m-0"><i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i></button>
                        </div>
                            <div class="ts-col-auto ts-d-none ts-d-lg-flex ts-justify-content__flex-end">
                                <ul class="yamm main-menu nav navbar-nav">
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:menu",
                                        "top.menu",
                                        Array(
                                            "ALLOW_MULTI_SELECT" => "N",
                                            "CHILD_MENU_TYPE" => "left",
                                            "DELAY" => "N",
                                            "MAX_LEVEL" => "2",
                                            "MENU_CACHE_GET_VARS" => array(""),
                                            "MENU_CACHE_TIME" => "3600",
                                            "MENU_CACHE_TYPE" => "N",
                                            "MENU_CACHE_USE_GROUPS" => "N",
                                            "ROOT_MENU_TYPE" => "top",
                                            "USE_EXT" => "Y"
                                        )
                                    );?>
                                </ul>
                            </div>
                    </div>

                    </div>
                </div>
            </header>
            // over header old-->


            <?if(defined("HOME_PAGE") && HOME_PAGE === true):?>
                                    <?$APPLICATION->IncludeFile("/include/index_form_search.php", Array(), Array(
                                        "MODE"      => "html",
                                        "NAME"      => "Форма поиска на главной странице",
                                    ));?>
            <?endif?>

            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "new_site_central",
                Array(
                    "CACHE_TIME" => "0",
                    "CACHE_TYPE" => "A",
                    "COMPONENT_TEMPLATE" => "new_site_central",
                    "NOINDEX" => "Y",
                    "QUANTITY" => "1",
                    "TYPE" => "CENTER_0"
                )
            );?>

            <?if(defined("HOME_PAGE") && HOME_PAGE === true):?>
                <h1 style="display: none"><?=$APPLICATION->ShowTitle(false)?></h1>
            <?elseif(defined("ERROR_404") && ERROR_404 == "Y"):?>

            <?else:?>
			<?if($APPLICATION->GetDirProperty("SHOW_SECTION_TITLE") == "Y"):?>
			<div class="section-title-page area-bg parallax" style="background-image: url(<?if(!empty($APPLICATION->GetDirProperty("SECTION_AREA_BG"))):?><?=$APPLICATION->GetDirProperty("SECTION_AREA_BG")?><?else:?>/images/bg-1.jpg<?endif;?>)">
				<div class="area-bg__inner">
				  <div class="container">
					<div class="row">
					  <div class="col-xs-12">
						<h1 class="b-title-page"><?= $APPLICATION->ShowTitle(false) ?></h1>
						<div class="b-title-page__info"></div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			<?endif;?>

                          <div class="ts-wrap ts-mb-3">
                              <div class="ts-row">
                                  <div class="ts-col-24">
                                   <?$APPLICATION->IncludeComponent(
                                        "bitrix:breadcrumb",
                                        "breadcrumb",
                                        Array(
                                            "PATH" => "",
                                            "SITE_ID" => "s1",
                                            "START_FROM" => "0"
                                        )
                                    );?>
                                <!-- end breadcrumb-->
                                  </div>
                              </div>

                </div>
                <div class="<?$APPLICATION->ShowViewContent('additional_class');?> section-area<?if($APPLICATION->GetDirProperty("CLASS_BG") != ""):?> <?=$APPLICATION->GetDirProperty("CLASS_BG")?><?endif;?>">
                    <div class="ts-wrap">
                        <?if($APPLICATION->GetDirProperty("SHOW_LEFT_SIDEBAR") != "N"):?>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR . "include/sidebar.php"
                            )); ?>
                            <div class="col-md-8">
                        <?elseif($APPLICATION->GetDirProperty("SHOW_RIGHT_SIDEBAR") != "N"):?>
                            <div class="col-md-8">
                        <?else:?>
                            <div class="ts-row">
                        <?endif;?>
						<!--<?if($APPLICATION->GetDirProperty("SHOW_TITLE") == "Y"):?>
							<h1><?= $APPLICATION->ShowTitle(false) ?></h1>
						<?endif?> -->
            <?endif?>