<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
   /** @var array $arParams */
   /** @var array $arResult */
   /** @global CMain $APPLICATION */
   /** @global CUser $USER */
   /** @global CDatabase $DB */
   /** @var CBitrixComponentTemplate $this */
   /** @var string $templateName */
   /** @var string $templateFile */
   /** @var string $templateFolder */
   /** @var string $componentPath */
   /** @var CBitrixComponent $component */
   $this->setFrameMode(true);
$GLOBALS['TOP_NEWS_ELEMENTS_ID']=array();
   ?>
<div id="feat-top-wrap" class="left relative">
   <?foreach($arResult as $arItem):?>
   <?
      $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
      $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
      ?>
	<?if (!empty($arItem["PROPERTY_PHOTO1_VALUE"])):
		$an_file = CFile::ResizeImageGet($arItem["PROPERTY_PHOTO1_VALUE"], array('width'=>350, 'height'=>450), BX_RESIZE_IMAGE_EXACT, true);
		$pre_photo=$an_file["src"];
	elseif (!empty($arItem["PREVIEW_PICTURE"])):
		$an_file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>350, 'height'=>450), BX_RESIZE_IMAGE_EXACT, true);
		$pre_photo=$an_file["src"];
		elseif (!empty($arItem["PROPERTY_OLD_PHOTO_VALUE"])):
		$pre_photo="/datas/photos/".$arItem["PROPERTY_OLD_PHOTO_VALUE"];
		else:
		$pre_photo=SITE_TEMPLATE_PATH."/images/nophoto349.jpg";
		endif;
		?>
   <div class="feat-wide5-main left relative" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
      <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" rel="bookmark">
         <div class="feat-wide5-img left relative">
            <img src="<?=$pre_photo?>" class="reg-img wp-post-image" alt="band">
		</div>
         <?if (!empty($arItem["PROPERTY_VIDEO_VALUE"])):?>
         <!--feat-wide5-img-->
         <div class="feat-vid-but">
            <i class="fa fa-play fa-3"></i>
         </div>
         <!--feat-vid-but-->
         <?endif;?>
         <div class="feat-wide5-text">
		 <?if (!empty($arItem["PROPERTY_THEME_VALUE"])):?>
				<?foreach($arItem["PROPERTY_THEME_VALUE"] as $k => $v):
				$res = CIBlockElement::GetByID($v);
				if($ar_res = $res->GetNext())
				  $theme = $ar_res['NAME'];
				?>
           		<span class="feat-cat"><?=$theme;?></span>
			<?endforeach;?>
          <?endif;?>
            <h2><?echo $arItem["NAME"]?></h2>
         </div>
         <!--feat-wide5-text-->
         <div class="feat-info-wrap">
            <?if (!empty($arItem["SHOW_COUNTER"])):?>
            <div class="feat-info-views">
				<i class="fa fa-eye fa-2"></i> <span class="feat-info-text"><?=$arItem["SHOW_COUNTER"]?></span>
            </div>
            <!--feat-info-views-->
            <?endif;?>
            <?if (!empty($arItem["PROPERTY_FORUM_MESSAGE_CNT_VALUE"])):?>
            <div class="feat-info-comm">
               <i class="fa fa-comment"></i> <span class="feat-info-text"><?=$arItem["PROPERTY_FORUM_MESSAGE_CNT_VALUE"]?></span>
            </div>
            <!--feat-info-comm-->
            <?endif;?>
         </div>
         <!--feat-info-wrap-->
      </a>
   </div>
   <!--feat-wide5-main-->
	<?$GLOBALS['TOP_NEWS_ELEMENTS_ID'][] = $arItem['ID'];?>
   <?endforeach;?>
</div>

