let promiseResolvePopupCancel, promiseRejectPopupCancel;

function showPopupCancelBooking() {
    return new Promise((resolve, reject) => {
        promiseResolvePopupCancel = resolve;
        promiseRejectPopupCancel = reject;

        $('body').append(`<div class="modal-layout js-popup-container active" data-modal="payback" id="payback">
            <!--контейнер модалки-->
            <!-- по саксесу вешаю на этот блок класс из-за которого следующий открывается-->
            <div class="modal-container" style="max-width: 25.4375rem;">
                <div class="modal-container-header">
                    <div class="text-medium">Возврат билетов</div>
                    <i class="small-link pop-close closePopup" onclick="promiseReject()"></i>
                </div>
                <div class="modal-container-content">
                    <div class="text-1">Вы уверены, что хотите отменить этот билет?</div>
                    <div class="form-block-footer">
                        <div class="input-form">
                            <div class="input-item s50">
                                <a class="btn btn-colored closePopup" onclick="promiseReject()">
                                    <span>Нет, не нужно</span>
                                </a>
                            </div>
                            <div class="input-item s50 text-center">
                                <a class="btn btn-colored red closePopup" onclick="promiseResolve()">
                                    <span>да, отменить!</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`);
    })
}
