<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("CRM_ADD_FORM"),
	"DESCRIPTION" => GetMessage("CRM_ADD_FORM_DESC"),
	"ICON" => "/images/include.gif",
	"PATH" => array(
		"ID" => "travelsoft",
	),
);
?>