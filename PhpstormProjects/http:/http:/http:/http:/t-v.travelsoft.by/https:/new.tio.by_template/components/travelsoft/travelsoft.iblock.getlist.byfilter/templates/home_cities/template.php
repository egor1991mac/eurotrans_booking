<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult)):?>

    <?if(!empty($arParams["TEXT_TITLE"])):?>
        <h2 class="ui-title-inner ui-title-inner_mrg-btn_lg">
            <span class="ui-title-inner__inner"><?=htmlspecialchars_decode($arParams["TEXT_TITLE"])?></span>
        </h2>
    <?endif?>

    <div class="row">
        <?foreach($arResult as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            $arImg = getSrc($arItem["PROPERTIES"]["PHOTO"]["VALUE"], array("width"=>384,"height"=>237));?>

            <div class="col-md-3" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <section class="b-post b-post_img-bg b-post_img-bg_type-4 clearfix">
                    <div class="entry-media"><img class="img-responsive" src="<?=$arImg[0]?>" alt="<?=$arItem["NAME"]?>"></div>
                    <?if(!empty($arItem["PROPERTIES"]["FEATURES_LIST"]["VALUE"])):?>
                        <div class="entry-main">
                            <div class="entry-header">
                                <h2 class="entry-title hov-title"><a class="hov-title__inner" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=implode(", ", $arItem["PROPERTIES"]["FEATURES_LIST"]["DISPLAY_VALUE"])?></a></h2>
                            </div>
                        </div>
                    <?endif?>
                    <a class="entry-label" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?><?if(!empty($arItem["PROPERTIES"]["AIR_TEMPERATURE"]["VALUE"])):?> <strong><?=$arItem["PROPERTIES"]["AIR_TEMPERATURE"]["VALUE"]?> <sup>0</sup>С</strong><?endif?></a>
                </section>
            </div>

        <?endforeach;?>

    </div>

<?endif?>