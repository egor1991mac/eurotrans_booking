<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
$this->setFrameMode(true);
?>

<?if(!empty($arResult)):?>


        <?if(!empty($arParams["TEXT_TITLE"])):?>
            <h2 class="ui-title-inner ui-title-inner_mrg-btn_lg ts-mb-6">
                <span class="ui-title-inner__inner"><?=htmlspecialchars_decode($arParams["TEXT_TITLE"])?></span>
            </h2>
        <?endif?>
        <div class="ts-row">

            <?foreach($arResult as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                $img = getSrc($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"], array('width' => 384, 'height' => 237));
                if(empty($img[0])){
                    $img = getSrc($arItem["PROPERTIES"]["PHOTO3"]["VALUE"], array('width' => 384, 'height' => 237), NO_PHOTO_PATH_384_237);
                }?>

                    <section class="b-post ts-col-24 ts-col-sm-12 ts-col-md-8 ts-mb-6 ts-mb-md-0 ts-mb-lg-0">
                        <div class="entry-media ts-width-100">
                            <a class="ts-width-100" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                <img class="ts-width-100 " src="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>">
                            </a>
                        </div>
                        <div class="entry-main">
                            <div class="entry-header">
                                <div class="entry-meta">
                                    <?if(!empty($arItem["PROPERTIES"]["AUTHOR"]["VALUE"]) && isset($arItem["PROPERTIES"]["AUTHOR"]["DESC"]) && !empty($arItem["PROPERTIES"]["AUTHOR"]["DESC"])):?>
                                        <span class="entry-meta__item">
                                            <a class="entry-meta__link text-primary" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                                <strong><?=$arItem["PROPERTIES"]["AUTHOR"]["DESC"]?></strong></a>
                                        </span>
                                    <?endif?>
                                    <?if(in_array(array(1,7),$GLOBALS["USER"]->GetUserGroupArray())):?>
                                        <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                                            <a class="entry-meta__link text-primary" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arResult["SHOW_COUNTER"]?></a>
                                        </span>
                                    <?endif?>
                                    <!--<span class="entry-meta__item"><i class="icon fa fa-comment-o text-primary"></i>
                                        <a class="entry-meta__link text-primary" href="blog-main.html">29</a>
                                    </span>-->
                                </div>
                                <h2 class="entry-title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                            </div>
                            <div class="entry-content">
                                <p>
                                    <?if(!empty($arItem["PREVIEW_TEXT"])):?>
                                        <?=substr2($arItem["~PREVIEW_TEXT"], 120)?>
                                    <?elseif(!empty($arItem["DETAIL_TEXT"])):?>
                                        <?=substr2($arItem["~DETAIL_TEXT"], 120)?>
                                    <?endif?>
                                </p>
                            </div>
                        </div>
                    </section>


            <?endforeach;?>

        </div>


<?endif?>

