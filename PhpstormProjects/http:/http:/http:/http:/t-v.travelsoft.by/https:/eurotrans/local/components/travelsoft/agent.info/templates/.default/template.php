<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="aside-section">
    <div class="content-column-wrap content-small">
        <div class="content-column-content">
            <div class="company-section">

                <? if (!empty($arResult['AGENT']['UF_AGENT_NAME'])): ?>
                    <div class="text-sm">Наименование агента</div>
                    <div class="text-medium">
                        <?= $arResult['AGENT']['UF_AGENT_NAME']; ?>
                    </div>
                <? endif; ?>

                <div class="company-section-row">
                    <? if (!empty($arResult['AGENT']['UF_LEGAL_ADDRESS'])): ?>
                        <div class="company-section-block">
                            <div class="text-sm">Юридический адрес</div>
                            <div class="text-1">
                                <?= $arResult['AGENT']['UF_LEGAL_ADDRESS']; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if (!empty($arResult['AGENT']['UF_BANK_DETAIS'])): ?>
                        <div class="company-section-block">
                            <div class="text-sm">Банковские реквезиты</div>
                            <div class="text-1">
                                <?= $arResult['AGENT']['UF_BANK_DETAIS']; ?>
                            </div>
                        </div>
                    <? endif; ?>
                </div>

                <div class="text-medium">Контактное лицо</div>
                <div class="company-section-row">
                    <? if (!empty($arResult['AGENT']['NAME']) && !empty($arResult['AGENT']['LAST_NAME'])): ?>
                        <div class="company-section-block">
                            <div class="text-sm">Представитель ФИО</div>
                            <div class="text-1">
                                <?= $arResult['AGENT']['LAST_NAME']; ?>
                                <?= $arResult['AGENT']['NAME']; ?>
                                <?= $arResult['AGENT']['SECOND_NAME']; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if (!empty($arResult['AGENT']['PERSONAL_MOBILE'])): ?>
                        <div class="company-section-block">
                            <div class="text-sm">Контактный телефон</div>
                            <a class="text-1" href="tel:+375172260994">
                                <?= $arResult['AGENT']['PERSONAL_MOBILE']; ?>
                            </a>
                        </div>
                    <? endif; ?>
                    <? if (!empty($arResult['AGENT']['UF_CONTRACT_FORM'])): ?>
                        <div class="company-section-block">
                            <div class="text-sm">Договор от</div>
                            <div class="text-1">
                                <?= $arResult['AGENT']['UF_CONTRACT_FORM']; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if (!empty($arResult['AGENT']['EMAIL'])): ?>
                        <div class="company-section-block">
                            <div class="text-sm">E-mail</div>
                            <a class="text-1" href="mailto:+375172260994">
                                <?= $arResult['AGENT']['EMAIL']; ?>
                            </a>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
        <div class="content-column-right">
            <a class="border-card-link" href="/office-agent/manual/">
                <div class="border-card small percent">
                    <div class="border-card-inner" style="background-image:url(/bitrix/templates/eurotrans/img/cards/percent.jpg);">
                        <? if (!empty($arResult['AGENT']['COMMISSION'])): ?>
                            <div class="border-card-name">
                                <div class="text-sm">Ваш комиссионный процент от стоимости билета</div>
                                <span>
                                    <?= $arResult['AGENT']['COMMISSION']; ?>%
                                </span>
                            </div>
                        <? endif; ?>
                        <div class="border-card-content bonus-content">
                            <div class="link-border">
                                <!--<span>Расчитать комиссию</span>-->
                                <span>Инструкция</span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</section>