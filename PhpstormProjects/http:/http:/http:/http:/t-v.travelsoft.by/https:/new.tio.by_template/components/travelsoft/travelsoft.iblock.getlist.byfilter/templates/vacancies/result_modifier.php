<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arVacancies_ = $arVacancies = array();
foreach ($arResult as $key=>$arItem) {

    if (!empty($arItem["PROPERTIES"]["COMPANY"]["VALUE"])) {
        $arVacancies[] = $arItem["PROPERTIES"]["COMPANY"]["VALUE"];
    }

}

if(!empty($arVacancies)) {

    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => COMPANY_ID_IBLOCK, "ID" =>$arVacancies), false, false, Array("IBLOCK_ID", "ID", "NAME"));
    while($ar_fields = $res->GetNext())
    {
        $arVacancies_[$ar_fields["ID"]] = $ar_fields["NAME"];
    }

}

foreach ($arResult as $key=>$arItem) {

    if (!empty($arItem["PROPERTIES"]["COMPANY"]["VALUE"])) {
        $arResult[$key]["PROPERTIES"]["COMPANY"]["DISPLAY_VALUE"] = $arVacancies_[$arItem["PROPERTIES"]["COMPANY"]["VALUE"]];
    }

}