<?php

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PUBLIC_AJAX_MODE", true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

try {

    $resp["data"] = (new travelsoft\eurotrans\Gateway())->getBusMapData(array(
        "bus_id" => $_REQUEST["bus_id"],
        "pickup_id" => $_REQUEST["pickup_id"],
        "return_id" => $_REQUEST["return_id"],
        "booking_period" => array(
            $_REQUEST["bus_id"] => array(
                "departure_time" => date(travelsoft\eurotrans\Settings::EXT_DATE_FORMAT, strtotime($_REQUEST["departure_date"] . " " . $_REQUEST["departure_time"])),
                "arrival_time" => date(travelsoft\eurotrans\Settings::EXT_DATE_FORMAT, strtotime($_REQUEST["arrival_date"] . " " . $_REQUEST["arrival_time"]))
            )
        )
    ));

    if (empty($resp["data"])) {
        throw new Exception("");
    }

    $resp["error"] = false;
    $resp["is_agent"] = travelsoft\eurotrans\Utils::getApiAccessId() ? true : false;

    travelsoft\eurotrans\Utils::jsonResponse(json_encode($resp));
} catch (Exception $ex) {

    travelsoft\eurotrans\Utils::jsonResponse(json_encode(array("error" => true)));
}
