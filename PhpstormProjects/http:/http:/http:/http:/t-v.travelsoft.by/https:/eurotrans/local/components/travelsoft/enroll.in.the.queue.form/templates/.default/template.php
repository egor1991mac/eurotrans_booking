<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(false);
?>
<? if (!empty($arResult['ERRORS'])): ?>

    <div class="booking-block-inner" style="padding-top: 0; padding-bottom: 0;">
        <div class="content-column-content-inner">
            <p style="color: red;">
                <? foreach ($arResult["ERRORS"] as $error) {
                    echo $error . '<br/>';
                } ?>
            </p>
        </div>
    </div>

<? endif; ?>
<? if (!empty($arResult['MESSAGE_OK'])): ?>

    <div class="booking-block-inner" style="padding-top: 0; padding-bottom: 0;">
        <div class="content-column-content-inner">
            <p style="color: green;">
                <?= $arResult['MESSAGE_OK'] ?>
            </p>
        </div>
    </div>

    <script>
        let arrDataLayer = {
            'event' : 'GAEvent',
            'eventCategory' : 'Очередь',
            'eventAction' : 'Запись в очередь',
            'eventLabel' : 'Запись в очередь <?=$arResult['FROM']?>-<?=$arResult['TO']?> на <?=$arResult['DATE_FROM']?>',
            'eventValue' : 0
        };
        dataLayer.push(arrDataLayer);
    </script>

<? else: ?>

    <form class="form-block-wrapper js-validate" method="POST" action="<?= POST_FORM_ACTION_URI ?>">
        <?= bitrix_sessid_post() ?>

        <input type="hidden" name="DO_NOT_SHOW_DATA_LAYER" value="true">

        <? if (!empty($arResult["HIDDEN_CHECK_STRING"])): ?>
            <input type="hidden" name="<?= $arResult["HIDDEN_CHECK_STRING"] ?>" value="true"/>
        <? endif; ?>

        <div class="form-block-section">
            <div class="title h3">
                Оставьте свой номер телефона и мы свяжемся с Вами как только появятся места.
            </div>
            <div class="input-form">

                <? foreach ($arResult["FORM_FIELDS"] as $name => $field): ?>
                    <? if (!empty($name) && !empty($field["TYPE"])): ?>

                        <? if ($field["TYPE"] == "hidden"): ?>

                            <input name="<?= $name ?>" type="hidden" value="<?= $field["VALUE"] ?>"/>

                        <? else: ?>

                            <div class="input-item <? if ($field["WIDTH"] == "6"): ?>s50<? elseif ($field["WIDTH"] == "12"): ?>s100<? endif; ?>">
                                <div class="input-wrapper <? if ($field["REQUIRED"]): ?>req<? endif; ?>">

                                    <? if (!empty($field["LABEL"])): ?>

                                        <label for="<?= $name ?>"><?= $field["LABEL"] ?></label>

                                    <? endif; ?>

                                    <? if (($field["TYPE"] == "text") || ($field["TYPE"] == "tel")): ?>

                                        <input name="<?= $name ?>" class="input-main" type="<?= $field["TYPE"] ?>"
                                               data-validation="length" data-validation-length="min2"
                                               data-validation-error-msg="Не заполнено"
                                               placeholder="<?= $field["PLACEHOLDER"] ?>" id="<?= $name ?>"
                                               value="<?= $field["VALUE"] ?>"
                                            <?= $field["REQUIRED"] ?>>

                                    <? elseif ($field["TYPE"] == "textarea"): ?>

                                        <textarea name="<?= $name ?>" class="input-main" rows="5" id="<?= $name ?>"
                                            <?= $field["REQUIRED"] ?>><?= $field["VALUE"] ?></textarea>

                                    <? endif; ?>


                                    <? if (!empty($field["ERROR"])): ?>

                                        <span class="help-block form-error"><?= $field["ERROR"] ?></span>

                                    <? endif; ?>

                                </div>
                            </div>

                        <? endif; ?>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
        </div>

        <div class="form-block-footer">
            <div class="form-footer-notify">
                <div class="text-sm">Поля для обязательного заполнения</div>
            </div>
            <button name="SUBMIT" value="SEND" class="btn btn-colored">
            <span>
                <? if (!empty($arParams["BUTTON_TEXT"])): ?>
                    <?= $arParams["BUTTON_TEXT"]; ?>
                <? else: ?>
                    Задать вопрос
                <? endif; ?>
            </span>
            </button>
        </div>
    </form>

<? endif; ?>