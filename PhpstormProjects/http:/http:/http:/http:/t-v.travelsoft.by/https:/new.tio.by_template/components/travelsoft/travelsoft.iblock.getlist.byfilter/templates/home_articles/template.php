<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
$this->setFrameMode(true);
//$this->addExternalJS(SITE_TEMPLATE_PATH."/assets/js/jquery.ellipsis.js");
?>

<?if(!empty($arResult)):?>

    <?if(!empty($arParams["TEXT_TITLE"])):?>
        <h2 class="ui-title-inner ui-title-inner_mrg-btn_lg">
            <span class="ui-title-inner__inner"><?=htmlspecialchars_decode($arParams["TEXT_TITLE"])?></span>
        </h2>
    <?endif?>

    <?$i = 0;?>
    <div class="ts-wrap">
        <div class="ts-row">

    <?foreach($arResult as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $img = getSrc($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"], array('width' => 828, 'height' => 400));
        if(empty($img[0])){
            $img = getSrc($arItem["PROPERTIES"]["PHOTO3"]["VALUE"], array('width' => 384, 'height' => 237), NO_PHOTO_PATH_360_222);
        }?>


        <?if($i == 0):?>
        <div class="ts-col-24">
            <article class="ts-d-block ts-width-100 ts-mb-6" style="background: url(<?=$img[0]?>); background-size: cover; background-repeat: no-repeat; height:400px ">
            <div class="ts-col-24 ts-height-100 ts-flex-direction__column ts-justify-content__flex-end ts-pb-5">
                <h2 class="entry-title ts-width-80" ><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="color:white"><?=$arItem["NAME"]?></a></h2>
                <div class="entry-content">
                    <p style="color: #ffffffe3;">
                        <?if(!empty($arItem["PREVIEW_TEXT"])):?>
                            <?=substr2($arItem["~PREVIEW_TEXT"], 120)?>
                        <?elseif(!empty($arItem["DETAIL_TEXT"])):?>
                            <?=substr2($arItem["~DETAIL_TEXT"], 120)?>
                        <?endif?>
                    </p>
                </div>
            </div>
        </article>
        </div>

        <?else:?>



            <div class="ts-col-24 ts-col-sm-12  <? if (count($arResult) != $i):?> ts-pb-6 <? endif; ?> " id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="b-post">
                    <div class="ts-width-100">
                        <a class="ts-width-100" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                            <img class="ts-width-100" src="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>">
                        </a>
                    </div>
                    <div class="entry-main">
                        <div class="entry-header">
                            <div class="entry-meta">
                                <?if(!empty($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["VALUE"]) && isset($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"]) && !empty($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"])):?>
                                    <span class="entry-meta__item">
                                            <a class="entry-meta__link text-primary" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                                <strong><?=$arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"]?></strong></a>
                                        </span>
                                <?endif?>
                                <?if(in_array(array(1,7),$GLOBALS["USER"]->GetUserGroupArray())):?>
                                    <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                                            <a class="entry-meta__link text-primary" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arResult["SHOW_COUNTER"]?></a>
                                        </span>
                                <?endif?>
                                <!--<span class="entry-meta__item"><i class="icon fa fa-comment-o text-primary"></i>
                                    <a class="entry-meta__link text-primary" href="blog-main.html">29</a>
                                </span>-->
                            </div>
                            <h2 class="entry-title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                        </div>
                        <div class="entry-content">
                            <p>
                                <?if(!empty($arItem["PREVIEW_TEXT"])):?>
                                    <?=substr2($arItem["~PREVIEW_TEXT"], 120)?>
                                <?elseif(!empty($arItem["DETAIL_TEXT"])):?>
                                    <?=substr2($arItem["~DETAIL_TEXT"], 120)?>
                                <?endif?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>



        <?endif?>

        <?$i++?>

    <?endforeach;?>
        </div>
    <?if(!empty($arParams["DESCRIPTION_LINK"])):?>
        <div class="home_articles moreLM">
            <a href="<?=$arParams["DESCRIPTION_LINK"]?>"><?if(!empty($arParams["DESCRIPTION_NAMELINK"])):?><?=$arParams["DESCRIPTION_NAMELINK"]?><?else:?><?=GetMessage('NAMELINK_NEWS_BLOCK')?><?endif?></a>
        </div>
    <?endif?>
    </div>
<?endif?>

<script>
  //  $(".ellipsis").ellipsis();
</script>