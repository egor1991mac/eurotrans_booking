<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
$this->setFrameMode(true);
?>
<?if(!empty($arResult)):?>
    <div class="ts-d-block ts-mt-4 ts-mt-lg-0 ts-width-100 ts-px-2">
    <?if(!empty($arParams["TEXT_TITLE"])):?>
        <div class="widget-title ui-title-inner ui-title-inner_lg ts-mb-0 ts-mb-md-3">
            <span class="ui-title-inner__inner"><?=htmlspecialchars_decode($arParams["TEXT_TITLE"])?></span>
        </div>
    <?endif?>
    <div class="widget-content">
        <div class="posts-group-4 ts-row">
            <?foreach($arResult as $arItem):?>

                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				$img = getSrc($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"], array('width' => 120, 'height' => 95));
				if(empty($img[0])){
					$img = getSrc($arItem["PROPERTIES"]["PHOTO3"]["VALUE"], array('width' => 120, 'height' => 95), NO_PHOTO_PATH_120_95);
				}?>

                <section class="ts-col-24 ts-col-sm-12 ts-col-lg-24 b-post b-post_img-left b-post_img-120 clearfix" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="entry-media ">
                        <a class="js-zoom-images img-hover-effect" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                            <img class="img-responsive hov-scal" src="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>">
                        </a>
                    </div>
                    <div class="entry-main">
                        <div class="entry-header">
                            <div class="entry-title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                            <div class="entry-meta">
                                <span class="entry-meta__item">
                                    <a class="entry-meta__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                        <strong><?=$arItem["DISPLAY_ACTIVE_FROM"]?></strong>
                                    </a>
                                </span>
                                <?if(in_array(array(1,7),$GLOBALS["USER"]->GetUserGroupArray())):?>
                                    <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                                        <a class="entry-meta__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arResult["SHOW_COUNTER"]?></a>
                                    </span>
                                <?endif?>
                            </div>
                        </div>
                    </div>
                </section>

            <?endforeach;?>
        </div>
    </div>
    </div>
<?endif?>