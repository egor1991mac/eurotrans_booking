<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
$arComponentDescription = array(
    'NAME' => "Информация о агенте",
    'DESCRIPTION' => "Информация о агенте eurotrans.by",
    'SORT' => 40,
    'CACHE_PATH' => 'Y',
    'PATH' => array('ID' => 'travelsoft')
);
