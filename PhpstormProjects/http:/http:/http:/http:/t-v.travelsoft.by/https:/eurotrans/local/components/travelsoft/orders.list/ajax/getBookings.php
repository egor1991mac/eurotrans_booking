<?php
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PUBLIC_AJAX_MODE", true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');
Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

$data = $_POST;
$infoBookings = travelsoft\eurotrans\Utils::getUserBookings($data);

if (!empty($infoBookings['bookings'])) {

    foreach ($infoBookings['bookings'] as &$bookings) {

        $bookings['bookings'] = \travelsoft\eurotrans\Utils::getPointsNameByID($bookings['bookings']);


        foreach($bookings['bookings'] as &$booking){
            foreach ($booking['arrSeatsId'] as &$seat) {

                $url = "/local/modules/travelsoft.eurotrans.booking/pages/download.ticket.php?booking_id="
                    . $seat["booking_id"] . "&seat=" . $seat["name"]
                    . "&filename=" . $seat["booking_id"] . "_" . $seat["name"]
                    . "_" . $seat["last_name"] . "_" . $seat["first_name"] . ".pdf";

                $seat['url'] = $url;
            }
            unset($seat);
        }
        unset($booking);

        //$bookings['data']['print_urls'] = json_encode($printUrls);
    }
    unset($bookings);
}

travelsoft\eurotrans\Utils::jsonResponse(json_encode($infoBookings));
