<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult)):?>

    <?if(!empty($arParams["TEXT_TITLE"])):?>
        <h2 class="ui-title-inner ui-title-inner_mrg-btn_lg">
            <span class="ui-title-inner__inner"><?=htmlspecialchars_decode($arParams["TEXT_TITLE"])?></span>
        </h2>
    <?endif?>

    <div class="row fhotels lm-tab">
        <?$i = 1;?>
        <?foreach($arResult as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            $arImg = getSrc($arItem["PROPERTIES"]["FLAG"]["VALUE"], array("width"=>24,"height"=>24));?>

            <?if(($i == 1 || ($i-1) % 6 == 0) && count($arResult) >= $i):?>
                <div class="col-md-4 col-sm-6 mb-40">
                    <div class="fhotel_main">
                        <ul>
            <?endif?>

            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <?if(!empty($arImg[0])):?>
                        <img class="flag" src="<?=$arImg[0]?>">
                    <?endif?>
                    <strong><?=$arItem["NAME"]?></strong>
                    <?if((!empty($arItem["PROPERTIES"]["PRICE"]["VALUE"]) && !empty($arItem["PROPERTIES"]["CURRENCY"]["VALUE"]))):?>
                        <span class="price"><?=GetMessage('PRICE_FROM')?><strong><?=$arItem["PROPERTIES"]["PRICE"]["VALUE"]?></strong><span class="pln"><?=strip_tags($arItem["PROPERTIES"]["CURRENCY"]["DISPLAY_VALUE"])?></span></span>
                    <?endif?>
                </a>
            </li>

            <?if($i % 6 == 0):?>
                        </ul>
                        <div class="moreLM">
                            <?if($i >= 13 && count($arResult) >= 18 && !empty($arParams["DESCRIPTION_LINK"])):?>
                                <a href="<?=$arParams["DESCRIPTION_LINK"]?>"><?if(!empty($arParams["DESCRIPTION_NAMELINK"])):?><?=$arParams["DESCRIPTION_NAMELINK"]?><?else:?><?=GetMessage('NAMELINK_COUNTRIES_BLOCK')?><?endif?></a>
                            <?endif?>
                        </div>
                    </div>
                </div>
            <?endif?>

            <?$i++;?>
        <?endforeach;?>

    </div>

<?endif?>