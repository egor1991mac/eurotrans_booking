<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$this->setFrameMode(false);

if (!strlen($arResult['ROUTE']["DETAIL_PAGE_URL"])) {
    $arResult['ROUTE']["DETAIL_PAGE_URL"] = "/routes/";
}

$this->SetViewTarget('route_link');

// ссылка для маршрута на заголовок первой вкладки страницы
echo $arResult['ROUTE']['DETAIL_PAGE_URL'];

$this->EndViewTarget();

if (!empty($arResult["ERRORS"])) {
    ?>

    <div class="booking-block-inner" style="padding-top: 0; padding-bottom: 0;">
        <div class="booking-block-head">
            <div class="title h2">
                <span>На  данный рейс все билеты забронированы.</span>
            </div>
        </div>
    </div>

    <?/*============= dataLayer part ============*/?>
    <script>
        let arrDataLayer = JSON.parse('<?= json_encode($arResult['DATA_LAYER']);?>');

        dataLayer.push(arrDataLayer);
    </script>
    <?/*============= end dataLayer part ============*/?>

    <?
    return;
}
?>

<div class="booking-block-inner" style="padding-top: 0;">
    <div class="booking-block-head">
        <div class="content-column-content-inner">
            <blockquote class="caption alert-block" style="margin: 1rem 0">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/icons/messagebox_warning_32.png">
                Пассажиры младше 14 лет не могут занимать места с номерами: 1,2,3,4.
            </blockquote>
        </div>
        <div class="title h2">
            <span>Найдено рейсов:</span>
        </div>
    </div>
    <?
    foreach ($arResult['OFFERS'] as $offer):

        $map_parameters = $offer['TRIP']["EXT_BUS_MAP_PARAMETERS"];
        if ($offer["ROUND_TRIP"]["EXT_BUS_MAP_PARAMETERS"]) {
            $map_parameters = array_merge($map_parameters, $offer["ROUND_TRIP"]["EXT_BUS_MAP_PARAMETERS"]);
        }
        ?>
        <div class="table-wrapper">
            <div class="table table-tickets">
                <div class="table-head">
                    <div class="table-item table-road">
                        <span class="dropdown-trigger">Рейс
                            <i class="info-i">
                                <svg class="icon icon-drop">
                                <use xlink:href="#info" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                            <div class="dropdown-target">
                                <div class="dropdown-inner">
                                    <div class="dropdown-content">
                                        <div class="text-1 dep-points-title">Точки отправления:</div>
                                        <? foreach ($arResult['DEP_POINTS'] as $arPoint): ?>
                                            <div class="text-1"><?= $arPoint['UF_NAME'] ?></div>
                                        <? endforeach ?>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                    <div class="table-item">
                        <span>Отправление</span>
                    </div>
                    <div class="table-item">
                        <span>Прибытие</span>
                    </div>
                    <div class="table-item">
                        <span>Время в пути</span>
                    </div>
                </div>

                <div class="table-row">
                    <div class="table-item table-road">
                        <a href="#"><?= $offer['TRIP']['DEPARTURE_POINT_NAME'] ?>
                            → <?= $offer['TRIP']['ARRIVAL_POINT_NAME'] ?></a>
                    </div>
                    <div class="table-item">
                        <div class="text-1"><?= $offer['TRIP']['DEPARTURE_TIME_FORMATTED'] ?></div>
                    </div>
                    <div class="table-item">
                        <div class="text-1"><?= $offer['TRIP']['ARRIVAL_TIME_FORMATTED'] ?></div>
                    </div>
                    <div class="table-item">
                        <div class="text-1"><?= $offer['TRIP']['WAY_TIME']['HH'] ?>
                            ч <?= $offer['TRIP']['WAY_TIME']['MM'] ?> мин
                        </div>
                    </div>
                    <? $randStr = randString() ?>
                    <div class="table-item">
                        <a class="btn btn-colored small js-book-link" id="booking<?= $randStr ?>"
                           href='javascript:eurotrans.loadBuses(<?= \Bitrix\Main\Web\Json::encode($map_parameters) ?>);eurotrans.loadBusMap("booking<?= $randStr ?>", <?= \Bitrix\Main\Web\Json::encode($map_parameters) ?>)'>
                            <span><?= $offer['PRICE'] . ' ' . $offer['CURRENCY'] ?></span>
                        </a>
                    </div>
                </div>
                <? foreach ($offer['TRIP']['EXT_BUS_MAP_PARAMETERS'] as $parameters): ?>
                    <div class="table-row" id="<?= $parameters["HTML_ID"] ?>"></div>
                <? endforeach ?>
                <? if ($offer['ROUND_TRIP']): ?>
                    <div class="table-row">
                        <div class="table-item table-road">
                            <a href="#"><?= $offer['ROUND_TRIP']['DEPARTURE_POINT_NAME'] ?>
                                → <?= $offer['ROUND_TRIP']['ARRIVAL_POINT_NAME'] ?></a>
                        </div>
                        <div class="table-item">
                            <div class="text-1"><?= $offer['ROUND_TRIP']['DEPARTURE_TIME_FORMATTED'] ?></div>
                        </div>
                        <div class="table-item">
                            <div class="text-1"><?= $offer['ROUND_TRIP']['ARRIVAL_TIME_FORMATTED'] ?></div>
                        </div>
                        <div class="table-item">
                            <div class="text-1"><?= $offer['ROUND_TRIP']['WAY_TIME']['HH'] ?>
                                ч <?= $offer['ROUND_TRIP']['WAY_TIME']['MM'] ?> мин
                            </div>
                        </div>
                    </div>
                    <? foreach ($offer['ROUND_TRIP']['EXT_BUS_MAP_PARAMETERS'] as $parameters): ?>
                        <div class="table-row" id="<?= $parameters["HTML_ID"] ?>"></div>
                    <? endforeach ?>
                <? endif ?>
            </div>
        </div>
    <? endforeach ?>
</div>

<script>
    generatePopup(document.documentElement.clientHeight, document.documentElement.clientWidth, window.location.href);
</script>

<script>
    BX.ready(function () {
        eurotrans.loadRequest(<?= \Bitrix\Main\Web\Json::encode($arParams['REQUEST']) ?>);
        eurotrans.loadPreviousLinks(<?= \Bitrix\Main\Web\Json::encode(array(
            "tickets" => $APPLICATION->GetCurPageParam("", array(), false),
            "routes" => $arResult['ROUTE']['DETAIL_PAGE_URL']
        ))?>)
    });
</script>

<?/*============= dataLayer part ============*/?>
<script>
    let arrDataLayer = JSON.parse('<?= json_encode($arResult['DATA_LAYER']);?>');

    dataLayer.push(arrDataLayer);
</script>
<?/*============= end dataLayer part ============*/?>