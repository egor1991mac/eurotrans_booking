<?php

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PUBLIC_AJAX_MODE", true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

try {
    if(!isset($_SESSION[travelsoft\eurotrans\Settings::sessionStoreId()])) {

        throw new Exception();
    }
    travelsoft\eurotrans\Utils::jsonResponse(json_encode(array("session" => "ok")));

} catch (Exception $error) {

    travelsoft\eurotrans\Utils::jsonResponse(json_encode(array("error" => "true")));
}

