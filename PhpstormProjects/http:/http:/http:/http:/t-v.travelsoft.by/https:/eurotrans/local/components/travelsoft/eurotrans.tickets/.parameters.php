<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/** @var array $arCurrentValues */
$arComponentParameters['PARAMETERS']['REQUEST'] = array(
    "PARENT" => "BASE",
    "NAME" => "Запрос на поиск билетов",
    "TYPE" => "STRING",
    "DEFAULT" => '={$_REQUEST["eurotrans"]}'
);

$arComponentParameters['PARAMETERS']['BOOKING_PAGE'] = array(
    "PARENT" => "BASE",
    "NAME" => "Страница бронирования билета",
    "TYPE" => "STRING"
);
