<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//$this->addExternalJS(SITE_TEMPLATE_PATH."/assets/js/jquery.ellipsis.js");
?>
<?if(!empty($arResult["ITEMS"])):?>

    <div class="l-main-content l-main-content_mrg-right_minus ts-pb-6">
        <div class="posts-group">
        <?$i = 1;?>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            $img = getSrc($arItem["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["VALUE"], array('width' => 360, 'height' => 220));
            if(empty($img[0])){
                $img = getSrc($arItem["DISPLAY_PROPERTIES"]["PHOTO3"]["VALUE"], array('width' => 360, 'height' => 220), NO_PHOTO_PATH_360_222);
            }
            ?>


            <?if($i == 1 || ($i - 1) % 3 == 0):?>
                <div class="b-post b-post_bg-blue" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="ts-row">
                    <div class="entry-media ts-col-24 ts-col-sm-12">
                        <a class="ts-width-100" href="<?=SITE_DIR?>profi/novosti/<?=$arItem["CODE"]?>">
                            <img class="ts-width-100" src="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>">
                        </a>
                    </div>
                    <div class="entry-main ts-p-4 ts-px-sm-2 ts-col-24 ts-col-sm-12">

                            <div class="entry-header">
                                <!--<div class="entry-meta">
                                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["VALUE"]) && isset($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"]) && !empty($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"])):?>
                                        <span class="entry-meta__item">
                                                <a class="entry-meta__link text-primary" href="<?=SITE_DIR?>profi/novosti/<?=$arItem["CODE"]?>">
                                                    <strong><?=$arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"]?></strong></a>
                                            </span>
                                    <?endif?>
                                    <?if(in_array(array(1,7),$GLOBALS["USER"]->GetUserGroupArray())):?>
                                        <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                                            <a class="entry-meta__link text-primary" href="<?=SITE_DIR?>profi/novosti/<?=$arItem["CODE"]?>"><?=$arItem["SHOW_COUNTER"]?></a>
                                        </span>
                                    <?endif?>
                                    <!--<span class="entry-meta__item"><i class="icon fa fa-comment-o text-primary"></i>
                                        <a class="entry-meta__link text-primary" href="blog-main.html">29</a>
                                    </span>
                                </div> -->
                                <h2 class="entry-title"><a href="<?=SITE_DIR?>profi/novosti/<?=$arItem["CODE"]?>"><?=$arItem["NAME"]?></a></h2>
                            </div>
                            <div class="entry-content ts-width-90">
                                <p>
                                    <?if(!empty($arItem["PREVIEW_TEXT"])):?>
                                        <?=substr2($arItem["~PREVIEW_TEXT"], 120)?>
                                    <?elseif(!empty($arItem["DETAIL_TEXT"])):?>
                                        <?=substr2($arItem["~DETAIL_TEXT"], 120)?>
                                    <?endif?>
                                </p>
                            </div>

                        <div class="entry-footer"><a class="btn-bd-primary" href="<?=SITE_DIR?>profi/novosti/<?=$arItem["CODE"]?>"><?=GetMessage('NEWS_MORE_INFO')?></a></div>
                    </div>
                    </div>
                </div>

            <?else:?>

                <?if(($i + 1) % 3 == 0):?>
                    <div class="posts-group-2">
                        <div class="ts-row">
                <?endif;?>

                <div class="ts-col-24 ts-col-sm-12 ts-pb-6" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="b-post">
                        <div class="entry-media">
                            <a class=" ts-width-100" href="<?=SITE_DIR?>profi/novosti/<?=$arItem["CODE"]?>">
                                <img class="img-responsive  ts-width-100" src="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>">
                            </a>
                        </div>
                        <div class="entry-main">
                            <div class="entry-header">
                               <!-- <div class="entry-meta">
                                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["VALUE"]) && isset($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"]) && !empty($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"])):?>
                                        <span class="entry-meta__item">
                                            <a class="entry-meta__link text-primary" href="<?=SITE_DIR?>profi/novosti/<?=$arItem["CODE"]?>">
                                                <strong><?=$arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"]?></strong></a>
                                        </span>
                                    <?endif?>
                                    <?if(in_array(array(1,7),$GLOBALS["USER"]->GetUserGroupArray())):?>
                                        <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                                            <a class="entry-meta__link text-primary" href="<?=SITE_DIR?>profi/novosti/<?=$arItem["CODE"]?>"><?=$arResult["SHOW_COUNTER"]?></a>
                                        </span>
                                    <?endif?>
                                    <!--<span class="entry-meta__item"><i class="icon fa fa-comment-o text-primary"></i>
                                        <a class="entry-meta__link text-primary" href="blog-main.html">29</a>
                                    </span>
                                </div> -->
                                <h2 class="entry-title"><a href="<?=SITE_DIR?>profi/novosti/<?=$arItem["CODE"]?>"><?=$arItem["NAME"]?></a></h2>
                            </div>
                            <div class="entry-content">
                                <p>
                                    <?if(!empty($arItem["PREVIEW_TEXT"])):?>
                                        <?=substr2($arItem["~PREVIEW_TEXT"], 120)?>
                                    <?elseif(!empty($arItem["DETAIL_TEXT"])):?>
                                        <?=substr2($arItem["~DETAIL_TEXT"], 120)?>
                                    <?endif?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <?if($i % 3 == 0 || $i == (count($arResult["ITEMS"]) + 1)):?>
                        </div>
                    </div>
                <?endif?>

            <?endif?>

            <?$i++?>

        <?endforeach;?>

        </div>
    </div>

    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <div class="ts-row ts-justify-content__center ts-mb-6">
        <?=$arResult["NAV_STRING"]?>
        </div>
    <?endif;?>


<?endif?>
