<?php
use travelsoft\eurotrans\Settings;

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PUBLIC_AJAX_MODE", true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

try {

    $request = $_REQUEST;

    $phone = $request['phone'];
    $timestamp = $request['timestamp'];


    $booking = $_SESSION[Settings::sessionStoreId()][$timestamp];

    $trips = [
        'trip',
        'return_trip'
    ];

    $details = [];
    foreach ($trips as $trip){
        if(!empty($booking['details'][$trip])){
            $details[] = [
                'bus_id' => $booking['details'][$trip]['way']['bus_id'],
                'booking_date' => $booking['details'][$trip]['way']['departure_date'],
            ];
        }
    }

    $response = ['exist' => false];
    if(!empty($phone) && !empty($details)) {
        $response = travelsoft\eurotrans\Utils::checkOnBooking($phone, $details);
    }

    travelsoft\eurotrans\Utils::jsonResponse(json_encode($response));


} catch (Exception $ex) {

    travelsoft\eurotrans\Utils::jsonResponse(json_encode(array("error" => true)));
}

