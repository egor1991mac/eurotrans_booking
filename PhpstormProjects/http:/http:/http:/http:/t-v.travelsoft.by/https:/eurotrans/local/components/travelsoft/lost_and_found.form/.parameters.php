<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    "GROUPS" => array(
        "PARAMS" => array(
            "NAME" => GetMessage("MAIN_INCLUDE_PARAMS"),
        ),
    ),

    "PARAMETERS" => array(
        "TITLE" => Array(
            "NAME" => "Заголовок",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "ADDITIONAL_VALUES" => "N",
            "DEFAULT" => 'Заголовок',
            "PARENT" => "PARAMS",
        ),
        "FORM_ID" => Array(
            "NAME" => "ID формы",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "ADDITIONAL_VALUES" => "N",
            "DEFAULT" => 'FORM_ID',
            "PARENT" => "PARAMS",
        )
    ),
);
?>