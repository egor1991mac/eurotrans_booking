<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if (!empty($arResult["ITEMS"])): ?>

    <? if (!empty($arParams["TEXT_TITLE"])): ?>
        <h4 class="widget-title ui-title-inner ui-title-inner_lg">
            <span class="ui-title-inner__inner"><?= htmlspecialchars_decode($arParams["TEXT_TITLE"]) ?></span>
        </h4>
    <? endif ?>

    <div class="posts-group">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            if (!empty($arItem["DISPLAY_PROPERTIES"]["LOGO"]["VALUE"])) $img = getSrc($arItem["DISPLAY_PROPERTIES"]["LOGO"]["VALUE"], array('width' => 260, 'height' => 160, 'RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL_ALT), NO_PHOTO_PATH_260_160, 1);
            else
                $img = getSrc($arItem["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"][0], array('width' => 260, 'height' => 160, 'RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL_ALT), NO_PHOTO_PATH_260_160, 1);
            ?>
            <section class="b-post b-post_img-left b-post_img-260 b-post_img-260_mod-a b-post_bg-white clearfix"
                     id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="entry-media"><a class="js-zoom-images img-hover-effect"
                                            href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img
                                class="img-responsive hov-scal img-border" src="<?= $img[0] ?>"
                                alt="<?= $arItem["NAME"] ?>"/></a></div>
                <div class="entry-main">
                    <div class="entry-header">
                        <h4 class="entry-title"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
                        </h4>
                        <div class="entry-meta">
                            <? if (!empty($arItem["DISPLAY_PROPERTIES"]["CITY"]["DISPLAY_VALUE"])): ?><span
                                    class="entry-meta__item"><?= strip_tags($arItem["DISPLAY_PROPERTIES"]["CITY"]["DISPLAY_VALUE"]) ?></span><? endif; ?><? if (!empty($arItem["PROPERTIES"]["METRO"]["VALUE"])): ?>
                                <span class="entry-meta__item">станция
                                метро <?= strip_tags($arItem["DISPLAY_PROPERTIES"]["METRO"]["DISPLAY_VALUE"]) ?></span><? endif; ?>
                        </div>
                    </div>
                    <div class="entry-content">
                        <? if (!empty($arItem["PROPERTIES"]["ADDRESS"]["VALUE"])): ?>
                            <p><?= strip_tags($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]) ?></p>
                        <? endif; ?>
                        <? if (!empty($arItem["PROPERTIES"]["SITE"]["VALUE"])): ?>
                            <p><a href="/frame/?url=<?= strip_tags($arItem["PROPERTIES"]["SITE"]["VALUE"]) ?>"
                                  target="_blank"><?= strip_tags($arItem["PROPERTIES"]["SITE"]["VALUE"]) ?></a></p>
                        <? endif; ?>
                        <? if (!empty($arItem["PROPERTIES"]["PHONE"]["VALUE"][0])): ?>
                            <p>
                                <a href="tel:<?= strip_tags($arItem["PROPERTIES"]["PHONE"]["VALUE"][0]) ?>"><?= strip_tags($arItem["PROPERTIES"]["PHONE"]["VALUE"][0]) ?></a>
                            </p>
                        <? endif; ?>
                        <? if (!empty($arItem["PROPERTIES"]["ABOUT"]["VALUE"])): ?>
                            <p><?= substr2($arItem["DISPLAY_PROPERTIES"]["ABOUT"]["DISPLAY_VALUE"], 180) ?></p>
                        <? endif ?>
                        <? if (!empty($arItem['DISPLAY_PROPERTIES']['DESC']['~VALUE']['TEXT'])): ?>
                            <p><?= substr2($arItem['DISPLAY_PROPERTIES']['DESC']['~VALUE']['TEXT']); ?></p>
                        <? endif; ?>
                    </div>
                    <div class="entry-footer"><a class="btn-bd-primary" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">подробнее</a>
                    </div>
                </div>
            </section>
        <? endforeach; ?>
        <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
            <?= $arResult["NAV_STRING"] ?>
        <? endif; ?>
    </div>
<? endif ?>