<?

class CallPhoneForm extends CBitrixComponent
{

    protected $request = null;

    function processPostForm()
    {
        if (check_bitrix_sessid() && $this->request->isPost() && !empty($this->request->getPost('LOST_AND_FOUND')) &&
            $this->request->getPost('SUBMIT') !== "") {
            foreach ($this->arResult['FORM'] as &$HEADER) {

                foreach ($HEADER["FIELDS"] as $NAME => &$FIELD) {
                    $value = $this->request->getPost($NAME);

                    if ($FIELD["type"] == "datetime") {
                        $value .= ' ' . $this->request->getPost($NAME . '-HOUR') . ':';
                        $value .= $this->request->getPost($NAME . '-MINUTE') . ':00';

                    }

                    if ($FIELD['isRequired']) {
                        if ($value == "") {
                            $error = getMessage("TS_CP_EMPTY_FIELD", array("#FIELD#" => $FIELD["formLabel"]));
                            $this->arResult['ERRORS'][] = $error;
                        }
                    }
                    $FIELD['value'] = $value;
                }
            }

            if (!$this->arResult['ERRORS'])
                return true;
        }
        return false;
    }

    public function crmRequestWithResponse($queryFunction, $queryData = "")
    {
        if (!empty($queryFunction)) {
            $queryUrl = CRM_EUROTRANS;
            $queryUrl .= $queryFunction . '/';

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $queryUrl,
                CURLOPT_POSTFIELDS => $queryData,
            ));

            $result = curl_exec($curl);
            curl_close($curl);
            $result = json_decode($result, 1);
            return $result;
        }
        return false;
    }

    /**
     * Выполнение компонента
     */
    public function executeComponent()
    {
        $this->request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

        $this->arResult['FORM'] = Array(
            'APPLICATION_FORM' => Array(
                "NAME" => "Персональная информация:",
                "FIELDS" => Array(
                    "NAME" => Array(
                        "type" => "text",
                        "formLabel" => "Имя:",
                        "placeholder" => "Ваше имя",
                        "isRequired" => true
                    ),
                    "PHONE" => Array(
                        "type" => "tel",
                        "formLabel" => "Телефон:",
                        "placeholder" => "Ваш телефон",
                        "isRequired" => true
                    ),
                    "EMAIL" => Array(
                        "type" => "email",
                        "formLabel" => "E-mail указанный при бронировании:",
                        "placeholder" => "Ваш e-mail",
                        "isRequired" => true
                    ),
                )
            ),
            'INFORMATION_TRIP' => Array(
                "NAME" => "Информация о поездке:",
                "FIELDS" => Array(
                    "UF_CRM_1483795016" => Array(
                        "type" => "text",
                        "formLabel" => "Точка отправления:",
                        "placeholder" => "Точка отправления",
                        "isRequired" => true,
                    ),
                    "UF_CRM_1483795308" => Array(
                        "type" => "text",
                        "formLabel" => "Точка прибытия:",
                        "placeholder" => "Точка прибытия",
                        "isRequired" => true,
                    ),
                    "UF_CRM_1512372719" => Array(
                        "type" => "datetime",
                        "formLabel" => "Дата отправления:",
                        "formLabelTime" => "Время отправления:",
                        "isRequired" => true,
                    ),
                    "UF_CRM_1512632127" => Array(
                        "type" => "text",
                        "formLabel" => "Номер билета/брони:",
                        "placeholder" => "Номер билета/брони",
                        "isRequired" => false,
                    ),
                )
            ),
            'INFORMATION_ABOUT_THING' => Array(
                "NAME" => "Информация о забытой/найденной вещи:",
                "FIELDS" => Array(
                    "COMMENTS" => Array(
                        "type" => "textarea",
                        "formLabel" => "Опишите  более подробно потерянную или найденную вещь:", //, и тем быстрее мы сможем  найти вашу потерянную вещь или найти владельца найденного предмета
                        "isRequired" => true,
                    ),
                ),
            )
        );

        if ($this->processPostForm()) {

            if (!empty($this->arParams["TITLE"])) {
                $fields["TITLE"] = $this->arParams["TITLE"];
            }

            foreach ($this->arResult['FORM'] as $HEADER) {
                foreach ($HEADER["FIELDS"] as $NAME => $FIELD) {
                    if (($FIELD["type"] == "tel") || ($FIELD["type"] == "email")) {
                        $fields[$NAME] = Array('n0' => Array('VALUE' => $FIELD['value']));
                    } else {
                        $fields[$NAME] = $FIELD['value'];
                    }
                }
            }

            $queryData = http_build_query(array(
                'fields' => $fields,
                'params' => array("REGISTER_SONET_EVENT" => "Y")
            ));
            $queryFunction = 'crm.lead.add';
            //$queryFunction = 'crm.lead.fields';
            $result = $this->crmRequestWithResponse($queryFunction, $queryData);

            if (!empty($result["error_description"])) {
                $this->arResult['ERRORS'][] = $result["error_description"];
            } else {
                $this->arResult['MESSAGE_OK'] = "Спасибо. В ближайшее время мы свяжемся с Вами.";
            }
        }
        $this->includeComponentTemplate();
    }
}