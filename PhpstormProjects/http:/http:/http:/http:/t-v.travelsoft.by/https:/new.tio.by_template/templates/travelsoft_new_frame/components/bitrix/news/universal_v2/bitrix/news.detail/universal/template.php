<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalCss(SITE_TEMPLATE_PATH."/assets/plugins/slider-pro/slider-pro.css");
$this->addExternalJS(SITE_TEMPLATE_PATH."/assets/plugins/slider-pro/jquery.sliderPro.min.js");

$img = getSrc($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["VALUE"], array('width' => 1140, 'height' => 550));
if(empty($img[0])){
    $img = getSrc($arResult["DISPLAY_PROPERTIES"]["PHOTO3"]["VALUE"], array('width' => 1140, 'height' => 550));
}
?>
    <div class="l-main-content">
        <article class="b-post b-post-full clearfix">
            <div class="entry-main">
					<div class="entry-header">
                    <h4 class="entry-title"><?=$arResult["NAME"]?></h4>
                  </div>
                <?if(!empty($arResult["PROPERTIES"]["TEXT"]["VALUE"])):?>
                   <div class="entry-content">
                       <p><?=$arResult["DISPLAY_PROPERTIES"]["TEXT"]["DISPLAY_VALUE"]?></p>
				   </div>
                <?endif?>
                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DESC1"]["VALUE"]["TEXT"]) || !empty($arResult["DISPLAY_PROPERTIES"]["DESC2"]["VALUE"]["TEXT"]) || !empty($arResult["DISPLAY_PROPERTIES"]["DESC3"]["VALUE"]["TEXT"])):?>
                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DESC1"]["VALUE"]["TEXT"])):?>
                        <div class="entry-content">
                            <p><?=$arResult["DISPLAY_PROPERTIES"]["DESC1"]["DISPLAY_VALUE"]?></p>
                        </div>
                        <?if(isset($arResult["SLIDER1"]) && !empty($arResult["SLIDER1"])):?>
                            <div class="main-slider slider-pro" id="main-slider1" data-slider-width="78%" data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false">
                                <div class="sp-slides">
                                    <?foreach ($arResult["SLIDER1"] as $k=>$slide):?>
                                        <?$slide_desc = $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["DESCRIPTION"][$k];?>
                                        <!-- Slide -->
                                        <div class="sp-slide">
                                            <img class="sp-image" src="<?=$slide?>" alt="<?=!empty($slide_desc) ? $slide_desc : 'slide-'.$k+1?>"/>
                                            <?if(!empty($slide_desc)):?>
                                                <div class="main-slider__info sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <section class="b-post b-post_slider clearfix">
                                                                    <div class="entry-main">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title hov-title"><a class="hov-title__inner"><?=$slide_desc?></a></h2>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?endif?>
                                        </div>
                                    <?endforeach;?>
                                </div>
                            </div>
                        <?endif?>
                    <?endif?>
                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DESC2"]["VALUE"]["TEXT"])):?>
                        <div class="entry-content">
                            <p><?=$arResult["DISPLAY_PROPERTIES"]["DESC2"]["DISPLAY_VALUE"]?></p>
                        </div>
                        <?if(isset($arResult["SLIDER2"]) && !empty($arResult["SLIDER2"])):?>
                            <div class="main-slider slider-pro" id="main-slider2" data-slider-width="78%" data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false">
                                <div class="sp-slides">
                                    <?foreach ($arResult["SLIDER2"] as $k=>$slide):?>
                                        <?$slide_desc = $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO2"]["DESCRIPTION"][$k];?>
                                        <!-- Slide -->
                                        <div class="sp-slide">
                                            <img class="sp-image" src="<?=$slide?>" alt="<?=!empty($slide_desc) ? $slide_desc : 'slide-'.$k+1?>"/>
                                            <?if(!empty($slide_desc)):?>
                                                <div class="main-slider__info sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <section class="b-post b-post_slider clearfix">
                                                                    <div class="entry-main">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title hov-title"><a class="hov-title__inner"><?=$slide_desc?></a></h2>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?endif?>
                                        </div>
                                    <?endforeach;?>
                                </div>
                            </div>
                        <?endif?>
                    <?endif?>
                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DESC3"]["VALUE"]["TEXT"])):?>
                        <div class="entry-content">
                            <p><?=$arResult["DISPLAY_PROPERTIES"]["DESC3"]["DISPLAY_VALUE"]?></p>
                        </div>
                    <?endif?>
                <?else:?>
                    <div class="entry-content">
                        <p><?=$arResult["~DETAIL_TEXT"]?></p>
                        <br>
                        <?if(isset($arResult["SLIDER1"]) && !empty($arResult["SLIDER1"])):?>
                            <div class="main-slider slider-pro" id="main-slider1" data-slider-width="78%" data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false" style="margin: 0">
                                <div class="sp-slides">
                                    <?foreach ($arResult["SLIDER1"] as $k=>$slide):?>
                                        <?$slide_desc = $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["DESCRIPTION"][$k];?>
                                        <!-- Slide -->
                                        <div class="sp-slide">
                                            <img class="sp-image" src="<?=$slide?>" alt="<?=!empty($slide_desc) ? $slide_desc : 'slide-'.$k+1?>"/>
                                            <?if(!empty($slide_desc)):?>
                                                <div class="main-slider__info sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <section class="b-post b-post_slider clearfix">
                                                                    <div class="entry-main">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title hov-title"><a class="hov-title__inner"><?=$slide_desc?></a></h2>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?endif?>
                                        </div>
                                    <?endforeach;?>
                                </div>
                            </div>
                        <?endif?>
                    </div>
                <?endif?>
				<?if(!empty($arResult["PROPERTIES"]["VIDEO"]["VALUE"])):?>
					<iframe width="100%" height="600" src="https://www.youtube.com/embed/<?=$arResult["PROPERTIES"]["VIDEO"]["VALUE"]?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				<?endif;?>
                <div class="entry-footer">
                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["THEME"]["VALUE"])):?>
                        <div class="post-tags"><span class="entry-footer__title"><?=GetMessage('THEME')?></span>
                            <ul class="list-tags list-tags_grey list-unstyled">
                                <?if(isset($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"]) && !empty($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"])):?>
                                    <?foreach ($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"] as $theme):?>
                                        <li class="list-tags__item"><a class="list-tags__link btn btn-default" href="<?=$theme["DETAIL_PAGE_URL"]?>"><?=$theme["NAME"]?></a></li>
                                    <?endforeach;?>
                                <?else:?>
                                    <?if(is_array($arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"])):?>
                                        <?=implode2('</li>, <li class="list-tags__item">',$arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"])?>
                                    <?else:?>
                                        <li class="list-tags__item"><?=$arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"]?></li>
                                    <?endif?>
                                <?endif?>
                            </ul>
                        </div>
                    <?endif?>
                    <div class="entry-footer__social">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.share",
                            "main",
                            Array(
                                "HANDLERS" => array("facebook","twitter","vk"),
                                "HIDE" => "N",
                                "PAGE_TITLE" => $APPLICATION->GetTitle(),
                                "PAGE_URL" => $_SERVER["REQUEST_URI"],
                                "SHORTEN_URL_KEY" => "",
                                "SHORTEN_URL_LOGIN" => ""
                            )
                        );?>
                    </div>
                </div>
            </div>
        </article>
        <!-- end .post-->
    </div>
<script>
    $( document ).ready(function() {
        $(".entry-content img").each(function () {
                $(this).addClass('img-responsive');
            }
        );
        $(".entry-footer .list-tags li").each(function () {
                if($(this).find('a').hasClass('list-tags__link') === false){
                    $(this).find('a').addClass('list-tags__link btn btn-default');
                }
            }
        );
    });
</script>