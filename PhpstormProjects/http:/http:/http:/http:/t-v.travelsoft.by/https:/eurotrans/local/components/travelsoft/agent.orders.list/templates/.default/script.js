let promiseResolve, promiseReject;

function showPopupCancelBooking(id) {
    return new Promise((resolve, reject) => {
        promiseResolve = resolve;
        promiseReject = reject;

        $('body').append(`<div class="modal-layout js-popup-container active" data-modal="payback" id="payback">
            <!--контейнер модалки-->
            <!-- по саксесу вешаю на этот блок класс из-за которого следующий открывается-->
            <div class="modal-container" style="max-width: 55.4375rem;">
                <div class="modal-container-header">
                <div class="text-medium">Отмена билетов</div>
                <i class="small-link pop-close closePopup" onclick="promiseReject()"></i>
                </div>
                <div class="modal-container-content">
                    <div class="table-wrapper">
                        <div class="table table-tickets">
                            <div class="table-head">
                                <div class="table-item table-num">
                                    <span>№</span>
                                </div>
                                <div class="table-item">
                                    <span>Условия возврата</span>
                                </div>
                                <div class="table-item">
                                    <span>Размер возврата средств</span>
                                </div>
                            </div>
                            <div class="table-row">
                                <div class="table-item table-num">
                                    <div class="text-1">1</div>
                                </div>
                                <div class="table-item">
                                    <div class="text-1">При отказе от поездки в период времени от 48 часов до 24 часов до указанной поездки</div>
                                </div>
                                <div class="table-item">
                                    <div class="text-1">50%</div>
                                </div>
                            </div>
                            <div class="table-row">
                                <div class="table-item table-num">
                                    <div class="text-1">2</div>
                                </div>
                                <div class="table-item">
                                    <div class="text-1">При отказе от поездки в период времени до 24 часов до указанной поездки</div>
                                </div>
                                <div class="table-item">
                                    <div class="text-1">0%</div>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                    
                    <div class="text-1">Вы уверены, что хотите отменить билет ${id}?</div>
                    <div class="form-block-footer">
                        <div class="input-form">
                        <div class="input-item s33">
                            <a class="btn btn-colored closePopup" onclick="promiseReject()">
                            <span>Нет, не нужно</span>
                            </a>
                        </div>
                        <div class="input-item s33 text-center">
                            <a class="btn btn-colored red closePopup" onclick="promiseResolve()">
                            <span>да, отменить!</span>
                            </a>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`);
    })
}