<div class="ts-theme ts-wrap">
    <? if (empty($arResult["BOOKINGS_ID"])) {
        echo "<div class=\"booking-block-head\">
        <div class=\"title h2\">
            <span>Отсутвует номер брони</span>
        </div>
    </div>";
        return;
    } ?>

    <? if (!empty($_GET["err_back"]) && ($_GET["err_back"] == "true")): ?>

        <div class="title h2">
            <span>Ошибка при бронировании рейса обратно.</span>
        </div>

    <? endif; ?>
    <div class="ts-row ts-py-2">
        <div class="ts-col-24">
            <h5>
                Информация о вашей поездке:
            </h5>
        </div>
    </div>

    <div id="tickets-block" class="ts-row">
        <div class="ts-col-24 ts-col-sm-18">
            <div class="ts-row">
        <? foreach ($arResult["INFO_BOOKINGS"] as $booking): ?>
        <div class="ts-col-24 ts-col-md-12 ts-mt-2 ts-mt-md-0 ts-mb-2">
            <div class="card ts-border ts-py-2 ">
                <div class="ts-col-24 ts-pb-2">
                    <h6 class="tittle">
                        <span class="bold"><?= $booking['departure'] ?></span> →
                        <span class="bold"><?= $booking['arrival'] ?></span>
                    </h6>
                    <div class="ts-hr__dashed ts-pt-2"></div>
                </div>
                <div class="ts-col-24 ts-pb-1">
                        <ul class="route">
                                <li>
                                    <b>
                                    Номер брони:
                                    </b>

                                    <?= $booking["id"] ?>
                                </li>
                                <li>
                                    <b>
                                    Дата поездки:
                                    </b>

                                    <?= $booking["booking_datetime"]["date"] ?>
                                </li>
                                <li>
                                    <b>
                                    Отправление из:
                                    </b>
                                    <?= $booking['departure'] ?>
                                </li>
                                <li>
                                    <b>Время отправелния:</b>
                                    <?= $booking['booking_datetime']['time'] ?></li>
                                <li>
                                    <b>Прибытие в:</b>

                                    <?= $booking['arrival'] ?>
                                </li>
                                <li>
                                    <b>Время прибытия:</b>
                                    <?= $booking['stop_datetime']['time'] ?>
                                </li>
                                <li>
                                    <b>Рейс:</b>
                                    <?= $booking["TRIP_INFO"]["location_arr"][$booking["pickup_id"]]["name"] . " - " . $booking["TRIP_INFO"]["location_arr"][$booking["return_id"]]["name"]; ?>
                                </li>
                                <li>
                                    <b>Место:</b>
                                    <?= $booking["implodeSeatsId"]; ?>
                                </li>
                            </ul>
                        </ul>
                    <div class="ts-hr__dashed ts-pt-2"></div>
                </div>
                <div class="ts-col-24 ts-justify-content__space-between">
                    <? if ($booking["price"] != $booking['price_without_discount']): ?>
<!--
                            <h6>
                        Скидка:
                    </h6> -->
                    <h6 class="price">
                        <!-- Вывести скидку  если она нужна довоблять класс ts-hidden-->
                        <?= ($booking['price_without_discount']); ?>
                    </h6>

                    <div class="ts-hr__dashed ts-pt-1"></div>
                    <? endif; ?>
                </div>
                <div class="ts-col-24 ts-pt-2 ts-justify-content__space-between">
                    <h6>
                        Итого:
                    </h6>
                    <h6 class="price">
                        <?= $booking["price"] . " " . $booking["currency"]; ?>
                    </h6>
                </div>
                        <!--
                        Стоимость:
                        <span class="price bold">
                            <?= $booking["price_without_discount"] . " " . $booking["currency"] ?>
                            <? if ($booking["price"] != $booking['price_without_discount']): ?>
                                <li>
                                    Скидка
                                    <?if(!empty($booking['promotional_id'])):?>
                                        (за промокод)
                                    <?else:?>
                                        (за проезд туда-обратно)
                                    <?endif;?>
                                    :
                                    <span class="bold">
                            <?= ($booking['price_without_discount'] - $booking["price"]) . " " . $booking["currency"]; ?>
                        </span>
                                </li>
                                <li>
                                    Стоимость с учетом скидки:
                                    <span class="bold">
                            <?= $booking["price"] . " " . $booking["currency"]; ?>
                        </span>
                                </li>
                            <? endif; ?>
                            <? if ($booking["status"] == "confirmed"): ?>
                                <li>
                                    Билеты:
                                    <div>
                                        <? foreach ($booking["arrSeatsId"] as $k => $arSeats): ?>
                                            <a class="ticket-pdf"
                                               href="/local/modules/travelsoft.eurotrans.booking/pages/download.ticket.php?booking_id=<?= $arSeats["booking_id"] ?>&seat=<?= $arSeats["name"] ?>&filename=<?= $arSeats["booking_id"] . "_" . $arSeats["name"] . "_" . $arSeats["last_name"] . "_" . $arSeats["first_name"] ?>.pdf">
                                                <?= $arSeats["last_name"] . " " . $arSeats["first_name"] ?>
                                            </a><br/>
                                        <? endforeach; ?>
                                    </div>
                                </li>
                            <? endif; ?> -->

            </div>

        </div>
           <!-- <div class="input-item s50 feedback-item-reply" >
                <div class="title h3">
                    <span class="bold"><?= $booking['departure'] ?></span> →
                    <span class="bold"><?= $booking['arrival'] ?></span>
                </div>
                <ul class="tickets-info">
                    <li>
                        Номер брони:
                        <span class="bold date">
                            <?= $booking["id"] ?>
                        </span>
                    </li>
                    <li>
                        Дата поездки:
                        <span class="bold date">
                            <?= $booking["booking_datetime"]["date"] ?>
                        </span>
                    </li>
                    <li>
                        Отправление из:
                        <span class="bold location-from">
                            <?= $booking['departure'] ?>
                            в <?= $booking['booking_datetime']['time'] ?>
                        </span>
                    </li>
                    <li>Прибытие в:
                        <span class="bold location-to">
                            <?= $booking['arrival'] ?>
                            в <?= $booking['stop_datetime']['time'] ?>
                        </span>
                    </li>
                    <li>
                        Рейс:
                        <span class="bold trip">
                            <?= $booking["TRIP_INFO"]["location_arr"][$booking["pickup_id"]]["name"] . " - " . $booking["TRIP_INFO"]["location_arr"][$booking["return_id"]]["name"]; ?>
                        </span>
                    </li>
                    <li>
                        Место:
                        <span class="seats bold">
                            <?= $booking["implodeSeatsId"]; ?>
                        </span>
                    </li>
                    <li>
                        Стоимость:
                        <span class="price bold">
                            <?= $booking["price_without_discount"] . " " . $booking["currency"] ?>
                        </span>
                    </li>
                    <? if ($booking["price"] != $booking['price_without_discount']): ?>
                        <li>
                            Скидка
                            <?if(!empty($booking['promotional_id'])):?>
                                (за промокод)
                            <?else:?>
                                (за проезд туда-обратно)
                            <?endif;?>
                            :
                            <span class="bold">
                            <?= ($booking['price_without_discount'] - $booking["price"]) . " " . $booking["currency"]; ?>
                        </span>
                        </li>
                        <li>
                            Стоимость с учетом скидки:
                            <span class="bold">
                            <?= $booking["price"] . " " . $booking["currency"]; ?>
                        </span>
                        </li>
                    <? endif; ?>
                    <? if ($booking["status"] == "confirmed"): ?>
                        <li>
                            Билеты:
                            <div>
                                <? foreach ($booking["arrSeatsId"] as $k => $arSeats): ?>
                                    <a class="ticket-pdf"
                                       href="/local/modules/travelsoft.eurotrans.booking/pages/download.ticket.php?booking_id=<?= $arSeats["booking_id"] ?>&seat=<?= $arSeats["name"] ?>&filename=<?= $arSeats["booking_id"] . "_" . $arSeats["name"] . "_" . $arSeats["last_name"] . "_" . $arSeats["first_name"] ?>.pdf">
                                        <?= $arSeats["last_name"] . " " . $arSeats["first_name"] ?>
                                    </a><br/>
                                <? endforeach; ?>
                            </div>
                        </li>
                    <? endif; ?>
                </ul>
            </div> -->
        <? endforeach; ?>
            </div>
        </div>

        <div class="ts-col-24 ts-col-md-6">
            <div class="ts-row">
                <div class="ts-col-24 ts-col-sm-12 ts-col-lg-24 ts-mt-3 ts-my-sm-0 ts-d-block">
                    <div class="dop__info ts-width-100 ts-border ts-p-2">
                        <h6 class="summ ts-pb-1"> Итого:
                            <span>  <?= $arResult["TOTAL_PRICE"] . ' ' . travelsoft\eurotrans\Settings::CURRENT_CURRENCY ?> </span>
                        </h6>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="clear: both"></div>
    <div id="total-price-block">
        <!--<div class="title h3 bold">
            Общая стоимость:
            <span class="total-price">

            </span>

        </div> -->
        <? if (!empty($arResult["STATUS"]["TEXT"])): ?>
            <!--div class="title h3 bold">Статус:
                <span class="total-price">
                    <?= $arResult["STATUS"]["TEXT"] ?>
                </span>
            </div-->

            <? if (!$arResult["STATUS"]["VALUE"]): ?>
                <div>

                    <? if ($arResult["PAYMENT_METHOD"] == "cash"): ?>

                        <div class="btn btn-gray mt-20 mb-10" style="cursor: default;">
                            <span>Оплата при посадке</span>
                        </div>

                        <div class="title h3" style="margin-top:40px">Как работает бронирование?</div>
                        <div class="content-column-content-inner mt-0">
                            <blockquote class="caption">
                                <ul class="mt-10">
                                    <li>Подтвердите бронирование<br>
                                        <span style="font-size:12px">С вами свяжется наш оператор за день до отправления до 15:00 по указанным вами контактным данным. </span>
                                    </li>
                                    <li>Пройти сверку паспорта при посадке<br>
                                        <span style="font-size:12px">После посадки, в автобусе будет произведен расчет и выписка проездных документов.</span>
                                    </li>
                                    <li><s>При не подтверждении, бронь будет аннулирована автоматически.</s></li>
                                </ul>
                            </blockquote>
                        </div>

                    <? elseif (($arResult["PAYMENT_METHOD"] == "creditcard") && ($arResult["SHOW_FORM_BOOK_ONLINE"] == true)): ?>

                        <FORM ACTION="https://pay169.paysec.by/pay/order.cfm" METHOD="POST"
                              style="display: inline-block;">
                            <INPUT TYPE="HIDDEN" NAME="Merchant_ID" VALUE="488693">
                            <INPUT TYPE="HIDDEN" NAME="OrderNumber" VALUE="<?= $arResult["MAIN_BOOKING_ID"] ?>">
                            <INPUT TYPE="HIDDEN" NAME="Language" VALUE="RU">
                            <INPUT TYPE="HIDDEN" NAME="OrderAmount" VALUE="<?= $arResult["TOTAL_PRICE"] ?>">
                            <INPUT TYPE="HIDDEN" NAME="OrderCurrency"
                                   VALUE="<?= travelsoft\eurotrans\Settings::CURRENT_CURRENCY ?>">

                            <INPUT TYPE="HIDDEN" NAME="Lastname" value="<?= $arResult["PAYER"]["FEMALE"] ?>">
                            <INPUT TYPE="HIDDEN" NAME="Firstname" value="<?= $arResult["PAYER"]["NAME"] ?>">
                            <INPUT TYPE="HIDDEN" NAME="Email" value="<?= $arResult["PAYER"]["EMAIL"] ?>">
                            <INPUT TYPE="HIDDEN" NAME="MobilePhone" value="<?= $arResult["PAYER"]["PHONE"] ?>">

                            <INPUT TYPE="HIDDEN" NAME="URL_RETURN" VALUE="https://eurotrans.by/thank-you/">

                            <INPUT TYPE="SUBMIT" class="btn btn-colored mt-20 mb-10" style="line-height: 40px;"
                                   NAME="Submit"
                                   VALUE="Перейти к оплате">
                        </FORM>

                    <? elseif ($arResult["PAYMENT_METHOD"] == "erip"): ?>

                        <? /*<a class="btn btn-colored mt-20 mb-10" style="line-height: 40px;"
                           href="https://stand.besmart.by:4443/pls/ipay/!iSOU.Login">
                            Оплатить через iPay
                        </a>*/ ?>
                        <div class="title h3" style="margin-top:40px">Как оплатить через ЕРИП</div>
                        <div class="content-column-content-inner">
                            <blockquote class="caption mt-20"><b>Система "Расчет"(ЕРИП)-&gt; Билеты, Лотереи -&gt;
                                    Транспортные билеты -&gt; Eurotrans.by</b>
                                <br> Для оплаты необходимо ввести номер вашей брони:
                                <b><?= $arResult["MAIN_BOOKING_ID"]; ?></b>
                            </blockquote>
                        </div>

                    <? endif; ?>
                </div>
            <? endif; ?>
        <? endif; ?>
    </div>
    <div>
        <? if ($arResult['IS_AGENT']): ?>
            <a class="aside-list-item text-1" href="/office-agent/">К списку заказов</a>
        <? else: ?>
            <a class="aside-list-item text-1" href="/">Новое бронирование</a>
        <? endif; ?>
    </div>
</div>

<? /*============= dataLayer part ============*/ ?>
<? if (isset($_GET['show_data_layer']) && ($_GET['show_data_layer'] == 'true')): ?>
    <script>
        function removeURLParameter(url, parameter) {
            //prefer to use l.search if you have a location/link object
            let urlparts= url.split('?');
            if (urlparts.length>=2) {

                let prefix= encodeURIComponent(parameter)+'=';
                let pars= urlparts[1].split(/[&;]/g);

                //reverse iteration as may be destructive
                for (let i= pars.length; i-- > 0;) {
                    //idiom for string.startsWith
                    if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                        pars.splice(i, 1);
                    }
                }

                url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
                return url;
            } else {
                return url;
            }
        }
        // for only one show this script
        let newUrl = removeURLParameter(window.location.href, 'show_data_layer');
        history.pushState(null, null, newUrl);

        let arrDataLayer = JSON.parse('<?= json_encode($arResult['DATA_LAYER']);?>');

        dataLayer.push(arrDataLayer);
    </script>
<? endif; ?>
<? /*============= end dataLayer part ============*/ ?>