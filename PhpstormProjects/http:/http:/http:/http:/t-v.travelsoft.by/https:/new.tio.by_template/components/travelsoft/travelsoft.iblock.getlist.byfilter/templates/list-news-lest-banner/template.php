<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
   /** @var array $arParams */
   /** @var array $arResult */
   /** @global CMain $APPLICATION */
   /** @global CUser $USER */
   /** @global CDatabase $DB */
   /** @var CBitrixComponentTemplate $this */
   /** @var string $templateName */
   /** @var string $templateFile */
   /** @var string $templateFolder */
   /** @var string $componentPath */
   /** @var CBitrixComponent $component */
   $this->setFrameMode(true);
   ?>
<?$i=0;?>
<ul class="side-list left relative">
   <?foreach($arResult as $arItem):?>
   <?
      $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
      $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
      ?>
	<?if (!empty($arItem["PROPERTY_PHOTO4_VALUE"])):
	$file = CFile::ResizeImageGet($arItem["PROPERTY_PHOTO4_VALUE"], array('width'=>80, 'height'=>80), BX_RESIZE_IMAGE_EXACT, true);
	$pre_photo=$file["src"];
    elseif (!empty($arItem["PREVIEW_PICTURE"])):
	$an_file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>80, 'height'=>80), BX_RESIZE_IMAGE_EXACT, true);
	//print_r ($an_file);
	$pre_photo=$an_file["src"];
	elseif (!empty($arItem["PROPERTY_OLD_PHOTO_VALUE"])):
	$pre_photo="/datas/photos/".$arItem["PROPERTY_OLD_PHOTO_VALUE"];
	else:
	$pre_photo=SITE_TEMPLATE_PATH."/images/nophoto.jpg";
	endif;
	?>
<?$i++;?>
   <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
      <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" rel="bookmark" title="<?=$arItem["NAME"]?>">
         <div class="side-list-out">
            <div class="side-list-img left relative">
               <img width="auto" height="80" src="<?=$pre_photo?>" class="attachment-mvp-small-thumb wp-post-image" alt="<?echo $arItem["NAME"]?>">
            </div>
            <!--side-list-img-->
            <div class="side-list-in">
               <div class="side-list-text left relative">
				 <?if (!empty($arItem["PROPERTY_THEME_VALUE"])):?>
						<span class="side-list-cat">
							<?foreach($arItem["PROPERTY_THEME_VALUE"] as $k => $v):
								$res = CIBlockElement::GetByID($v);
								if($ar_res = $res->GetNext())
								  $theme = $ar_res['NAME'];
								  echo $theme; 
								  if($k<(count($arItem["PROPERTY_THEME_VALUE"])-1)) echo ", ";
							endforeach;?>
						</span>
				  <?endif;?>
				   <span class="side-list-cat"><?if (!empty($arItem["ACTIVE_FROM"])): print_r($arItem["ACTIVE_FROM"]);
												else: print_r(substr($arItem["DATE_CREATE"],0,10));
												endif;?>
<div class="couter-views">
	<?if (!empty($arItem["SHOW_COUNTER"])):?><i class="fa fa-eye fa-2"></i><span><?=$arItem["SHOW_COUNTER"]?></span><?endif;?>
	<?if (!empty($arItem["PROPERTY_FORUM_MESSAGE_CNT_VALUE"])):?><i class="fa fa-comment fa-2"></i><span><?=$arItem["PROPERTY_FORUM_MESSAGE_CNT_VALUE"]?></span><?endif;?>
</div>

</span>
                  <p><?echo substr2($arItem["NAME"], 64)?></p>
               </div>
               <!--side-list-text-->
            </div>
            <!--side-list-in-->
         </div>
         <!--side-list-out-->
      </a>
   </li>
	<?if ($i==3):?>
</ul>
         <div class="center mt-20">
            <?$APPLICATION->IncludeComponent(
               "bitrix:advertising.banner",
               ".default",
               Array(
               	"CACHE_TIME" => "0",
               	"CACHE_TYPE" => "A",
               	"COMPONENT_TEMPLATE" => ".default",
               	"NOINDEX" => "Y",
               	"QUANTITY" => "1",
               	"TYPE" => "LEFT_1"
               )
               );?>
         </div>
   <ul class="side-list left relative">
	<?endif;?>
   <?endforeach;?>
</ul>
