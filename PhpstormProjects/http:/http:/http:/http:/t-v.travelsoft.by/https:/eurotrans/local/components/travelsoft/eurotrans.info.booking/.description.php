<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
$arComponentDescription = array(
    'NAME' => "Информация о забранированном билете",
    'DESCRIPTION' => "Информация о забранированном билете eurotrans.by",
    'SORT' => 20,
    'CACHE_PATH' => 'Y',
    'PATH' => array('ID' => 'travelsoft')
);
