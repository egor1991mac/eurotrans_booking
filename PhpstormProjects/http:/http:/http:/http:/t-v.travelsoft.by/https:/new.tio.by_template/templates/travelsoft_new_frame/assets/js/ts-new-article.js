$(document).ready(function(){
	$(".owl-carousel").owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    items: 1,
    responsive:{
        0:{
            items:1,
            nav:true,
            loop:true
        },
        600:{
            items:1,
            nav:false,
            loop:true
        },
        1000:{
            items:1,
            nav:false,
            loop:true
        }
    }
});
});