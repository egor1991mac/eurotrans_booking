<?php

use travelsoft\eurotrans\Settings;

/**
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class TravelsoftEurotransBooking extends CBitrixComponent
{

    private function changeTime($timeWithSeconds)
    {

        $time = strtotime($timeWithSeconds);
        return date("H:i", $time);

    }

    private function deleteSecondsFromTime($response)
    {

        foreach ($response as &$trip) {
            if (!empty($trip['way']['departure_time'])) {
                $trip['way']['departure_time'] = $this->changeTime($trip['way']['departure_time']);
            }
            if (!empty($trip['way']['arrival_time'])) {
                $trip['way']['arrival_time'] = $this->changeTime($trip['way']['arrival_time']);
            }
        }

        return $response;
    }

    public function getPaymentMethods()
    {

        $arBusId = Array();
        if (!empty($this->arResult["details"]["trip"]["way"]["bus_id"])) {
            array_push($arBusId, $this->arResult["details"]["trip"]["way"]["bus_id"]);
        }
        if (!empty($this->arResult["details"]["return_trip"]["way"]["bus_id"])) {
            array_push($arBusId, $this->arResult["details"]["return_trip"]["way"]["bus_id"]);
        }

        $countPeople = $this->arResult["adults"] + $this->arResult["children"] + $this->arResult["social"];

        $result = \travelsoft\eurotrans\Utils::GetPaymentMethods($arBusId, $this->arResult["round_trip"], $this->arResult['promotional_code_applied'], $countPeople);
        $this->arResult["payment_methods"] = $result;
    }

    public function executeComponent()
    {
        \Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

        try {

            if (isset($_GET["id"])) {
                $timestamp = $_GET["id"];
            } else {
                $timestamp = key($_SESSION[Settings::sessionStoreId()]);
            }

            $this->arResult = $_SESSION[Settings::sessionStoreId()][$timestamp];
            $this->arResult['details'] = $this->deleteSecondsFromTime($this->arResult['details']);

            if (empty($this->arResult) || empty($this->arResult["details"]["trip"]["way"])) {
                $this->arResult["ERRORS"][] = "EMPTY_REQUEST";
            }

            if (empty($this->arResult["ERRORS"])) {
                $details_for_check_to = $this->_getPreparedDetailsDataForSeatsCheck(array(), $this->arResult["details"]["trip"]["way"]);

                $seats_ids_to = $this->_getPreparedSeatsIdsForCheck(array(), $this->arResult["details"]["trip"]["way"]["seats"]);

                if ($this->arResult["details"]["trip"]["transfer"]) {
                    $seats_ids_to = $this->_getPreparedSeatsIdsForCheck($seats_ids_to, $this->arResult["details"]["trip"]["transfer"]["seats"]);
                    $details_for_check_to = $this->_getPreparedDetailsDataForSeatsCheck($details_for_check_to, $this->arResult["details"]["trip"]["transfer"]);
                }
                if (!\travelsoft\eurotrans\Utils::checkSeatsAvailable($details_for_check_to, $seats_ids_to)) {

                    $this->arResult["ERRORS"]["BOOKING"] = "Это место уже забронировано, выберите другое место пожалуйста!";
                }

                if ($this->arResult["details"]["return_trip"]) {
                    $seats_ids_from = $this->_getPreparedSeatsIdsForCheck(array(), $this->arResult["details"]["return_trip"]["way"]["seats"]);
                    $details_for_check_from = $this->_getPreparedDetailsDataForSeatsCheck(array(), $this->arResult["details"]["return_trip"]["way"]);

                    if ($this->arResult["details"]["return_trip"]["transfer"]) {
                        $seats_ids_from = $this->_getPreparedSeatsIdsForCheck($seats_ids_from, $this->arResult["details"]["return_trip"]["transfer"]["seats"]);
                        $details_for_check_from = $this->_getPreparedDetailsDataForSeatsCheck($details_for_check_from, $this->arResult["details"]["return_trip"]["transfer"]);
                    }

                    if (!\travelsoft\eurotrans\Utils::checkSeatsAvailable($details_for_check_from, $seats_ids_from)) {

                        $this->arResult["ERRORS"]["BOOKING"] = "Это место уже забронировано, выберите другое место пожалуйста!";
                    }
                }

            }

            if (empty($this->arResult["ERRORS"]) && !empty($this->arResult['promotional_id']) && ($this->arResult['promotional_code_applied'] == 1) && ($this->arResult['count_promotional_applying'] > 0)) {

                $response_promotional = \travelsoft\eurotrans\Utils::checkPromocodeOnUsing($this->arResult['promotional_id'], $this->arResult['count_promotional_applying'], $this->arResult['details']['trip']['way']['bus_id'], $this->arResult['details']['trip']['way']['departure_date']);
                if ($response_promotional['error'] == true) {
                    $this->arResult["ERRORS"]["BOOKING"] = "Этот промокод уже использован!";
                }
            }

            if (empty($this->arResult["ERRORS"])) {

                // получение информации по основному рейсу
                $this->arResult["details"]["trip"]["way"]["info"] = travelsoft\eurotrans\Utils::getTripInfo($this->arResult["details"]["trip"]["way"]["bus_id"]);

                if ($this->arResult["details"]["trip"]["transfer"]) {
                    $this->arResult["details"]["trip"]["transfer"]["info"] = travelsoft\eurotrans\Utils::getTripInfo($this->arResult["details"]["trip"]["transfer"]["bus_id"]);
                }

                if ($this->arResult["details"]["return_trip"]) {
                    // получение информации по основному рейсу
                    $this->arResult["details"]["return_trip"]["way"]["info"] = travelsoft\eurotrans\Utils::getTripInfo($this->arResult["details"]["return_trip"]["way"]["bus_id"]);

                    if ($this->arResult["details"]["return_trip"]["transfer"]) {
                        $this->arResult["details"]["return_trip"]["transfer"]["info"] = travelsoft\eurotrans\Utils::getTripInfo($this->arResult["details"]["return_trip"]["transfer"]["bus_id"]);
                    }
                }

                $this->arResult['DEPARTURE_POINT_NAME'] = \travelsoft\eurotrans\Utils::getDeparturePointNameById($this->arResult['location_from']);
                $this->arResult['ARRIVAL_POINT_NAME'] = \travelsoft\eurotrans\Utils::getArrivalPointNameByDepAndArrivId($this->arResult['location_from'], $this->arResult['location_to']);

                $this->checkRequest($timestamp);
            }

            $this->getPaymentMethods();

            $citizenship = \travelsoft\eurotrans\Utils::getCitizenship($this->arResult["details"]["trip"]["way"]["info"]["arr"]["route_id"]);
            if ($this->arResult["details"]["return_trip"]) {
                $citizenship_return = \travelsoft\eurotrans\Utils::getCitizenship($this->arResult["details"]["return_trip"]["way"]["info"]["arr"]["route_id"]);

                $citizenship = array_uintersect($citizenship_return, $citizenship, function ($v1, $v2) {
                    if ($v1["id"] === $v2["id"]) {
                        return 0;
                    }
                    return -1;
                });
            }
            $this->arResult["CITIZENSHIP"] = $citizenship;
            $this->arResult['DOCUMENT_TYPE'] = \travelsoft\eurotrans\Utils::GetDocumentTypes();
            $this->arResult["IS_AGENT"] = !empty(\travelsoft\eurotrans\Utils::getApiAccessId()) ? true : false;

            $allowBook = false;
            if ($this->arResult["IS_AGENT"]) {
                $agentInfo = \travelsoft\eurotrans\Utils::getAgentInfo();

                $allowBook = $agentInfo['allow_book'] == null ? false : $agentInfo['allow_book'];
            }
            $this->arResult['AGENT_ALLOW_BOOK'] = $allowBook;

            // Data layer
            $this->arResult["DATA_LAYER"] = $this->generateDataLayer();

            $this->setDefaultValuesForRegisterUsers();

            $this->IncludeComponentTemplate();
        } catch (\Exception $ex) {

            (new travelsoft\eurotrans\Logger())
                ->write("Component: " . $this->__name . "; Error: " . $ex->getMessage());
            ShowError("Произошла ошибка в работе системы бронирования.");
        }
    }

    protected function setDefaultValuesForRegisterUsers()
    {
        global $USER;

        if ($USER->IsAuthorized()) {

            if (!CSite::InGroup([8])) {
                $rsUser = CUser::GetByID($USER->GetID());
                $arUser = $rsUser->Fetch();

                $_POST["eurotrans"]["payer"]["email"] = $arUser['EMAIL'];

                if (!$this->arResult["IS_AGENT"]) {
                    $_POST["eurotrans"]["payer"]["name"] = $arUser['NAME'];
                    $_POST["eurotrans"]["payer"]["last_name"] = $arUser['LAST_NAME'];
                    $_POST["eurotrans"]["payer"]["phone"] = $arUser['PERSONAL_MOBILE'];
                }
            }
        }
    }

    public function checkRequest($timestamp)
    {
        if (strlen($_POST["eurotrans"]["booking"])) {
            if (!check_bitrix_sessid()) {
                $this->arResult["FORM_ERRORS"][] = "END_OF_SESSION";
            }

            if (empty($this->arResult["FORM_ERRORS"])) {

                // booking process
                // тут должна быть проверка по заполненым данным пассажиров и плательщика
                //

                //$pattern = '/^(\+375(?:29|44|33|25)\d{7})$/';
                $patternPhone = '/^(\+\d{11,12})$/';
                $patternBirthday = '/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/';

                foreach ($_POST["eurotrans"]["passangers"] as $key => $passenger) {
                    if (!empty($passenger["phone"])) {
                        if (preg_match($patternPhone, $passenger["phone"]) == 0) {
                            $this->arResult['ERRORS']['PHONE'][$key] = 'Вы ввели номер телефона в неверном формате';
                        }
                    }
                    if (!empty($passenger["phone_additional"])) {
                        if (preg_match($patternPhone, $passenger["phone_additional"]) == 0) {
                            $this->arResult['ERRORS']['PHONE_ADDITIONAL'][$key] = 'Вы ввели номер телефона в неверном формате';
                        }
                    }

                    if (empty($passenger["birthday"]) || preg_match($patternBirthday, $passenger["birthday"]) == 0) {
                        $this->arResult['ERRORS']['BIRTHDAY'][$key] = 'Вы ввели не верную дату рождения.';
                    }

                    if (empty($passenger["documentType"])) {
                        $this->arResult['ERRORS']['DOCUMENT_TYPE'][$key] = 'Вы не указали ваш документ.';
                    }
                    if (empty($passenger["document_number"])) {
                        $this->arResult['ERRORS']['DOCUMENT_NUMBER'][$key] = 'Вы не указали номер документа.';
                    }
                    if (empty($passenger["series"])) {
                        $this->arResult['ERRORS']['SERIES'][$key] = 'Вы не указали серию документа.';
                    }
                }
                if (empty($_POST["eurotrans"]["payer"]["name"])) {
                    $this->arResult['ERRORS']['PAYER']['NAME'] = 'Вы не ввели имя плательщика';
                }
                if (empty($_POST["eurotrans"]["payer"]["last_name"])) {
                    $this->arResult['ERRORS']['PAYER']['LAST_NAME'] = 'Вы не ввели фамилию плательщика';
                }
                if (empty($_POST["eurotrans"]["payer"]["email"])) {
                    $this->arResult['ERRORS']['PAYER']['EMAIL'] = 'Вы не ввели e-mail плательщика';
                }

                if (!empty($_POST["eurotrans"]["payer"]["phone"])) {
                    if (preg_match($patternPhone, $_POST["eurotrans"]["payer"]["phone"]) == 0) {
                        $this->arResult['ERRORS']['PAYER']['PHONE'] = 'Вы ввели номер телефона в неверном формате';
                    }
                } else {
                    $this->arResult['ERRORS']['PAYER']['PHONE'] = 'Вы не ввели номер телефона';
                }
                if (empty($_POST["eurotrans"]["payer"]["payment_method"])) {
                    $this->arResult['ERRORS']['PAYER']['PAYMENT_METHOD'] = 'Вы не выбрали способ оплаты';
                }

                if (empty($this->arResult['ERRORS'])) {
                    $result = \travelsoft\eurotrans\Utils::booking($_SESSION[Settings::sessionStoreId()][$timestamp], $_POST["eurotrans"]["passangers"], $_POST["eurotrans"]["payer"]);
                    if (!$result["error"]) {
                        $_SESSION[Settings::sessionStoreId()][$timestamp]["AR_BOOKING_ID"] = $result["booking_id_arr"];
                        //LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam("ok", array(), false));

                        $error_booking_back_param = '';
                        if (!empty($result["error_booking_back"])) {
                            $error_booking_back_param = '&err_back=true';
                        }
                        LocalRedirect("/thank-you/?id=" . $timestamp . $error_booking_back_param . '&show_data_layer=true');
                    } else {
                        $this->arResult["ERRORS"]["BOOKING"] = $result["text"];
                    }
                }
            }
        }
    }

    /**
     * @param array $details_for_check
     * @param array $way_details
     * @return array
     */
    protected function _getPreparedDetailsDataForSeatsCheck(array $details_for_check, array $way_details)
    {

        $details_for_check[$way_details["bus_id"]] = array(
            "bus_id" => $way_details["bus_id"],
            "pickup_id" => $way_details["location_from_id"],
            "return_id" => $way_details["location_to_id"],
            "booking_period" => array(
                $way_details["bus_id"] => array(
                    "departure_time" => date(travelsoft\eurotrans\Settings::EXT_DATE_FORMAT_WITHOUT_SECONDS, strtotime($way_details["departure_date"] . " " . $way_details["departure_time"])),
                    "arrival_time" => date(travelsoft\eurotrans\Settings::EXT_DATE_FORMAT_WITHOUT_SECONDS, strtotime($way_details["arrival_date"] . " " . $way_details["arrival_time"]))
                )
            )
        );
        return $details_for_check;
    }

    /**
     * @param array $seats_ids
     * @param array $seats
     * @return array
     */
    protected function _getPreparedSeatsIdsForCheck(array $seats_ids, array $seats = null)
    {

        foreach ($seats as $__seats) {
            $seats_ids = array_merge($seats_ids, array_keys($__seats));
        }

        return $seats_ids;
    }

    protected function generateDataLayer()
    {
        $ways = [
            $this->arResult['details']['trip']['way'],
            $this->arResult['details']['return_trip']['way'],
        ];
        $products = [];
        foreach ($ways as $way) {

            $first_key = key($way['info']['location_arr']);
            end($way['info']['location_arr']);
            $last_key = key($way['info']['location_arr']);

            foreach ($way['tickets_category_count'] as $ticket_key => $ticket) {
                if ($ticket['count'] > 0) {
                    $ticket_type = '';
                    switch ($ticket['category']) {
                        case 'adults':
                            $ticket_type = 'Взрослый';
                            break;
                        case 'children':
                            $ticket_type = 'Детский';
                            break;
                        case 'social':
                            $ticket_type = 'Льготный';
                            break;
                    }

                    $products[] = [
                        'name' => travelsoft\eurotrans\Utils::deleteBrackets($way['info']['location_arr'][$way['location_from_id']]['name']) . '-' . travelsoft\eurotrans\Utils::deleteBrackets($way['info']['location_arr'][$way['location_to_id']]['name']) . ' ' . $ticket_type,
                        'id' => $first_key . '_' . $last_key . '_' . $ticket_key,
                        'price' => "" . $ticket["amount_2"] . "",        //Цена билета в рос рублях
                        'category' => travelsoft\eurotrans\Utils::deleteBrackets($way['info']['location_arr'][$first_key]['name']) . '-' . travelsoft\eurotrans\Utils::deleteBrackets($way['info']['location_arr'][$last_key]['name']),    //Название главного маршрута
                        'brand' => $ticket_type,        //Используем для указания категории билета
                        'variant' => $way['departure_date'],        //Дата рейса
                        'quantity' => $ticket['count']
                    ];
                }
            }
        }
        $arrDataLayer['ip'] = $_SERVER['REMOTE_ADDR'];
        $arrDataLayer['event'] = 'checkout';
        $arrDataLayer['ecommerce']['currencyCode'] = 'RUB';
        $arrDataLayer['ecommerce']['checkout']['products'] = $products;

        return $arrDataLayer;
    }
}
