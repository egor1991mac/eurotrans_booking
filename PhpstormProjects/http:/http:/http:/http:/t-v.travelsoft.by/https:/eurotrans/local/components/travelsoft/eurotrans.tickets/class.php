<?php

use travelsoft\eurotrans\Settings;

/**
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class TravelsoftEurotransTickets extends CBitrixComponent
{

    public function executeComponent()
    {

        $not_offers_on = '';
        Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

        try {

            if (!strlen($this->arParams['BOOKING_PAGE'])) {
                throw new \Exception("Не указана страница для перехода на бронирование");
            }

            if (isset($_REQUEST['redirect'])) {
                if (isset($_REQUEST['time'])) {
                    $this->arParams['BOOKING_PAGE'] .= "?id=" . $_REQUEST['time'];
                }
                LocalRedirect($this->arParams['BOOKING_PAGE']);
            }

            if ($this->checkRequest()) {

                $this->arResult['DEPARTURE_POINT_NAME'] = \travelsoft\eurotrans\Utils::getDeparturePointNameById($this->arParams['REQUEST']['location_from']);
                $this->arResult['ARRIVAL_POINT_NAME'] = \travelsoft\eurotrans\Utils::getArrivalPointNameByDepAndArrivId($this->arParams['REQUEST']['location_from'], $this->arParams['REQUEST']['location_to']);

                $gateway = new travelsoft\eurotrans\Gateway;

                $parameters["location_from"] = $this->arParams['REQUEST']['location_from'];
                $parameters["location_to"] = $this->arParams['REQUEST']['location_to'];
                $parameters["date"] = date('Y-m-d', strtotime($this->arParams['REQUEST']['date_from']));
                $parameters["currency"] = Settings::CURRENT_CURRENCY;
                $parameters["is_user_operator"] = CSite::InGroup([8, 9]);

                $this->arResult['OFFERS'] = array();

                if ($this->arParams['REQUEST']['round_trip']) {

                    $parameters["return_date"] = date('Y-m-d', strtotime($this->arParams['REQUEST']['date_back']));
                    $extOffers = $gateway->getOffers($parameters);

                    if ($extOffers['travel'] && $extOffers['return_travel']) {

                        if ($extOffers['travel']["offers"]["before_transfer"] && $extOffers['travel']["offers"]["after_transfer"]) {

                            $trip = $this->getOffersWithTransfer($extOffers['travel']["offers"], "", false, $this->arParams['REQUEST']["location_from"]);
                        } else {

                            foreach ($extOffers['travel']['offers'] as $offer) {

                                $complexOffer["PRICE"] = $this->priceCalculating($offer['tickets']);
                                $complexOffer["PRICE_WITHOUT_DISCOUNT"] = $this->priceCalculating($offer['tickets'], 'without_discount');
                                $complexOffer["PRICE_2"] = $this->priceCalculating($offer['tickets'], 'price_2');
                                $complexOffer["PRICE_WITHOUT_DISCOUNT_2"] = $this->priceCalculating($offer['tickets'], 'without_discount_2');
                                $complexOffer["CURRENCY"] = $parameters["currency"];
                                $complexOffer['TRIP'] = $this->getOfferResult($offer, "", false, $complexOffer["PRICE"], $complexOffer["PRICE_WITHOUT_DISCOUNT"], $complexOffer["PRICE_2"], $complexOffer["PRICE_WITHOUT_DISCOUNT_2"], $complexOffer["CURRENCY"], $this->getTicketsCategory($offer['tickets']), $this->arParams["REQUEST"]["location_from"], $this->arParams["REQUEST"]["location_to"]);


                                $trip[] = $complexOffer;
                            }
                        }

                        if ($extOffers['return_travel']["offers"]["before_transfer"] && $extOffers['return_travel']["offers"]["after_transfer"]) {

                            $roundtrip = $this->getOffersWithTransfer($extOffers['return_travel']["offers"], "", true, $this->arParams['REQUEST']["location_to"]);
                        } else {

                            foreach ($extOffers['return_travel']['offers'] as $offer) {

                                $complexOffer["PRICE"] = $this->priceCalculating($offer['tickets']);
                                $complexOffer["PRICE_WITHOUT_DISCOUNT"] = $this->priceCalculating($offer['tickets'], 'without_discount');
                                $complexOffer["PRICE_2"] = $this->priceCalculating($offer['tickets'], 'price_2');
                                $complexOffer["PRICE_WITHOUT_DISCOUNT_2"] = $this->priceCalculating($offer['tickets'], 'without_discount_2');
                                $complexOffer["CURRENCY"] = $parameters["currency"];
                                $complexOffer['TRIP'] = $this->getOfferResult($offer, "", true, $complexOffer["PRICE"], $complexOffer["PRICE_WITHOUT_DISCOUNT"], $complexOffer["PRICE_2"], $complexOffer["PRICE_WITHOUT_DISCOUNT_2"], $complexOffer["CURRENCY"], $this->getTicketsCategory($offer['tickets']), $this->arParams["REQUEST"]["location_to"], $this->arParams["REQUEST"]["location_from"]);


                                $roundtrip[] = $complexOffer;
                            }
                        }

                        foreach ($trip as $complexOffer) {

                            foreach ($roundtrip as $rtcomplexOffer) {

                                $group = randString(7);

                                $complexOffer = $this->resetOfferData($complexOffer, $group);
                                $rtcomplexOffer = $this->resetOfferData($rtcomplexOffer, $group);

                                $this->arResult['OFFERS'][] = array(
                                    "TRIP" => $complexOffer["TRIP"],
                                    "ROUND_TRIP" => $rtcomplexOffer["TRIP"],
                                    "PRICE" => $complexOffer["PRICE"] + $rtcomplexOffer["PRICE"],
                                    "CURRENCY" => Settings::CURRENT_CURRENCY
                                );
                            }
                        }
                    } else if (!$extOffers['travel']) {
                        $not_offers_on = 'return_date_from';
                    } else if (!$extOffers['return_travel']) {
                        $not_offers_on = 'return_date_back';
                    }
                } else {

                    $extOffers = $gateway->getOffers($parameters);

                    if ($extOffers['travel']) {

                        if ($extOffers['travel']["offers"]["before_transfer"] && $extOffers['travel']["offers"]["after_transfer"]) {

                            $this->arResult['OFFERS'] = $this->getOffersWithTransfer($extOffers['travel']["offers"], randString(7), false, $this->arParams['REQUEST']["location_from"]);
                        } else {

                            foreach ($extOffers['travel']['offers'] as $offer) {

                                $complexOffer["PRICE"] = $this->priceCalculating($offer['tickets']);
                                $complexOffer["PRICE_WITHOUT_DISCOUNT"] = $this->priceCalculating($offer['tickets'], 'without_discount');
                                $complexOffer["PRICE_2"] = $this->priceCalculating($offer['tickets'], 'price_2');
                                $complexOffer["PRICE_WITHOUT_DISCOUNT_2"] = $this->priceCalculating($offer['tickets'], 'without_discount_2');
                                $complexOffer["CURRENCY"] = $parameters["currency"];
                                $complexOffer['TRIP'] = $this->getOfferResult($offer, randString(7), false, $complexOffer["PRICE"], $complexOffer["PRICE_WITHOUT_DISCOUNT"], $complexOffer["PRICE_2"], $complexOffer["PRICE_WITHOUT_DISCOUNT_2"], $complexOffer["CURRENCY"], $this->getTicketsCategory($offer['tickets']), $this->arParams["REQUEST"]["location_from"], $this->arParams["REQUEST"]["location_to"]);

                                $this->arResult['OFFERS'][] = $complexOffer;
                            }
                        }
                    } else if (!$extOffers['travel']) {
                        $not_offers_on = 'date_to';
                    }
                }
            }

            Bitrix\Main\Loader::includeModule('iblock');
            $this->arResult['ROUTE'] = CIBlockElement::GetList(false, array(
                "IBLOCK_ID" => Settings::routesStoreId(),
                "PROPERTY_DEP_CITY" => current(travelsoft\eurotrans\stores\Cities::get(array(
                    "filter" => array("XML_ID" => $this->arParams['REQUEST']['location_from']),
                    "select" => array("ID")
                )))["ID"],
                "PROPERTY_ARR_CITY" => current(travelsoft\eurotrans\stores\Cities::get(array(
                    "filter" => array("XML_ID" => $this->arParams['REQUEST']['location_to']),
                    "select" => array("ID")
                )))["ID"],
                "ACTIVE" => "Y"
            ), false, false, array("ID", "XML_ID", "DETAIL_PAGE_URL", "IBLOCK_ID", "PROPERTY_DEP_POINTS", "PROPERTY_DEP_CITY", "PROPERTY_ARR_CITY"))->GetNext();

            if (strlen($this->arResult['ROUTE']["DETAIL_PAGE_URL"])) {
                if ($this->arResult['ROUTE']["PROPERTY_DEP_CITY_VALUE"] > 0 &&
                    $this->arResult['ROUTE']["PROPERTY_ARR_CITY_VALUE"] > 0) {

                    $arCities = \travelsoft\eurotrans\stores\Cities::get(array(
                        "filter" => array(
                            "ID" => array(
                                $this->arResult['ROUTE']["PROPERTY_DEP_CITY_VALUE"],
                                $this->arResult['ROUTE']["PROPERTY_ARR_CITY_VALUE"]
                            )),
                        "select" => array("ID", "XML_ID")));

                    if (isset($arCities[$this->arResult['ROUTE']["PROPERTY_DEP_CITY_VALUE"]]["XML_ID"]) &&
                        isset($arCities[$this->arResult['ROUTE']["PROPERTY_ARR_CITY_VALUE"]]["XML_ID"])) {

                        $this->arResult['ROUTE']["DETAIL_PAGE_URL"] .= "?" . urlencode("eurotrans[location_from]") . "=" . $arCities[$this->arResult['ROUTE']["PROPERTY_DEP_CITY_VALUE"]]["XML_ID"]
                            . "&" . urlencode("eurotrans[location_to]") . "=" . $arCities[$this->arResult['ROUTE']["PROPERTY_ARR_CITY_VALUE"]]["XML_ID"];
                    }
                }
            }

            if (!empty($this->arResult['ROUTE']['PROPERTY_DEP_POINTS_VALUE'])) {

                $this->arResult['DEP_POINTS'] = \travelsoft\eurotrans\stores\DAPoints::get(array(
                    'filter' => array("UF_XML_ID" => $this->arResult['ROUTE']['PROPERTY_DEP_POINTS_VALUE']),
                    'select' => array('UF_NAME')
                ), true);
            }

            if (empty($this->arResult['OFFERS'])) {
                $this->arResult["ERRORS"][] = "TICKETS_NOT_FOUND";

                //продолжение в файле /event-registration/index.php
                $GLOBALS[travelsoft\eurotrans\Settings::globalStoreId()]["TICKETS_NOT_FOUND"] = true;
                switch ($not_offers_on) {
                    case 'return_date_from':
                        $date = $this->arParams['REQUEST']['date_from'];
                        $link_first = "/event-registration/" .
                            "?eurotrans[location_from]=" . $this->arParams['REQUEST']['location_to'] .
                            "&eurotrans[location_to]=" . $this->arParams['REQUEST']['location_from'] .
                            "&eurotrans[date_from]=" . $this->arParams['REQUEST']['date_back'] .
                            "&eurotrans[adults]=" . $this->arParams['REQUEST']['adults'] .
                            "&eurotrans[children]=" . $this->arParams['REQUEST']['children'] .
                            "&eurotrans[social]=" . $this->arParams['REQUEST']['social'];

                        $date_from = date('d.m.Y', strtotime('+1 day', strtotime($this->arParams['REQUEST']['date_from'])));
                        $date_back = ($date_from > $this->arParams['REQUEST']['date_back']) ? $date_from : $this->arParams['REQUEST']['date_back'];
                        $link_second = "/event-registration/" .
                            "?eurotrans[location_from]=" . $this->arParams['REQUEST']['location_from'] .
                            "&eurotrans[location_to]=" . $this->arParams['REQUEST']['location_to'] .
                            "&eurotrans[date_from]=" . $date_from .
                            "&eurotrans[date_back]=" . $date_back .
                            "&eurotrans[adults]=" . $this->arParams['REQUEST']['adults'] .
                            "&eurotrans[children]=" . $this->arParams['REQUEST']['children'] .
                            "&eurotrans[social]=" . $this->arParams['REQUEST']['social'] .
                            "&eurotrans[round_trip]=1";

                        $resultString = "На дату $date мест нет, вы можете забронировать билет <a href='$link_first'>в одну сторону</a> или выбрать другую дату.";
                        break;
                    case 'return_date_back':
                        $date = $this->arParams['REQUEST']['date_back'];
                        $link_first = "/event-registration/" .
                            "?eurotrans[location_from]=" . $this->arParams['REQUEST']['location_from'] .
                            "&eurotrans[location_to]=" . $this->arParams['REQUEST']['location_to'] .
                            "&eurotrans[date_from]=" . $this->arParams['REQUEST']['date_from'] .
                            "&eurotrans[adults]=" . $this->arParams['REQUEST']['adults'] .
                            "&eurotrans[children]=" . $this->arParams['REQUEST']['children'] .
                            "&eurotrans[social]=" . $this->arParams['REQUEST']['social'];

                        $link_second = "/event-registration/" .
                            "?eurotrans[location_from]=" . $this->arParams['REQUEST']['location_to'] .
                            "&eurotrans[location_to]=" . $this->arParams['REQUEST']['location_from'] .
                            "&eurotrans[date_from]=" . $this->arParams['REQUEST']['date_from'] .
                            "&eurotrans[date_back]=" . date('d.m.Y', strtotime('+1 day', strtotime($this->arParams['REQUEST']['date_back']))) .
                            "&eurotrans[adults]=" . $this->arParams['REQUEST']['adults'] .
                            "&eurotrans[children]=" . $this->arParams['REQUEST']['children'] .
                            "&eurotrans[social]=" . $this->arParams['REQUEST']['social'] .
                            "&eurotrans[round_trip]=1";

                        $resultString = "На дату $date мест нет, вы можете забронировать билет <a href='$link_first'>в одну сторону</a> или выбрать другую дату.";
                        break;
                    case 'date_to':
                        $date = $this->arParams['REQUEST']['date_from'];
                        $link_first = "/event-registration/" .
                            "?eurotrans[location_from]=" . $this->arParams['REQUEST']['location_from'] .
                            "&eurotrans[location_to]=" . $this->arParams['REQUEST']['location_to'] .
                            "&eurotrans[date_from]=" . date('d.m.Y', strtotime('+1 day', strtotime($this->arParams['REQUEST']['date_from']))) .
                            "&eurotrans[adults]=" . $this->arParams['REQUEST']['adults'] .
                            "&eurotrans[children]=" . $this->arParams['REQUEST']['children'] .
                            "&eurotrans[social]=" . $this->arParams['REQUEST']['social'];

                        $resultString = "На дату $date мест нет, вы можете забронировать билет <a href='$link_first'>на другую дату</a>.";

                        break;
                    default:
                        $resultString = '';
                        break;
                }

                $this->arResult['NO_DATE_STRING'] = $resultString;
            }

            // Data layer
            $this->arResult["DATA_LAYER"] = $this->generateDataLayer($not_offers_on);
            CJSCore::Init();
            $this->IncludeComponentTemplate();
        } catch (Exception $ex) {

            (new travelsoft\eurotrans\Logger())
                ->write("Component: " . $this->__name . "; Error: " . $ex->getMessage());
            ShowError("Произошла ошибка в работе компонента.");
        }
    }

    /**
     * @param array $offers
     * @param string $group
     * @param boolean $is_return
     * @param int $location_from_id
     * @return array
     */
    public function getOffersWithTransfer($offers, $group, $is_return = false, $location_from_id)
    {

        $btkim = $this->beforeTransferKeyIsMain($offers);

        if ($btkim) {

            // основной рейс
            $arr_main_trips = $offers["before_transfer"];
            // трансфер
            $arr_transfers_trips = $offers["after_transfer"];
        } else {

            // основной рейс
            $arr_main_trips = $offers["after_transfer"];
            // трансфер
            $arr_transfers_trips = $offers["before_transfer"];
        }

        $transfers_groups = $this->getTransfersGroups();

        $result = null;
        foreach ($arr_main_trips as $main_trips) {

            foreach ($main_trips as $main_trip) {

                if (isset($transfers_groups[$main_trip["bus_id"]])) {

                    foreach ($arr_transfers_trips as $transfer_trips) {

                        $transfer = null;

                        foreach ($transfer_trips as $transfer_trip) {

                            if (in_array($transfer_trip["bus_id"], $transfers_groups[$main_trip["bus_id"]])) {
                                if ($btkim) {

                                    if (strtotime($main_trip["arrival_time"]) <= strtotime($transfer_trip["departure_time"]) &&
                                        date('d.m.Y', strtotime($transfer_trip["departure_time"])) === date('d.m.Y', strtotime($main_trip["arrival_time"]))) {

                                        $transfer = $transfer_trip;
                                        // избегаем дублежей в переборе трансферов
                                        unset($transfers_groups[$main_trip["bus_id"]][array_search($transfer_trip["bus_id"], $transfers_groups[$main_trip["bus_id"]])]);
                                        break;
                                    }
                                } elseif (strtotime($main_trip["departure_time"]) >= strtotime($transfer_trip["arrival_time"]) &&
                                    date('d.m.Y', strtotime($transfer_trip["arrival_time"])) === date('d.m.Y', strtotime($main_trip["departure_time"]))) {
                                    $transfer = $transfer_trip;

                                    // избегаем дублежей в переборе трансферов
                                    unset($transfers_groups[$main_trip["bus_id"]][array_search($transfer_trip["bus_id"], $transfers_groups[$main_trip["bus_id"]])]);
                                    break;
                                }
                            }
                        }

                        if ($transfer) {

                            $main_location_to_id = array_pop($main_trip["locations"])["city_id"];
                            $transfer_location_to_id = array_pop($transfer["locations"])["city_id"];

                            $main_trip_price = $this->priceCalculating($main_trip['tickets']);
                            $transfer_trip_price = $this->priceCalculating($transfer['tickets']);
                            $main_tickets_category_count = $this->getTicketsCategory($main_trip['tickets']);

                            $transfer_tickets_category_count = $this->getTicketsCategory($transfer['tickets']);
                            $complexOffer["PRICE"] = $main_trip_price + $transfer_trip_price;
                            $complexOffer["CURRENCY"] = Settings::CURRENT_CURRENCY;

                            if ($btkim) {

                                $complexOffer["TRIP"]["EXT_BUS_MAP_PARAMETERS"] = array(
                                    array(
                                        "DEPARTURE_DATE" => date('d.m.Y', strtotime($main_trip['departure_time'])),
                                        "DEPARTURE_TIME" => date('H:i:s', strtotime($main_trip['departure_time'])),
                                        "ARRIVAL_DATE" => date('d.m.Y', strtotime($main_trip['arrival_time'])),
                                        "ARRIVAL_TIME" => date('H:i:s', strtotime($main_trip['arrival_time'])),
                                        "TICKETS_CATEGORY_COUNT" => $main_tickets_category_count,
                                        "IS_TRANSFER" => false,
                                        "IS_RETURN" => $is_return,
                                        "GROUP" => $group,
                                        "PRICE" => $main_trip_price,
                                        "CURRENCY" => $complexOffer["CURRENCY"],
                                        "BUS_ID" => $main_trip["bus_id"],
                                        "LOCATION_FROM_ID" => $main_trip["locations"][0]["city_id"],
                                        "LOCATION_TO_ID" => $main_location_to_id,
                                        "HTML_ID" => "bus_map__" . randString(7)
                                    ),
                                    array(
                                        "DEPARTURE_DATE" => date('d.m.Y', strtotime($transfer['departure_time'])),
                                        "DEPARTURE_TIME" => date('H:i:s', strtotime($transfer['departure_time'])),
                                        "ARRIVAL_DATE" => date('d.m.Y', strtotime($transfer['arrival_time'])),
                                        "ARRIVAL_TIME" => date('H:i:s', strtotime($transfer['arrival_time'])),
                                        "TICKETS_CATEGORY_COUNT" => $transfer_tickets_category_count,
                                        "IS_TRANSFER" => true,
                                        "IS_RETURN" => $is_return,
                                        "GROUP" => $group,
                                        "PRICE" => $transfer_trip_price,
                                        "CURRENCY" => $complexOffer["CURRENCY"],
                                        "BUS_ID" => $transfer["bus_id"],
                                        "LOCATION_FROM_ID" => $transfer["locations"][0]["city_id"],
                                        "LOCATION_TO_ID" => $location_from_id,
                                        "HTML_ID" => "bus_map__" . randString(7)
                                    )
                                );
                                $complexOffer["TRIP"]["DEPARTURE_POINT_NAME"] = $is_return ? $this->arResult['DEPARTURE_POINT_NAME'] : $this->arResult['ARRIVAL_POINT_NAME'];
                                $complexOffer["TRIP"]["ARRIVAL_POINT_NAME"] = $is_return ? $this->arResult['ARRIVAL_POINT_NAME'] : $this->arResult['DEPARTURE_POINT_NAME'];
                                $complexOffer["TRIP"]["WAY_TIME"] = travelsoft\eurotrans\Utils::wayTimeCalculation($main_trip['departure_time'], $transfer['arrival_time']);

                                $complexOffer["TRIP"]["DEPARTURE_TIME"] = $main_trip['departure_time'];
                                $complexOffer["TRIP"]["DEPARTURE_TIME_FORMATTED"] = date('d.m.Y, H:i', strtotime($main_trip['departure_time']));
                                $complexOffer["TRIP"]["ARRIVAL_TIME"] = $transfer['arrival_time'];
                                $complexOffer["TRIP"]["ARRIVAL_TIME_FORMATTED"] = date('d.m.Y, H:i', strtotime($transfer['arrival_time']));
                            } else {

                                $complexOffer["TRIP"]["EXT_BUS_MAP_PARAMETERS"] = array(
                                    array(
                                        "DEPARTURE_DATE" => date('d.m.Y', strtotime($transfer['departure_time'])),
                                        "DEPARTURE_TIME" => date('H:i:s', strtotime($transfer['departure_time'])),
                                        "ARRIVAL_DATE" => date('d.m.Y', strtotime($transfer['arrival_time'])),
                                        "ARRIVAL_TIME" => date('H:i:s', strtotime($transfer['arrival_time'])),
                                        "TICKETS_CATEGORY_COUNT" => $transfer_tickets_category_count,
                                        "IS_TRANSFER" => true,
                                        "IS_RETURN" => $is_return,
                                        "GROUP" => $group,
                                        "PRICE" => $transfer_trip_price,
                                        "CURRENCY" => $complexOffer["CURRENCY"],
                                        "BUS_ID" => $transfer["bus_id"],
                                        "LOCATION_FROM_ID" => $location_from_id,
                                        "LOCATION_TO_ID" => $transfer_location_to_id,
                                        "HTML_ID" => "bus_map__" . randString(7)
                                    ),
                                    array(
                                        "DEPARTURE_DATE" => date('d.m.Y', strtotime($main_trip['departure_time'])),
                                        "DEPARTURE_TIME" => date('H:i:s', strtotime($main_trip['departure_time'])),
                                        "ARRIVAL_DATE" => date('d.m.Y', strtotime($main_trip['arrival_time'])),
                                        "ARRIVAL_TIME" => date('H:i:s', strtotime($main_trip['arrival_time'])),
                                        "TICKETS_CATEGORY_COUNT" => $main_tickets_category_count,
                                        "IS_TRANSFER" => false,
                                        "IS_RETURN" => $is_return,
                                        "GROUP" => $group,
                                        "PRICE" => $main_trip_price,
                                        "CURRENCY" => $complexOffer["CURRENCY"],
                                        "BUS_ID" => $main_trip["bus_id"],
                                        "LOCATION_FROM_ID" => $main_trip["locations"][0]["city_id"],
                                        "LOCATION_TO_ID" => $main_location_to_id,
                                        "HTML_ID" => "bus_map__" . randString(7)
                                    )
                                );

                                $complexOffer["TRIP"]["DEPARTURE_POINT_NAME"] = $is_return ? $this->arResult['DEPARTURE_POINT_NAME'] : $this->arResult['ARRIVAL_POINT_NAME'];
                                $complexOffer["TRIP"]["ARRIVAL_POINT_NAME"] = $is_return ? $this->arResult['ARRIVAL_POINT_NAME'] : $this->arResult['DEPARTURE_POINT_NAME'];
                                $complexOffer["TRIP"]["WAY_TIME"] = travelsoft\eurotrans\Utils::wayTimeCalculation($transfer['departure_time'], $main_trip['arrival_time']);
                                $complexOffer["TRIP"]["DEPARTURE_TIME"] = $transfer['departure_time'];
                                $complexOffer["TRIP"]["DEPARTURE_TIME_FORMATTED"] = date('d.m.Y, H:i', strtotime($transfer['departure_time']));
                                $complexOffer["TRIP"]["ARRIVAL_TIME"] = $main_trip['arrival_time'];
                                $complexOffer["TRIP"]["ARRIVAL_TIME_FORMATTED"] = date('d.m.Y, H:i', strtotime($main_trip['arrival_time']));
                            }
                            $result[] = $complexOffer;
                            break;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getTransfersGroups()
    {

        $cache = new \travelsoft\eurotrans\adapters\Cache("travelsoft_eurotrans_transfers_trips");
        if (empty($transfers_groups = $cache->get())) {
            $transfers_groups = $cache->caching(function () {
                return (new \travelsoft\eurotrans\Gateway())->getAllTransfers();
            });
        }

        return $transfers_groups;
    }

    /**
     * @param array $offers
     * @return boolean
     */
    public function beforeTransferKeyIsMain($offers)
    {

        $transfers_groups = $this->getTransfersGroups();

        foreach ($offers["before_transfer"] as $before_transfers) {

            foreach ($before_transfers as $before_transfer) {

                if (isset($transfers_groups[$before_transfer["bus_id"]])) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param array $offer
     * @param string $group
     * @param boolean $is_return
     * @param string $price
     * @param $price_without_discount
     * @param $price_2
     * @param $price_without_discount_2
     * @param string $currency
     * @param array $tickets_category_count
     * @param int $location_from_id
     * @param $location_to_id
     * @return array
     */
    public function getOfferResult($offer, $group, $is_return = false, $price, $price_without_discount, $price_2, $price_without_discount_2, $currency, $tickets_category_count, $location_from_id, $location_to_id)
    {

        //$location_to_id = array_pop($offer["locations"])["city_id"];

        $result["BUS_ID"] = $offer["bus_id"];
        $result["FIRST_POINT_ROUTE_NAME"] = $offer["locations"][0]['content'];
        $result["LAST_POINT_ROUTE_NAME"] = $offer["locations"][count($offer["locations"]) - 1]['content'];

        $result["DEPARTURE_POINT_NAME"] = $is_return ? $this->arResult['ARRIVAL_POINT_NAME'] : $this->arResult['DEPARTURE_POINT_NAME'];
        $result["ARRIVAL_POINT_NAME"] = $is_return ? $this->arResult['DEPARTURE_POINT_NAME'] : $this->arResult['ARRIVAL_POINT_NAME'];
        $result["DEPARTURE_TIME"] = $offer['departure_time'];
        $result["DEPARTURE_TIME_FORMATTED"] = date('d.m.Y,', strtotime($offer['departure_time'])) . ' <span class="time">' . date('H:i', strtotime($offer['departure_time'])) . '</span>';
        $result["ARRIVAL_TIME"] = $offer['arrival_time'];
        $result["ARRIVAL_TIME_FORMATTED"] = date('d.m.Y,', strtotime($offer['arrival_time'])) . ' <span class="time">' . date('H:i', strtotime($offer['arrival_time'])) . '</span>';
        $result["WAY_TIME"] = travelsoft\eurotrans\Utils::wayTimeCalculation($offer['departure_time'], $offer['arrival_time']);

        $result["EXT_BUS_MAP_PARAMETERS"] = array(
            array(
                "DEPARTURE_DATE" => date('d.m.Y', strtotime($offer['departure_time'])),
                "DEPARTURE_TIME" => date('H:i:s', strtotime($offer['departure_time'])),
                "ARRIVAL_DATE" => date('d.m.Y', strtotime($offer['arrival_time'])),
                "ARRIVAL_TIME" => date('H:i:s', strtotime($offer['arrival_time'])),
                "TICKETS_CATEGORY_COUNT" => $tickets_category_count,
                "IS_TRANSFER" => false,
                "IS_RETURN" => $is_return,
                "GROUP" => $group,
                "BUS_ID" => $offer["bus_id"],
                "LOCATION_FROM_ID" => $location_from_id,
                "LOCATION_TO_ID" => $location_to_id,
                "HTML_ID" => "bus_map__" . randString(7),
                "PRICE" => $price,
                "PRICE_WITHOUT_DISCOUNT" => $price_without_discount,
                "PRICE_2" => $price_2,
                "PRICE_WITHOUT_DISCOUNT_2" => $price_without_discount_2,
                "CURRENCY" => $currency,
                'CLOSING' => !empty($offer['closing_time']) ? (time() > strtotime(date('Y-m-d', strtotime($offer['departure_time'])) . ' ' . $offer['closing_time'])) : false,
                'CLOSING_TEXT' => $offer['closing_text'],
            )
        );

        return $result;
    }

    /**
     * @return boolean
     */
    public function checkRequest()
    {

        if (!$this->arParams['REQUEST']['location_from']) {
            $this->arResult['ERRORS'][] = "LOCATION_FROM_IS_EMPTY";
        }

        if (!$this->arParams['REQUEST']['location_to']) {
            $this->arResult['ERRORS'][] = "LOCATION_TO_IS_EMPTY";
        }

        if (!$this->arParams['REQUEST']['date_from']) {
            $this->arResult['ERRORS'][] = "DATE_FROM_IS_EMPTY";
        }

        if ($this->arParams['REQUEST']['round_trip'] == 1 && !$this->arParams['REQUEST']['date_back']) {
            $this->arResult['ERRORS'][] = "DATE_BACK_IS_EMPTY";
        }

        if (
            $this->arParams['REQUEST']['adults'] <= 0 &&
            $this->arParams['REQUEST']['children'] <= 0 &&
            $this->arParams['REQUEST']['social'] <= 0
        ) {
            $this->arResult['ERRORS'][] = "PEOPLE_COUNT_IS_EMPTY";
        }

        return empty($this->arResult['ERRORS']);
    }

    /**
     * @param array $tickets
     * @param string $column
     * @return float
     */
    public function priceCalculating($tickets, $column = 'price')
    {

        $tc = Settings::tickesCategories();

        $price = 0.00;

        $vars = array(
            "adults", "children", "social"
        );

        foreach ($vars as $var) {

            foreach ($tickets as $key => $ticket) {

                if (in_array($ticket['ticket_type'], $tc[$var])) {
                    $price += $ticket[$column] * $this->arParams['REQUEST'][$var];
                    unset($tickets[$key]);
                    break;
                }
            }
        }

        return (float)$price;
    }

    /**
     * @param array $tickets
     * @return array
     */
    public function getTicketsCategory($tickets)
    {

        $tc = Settings::tickesCategories();

        $vars = array(
            "adults", "children", "social"
        );

        $cats = array();

        foreach ($vars as $var) {

            foreach ($tickets as $key => $ticket) {

                if (in_array($ticket['ticket_type'], $tc[$var])) {
                    $cats[$ticket["ticket_id"]]["count"] += $this->arParams['REQUEST'][$var];
                    $cats[$ticket["ticket_id"]]["amount"] = $ticket["price"];
                    $cats[$ticket["ticket_id"]]["price"] = $ticket["without_discount"];
                    $cats[$ticket["ticket_id"]]["amount_2"] = $ticket["price_2"];
                    $cats[$ticket["ticket_id"]]["price_2"] = $ticket["without_discount_2"];
                    $cats[$ticket["ticket_id"]]["category"] = $var;
                    $cats[$ticket["ticket_id"]]["discount_for"] = $ticket['discount_for'];
                    unset($tickets[$key]);
                    break;
                }
            }
        }
        return $cats;
    }

    /**
     * @param array $offer
     * @param string $group
     * @return array
     */
    public function resetOfferData($offer, $group)
    {

        if (count($offer["TRIP"]["EXT_BUS_MAP_PARAMETERS"]) === 2) {
            foreach ($offer["TRIP"]["EXT_BUS_MAP_PARAMETERS"] as &$com_arr) {
                $com_arr["GROUP"] = $group;
                $com_arr["HTML_ID"] = "bus_map__" . randString(7);
            }
        } else {
            $offer["TRIP"]["EXT_BUS_MAP_PARAMETERS"][0]["GROUP"] = $group;
            $offer["TRIP"]["EXT_BUS_MAP_PARAMETERS"][0]["HTML_ID"] = "bus_map__" . randString(7);
        }

        return $offer;
    }

    private function generateDataLayer($not_offers_on)
    {
        $products = [];
        if (count($this->arResult['OFFERS']) > 0) {
            $offer = current($this->arResult['OFFERS']);
            $trips = [
                $offer['TRIP'],
                $offer['ROUND_TRIP']
            ];
            foreach ($trips as $trip) {
                foreach ($trip['EXT_BUS_MAP_PARAMETERS'] as $ext_bus_map_parameter) {
                    foreach ($ext_bus_map_parameter['TICKETS_CATEGORY_COUNT'] as $ticket_key => $ticket) {
                        if ($ticket['count'] > 0) {
                            $ticket_type = '';
                            switch ($ticket['category']) {
                                case 'adults':
                                    $ticket_type = 'Взрослый';
                                    break;
                                case 'children':
                                    $ticket_type = 'Детский';
                                    break;
                                case 'social':
                                    $ticket_type = 'Льготный';
                                    break;
                            }
                            // for empty template
                            if (CSite::InDir("/routes/")) {

                                $products[] = [
                                    'name' => travelsoft\eurotrans\Utils::deleteBrackets($trip['DEPARTURE_POINT_NAME']) . '-' . travelsoft\eurotrans\Utils::deleteBrackets($trip['ARRIVAL_POINT_NAME']) . ' ' . $ticket_type,
                                    'id' => $_GET['eurotrans']['location_from'] . '_' . $_GET['eurotrans']['location_to'] . '_' . $ticket_key,
                                    'price' => $ticket['amount_2'] . '',        //Цена билета в рос рублях
                                    'category' => travelsoft\eurotrans\Utils::deleteBrackets($trip['FIRST_POINT_ROUTE_NAME']) . '-' . travelsoft\eurotrans\Utils::deleteBrackets($trip['LAST_POINT_ROUTE_NAME']),    //Название главного маршрута
                                    'brand' => $ticket_type,        //Используем для указания категории билета
                                    'list' => 'Маршрут ' . travelsoft\eurotrans\Utils::deleteBrackets($trip['DEPARTURE_POINT_NAME']) . '-' . travelsoft\eurotrans\Utils::deleteBrackets($trip['ARRIVAL_POINT_NAME'])
                                ];
                            } else {

                                $products[] = [
                                    'name' => travelsoft\eurotrans\Utils::deleteBrackets($trip['DEPARTURE_POINT_NAME']) . '-' . travelsoft\eurotrans\Utils::deleteBrackets($trip['ARRIVAL_POINT_NAME']) . ' ' . $ticket_type,
                                    'id' => $ext_bus_map_parameter['LOCATION_FROM_ID'] . '_' . $ext_bus_map_parameter['LOCATION_TO_ID'] . '_' . $ticket_key,
                                    'price' => $ticket['amount_2'] . '',        //Цена билета в рос рублях
                                    'category' => travelsoft\eurotrans\Utils::deleteBrackets($trip['FIRST_POINT_ROUTE_NAME']) . '-' . travelsoft\eurotrans\Utils::deleteBrackets($trip['LAST_POINT_ROUTE_NAME']),    //Название главного маршрута
                                    'brand' => $ticket_type,        //Используем для указания категории билета
                                    'variant' => $ext_bus_map_parameter['DEPARTURE_DATE']        //Дата рейса
                                ];
                            }
                        }
                    }
                }
            }
            $arrDataLayer['ip'] = $_SERVER['REMOTE_ADDR'];
            $arrDataLayer['event'] = 'ecomm';
            $arrDataLayer['ecommerce']['currencyCode'] = 'RUB';
            // for empty template
            if (CSite::InDir("/routes/")) {
                $arrDataLayer['ecommerce']['impressions'] = $products;
            } else {
                $arrDataLayer['ecommerce']['detail']['products'] = $products;
            }
        } else {

            switch ($not_offers_on) {
                case 'date_to':
                case 'return_date_from':
                    $eventLabel = 'Мест по маршруту ' . $this->arResult['DEPARTURE_POINT_NAME'] . '-' . $this->arResult['ARRIVAL_POINT_NAME'] . ' на ' . $_GET['eurotrans']['date_from'] . ' не найдено';
                    break;
                case 'return_date_back':
                    $eventLabel = 'Мест по маршруту ' . $this->arResult['ARRIVAL_POINT_NAME'] . '-' . $this->arResult['DEPARTURE_POINT_NAME'] . ' на ' . $_GET['eurotrans']['date_back'] . ' не найдено';
                    break;
                default:
                    $eventLabel = '';
                    break;
            }
            $arrDataLayer = [];
            if (!CSite::InDir("/routes/")) {
                $arrDataLayer = [
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'event' => 'GAEvent',
                    'eventCategory' => 'Поиск рейса',
                    'eventAction' => 'Мест нет',
                    'eventLabel' => $eventLabel,
                    'eventValue' => 0
                ];
            }
        }

        return $arrDataLayer;
    }
}
