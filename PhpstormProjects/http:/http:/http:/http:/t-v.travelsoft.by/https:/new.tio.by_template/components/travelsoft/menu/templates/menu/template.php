<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (!empty($arResult)):?>
<div class="side-widget">
	<?foreach ($arResult as $arItem) :?>
	<a class="<?if ($arItem["SELECTED"]):?>inf-but-selected<?else:?>inf-but<?endif?>" href="<?= $arItem['link']?>"><?= $arItem['name']?></a>
	<?endforeach?>
</div>
 <?endif?>
