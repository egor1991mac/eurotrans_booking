<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/** @var array $arCurrentValues */

$arComponentParameters['PARAMETERS']['DETAIL_BOOKING_PAGE'] = array(
    "PARENT" => "BASE",
    "NAME" => "Страница детальной информации по забронированным билетам",
    "TYPE" => "STRING",
    "DEFAULT" => "/booking-list/detail.php?booking_id=#booking_id#"
);
