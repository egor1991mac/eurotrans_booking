<?php

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PUBLIC_AJAX_MODE", true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

try {

    $result = array(
        $_POST['location_from'] => array($_POST['location_to'] => array()),
        $_POST['location_to'] => array($_POST['location_from'] => array())
    );
    
    if ($_POST['location_from'] > 0 && $_POST['location_to'] > 0) {

        $result[$_POST['location_from']][$_POST['location_to']] = travelsoft\eurotrans\Utils::getTripsDatesByLocations($_POST['location_from'], $_POST['location_to']);
        $result[$_POST['location_to']][$_POST['location_from']] =  travelsoft\eurotrans\Utils::getTripsDatesByLocations($_POST['location_to'], $_POST['location_from']);
    }

    echo json_encode($result);
} catch (Exception $ex) {

    echo Json::encode(array("error_message" => "Возникла ошибка при обработке запроса. "
        . "За дополнительной информацией обратитесь к администратору сайта"));
}