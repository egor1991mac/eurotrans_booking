<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$this->setFrameMode(true);
?>

<script>

    if (typeof window.eurotrans !== 'object') {
        window.eurotrans = {};
    }

    window.eurotrans.storage = {
        sessid: "<?= bitrix_sessid() ?>",
        dates: <?
if (!empty($arResult['DATES'])) {
    echo json_encode($arResult['DATES']);
} else {
    echo "{}";
}
?>,
        locationsTo: <?
if (!empty($arResult['ARRIVAL_POINTS'])) {
    echo json_encode($arResult['ARRIVAL_POINTS']);
} else {
    echo "{}";
}
?>,
        request: <?
if (!empty($arParams['REQUEST'])) {
    echo json_encode($arParams['REQUEST']);
} else {
    echo "{}";
}
?>,
        urls: {
            locationsTo: "<?= $this->__component->__path ?>/ajax/arrivalpoints.php",
            dates: "<?= $this->__component->__path ?>/ajax/dates.php"
        }
    };

</script>
<form id="main-search-tickets-from" class="page-head-form" action="<?= $arParams['SEARCH_TICKETS_PAGE'] ?>">
    <div class="input-item s100">
        <div class="input-wrapper">
            <div class="select-item">
                <label class="select-label js-condition">
                    <span class="select-main">
                        <input id="round-trip" class="select-real" type="checkbox" value="1" name="eurotrans[round_trip]">
                        <span class="select-checked"></span>
                    </span>
                    <span class="select-text"><?= GetMessage('TRAVELSOFT_EUROTRANS_ROUND_TRIP_TITLE'); ?></span>
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="input-wrapper" style="width: 45%">
            <div class="label"><?= GetMessage('TRAVELSOFT_EUROTRANS_LOCATION_FROM_TITLE'); ?></div>

            <div id="location_from-box" class="select-check js-select-custom depends-on">
                <div class="selects" data-placeholder="<?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE') ?>">
                    <input type="text" autocomplete="off"
                           data-search="location_from"
                           class="search-input" placeholder="<?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE')?>"/>
                    <i class="arr-down">
                        <svg class="icon icon-drop">
                            <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                </div>
                <div class="dropdown-target">
                    <div class="dropdown-inner">
                        <div class="dropdown-content">
                            <div class="select-u-body select-list">
                                <? foreach ($arResult['DEPARTURE_POINTS'] as $location): ?>
                                    <label class="option">
                                        <input type="radio" name="eurotrans[location_from]" value="<?= $location['id'] ?>"
                                               data-name="<?=$location['name']?>">
                                        <span class="location-from-title"><?= $location['name'] ?></span>
                                    </label>
                                <? endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="input-wrapper" style="width: 10%">
            <div class="label">&nbsp;</div>
            <div class="select-check">
                <div class="swapper" id="swapper"></div>
            </div>
        </div>
        <div class="input-wrapper" style="width: 45%">
            <div class="label"><?= GetMessage('TRAVELSOFT_EUROTRANS_LOCATION_TO_TITLE') ?></div>
            <div class="select-check js-select-custom depends-on">
                <div class="selects" data-placeholder="<?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE') ?>">
                    <input type="text" autocomplete="off"
                           data-search="location_to"
                           class="search-input" placeholder="<?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE')?>">
                    <i class="arr-down">
                        <svg class="icon icon-drop">
                        <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                </div>
                <div class="dropdown-target">
                    <div class="dropdown-inner">
                        <div class="dropdown-content">
                            <div class="select-u-body select-list text-center" id="location_to-box">
                                <img src="<?= $templateFolder . '/images/preloader.gif' ?>">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group left-calendar-icon">
        <div class="input-wrapper disabled js-condition-target">
            <label><?= GetMessage('TRAVELSOFT_EUROTRANS_DATE_FROM_TITLE') ?></label>
            <input id="date-from" name="eurotrans[date_from]" class="input-main datepicker-mtfs" type="text" placeholder="-- <?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE') ?> --"  readonly>
            <svg class="icon icon-drop">
            <use xlink:href="#calendar" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
            </svg>
            <div class="arrow-calendar arrow-calendar-left"></div>
            <div class="arrow-calendar arrow-calendar-right"></div>
        </div>
        <div class="input-wrapper disabled js-condition-target">
            <label><?= GetMessage('TRAVELSOFT_EUROTRANS_DATE_TO_TITLE') ?></label>
            <input id="date-back" name="eurotrans[date_back]" class="input-main datepicker-mtfs" type="text" placeholder="-- <?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE') ?> --"  readonly >
            <svg class="icon icon-drop">
            <use xlink:href="#calendar" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
            </svg>
            <div class="arrow-calendar arrow-calendar-left"></div>
            <div class="arrow-calendar arrow-calendar-right"></div>
        </div>
    </div>
    <div class="form-group">
        <div class="input-wrapper">
            <div class="label"><?= GetMessage("TRAVELSOFT_EUROTRANS_PEOPLE_CNT_TITLE") ?>

                <div class="roadmap-item-point road-popup" style="padding-left: 0;">
                    <div class="roadmap-item-point-inner dropdown-trigger">
                        <div class="question"></div>
                        <div class="dropdown-target arrow-bottom">
                            <div class="dropdown-inner">
                                <div class="dropdown-content">
                                    <div class="roadmap-item-description">
                                        <div class="roadmap-item-text">

                                            <?= GetMessage("TRAVELSOFT_EUROTRANS_SOCIAL_CONTENT")?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="select-check js-select-custom depends-on">
                <button class="selects" data-placeholder="Выберите">
                    <span class="btn-text" id="people-cnt"></span>
                    <i class="arr-down">
                        <svg class="icon icon-drop">
                        <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                </button>
                <div class="dropdown-target">
                    <div class="dropdown-inner no-mb">
                        <div class="dropdown-content">
                            <div class="select-u-body select-list">
                                <div class="input-wrapper sub-input-wrapper">
                                    <span class="label"><?= GetMessage('TRAVELSOFT_EUROTRANS_ADULTS_TITLE') ?></span>
                                    <div class="people-choose">

                                        <input type="button" class="minus" data-link-selector="input[name='eurotrans[adults]']" value="-">

                                        <input class="people-input" name="eurotrans[adults]" value="<?= intVal($arParams["REQUEST"]['adults']) ?>" type="text" autocomplete="off">

                                        <input type="button" class="plus" data-link-selector="input[name='eurotrans[adults]']" value="+">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-inner no-mb">
                        <div class="dropdown-content">
                            <div class="select-u-body select-list">
                                <div class="input-wrapper sub-input-wrapper">
                                    <span class="label"><?= GetMessage('TRAVELSOFT_EUROTRANS_CHILDREN_TITLE') ?></span>
                                    <div class="people-choose">

                                        <input type="button" class="minus" data-link-selector="input[name='eurotrans[children]']" value="-">

                                        <input class="people-input" name="eurotrans[children]" value="<?= intVal($arParams["REQUEST"]['children']) ?>" type="text" autocomplete="off">

                                        <input type="button" class="plus" data-link-selector="input[name='eurotrans[children]']" value="+">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-inner no-mb">
                        <div class="dropdown-content">
                            <div class="select-u-body select-list">
                                <div class="input-wrapper sub-input-wrapper">
                                    <span class="lh-12px label">
                                        <?= GetMessage("TRAVELSOFT_EUROTRANS_SOCIAL_TITLE")?>
                                    </span>

                                    <div class="people-choose">

                                        <input type="button" class="minus" data-link-selector="input[name='eurotrans[social]']" value="-">

                                        <input class="people-input" name="eurotrans[social]" value="<?= intVal($arParams["REQUEST"]['social']) ?>" type="text" autocomplete="off">

                                        <input type='button' value="+" class="plus" data-link-selector="input[name='eurotrans[social]']">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="input-wrapper">
            <button class="btn btn-colored small" type="sumbit">
                <span><?= GetMessage("TRAVELSOFT_EUROTRANS_BTN_TITLE") ?></span>
            </button>
        </div>
    </div>
</form>