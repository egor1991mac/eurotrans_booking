<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (!EMPTY($arResult['ERRORS'])): ?>
    <div class="content-column-content-inner">
        <p style="color: red;">
            <? foreach ($arResult["ERRORS"] as $error) {
                echo $error . '<br/>';
            } ?>
        </p>
    </div>
<? endif ?>
<? if (!EMPTY($arResult['MESSAGE_OK'])): ?>
    <div class="content-column-content-inner">
        <p style="color: green;">
            <?= $arResult['MESSAGE_OK'] ?>
        </p>
    </div>
<? endif ?>
<form class="form-block-wrapper js-validate" method="POST" action="<?= POST_FORM_ACTION_URI ?>">
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="LOST_AND_FOUND" value="true"/>
    <? foreach ($arResult['FORM'] as $TITLE => $FORM): ?>
        <div class="form-block-section">
            <div class="title h3"><?= $FORM["NAME"] ?></div>
            <div class="input-form">
                <? foreach ($FORM["FIELDS"] as $NAME => $arItem): ?>

                    <? if ($arItem["type"] == "text"): ?>
                        <div class="input-item <?if($TITLE == "APPLICATION_FORM"):?>s33<?else:?>s50<?endif;?>">
                            <div class="input-wrapper <? if ($arItem["isRequired"] == true): ?>req<? endif; ?>">
                                <label><?= $arItem["formLabel"] ?></label>
                                <input class="input-main"
                                       type="text"
                                       placeholder="<?= $arItem["placeholder"] ?>"
                                       name="<?= $NAME ?>"
                                    <? if ($arItem["isRequired"] == true): ?>
                                       data-validation="length required"
                                       data-validation-length="min2"
                                       data-validation-error-msg="Не заполнено"
                                    <? endif; ?>>
                            </div>
                        </div>
                    <? elseif ($arItem["type"] == "tel"): ?>
                        <div class="input-item s33">
                            <div class="input-wrapper <? if ($arItem["isRequired"] == true): ?>req<? endif; ?>">
                                <label><?= $arItem["formLabel"] ?></label>
                                <input class="input-main"
                                       type="tel"
                                       placeholder="<?= $arItem["placeholder"] ?>"
                                       name="<?= $NAME ?>"
                                       data-validation="length required"
                                       data-validation-length="min2"
                                       data-validation-error-msg="Не заполнено">
                            </div>
                        </div>
                    <? elseif ($arItem["type"] == "email"): ?>
                        <div class="input-item s33">
                            <div class="input-wrapper <? if ($arItem["isRequired"] == true): ?>req<? endif; ?>">
                                <label><?= $arItem["formLabel"] ?></label>
                                <input class="input-main"
                                       type="email"
                                       placeholder="<?= $arItem["placeholder"] ?>"
                                       name="<?= $NAME ?>"
                                       data-validation="length required"
                                       data-validation-length="min2"
                                       data-validation-error-msg="Не заполнено">
                            </div>
                        </div>
                    <? elseif ($arItem["type"] == "datetime"): ?>
                        <div class="input-item s50">
                            <div class="input-wrapper <? if ($arItem["isRequired"]): ?>req<? endif; ?>">
                                <label><?= $arItem["formLabel"] ?></label>
                                <input class="input-main datepicker not-current-day" type="text"
                                       placeholder="-- Дата --"
                                       data-validation="required" name="<?= $NAME ?>"
                                       data-validation-error-msg="Не выбрано" readonly>
                                <svg class="icon icon-drop">
                                    <use xlink:href="#calendar"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="input-item s50">
                            <div class="input-wrapper double <? if ($arItem["isRequired"]): ?>req<? endif; ?>">
                                <div class="label"><?= $arItem["formLabelTime"] ?></div>
                                <div class="select-check js-select-custom">
                                    <button class="selects" data-placeholder="Часы">
                                        <span class="btn-text">Часы</span>
                                        <i class="arr-down">
                                            <svg class="icon icon-drop">
                                                <use xlink:href="#strelka"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </button>
                                    <div class="dropdown-target">
                                        <div class="dropdown-inner">
                                            <div class="dropdown-content">
                                                <div class="select-u-body select-list">
                                                    <? foreach (Array("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23") as $hour): ?>
                                                        <label class="option">
                                                            <input type="radio" name="<?= $NAME ?>-HOUR"
                                                                   value="<?= $hour ?>"
                                                                   data-validation="required">
                                                            <span>
                                                                <?= $hour ?> ч
                                                            </span>
                                                        </label>
                                                    <? endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="select-check js-select-custom">
                                    <button class="selects" data-placeholder="Минуты">
                                        <span class="btn-text">Минуты</span>
                                        <i class="arr-down">
                                            <svg class="icon icon-drop">
                                                <use xlink:href="#strelka"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </button>
                                    <div class="dropdown-target">
                                        <div class="dropdown-inner">
                                            <div class="dropdown-content">
                                                <div class="select-u-body select-list">
                                                    <? foreach (Array("00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55") as $minute): ?>
                                                        <label class="option">
                                                            <input type="radio" name="<?= $NAME ?>-MINUTE"
                                                                   value="<?= $minute ?>"
                                                                   data-validation="required">
                                                            <span>
                                                                <?= $minute ?> мин
                                                            </span>
                                                        </label>
                                                    <? endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <? elseif ($arItem["type"] == "textarea"): ?>
                        <div class="input-item s100">
                            <div class="input-wrapper">
                                <label for="text1"><?= $arItem["formLabel"] ?></label>
                                <textarea class="input-main" name="<?= $NAME ?>" rows="5" id="text1"> </textarea>
                            </div>
                        </div>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
        </div>
    <? endforeach; ?>
    <div class="form-block-footer">
        <div class="form-footer-notify">
            <div class="text-sm">Поля для обязательного заполнения</div>
        </div>
        <div class="input-form">
            <div class="input-item s33" style="float: right">
                <button name="SUBMIT" value="submit" class="btn btn-colored blue">
                    <span>Отправить заявку</span>
                </button>
            </div>
        </div>
    </div>
</form>
<style>
    .hideRound {
        display: none;
    }
</style>
<script>
    function showRoundTrip() {
        var hideRound = $('.forSearchHide');
        hideRound.toggleClass('hideRound');

        var selectFrom = $('#UF_CRM_1483795016').find("div.select-list");
        var inputFrom = selectFrom.find('input[name=UF_CRM_1483795016]');
        inputFrom.each(function () {
            if ($(this).prop('checked')) {
                var backFrom = $("#UF_CRM_1512117684");
                backFrom.find('input[value="' + $(this).val() + '"]').prop('checked', true);
                backFrom.find('div.select-list').trigger('reinit');
            }
        });

        var selectTo = $('#UF_CRM_1483795308').find("div.select-list");
        var inputTo = selectTo.find('input[name=UF_CRM_1483795308]');
        inputTo.each(function () {
            if ($(this).prop('checked')) {
                var toFrom = $("#UF_CRM_1512034491");
                toFrom.find('input[value="' + $(this).val() + '"]').prop('checked', true);
                toFrom.find('div.select-list').trigger('reinit');
            }
        });
    }
</script>