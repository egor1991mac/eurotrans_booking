<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>



        <?foreach ($arResult["GROUP"] as $section):?>
            <div class="ts-col-24">
            <div class="block_section ts-width-100">
                <h2 class="ui-title-inner ui-title-inner_mrg-btn_lg">
                    <span class="ui-title-inner__inner"><?=$section["NAME"]?></span>
                </h2>
                <div class="fhotel_main ts-row">


                        <?foreach($section["ITEMS"] as $arItem):?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>

                            <div class="ts-col-24 ts-col-md-8" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                <a class="ts-d-block ts-width-100" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                    <?if(!empty($arItem["FLAG"][0])):?>
                                        <img class="flag" src="<?=$arItem["FLAG"][0]?>">
                                    <?endif?>
                                    <strong><?=$arItem["NAME"]?></strong>
                                    <?if((!empty($arItem["PRICE"]) && !empty($arItem["CURRENCY"]))):?>
                                        <span class="price"><?=GetMessage('PRICE_FROM')?><strong><?=$arItem["PRICE"]?></strong><span class="pln"><?=$arItem["CURRENCY"]?></span></span>
                                    <?endif?>
                                </a>
                            </div>

                        <?endforeach;?>


                </div>
            </div>
            </div>
        <?endforeach;?>



<?endif?>