<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="ts-row">
 
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $img = getSrc($arItem["PROPERTIES"]["PHOTO"]["VALUE"], array('width' => 384, 'height' => 237));
        ?>

        <div class="ts-col-24 ts-col-sm-12 ts-col-lg-8 ts-mb-6">
            <section class="b-post ts-width-100 ts-d-xs-flex ts-flex-direction__column ts-m-0 ts-px-0" style="max-height: 300px;" >
                <div class="entry-media">
                    <a class="" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <img class="ts-width-100" src="<?= $img[0] ?>" alt="<?= $arItem["NAME"] ?>">
                    </a>
                </div>
                <div class="entry-main ts-height-100 ts-flex-grow__1">
                    <div class="entry-header ts-height-100 ts-d-flex ts-flex-direction__column ts-justify-content__space-around">
                        <h2 class="entry-title ts-m-0 ts-py-1"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                         <div class="entry-meta">
                            <? if (!empty($arItem["PROPERTIES"]["AUTHOR"]["VALUE"]) && isset($arItem["PROPERTIES"]["AUTHOR"]["DESC"]) && !empty($arItem["PROPERTIES"]["AUTHOR"]["DESC"])): ?>
                                <span class="entry-meta__item ">
                                            <a class="entry-meta__link text-primary"
                                               href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                                <strong class="ts-d-block"><?= $arItem["PROPERTIES"]["AUTHOR"]["DESC"] ?></strong></a>
                                        </span>
                            <? endif ?>
                            <? if (in_array(array(1, 7), $GLOBALS["USER"]->GetUserGroupArray())): ?>
                                <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                                            <a class="entry-meta__link text-primary"
                                               href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arResult["SHOW_COUNTER"] ?></a>
                                        </span>
                            <? endif ?>
                           <!-- <span class="entry-meta__item"><i class="icon fa fa-comment-o text-primary"></i>
                                <a class="entry-meta__link text-primary" href="blog-main.html">29</a>
                            </span> -->
                        </div>
                        <div class="entry-title ts-my-0">
                            <?if (!empty($arItem["PROPERTIES"]["TYPE"]["VALUE"])):?>
                                <div class="entry-meta__item ts-width-100">
                                     <?= strip_tags(implode(', ', (array)$arItem["PROPERTIES"]["TYPE"]['VALUE']))?>
							  </div>
                           <?endif;?>
                           <?if (!empty($arItem["PROPERTIES"]["DATE"]["VALUE"])):?>
							 <div class="entry-meta__item">
								 <strong><?= substr2(implode(', ', (array)$arItem["DISPLAY_PROPERTIES"]["DATE"]['DISPLAY_VALUE']), 24)?></strong>
							 </div>
                           <?endif;?>
                        </div>
                    </div>
                    <div class="entry-content">
                        <p>
                            <? if (!empty($arItem["PREVIEW_TEXT"])): ?>
                                <?= substr2($arItem["~PREVIEW_TEXT"], 120) ?>
                            <? elseif (!empty($arItem["DETAIL_TEXT"])): ?>
                                <?= substr2($arItem["~DETAIL_TEXT"], 120) ?>
                            <? endif ?>
                        </p>
                    </div>
                </div>
            </section>
        </div>

    <? endforeach; ?>
</div>

