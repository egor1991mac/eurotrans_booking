<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
    <section class="widget section-sidebar">
        <div class="widget-content">
            <ul class="widget-list list list-mark-4">

                <? foreach ($arResult as $arItem): ?>
                    <? if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>

                    <li class="widget-list__item">
                        <a class="widget-list__link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    </li>

                    <?/* if ($arItem["SELECTED"]): */?><!--
                        <li><a href="<?/*= $arItem["LINK"] */?>" class="selected"><?/*= $arItem["TEXT"] */?></a></li>
                    <?/* else: */?>
                        <li><a href="<?/*= $arItem["LINK"] */?>"><?/*= $arItem["TEXT"] */?></a></li>
                    --><?/* endif */?>

                <? endforeach ?>

            </ul>
        </div>
    </section>
<? endif ?>