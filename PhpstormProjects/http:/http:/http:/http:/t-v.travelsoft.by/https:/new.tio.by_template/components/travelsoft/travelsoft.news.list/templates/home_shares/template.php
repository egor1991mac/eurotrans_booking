<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
/*\Bitrix\Main\Loader::includeModule("travelsoft.currency");*/
?>

<?if(!empty($arResult["ITEMS"])):?>

    <?if(!empty($arParams["TEXT_TITLE"])):?>
                    <div class="ts-wrap ts-px-4">
                         <div class="ui-title-inner ts-mb-6"><span class="ui-title-inner__inner"><?=$arParams["TEXT_TITLE"]?></span></div>
                    </div>
    <?endif?>
    <div class="section-carousel-1 owl-carousel owl-theme owl-theme_control-right owl-theme_control-right_full-cont enable-owl-carousel fhotels" data-min480="1" data-min768="2" data-min992="3" data-min1200="5" data-min1920="5" data-pagination="false" data-navigation="true" data-auto-play="4000" data-stop-on-hover="true">

        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            $img = getSrc($arItem["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"], array('width' => 380, 'height' => 256)); ?>

            <?$discount_price = 0;?>
            <div class="testimonial-item fhotel_main slick-slide slick-active" data-slick-index="0" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a title="Hotel Delfin" class="featured_hotel medium" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <span class="fhotel_img_container">
                        <span class="hotel_flags"></span>
                        <img class="hotel_img ts-width-100" alt="<?=$arItem["NAME"]?>" src="<?=$img[0]?>" style="opacity: 1;">
                        <div class="entry-main">
                            <div class="entry-header">
                                <span class="entry-title hov-title"><span class="hov-title__inner"><?=$arItem["NAME"]?></span></span>
                            </div>
                        </div>
                    </span>
					<?/*
                    <div class="text-block-shares">
                        <span class="fhotel_region_header">
                            <?if(!empty($arItem["DISPLAY_PROPERTIES"]["COUNTRY"]["VALUE"])):?>
                                <span class="fhotel_region_name"><?=strip_tags($arItem["DISPLAY_PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"])?></span>
                            <?endif?>
                            <?if((!empty($arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]) && !empty($arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"]))):?>
                                <div class="ymaxPrice_featuredlist">
                                    <span class="price"><small><?=GetMessage('STOCK_PRICE_FOR')?></small>  <strong><?=$arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]?><span class="price-line_through"></span></strong><span class="pln"><?=strip_tags($arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["DISPLAY_VALUE"])?></span></span>
                                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["DISCOUNT"]["VALUE"])):?>
                                        <?$discount_price = $arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"] - ($arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]*$arItem["DISPLAY_PROPERTIES"]["DISCOUNT"]["VALUE"]/100);?>
                                        <span class="ymax_percent">-<?=$arItem["DISPLAY_PROPERTIES"]["DISCOUNT"]["VALUE"]?>%</span>
                                    <?elseif(!empty($arItem["DISPLAY_PROPERTIES"]["DISCOUNT_FIX"]["VALUE"])):?>
                                        <?$discount_price = $arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"] - $arItem["DISPLAY_PROPERTIES"]["DISCOUNT_FIX"]["VALUE"];?>
                                        <span class="ymax_percent">-<?=$arItem["DISPLAY_PROPERTIES"]["DISCOUNT_FIX"]["VALUE"]?></span>
                                    <?endif?>
                                </div>
                            <?endif?>
                            <?if(isset($discount_price) && $discount_price > 0):?>
                                <span class="fhotel_price">
                                    <span class="price"><strong><?=$discount_price?></strong><span class="pln"><?=strip_tags($arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["DISPLAY_VALUE"])?></span></span>
                                </span>
                            <?endif?>
                        </span>

                        <?if(!empty($arItem["DISPLAY_PROPERTIES"]["HOTEL"]["VALUE"])):?>
                            <span class="fhotel_name">
                                <span class="hotel_string">
                                    <span class="table-cell">
                                        <span class="hotel_name"><?=strip_tags($arItem["DISPLAY_PROPERTIES"]["HOTEL"]["DISPLAY_VALUE"])?></span>
                                        <span class="stars star40"></span>
                                    </span>
                                </span>
                            </span>
                        <?endif?>
                    </div>
*/?>
                </a>
            </div>

        <?endforeach;?>
    </div>

<?endif?>

