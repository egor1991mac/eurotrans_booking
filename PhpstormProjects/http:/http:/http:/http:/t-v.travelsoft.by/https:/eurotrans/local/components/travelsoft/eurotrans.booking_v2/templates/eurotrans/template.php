<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$this->setFrameMode(false);
$this->addExternalJs("{$templateFolder}/MapAdapter.js");

/**
 * @param array $seats
 * @param bool $without_seating
 * @return string
 */
function __getSeatsString(array $seats = null, $without_seating = false)
{
    if ($without_seating == 'true') {

        return "Без номера.";

    } else {

        $seats__ = \travelsoft\eurotrans\Utils::getSeatsArray($seats);

        return implode(", ", !empty($seats__) ? $seats__ : array("-"));

    }
}

// ссылки на предыдущие страницы
$this->SetViewTarget('route_link');
if (strlen($arResult["previous_links"]["routes"])) {
    echo $arResult["previous_links"]["routes"];
} else {
    echo "/routes/";
}
$this->EndViewTarget();

$this->SetViewTarget('tickets_link');
if (strlen($arResult["previous_links"]["tickets"])) {
    echo $arResult["previous_links"]["tickets"];
} else {
    echo "/event-registration/";
}
$this->EndViewTarget();
?>

<? $this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery.mask.js"); ?>

<div class="ts-theme" id="js_booking_form">
    <div class="ts-wrap ts-px-lg-5 ts-py-2 ts-pb-5">
        <? if (!empty($arResult["ERRORS"]["BOOKING"])): ?>
            <div class="booking-block-inner">
                <div id="tickets-block">
                    <div class="input-item s100">
                        <div class="title h3">
                            <?= $arResult["ERRORS"]["BOOKING"]; ?>
                        </div>
                    </div>
                </div>
            </div>
            <? return; ?>

        <? endif; ?>
        <!--<div class="ts-row ts-py-2">
            <div class="ts-col-24">
                <h5>
                    Информация о пассажирах:
                </h5>
            </div>
        </div> -->
        <div class="ts-row ts-justify-content__space-between ts-pt-6">
            <div class="ts-col-24 ts-col-lg-17 ts-order__10 ts-order-lg__0">
                <? $people = $arResult["adults"] + $arResult["children"] + $arResult["social"] ?>

                <!-- Форма Авторизации и регистарции -->

                <?
                global $USER;
                if (!$USER->IsAuthorized()):?>
                    <div class="btn-switcher ts-mt-5">
                        <a class="btn-switcher-elem active" href="#tab-id-1">
                            <span>Я здесь впервые</span>
                        </a>
                        <a class="btn-switcher-elem" href="#tab-id-2">
                            <span>Я уже совершал заказы</span>
                        </a>
                    </div>

                    <div class="btn-switcher-tabs">
                        <div id="tab-id-1" class="btn-switcher-tab active">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.register",
                                "booking",
                                Array(
                                    "AUTH" => "Y",
                                    "COMPOSITE_FRAME_MODE" => "A",
                                    "COMPOSITE_FRAME_TYPE" => "AUTO",
                                    "REQUIRED_FIELDS" => array("NAME", "LAST_NAME", "PERSONAL_MOBILE"),
                                    "SET_TITLE" => "Y",
                                    "SHOW_FIELDS" => array("EMAIL", "NAME", "LAST_NAME", "PERSONAL_MOBILE"),
                                    "SUCCESS_PAGE" => $_SERVER['REQUEST_URI'],
                                    "USER_PROPERTY" => [],
                                    "USER_PROPERTY_NAME" => "",
                                    "USE_BACKURL" => "N",
                                )
                            ); ?>
                        </div>
                        <div id="tab-id-2" class="btn-switcher-tab">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:system.auth.form",
                                "booking",
                                Array(
                                    "COMPOSITE_FRAME_MODE" => "A",    // Голосование шаблона компонента по умолчанию
                                    "COMPOSITE_FRAME_TYPE" => "AUTO",    // Содержимое компонента
                                    "FORGOT_PASSWORD_URL" => "/personal-account/",    // Страница забытого пароля
                                    "PROFILE_URL" => "/personal-account/",    // Страница профиля
                                    "REGISTER_URL" => "/personal-account/",    // Страница регистрации
                                    "SHOW_ERRORS" => "Y",    // Показывать ошибки
                                )
                            ); ?><br>
                        </div>
                    </div>
                <? else: ?>
                <!-- Форма бронирования, заполнение данных -->

                <div class="ts-col-24">

                    <form action="<?= POST_FORM_ACTION_URI ?>" id="booking-details" method="POST">
                        <?= bitrix_sessid_post() ?>
                            <div class="ts-row">
                                <!-- Навигация табов -->
                                <!-- Контент табов -->
                                    <? for ($i = 1; $i <= $people; $i++): ?>
                                <div class="ts-tabs ts-border-top-none ts-mb-2">
                                        <div class="ts-tabs__nav">
                                            <div class="ts-tabs__nav-item  ts-col-24 ts-col-sm-9 ts-flex-direction__row">
                                                <span id="passanger-title-<?= $i ?>" class="passanger-title bold ts-mr-1">
                                                    Пассажир №<?= $i ?>
                                                </span>
                                                <? if ($i <= $arResult["adults"]): ?>
                                                    <span class="passanger-title">(Взрослый)</span>
                                                <? elseif ($i > $arResult["adults"] && $i <= $arResult["adults"] + $arResult["children"]): ?>
                                                    <span class="passanger-title">(Детский)</span>
                                                <? elseif ($i > $arResult["adults"] + $arResult["children"] && $i <= $people): ?>
                                                    <span class="passanger-title">(Льготный)</span>
                                                <? endif ?>
                                            </div>

                                        </div>
                                        <div  class="white-line ts-col-24 ts-col-sm-9"></div>
                                    <div class="ts-tabs__content ts-border">
                                        <div class="ts-tabs__content-item active ts-py-2">
                                            <div class="ts-width ts-d-flex ts-flex-wrap__wrap mt-10">

                                                <div class="ts-col-24 ts-col-sm-8">

                                                    <? $rstr = randString(7) ?>
                                                    <div class="input-wrapper ts-width-100 req">

                                                        <label for="input-<?= $rstr ?>" class="f-s">
                                                            Фамилия
                                                        </label>

                                                        <div id="error-area-<?= $rstr ?>" class="error-area"
                                                             data-scrolltoid="input-<?= $rstr ?>"></div>
                                                        <input data-link-error-area="<?= $rstr ?>"
                                                               data-need-validation="yes"
                                                               data-validators="is_empty"
                                                               data-validation-f="Фамилия"
                                                               id="input-<?= $rstr ?>"
                                                               class="input-main"
                                                               name="eurotrans[passangers][<?= $i ?>][last_name]"
                                                               value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["last_name"]) ?>">
                                                    </div>
                                                </div>
                                                <div class="ts-col-24 ts-col-sm-8">

                                                    <? $rstr = randString(7) ?>
                                                    <div class="input-wrapper ts-width-100 req">

                                                        <label for="input-<?= $rstr ?>" class="f-s">
                                                            Имя
                                                        </label>

                                                        <div id="error-area-<?= $rstr ?>" class="error-area"
                                                             data-scrolltoid="input-<?= $rstr ?>"></div>

                                                        <input data-link-error-area="<?= $rstr ?>"
                                                               data-need-validation="yes"
                                                               data-validators="is_empty"
                                                               data-validation-f="Имя"
                                                               id="input-<?= $rstr ?>"
                                                               class="input-main"
                                                               name="eurotrans[passangers][<?= $i ?>][name]"
                                                               value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["name"]) ?>">
                                                    </div>

                                                </div>
                                                <div class="ts-col-24 ts-col-sm-8">

                                                    <? $rstr = randString(7) ?>
                                                    <div class="input-wrapper ts-width-100 req">

                                                        <label for="input-<?= $rstr ?>" class="f-s">
                                                            Отчество
                                                        </label>

                                                        <div id="error-area-<?= $rstr ?>" class="error-area"
                                                             data-scrolltoid="input-<?= $rstr ?>"></div>
                                                        <input data-link-error-area="<?= $rstr ?>"
                                                               data-need-validation="yes"
                                                               data-validators="is_empty"
                                                               data-validation-f="Отчество"
                                                               id="input-<?= $rstr ?>"
                                                               class="input-main"
                                                               name="eurotrans[passangers][<?= $i ?>][patronymic]"
                                                               value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["patronymic"]) ?>">
                                                    </div>
                                                </div>

                                                <div class="ts-col-24 ts-col-sm-8">

                                                    <? $rstr = randString(7) ?>
                                                    <div class="input-wrapper ts-width-100 req">

                                                        <label for="input-<?= $rstr ?>" class="f-s">
                                                            Документ
                                                        </label>

                                                        <div id="error-area-<?= $rstr ?>" class="error-area"
                                                             data-scrolltoid="input-<?= $rstr ?>"></div>
                                                        <div id="social-box" class="select-check js-select-custom">
                                                            <button class="selects" data-placeholder="">
                                                                <span class="btn-text"></span>
                                                                <i class="arr-down">
                                                                    <svg class="icon icon-drop">
                                                                        <use xlink:href="#strelka"
                                                                             xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                    </svg>
                                                                </i>
                                                            </button>
                                                            <div class="dropdown-target">
                                                                <div class="dropdown-inner">
                                                                    <div class="dropdown-content">
                                                                        <div data-need-validation="yes"
                                                                             data-link-error-area="<?= $rstr ?>"
                                                                             data-validators="custom_select_is_selected"
                                                                             data-validation-fn="eurotrans[passangers][<?= $i ?>][documentType]"
                                                                             class="select-u-body select-list text-center">
                                                                            <? foreach ($arResult["DOCUMENT_TYPE"] as $document_type): ?>
                                                                                <label class="option">
                                                                                    <input
                                                                                            data-series="<?= $document_type["show_series"] ?>"
                                                                                            data-group-number="<?= $i; ?>"
                                                                                        <? if (!empty($_POST["eurotrans"]["passangers"][$i]["documentType"])): ?>
                                                                                            <? if (htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["documentType"]) == $document_type["id"]): ?>
                                                                                                checked
                                                                                            <? endif; ?>
                                                                                        <? else: ?>
                                                                                            <? if ($document_type["id"] == 4): ?>
                                                                                                checked
                                                                                            <? endif; ?>
                                                                                        <? endif; ?>
                                                                                            type="radio"
                                                                                            name="eurotrans[passangers][<?= $i ?>][documentType]"
                                                                                            value="<?= $document_type["id"] ?>">
                                                                                    <span class="social-title"><?= $document_type["name"] ?></span>
                                                                                </label>
                                                                            <? endforeach; ?>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="ts-col-8 ts-col-sm-4">

                                                    <? $rstr = randString(7) ?>
                                                    <div class="input-wrapper ts-width-100 req">

                                                        <label for="input-<?= $rstr ?>" class="f-s">
                                                            Серия
                                                        </label>

                                                        <!--<div id="error-area-<?= $rstr ?>" class="error-area" data-scrolltoid="payer-title">
                                                            <? if (!empty($arResult['ERRORS']['SERIES'])): ?>
                                                                <?= $arResult['ERRORS']['SERIES'][$i]; ?>
                                                            <? endif; ?>
                                                        </div> -->
                                                        <input data-link-error-area="<?= $rstr ?>"
                                                               data-need-validation="yes"
                                                               data-validators="is_empty"
                                                               data-validation-f="Серия"
                                                               id="input-<?= $rstr ?>"
                                                               class="input-main"
                                                               name="eurotrans[passangers][<?= $i ?>][series]"
                                                               value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["series"]) ?>">
                                                    </div>
                                                </div>

                                                <div class="ts-col-16  ts-col-sm-12">

                                                    <? $rstr = randString(7) ?>
                                                    <div class="input-wrapper  ts-width-100 req">

                                                        <label for="input-<?= $rstr ?>" class="f-s">
                                                            Номер
                                                        </label>

                                                        <!--<div id="error-area-<?= $rstr ?>" class="error-area" data-scrolltoid="payer-title">
                                                            <? if (!empty($arResult['ERRORS']['DOCUMENT_NUMBER'])): ?>
                                                                <?= $arResult['ERRORS']['DOCUMENT_NUMBER'][$i]; ?>
                                                            <? endif; ?>
                                                        </div>-->
                                                        <input data-link-error-area="<?= $rstr ?>"
                                                               data-need-validation="yes"
                                                               data-validators="is_empty"
                                                               data-validation-f="Номер"
                                                               id="input-<?= $rstr ?>"
                                                               class="input-main"
                                                               name="eurotrans[passangers][<?= $i ?>][document_number]"
                                                               value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["document_number"]) ?>">
                                                    </div>
                                                </div>

                                                <div class="ts-col-24 ts-col-sm-12">

                                                    <? $rstr = randString(7) ?>
                                                    <div class="input-wrapper ts-width-100 req">

                                                        <label for="input-<?= $rstr ?>" class="f-s">
                                                            Дата рождения
                                                        </label>

                                                        <!--<div id="error-area-<?= $rstr ?>" class="error-area"
                                                             data-scrolltoid="input-<?= $rstr ?>">
                                                            <? if (!empty($arResult['ERRORS']['BIRTHDAY'])): ?>
                                                                <?= $arResult['ERRORS']['BIRTHDAY'][$i]; ?>
                                                            <? endif; ?>
                                                        </div> -->
                                                        <input data-link-error-area="<?= $rstr ?>"
                                                               data-need-validation="yes"
                                                               data-validators="is_empty"
                                                               data-validation-f="Дата рождения"
                                                               data-birthday="true"
                                                               id="input-<?= $rstr ?>"
                                                               class="input-main"
                                                               placeholder="01.01.1970"
                                                               name="eurotrans[passangers][<?= $i ?>][birthday]"
                                                               value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["birthday"]) ?>">
                                                    </div>
                                                </div>

                                                <? if (!empty($arResult["CITIZENSHIP"])): ?>
                                                    <div class="ts-col-24 ts-col-sm-12">
                                                        <? $rstr = randString(7) ?>
                                                        <div class="input-wrapper ts-width-100 req">

                                                            <label for="input-<?= $rstr ?>" class="f-s">
                                                                Гражданство
                                                            </label>

                                                            <div id="error-area-<?= $rstr ?>" class="error-area"
                                                                 data-scrolltoid="input-<?= $rstr ?>"></div>
                                                            <div id="social-box" class="select-check js-select-custom">
                                                                <button class="selects" data-placeholder="">
                                                                    <span class="btn-text"></span>
                                                                    <i class="arr-down">
                                                                        <svg class="icon icon-drop">
                                                                            <use xlink:href="#strelka"
                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                        </svg>
                                                                    </i>
                                                                </button>
                                                                <div class="dropdown-target">
                                                                    <div class="dropdown-inner">
                                                                        <div class="dropdown-content">
                                                                            <div data-need-validation="yes"
                                                                                 data-link-error-area="<?= $rstr ?>"
                                                                                 data-validators="custom_select_is_selected"
                                                                                 data-validation-fn="eurotrans[passangers][<?= $i ?>][citizensip]"
                                                                                 class="select-u-body select-list text-center">
                                                                                <? foreach ($arResult["CITIZENSHIP"] as $citizenship): ?>
                                                                                    <label class="option">
                                                                                        <input
                                                                                            <? if ($citizenship["id"] == 21): ?>checked<? endif; ?>
                                                                                            type="radio"
                                                                                            name="eurotrans[passangers][<?= $i ?>][citizensip]"
                                                                                            value="<?= $citizenship["id"]; ?>">
                                                                                        <span class="social-title"><?= $citizenship["country_title"]; ?></span>
                                                                                    </label>
                                                                                <? endforeach; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <? endif; ?>
                                                <div class="ts-col-24 ts-justify-content-md__flex-end">


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <? endfor ?>
                            </div>
                            <!-- Способ оплаты -->



                            <!-- Агент -->
                        <div class="ts-row ts-py-2">
                            <h6 class="color-blue">
                                Контактактные данные:
                            </h6>
                        </div>
                        <div class="ts-row ts-py-2 ts-border">
                        <div class="ts-col-24 ts-col-sm-8">

                            <div class="input-wrapper req ts-width-100">

                                <label for="input-payer-email" class="f-s">
                                    Email
                                </label>

                                <div id="error-area-payer-email" class="error-area" data-scrolltoid="payer-title">
                                    <? if (!empty($arResult['ERRORS']['DATA']['PHONE'])): ?>
                                        <?= $arResult['ERRORS']['DATA']['PHONE']; ?>
                                    <? endif; ?>
                                </div>
                                <input data-link-error-area="payer-email"
                                       data-need-validation="yes"
                                       data-validators="is_empty|is_email"
                                       data-validation-f="Email"
                                       id="input-payer-email"
                                       class="input-main"
                                       name="eurotrans[data][email]"
                                       placeholder="euro@gmail.com"
                                       value="<?= htmlspecialchars($_POST["eurotrans"]["data"]["email"]) ?>">
                                </div>
                            </div>
                            <div class="ts-col-24 ts-col-sm-8">

                                <div class="input-wrapper req  ts-width-100">

                                    <label for="input-payer-phone" class="f-s">
                                        Телефон
                                    </label>

                                    <div id="error-area-payer-phone" class="error-area" data-scrolltoid="payer-title">
                                        <? if (!empty($arResult['ERRORS']['DATA']['PHONE'])): ?>
                                            <?= $arResult['ERRORS']['DATA']['PHONE']; ?>
                                        <? endif; ?>
                                    </div>
                                    <input data-link-error-area="payer-phone"
                                           data-need-validation="yes"
                                           data-validators="is_empty|is_phone"
                                           data-validation-f="Телефон"
                                           id="input-payer-phone"
                                           class="input-main"
                                           name="eurotrans[data][phone]"
                                           placeholder="+375291234567"
                                           value="<?= htmlspecialchars($_POST["eurotrans"]["data"]["phone"]) ?>">
                                </div>
                            </div>
                            <div class="ts-col-24 ts-col-sm-8">

                                <div class="input-wrapper  ts-width-100">

                                    <label for="input-payer-phone" class="f-s">
                                        Второй Телефон
                                    </label>

                                    <div id="error-area-payer-phone" class="error-area" data-scrolltoid="payer-title">
                                        <? if (!empty($arResult['ERRORS']['DATA']['PHONE'])): ?>
                                            <?= $arResult['ERRORS']['DATA']['PHONE']; ?>
                                        <? endif; ?>
                                    </div>
                                    <input id="input-payer-phone-second"
                                           class="input-main"
                                           name="eurotrans[data][second-phone]"
                                           placeholder="+375291234567"
                                           >
                                </div>
                            </div>
                        </div>
                            <? if ($arResult['IS_AGENT']): ?>
                                <div class="ts-row ts-mt-4 ts-mb-2">
                                    <h5>
                                        Плательщик:
                                    </h5>
                                </div>
                            <? endif; ?>

                                <? if ($arResult['IS_AGENT']): ?>
                                <section id="agency" class="ts-row ts-border ts-py-1 ">
                                    <div class="ts-col-24 ts-col-sm-8">

                                        <? $rstr = randString(7) ?>
                                        <div class="input-wrapper ts-width-100 req">

                                            <label for="input-<?= $rstr ?>" class="f-s">
                                                Фамилия
                                            </label>

                                            <div id="error-area-<?= $rstr ?>" class="error-area" data-scrolltoid="payer-title">
                                                <? if (!empty($arResult['ERRORS']['DATA']['NAME'])): ?>
                                                    <?= $arResult['ERRORS']['DATA']['NAME']; ?>
                                                <? endif; ?>
                                            </div>
                                            <input data-link-error-area="<?= $rstr ?>"
                                                   data-need-validation="yes"
                                                   data-validators="is_empty"
                                                   data-validation-f="Фамилия"
                                                   id="input-<?= $rstr ?>"
                                                   class="input-main"
                                                   name="eurotrans[data][last_name]"
                                                   placeholder="Фамилия"
                                                   value="<?= htmlspecialchars($_POST["eurotrans"]["data"]["last_name"]) ?>">
                                        </div>
                                    </div>

                                    <div class="ts-col-24 ts-col-sm-8">

                                        <? $rstr = randString(7) ?>
                                        <div class="input-wrapper ts-width-100 req">

                                            <label for="input-<?= $rstr ?>" class="f-s">
                                                Имя
                                            </label>

                                            <div id="error-area-<?= $rstr ?>" class="error-area" data-scrolltoid="payer-title">
                                                <? if (!empty($arResult['ERRORS']['DATA']['NAME'])): ?>
                                                    <?= $arResult['ERRORS']['DATA']['NAME']; ?>
                                                <? endif; ?>
                                            </div>
                                            <input data-link-error-area="<?= $rstr ?>"
                                                   data-need-validation="yes"
                                                   data-validators="is_empty"
                                                   data-validation-f="Имя"
                                                   id="input-<?= $rstr ?>"
                                                   class="input-main"
                                                   name="eurotrans[data][name]"
                                                   placeholder="Имя"
                                                   value="<?= htmlspecialchars($_POST["eurotrans"]["data"]["name"]) ?>">
                                        </div>
                                    </div>

                                    <div class="ts-col-24 ts-col-sm-8">

                                        <div class="input-wrapper ts-width-100 req">

                                            <label for="input-payer-phone" class="f-s">
                                                Телефон
                                            </label>

                                            <div id="error-area-payer-phone" class="error-area" data-scrolltoid="payer-title">
                                                <? if (!empty($arResult['ERRORS']['DATA']['PHONE'])): ?>
                                                    <?= $arResult['ERRORS']['DATA']['PHONE']; ?>
                                                <? endif; ?>
                                            </div>
                                            <input data-link-error-area="payer-phone"
                                                   data-need-validation="yes"
                                                   data-validators="is_empty|is_phone"
                                                   data-validation-f="Телефон"
                                                   id="input-payer-phone"
                                                   class="input-main"
                                                   name="eurotrans[data][phone]"
                                                   placeholder="+375291234567"
                                                   value="<?= htmlspecialchars($_POST["eurotrans"]["data"]["phone"]) ?>">
                                        </div>
                                    </div>
                                <? else: ?>
                                    <? if (!empty($_POST["eurotrans"]["data"]["phone"])): ?>
                                        <input id="input-payer-phone"
                                               type="hidden"
                                               class="input-main"
                                               name="eurotrans[data][phone]"
                                               value="<?= htmlspecialchars($_POST["eurotrans"]["data"]["phone"]) ?>">
                                    <? else: ?>
                                        <div class="input-item s50">

                                            <div class="input-wrapper req">

                                                <label for="input-payer-phone" class="f-s">
                                                    Телефон
                                                </label>

                                                <div id="error-area-payer-phone" class="error-area" data-scrolltoid="payer-title">
                                                    <? if (!empty($arResult['ERRORS']['DATA']['PHONE'])): ?>
                                                        <?= $arResult['ERRORS']['DATA']['PHONE']; ?>
                                                    <? endif; ?>
                                                </div>
                                                <input data-link-error-area="payer-phone"
                                                       data-need-validation="yes"
                                                       data-validators="is_empty|is_phone"
                                                       data-validation-f="Телефон"
                                                       id="input-payer-phone"
                                                       class="input-main"
                                                       name="eurotrans[data][phone]"
                                                       placeholder="+375291234567"
                                                       value="<?= htmlspecialchars($_POST["eurotrans"]["data"]["phone"]) ?>">
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    </section>
                                <? endif; ?>
                            <div class="ts-row ts-py-2 ts-mt-2">
                                <h6 class="color-blue">
                                    Примечание:
                                </h6>
                            </div>
                                <!-- Примечание -->
                            <div class="ts-row">
                                <div class="ts-col-24 ts-border ts-py-2">
                                    <? $rstr = randString(7) ?>
                                    <div class="input-wrapper ts-width-100">

                                        <label for="input-<?= $rstr ?>" class="f-s">
                                            Введите сообщение
                                        </label>
                                        <textarea id="input-<?= $rstr ?>"
                                                  class="input-main"
                                                  name="eurotrans[data][comment]"><?= htmlspecialchars($_POST["eurotrans"]["data"]["comment"]) ?></textarea>
                                    </div>
                                </div>
                            </div>
                        <? if ($arResult['IS_AGENT']): ?>
                            <!-- Способ оплаты для агента -->
                            <? if ($arResult['AGENT_ALLOW_BOOK']): ?>

                                <div class="input-wrapper">
                                    <label class="f-s bold color-blue" >Выберите способ оплаты: </label>

                                    <label for="payment-method-cashless" class="f-s">
                                        Оплата онлайн:
                                    </label>

                                    <input name="eurotrans[data][payment_method]"
                                           value="cashless"
                                           id="payment-method-cashless"
                                           checked
                                           type="radio">

                                    <label for="payment-method-cash" class="f-s">
                                        Оплата клиентом в автобусе:
                                    </label>

                                    <input name="eurotrans[data][payment_method]"
                                           value="cash"
                                           id="payment-method-cash"
                                           type="radio">
                                </div>

                            <? else: ?>

                                <input type="hidden" name="eurotrans[data][payment_method]" value="cashless"/>

                            <? endif; ?>
                        <? else: ?>
                            <!-- Способ оплты для пользовотелей-->
                            <? if (!empty($arResult["payment_types"])): ?>
                                <section id="payment-user">
                                    <div class="ts-row ts-py-2 ts-mt-2">
                                        <div class="ts-width-100">
                                            <h6 class="color-blue">Выберите способ оплаты: </h6>
                                        </div>
                                    </div>
                                    <div class="ts-row">
                                        <? foreach ($arResult["payment_types"] as $k => $payment_type): ?>

                                            <div class="ts-col-24 ts-col-sm-12 ts-col-lg-10 ts-px-0 ts-pb-1 ts-pb-sm-0 ts-pr-sm-2">
                                                <? if($k == 0): ?>
                                                    <label for="payment-type-<?= $k ?>" class="ts-d-flex ts-border ts-width-100 check__payment ts-align-items__center ts-justify-content__center active">
                                                        <span><?= $payment_type["label"] ?> :</span>
                                                        <i class="ts-d-flex ts-align-items__center ts-ml-2">

                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 4H4c-1.11 0-1.99.89-1.99 2L2 18c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V6c0-1.11-.89-2-2-2zm0 14H4v-6h16v6zm0-10H4V6h16v2z"/></svg></i>


                                                        </i>
                                                    </label>
                                                <? else: ?>
                                                <label for="payment-type-<?= $k ?>" class="ts-d-flex ts-border ts-width-100 check__payment ts-align-items__center ts-justify-content__center">
                                                    <span><?= $payment_type["label"] ?> :</span>
                                                    <i class="ts-d-flex ts-align-items__center ts-ml-2">

                                                        <svg aria-hidden="true" data-prefix="fas" data-icon="hand-holding-usd" class="svg-inline--fa fa-hand-holding-usd fa-w-17" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 544 512"><path fill="currentColor" d="M257.6 144.3l50 14.3c3.6 1 6.1 4.4 6.1 8.1 0 4.6-3.8 8.4-8.4 8.4h-32.8c-3.6 0-7.1-.8-10.3-2.2-4.8-2.2-10.4-1.7-14.1 2l-17.5 17.5c-5.3 5.3-4.7 14.3 1.5 18.4 9.5 6.3 20.3 10.1 31.8 11.5V240c0 8.8 7.2 16 16 16h16c8.8 0 16-7.2 16-16v-17.6c30.3-3.6 53.3-31 49.3-63-2.9-23-20.7-41.3-42.9-47.7l-50-14.3c-3.6-1-6.1-4.4-6.1-8.1 0-4.6 3.8-8.4 8.4-8.4h32.8c3.6 0 7.1.8 10.3 2.2 4.8 2.2 10.4 1.7 14.1-2l17.5-17.5c5.3-5.3 4.7-14.3-1.5-18.4-9.5-6.3-20.3-10.1-31.8-11.5V16c0-8.8-7.2-16-16-16h-16c-8.8 0-16 7.2-16 16v17.6c-30.3 3.6-53.3 31-49.3 63 2.9 23 20.7 41.3 42.9 47.7zm276.3 183.8c-11.2-10.7-28.5-10-40.3 0L406.4 402c-10.7 9.1-24 14-37.8 14H256.9c-8.3 0-15.1-7.2-15.1-16s6.8-16 15.1-16h73.9c15.1 0 29-10.9 31.4-26.6 3.1-20-11.5-37.4-29.8-37.4H181.3c-25.5 0-50.2 9.3-69.9 26.3L67.5 384H15.1C6.8 384 0 391.2 0 400v96c0 8.8 6.8 16 15.1 16H352c13.7 0 27-4.9 37.8-14l142.8-121c14.4-12.1 15.5-35.3 1.3-48.9z"></path></svg>


                                                    </i>
                                                    <? endif; ?>
                                                </label>

                                                <input name="eurotrans[data][payment_type]"
                                                       value="<?= $payment_type["name"] ?>"
                                                       class="ts-d-none"
                                                       id="payment-type-<?= $k ?>"
                                                       <? if ($payment_type["selected"]): ?>checked<? endif; ?>
                                                       type="radio">

                                            </div>
                                        <? endforeach; ?>

                                    </div>
                                </section>
                            <? endif; ?>

                            <input type="hidden" name="eurotrans[data][payment_method]" value="cashless"/>
                        <? endif; ?>
                                <div class="ts-hr__dashed ts-mt-3"></div>
                                <div class="ts-row">
                                <div class="ts-col-24 ts-py-2">
                                    <span style="color: red">*</span> - поля для обязательного заполнения
                                </div>

                                <div class="ts-col-24">
                                    <div class="select-item">
                                        <label class="ts-d-xs-flex ts-align-items__center select-label js-condition ">
                                            <span class="select-main">
                                                <input required
                                                       class="select-real ts-width-100"
                                                       type="checkbox"
                                                       name="agree_with_rules">
                                                <span class="select-checked"></span>
                                            </span>
                                                        <span class="select-text" >
                                                Согласен с
                                                <a href="/customers/pravila-perevozki/" style="display: inline-block;"
                                                   target="_blank">правилами перевозки пассажиров</a>,
                                                <a href="/customers/baggage_rules/" style="display: inline-block;" target="_blank">правилами провоза багажа</a> и
                                                <a href="/customers/offer/" style="display: inline-block;" target="_blank">условиями оферты</a>.
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                </div>





                        <div class="ts-row ts-mt-3 ts-justify-content__flex-end">
                             <div id="eurotrans-booking-btn" style="width:100%">
                                                            <input type="submit"
                                                                   id="eurotrans-booking-btn-submit"
                                                                   name="eurotrans[booking]"
                                                                   value="Резервировать"
                                                                   class="btn btn-colored ts-p-2">

                                                         </div>
                        </div>
                    </form>
                </div>
                <? endif; ?>

            </div>
            <div class="ts-col-24 ts-col-lg-7 ts-order__0 ts-order-lg__10 ts-mb-3 ts-mb-lg-0">
                <div class="ts-row ts-d-md-block ts-width-100 ts-justify-content__center">
                    <div class="ts-col-24  ts-mt-3 ts-my-sm-0">
                        <!-- Чек -->
                        <div class="ts-row">
                            <?
                            $keys = [
                                "trip",
                                "return_trip"
                            ];
                            $total_price = 0;
                            $discount = 0;
                            ?>
                            <? foreach ($keys as $k): ?>
                                <? if (!empty($arResult["details"][$k]["way"])): ?>
                                    <? $way = $arResult["details"][$k]["way"] ?>
                                    <div class="ts-col-24 ts-col-sm-12 ts-col-lg-24 ts-mt-2 ts-mt-md-0 ts-mb-2">
                                        <div class="card ts-border ts-py-2 ts-width-100">
                                            <div class="ts-col-24 ts-justify-content__space-between ts-align-items__center">
                                                <? if ($k == 'trip'): ?>
                                                    <h6 class="tittle">
                                                        ТУДА:
                                                    </h6>
                                                    <div class="arrow-left__header"></div>
                                                <? else: ?>
                                                    <h6 class="tittle">
                                                        ОБРАТНО:
                                                    </h6>
                                                    <div class="arrow-right__header"></div>
                                                <? endif; ?>
                                            </div>
                                            <div class="ts-col-24 ts-pt-1">
                                                <div class="ts-hr__dashed"></div>
                                            </div>
                                            <div class="ts-d-flex ts-py-2">
                                                <div class="ts-d-flex ts-flex-direction__column ts-justify-content__space-between ts-mr-2 ts-pl-2">
                                                    <div class="ts-d-block from" style="position: relative">

                                                        <div class="time">
                                                            <?= date('H:i', strtotime($way["departure_time"])) ?>
                                                        </div>
                                                        <div class="date">
                                                            <nobr><?= FormatDateFromDB($arResult['details'][$k]['way']['departure_date'], 'DD M')?></nobr>
                                                        </div>
                                                    </div>
                                                    <div class="ts-d-block to" style="position: relative">

                                                        <div class="time">
                                                        <?= date('H:i', strtotime($way["arrival_time"])); ?>
                                                        </div>
                                                        <div class="date">
                                                            <nobr><?= FormatDateFromDB($arResult['details'][$k]['way']['arrival_date'], 'DD M')?></nobr>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class=" ts-v-row ts-px-0 ts-my-2  ts-d-flex ts-flex-direction__column ts-justify-content__space-between"  >
                                                    <div class="ts-bg-white">
                                                        <div class="arrow-left">
                                                        </div>
                                                    </div>
                                                    <div class="ts-bg-white">
                                                        <div class="arrow-right">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ts-col-16">
                                                    <div class="ts-d-flex ts-flex-direction__column ts-justify-content__center  text-way ts-width-100 ts-mb-1" style="transform: translateY(-4px);">

                                                        <? $coordinates = explode(',', $way["info"]["location_arr"][$way["location_from_id"]]["bitrix_city"]['props']['MAP']['VALUE'])?>

                                                        <a class="js-popup-map" data-coords='{"title": "<?= $way["info"]["location_arr"][$way["location_from_id"]]["bitrix_city"]['fields']['NAME'];?>", "lat": <?= $coordinates[0] ?? ''?>, "lng": <?= $coordinates[1] ?? ''?>}'>
                                                            <?= $way["info"]["location_arr"][$way["location_from_id"]]["bitrix_city"]['fields']['NAME'] ?>
                                                        </a>
                                                        <div class="ts-d-block small"><?= $way["info"]["location_arr"][$way["location_from_id"]]["platform"] ?></div>
                                                    </div>
                                                    <div class="ts-d-flex ts-flex-direction__column ts-justify-content__center text-way ts-width-100" style="transform: translateY(4px);">

                                                        <? $coordinates = explode(',', $way["info"]["location_arr"][$way["location_to_id"]]["bitrix_city"]['props']['MAP']['VALUE'])?>

                                                        <a class="js-popup-map" data-coords='{"title": "<?= $way["info"]["location_arr"][$way["location_to_id"]]["bitrix_city"]['fields']['NAME'];?>","lat": <?= $coordinates[0] ?? ''?>, "lng": <?= $coordinates[1] ?? ''?>}'>
                                                            <?= $way["info"]["location_arr"][$way["location_to_id"]]["bitrix_city"]['fields']['NAME'] ?>
                                                        </a>
                                                        <div class="ts-d-block small"><?= $way["info"]["location_arr"][$way["location_to_id"]]["platform"] ?></div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="ts-col-24">
                                            <div class="ts-hr__dashed"></div>
                                            </div>

                                            <div class="ts-col-24 ts-justify-content__space-between ts-py-1">
                                                <b>
                                                    Место(а): <?= __getSeatsString($way["seats"], $arResult["without_seating"]); ?>
                                                </b>
                                            </div>
                                            <div class="ts-col-24">
                                                <div class="ts-hr__dashed"></div>
                                            </div>
                                            <? $discount += $way['price_without_discount'] - $way['price']; ?>
                                            <div class="ts-col-24 ts-justify-content__space-between ts-py-1 ts-align-items__center <?if($discount == 0): ?>ts-d-none<?endif;?>">

                                                <div>
                                                    <b>Скидка: </b>
                                                </div>
                                                <h6 class="tittle">
                                                    <?= $discount; ?> <?= \travelsoft\eurotrans\Settings::CURRENT_CURRENCY?>
                                                </h6>

                                                <div class="ts-hr__dashed ts-pt-1"></div>
                                            </div>
                                            <div class="ts-col-24 ts-justify-content__space-between ts-pt-1">
                                                <h6>
                                                    Итого:
                                                </h6>
                                                <h6 class="tittle">
                                                    <?= $way["price"]; ?> <?= \travelsoft\eurotrans\Settings::CURRENT_CURRENCY?>
                                                    <? $total_price += $way["price"]; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                            <? endforeach; ?>

                        </div>
                    </div>
                    <div class="ts-col-24  ts-d-block">
                        <div class="dop__info ts-width-100 ts-border ts-p-2">
                            <h6 class="summ ts-pb-1 ts-d-flex ts-justify-content__space-between"> К оплате:
                                <span><?=$total_price;?> <?= \travelsoft\eurotrans\Settings::CURRENT_CURRENCY?></span>
                            </h6>
                            <div class="passangers-count ts-d-flex ts-justify-content__space-between">
                                <div>Пассажиры:</div>
                                <div>
                                    <?foreach ($arResult["details"]["trip"]["way"]['tickets_category_count'] as $tickets):?>
                                        <?if($tickets['count'] > 0):?>
                                            <?= $tickets['count']?> <?= getTicketType($tickets['category'], $tickets['count']);?><br/>
                                        <?endif;?>
                                    <?endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>

<script>
    generatePopup(document.documentElement.clientHeight, document.documentElement.clientWidth, "<?= $arResult["previous_links"]["tickets"]; ?>");
</script>

<?
global $USER;
if (($USER->IsAdmin() && ($USER->GetLogin() == "ts@ts.com")) || (isset($_GET['user']) && ($_GET['user'] == 'admin'))) {
    ?>
    <script>
        let form = $('#booking-details');
        form.find("input[name='eurotrans[passangers][1][series]']").val(111111);
        form.find("input[name='eurotrans[passangers][1][document_number]']").val(111111111111);
        form.find("input[name='eurotrans[passangers][1][name]']").val("test_111111111111");
        form.find("input[name='eurotrans[passangers][1][last_name]']").val("test_female_111111111111");
        form.find("input[name='eurotrans[passangers][1][patronymic]']").val("test_patronymic_111111111111");
        form.find("input[name='eurotrans[passangers][1][birthday]']").val("25.08.2000");

        form.find("input[name='eurotrans[passangers][2][series]']").val(222222);
        form.find("input[name='eurotrans[passangers][2][document_number]']").val(222222222222);
        form.find("input[name='eurotrans[passangers][2][name]']").val("test_222222222222");
        form.find("input[name='eurotrans[passangers][2][last_name]']").val("test_female_222222222222");
        form.find("input[name='eurotrans[passangers][2][patronymic]']").val("test_patronymic_222222222222");
        form.find("input[name='eurotrans[passangers][2][birthday]']").val("02.02.2002");

        form.find("input[name='eurotrans[passangers][3][series]']").val(333333);
        form.find("input[name='eurotrans[passangers][3][document_number]']").val(333333333333);
        form.find("input[name='eurotrans[passangers][3][name]']").val("test_333333333333");
        form.find("input[name='eurotrans[passangers][3][last_name]']").val("test_female_333333333333");
        form.find("input[name='eurotrans[passangers][3][patronymic]']").val("test_patronymic_333333333333");
        form.find("input[name='eurotrans[passangers][3][birthday]']").val("03.03.2003");

        form.find("input[name='eurotrans[passangers][4][series]']").val(444444);
        form.find("input[name='eurotrans[passangers][4][document_number]']").val(444444444444);
        form.find("input[name='eurotrans[passangers][4][name]']").val("test_444444444444");
        form.find("input[name='eurotrans[passangers][4][last_name]']").val("test_female_444444444444");
        form.find("input[name='eurotrans[passangers][4][patronymic]']").val("test_patronymic_444444444444");
        form.find("input[name='eurotrans[passangers][4][birthday]']").val("04.04.2004");

        form.find("input[name='agree_with_rules']").prop('checked', true);
    </script>
    <?
} ?>

<? /*============= dataLayer part ============*/ ?>
<script>
    let arrDataLayer = JSON.parse('<?= json_encode($arResult['DATA_LAYER']);?>');
    dataLayer.push(arrDataLayer);
</script>
<? /*============= end dataLayer part ============*/ ?>
<?
global $USER;
?>
<script>
    let userGroups = <?= json_encode($USER->GetUserGroupArray())?>;

</script>
