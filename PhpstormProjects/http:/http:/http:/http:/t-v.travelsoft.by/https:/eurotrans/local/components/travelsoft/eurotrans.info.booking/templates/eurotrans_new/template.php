
<div class="ts-theme ts-wrap">
    <? if (empty($arResult["BOOKINGS_ID"])) {
        echo "<div class=\"booking-block-head\">
        <div class=\"title h2\">
            <span>Отсутвует номер брони</span>
        </div>
    </div>";
        return;
    } ?>

    <? if (!empty($_GET["err_back"]) && ($_GET["err_back"] == "true")): ?>

        <div class="title h2">
            <span>Ошибка при бронировании рейса обратно.</span>
        </div>

    <? endif; ?>


        <h4 class="color-blue ts-mb-2">


                Номер Брони:  <?= $arResult["MAIN_BOOKING_ID"]; ?>
        </h4>
    <div class="ts-row">


    <div class="ts-col-24 ts-col-lg-17 ts-flex-direction__column">
        <!-- Таймер -->
        <div class="ts-width-100">
            <div class="timer ts-border ts-p-2">
                <h6 id="timer" class="time ts-pb-1">
                                    <span class="min">
                                        20
                                    </span>
                    мин
                    <span class="sec">
                                        00</span>
                    сек
                </h6>

                    После истечения указанного времени бронь на выбранный рейс автоматически снимается

            </div>
        </div>
        <!-- способы оплаты -->
        <div class="ts-width-100 ts-py-2 ts-mt-2">
            <h6 class="color-blue">
                Способы оплаты:
            </h6>
        </div>
            <a class="payment_card ts-width-100 ts-mb-2 ts-p-2">
                <span class="arrow-right-pay"></span>
                <h6>Оплата картой:</h6>
                <ul class="ts-d-flex ts-ml-2 pay-img">
                    <li><img alt="visa" class="" src="<?=SITE_TEMPLATE_PATH?>/img/pay/1.png" style="height: 20px;"></li>
                    <li><img alt="visa" class="ts-d-none ts-d-sm-block" src="<?=SITE_TEMPLATE_PATH?>/img/pay/2.png"></li>
                    <li><img alt="visa" class="ts-d-none ts-d-sm-block" src="<?=SITE_TEMPLATE_PATH?>/img/pay/3.png"></li>
                    <li><img alt="visa" class="ts-d-none ts-d-sm-block" src="<?=SITE_TEMPLATE_PATH?>/img/pay/4.png">
                    <li><img alt="visa" class="ts-d-none ts-d-sm-block" src="<?=SITE_TEMPLATE_PATH?>/img/pay/5.png"></li>

                </ul>
            </a>
            <a href="https://eurotrans.by/personal-account/" class="payment_card ts-flex-direction__column ts-width-100 ts-p-2 ts-mb-2">

                <div class="ts-d-flex ts-justify-content__flex-start ts-width-100" style="position: relative">
                    <span class="arrow-right-pay" style="top: calc(50% - 9px); right: 0"></span>
                    <h6 style="margin: auto 0;"> Оплата ЕРИП:</h6>
                    <ul class="ts-d-flex ts-ml-2 pay-img">
                        <li><img alt="visa" src="<?=SITE_TEMPLATE_PATH?>/img/pay/6.png" style="height: 35px;"></li>
                    </ul>
                </div>
                <div class="blockquote ts-p-2 ts-mt-1" style="background: #f0f4f7">
                    Система "Расчет"(ЕРИП)-> Билеты, Лотереи -> Транспортные билеты -> Eurotrans.by
                    Для оплаты необходимо ввести номер вашей брони: <?= $arResult["MAIN_BOOKING_ID"]; ?>
                </div>
            </a>

    </div>



    <!-- Карточки -->
    <div class="ts-col-24 ts-col-lg-7">
    <div id="tickets-block" class="ts-row">
        <? $i = 0 ?>
        <? foreach ($arResult["INFO_BOOKINGS"] as $booking): ?>
        <div class="ts-col-24 ts-col-sm-12 ts-col-lg-24 ts-mt-2 ts-mt-md-0 ts-mb-2">
            <div class="card ts-border ts-py-2 ts-width-100">
                <div class="ts-col-24 ts-justify-content__space-between ts-align-items__center">
                    <? if ($i == 0): ?>
                        <h6 class="tittle">
                            ТУДА:
                        </h6>
                        <div class="arrow-left__header"></div>
                    <? else: ?>
                        <h6 class="tittle">
                            ОБРАТНО:
                        </h6>
                        <div class="arrow-right__header"></div>
                    <? endif; ?>
                </div>

                <div class="ts-col-24 ts-pt-1">
                    <div class="ts-hr__dashed"></div>
                </div>
                <div class="ts-d-flex ts-py-2">
                    <!-- Время и даты -->
                    <div class="ts-d-flex ts-flex-direction__column ts-justify-content__space-between ts-mr-2 ts-pl-2">
                        <div class="ts-d-block from" style="position: relative">

                            <div class="time">
                                <?= $booking['booking_datetime']['time'] ?>
                            </div>
                            <div class="date">
                                <nobr><?= FormatDateFromDB($arResult['details'][$k]['way']['departure_date'], 'DD M')?></nobr>
                            </div>
                        </div>
                        <div class="ts-d-block to ts-mt-2" style="position: relative">

                            <div class="time">
                                <?= $booking['stop_datetime']['time'] ?>
                            </div>
                            <div class="date">
                                <nobr><?= FormatDateFromDB($booking['stop_datetime']['time'], 'DD M')?></nobr>
                            </div>
                        </div>
                    </div>
                    <!-- Графика -->
                    <div class=" ts-v-row ts-px-0 ts-my-2  ts-d-flex ts-flex-direction__column ts-justify-content__space-between"  >
                        <div class="ts-bg-white">
                            <div class="arrow-left">
                            </div>
                        </div>
                        <div class="ts-bg-white">
                            <div class="arrow-right">
                            </div>
                        </div>
                    </div>
                    <!-- Места  отправления и прибытия -->
                    <div class="ts-col-16">
                        <div class="ts-d-flex ts-flex-direction__column ts-justify-content__center  text-way ts-width-100 ts-mb-2" style="transform: translateY(-4px);">
                            <?= $booking['departure'] ?>
                          <!--  <? $coordinates = explode(',', $way["info"]["location_arr"][$way["location_from_id"]]["bitrix_city"]['props']['MAP']['VALUE'])?>

                            <a class="js-popup-map" data-coords='{"title": "<?= $way["info"]["location_arr"][$way["location_from_id"]]["bitrix_city"]['fields']['NAME'];?>", "lat": <?= $coordinates[0] ?? ''?>, "lng": <?= $coordinates[1] ?? ''?>}'>
                                <?= $way["info"]["location_arr"][$way["location_from_id"]]["bitrix_city"]['fields']['NAME'] ?>
                            </a>
                            <div class="ts-d-block small"><?= $way["info"]["location_arr"][$way["location_from_id"]]["platform"] ?></div> -->
                        </div>
                        <div class="ts-d-flex ts-flex-direction__column ts-justify-content__center text-way ts-width-100" style="transform: translateY(4px);">
                            <?= $booking['arrival'] ?>
                            <!--
                            <? $coordinates = explode(',', $way["info"]["location_arr"][$way["location_to_id"]]["bitrix_city"]['props']['MAP']['VALUE'])?>

                            <a class="js-popup-map" data-coords='{"title": "<?= $way["info"]["location_arr"][$way["location_to_id"]]["bitrix_city"]['fields']['NAME'];?>","lat": <?= $coordinates[0] ?? ''?>, "lng": <?= $coordinates[1] ?? ''?>}'>
                                <?= $way["info"]["location_arr"][$way["location_to_id"]]["bitrix_city"]['fields']['NAME'] ?>
                            </a>
                            <div class="ts-d-block small"><?= $way["info"]["location_arr"][$way["location_to_id"]]["platform"] ?></div> -->
                        </div>
                    </div>
                </div>
                <div class="ts-col-24">
                    <div class="ts-hr__dashed"></div>
                </div>
                <div class="ts-col-24 ts-justify-content__space-between ts-py-1">
                    <ul>
                        <li>
                            <b>
                                Место(а):
                            </b>
                            <?= $booking["implodeSeatsId"]; ?>
                        </li>
                       <!-- <li>
                            <b>
                            Номер брони:
                            </b>
                            <?= $booking["id"] ?>
                        </li>
                        <li>
                            <? if ($booking["status"] == "confirmed"): ?>
                                <b>Статус заказа:</b>
                                оплачен
                            <? else: ?>
                                <b>Статус заказа:</b>
                                зарезервирован
                            <? endif; ?>
                        </li> -->
                    </ul>
                </div>
                <div class="ts-col-24">
                    <div class="ts-hr__dashed"></div>
                </div>
                <? if ($booking["price"] != $booking['price_without_discount']): ?>
                <div class="ts-col-24 ts-justify-content__space-between ts-py-1 ts-align-items__center ">

                    <div>
                        <b>Скидка: </b>
                    </div>
                    <h6 class="tittle">
                        <?= ($booking['price_without_discount'] - $booking["price"]) . " " . $booking["currency"]; ?>
                    </h6>

                    <div class="ts-hr__dashed ts-pt-1"></div>
                </div>
                <? endif; ?>
                <div class="ts-col-24 ts-justify-content__space-between ts-pt-1">
                    <h6>
                        Итого:
                    </h6>
                    <h6 class="tittle">

                        <?= $booking["price"] . " " . $booking["currency"]; ?>
                    </h6>
                </div>
            </div>
        </div>
<!--
            <div class="input-item s50 feedback-item-reply" >
                <ul class="tickets-info">
                    <li>
                        <? if ($booking["status"] == "confirmed"): ?>
                            Статус заказа: оплачен.
                        <? else: ?>
                            Статус заказа: зарезервирован.
                        <? endif; ?>
                    </li>
                    <li>
                        Дата заказа:
                        <span class="bold date">
                            <?= $booking["booking_datetime"]["date"] ?>
                        </span>
                    </li>
                    <li>
                        Отправление из:
                        <span class="bold location-from">
                            <?= $booking['departure'] ?>
                            в <?= $booking['booking_datetime']['time'] ?>
                        </span>
                    </li>
                    <li>Прибытие в:
                        <span class="bold location-to">
                            <?= $booking['arrival'] ?>
                            в <?= $booking['stop_datetime']['time'] ?>
                        </span>
                    </li>
                    <li>
                        Рейс:
                        <span class="bold trip">
                            <?= $booking["TRIP_INFO"]["location_arr"][$booking["pickup_id"]]["name"] . " - " . $booking["TRIP_INFO"]["location_arr"][$booking["return_id"]]["name"]; ?>
                        </span>
                    </li>
                    <li>
                        Место:
                        <span class="seats bold">
                            <?= $booking["implodeSeatsId"]; ?>
                        </span>
                    </li>
                    <li>
                        Стоимость:
                        <span class="price bold">
                            <?= $booking["price_without_discount"] . " " . $booking["currency"] ?>
                        </span>
                    </li>
                    <? if ($booking["price"] != $booking['price_without_discount']): ?>
                        <li>
                            Скидка
                            <? if (!empty($booking['promotional_id'])): ?>
                                (за промокод)
                            <? else: ?>
                                (за проезд туда-обратно)
                            <? endif; ?>
                            :
                            <span class="bold">
                            <?= ($booking['price_without_discount'] - $booking["price"]) . " " . $booking["currency"]; ?>
                        </span>
                        </li>
                        <li>
                            Стоимость с учетом скидки:
                            <span class="bold">
                            <?= $booking["price"] . " " . $booking["currency"]; ?>
                        </span>
                        </li>
                    <? endif; ?>
                    <? if ($booking["status"] == "confirmed"): ?>
                        <li>
                            Билеты:
                            <div>
                                <? foreach ($booking["arrSeatsId"] as $k => $arSeats): ?>
                                    <a class="ticket-pdf"
                                       href="/local/modules/travelsoft.eurotrans.booking/pages/download.ticket.php?booking_id=<?= $arSeats["booking_id"] ?>&seat=<?= $arSeats["name"] ?>&filename=<?= $arSeats["booking_id"] . "_" . $arSeats["name"] . "_" . $arSeats["last_name"] . "_" . $arSeats["first_name"] ?>.pdf">
                                        <?= $arSeats["last_name"] . " " . $arSeats["first_name"] ?>
                                    </a><br/>
                                <? endforeach; ?>
                            </div>
                        </li>
                    <? endif; ?>
                </ul>
            </div>-->
        <? $i++ ?>
        <? endforeach; ?>
    </div>
        <div class="ts-col-24  ts-px-0 ts-d-block">
            <div class="dop__info ts-width-100 ts-border ts-p-2">
                <h6 class="summ ts-pb-1 ts-d-flex ts-justify-content__space-between"> К оплате:
                    <span>  <?= $arResult["TOTAL_PRICE"] . ' ' . travelsoft\eurotrans\Settings::CURRENT_CURRENCY ?> </span>
                </h6>
                <div class="passangers-count ts-d-flex ts-justify-content__space-between">
                    <div>Пассажиры:</div>
                    <div>
                        <?foreach ($arResult["details"]["trip"]["way"]['tickets_category_count'] as $tickets):?>
                            <?if($tickets['count'] > 0):?>
                                <?= $tickets['count']?> <?= getTicketType($tickets['category'], $tickets['count']);?><br/>
                            <?endif;?>
                        <?endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    <div id="total-price-block">
        <? if (!empty($arResult["STATUS"]["TEXT"])): ?>

            <? if (!$arResult["STATUS"]["VALUE"]): ?>
                <div>

                    <? if ($arResult['PAYMENT_TYPE'] == 'cashless'): ?>

                        <? if (($arResult["PAYMENT_METHOD"] == "creditcard") && ($arResult["SHOW_FORM_BOOK_ONLINE"] == true)): ?>

                            <FORM ACTION="https://pay169.paysec.by/pay/order.cfm" METHOD="POST"
                                  style="display: inline-block;">
                                <INPUT TYPE="HIDDEN" NAME="Merchant_ID" VALUE="488693">
                                <INPUT TYPE="HIDDEN" NAME="OrderNumber" VALUE="<?= $arResult["MAIN_BOOKING_ID"] ?>">
                                <INPUT TYPE="HIDDEN" NAME="Language" VALUE="RU">
                                <INPUT TYPE="HIDDEN" NAME="OrderAmount" VALUE="<?= $arResult["TOTAL_PRICE"] ?>">
                                <INPUT TYPE="HIDDEN" NAME="OrderCurrency"
                                       VALUE="<?= travelsoft\eurotrans\Settings::CURRENT_CURRENCY ?>">

                                <INPUT TYPE="HIDDEN" NAME="Lastname" value="<?= $arResult["PAYER"]["FEMALE"] ?>">
                                <INPUT TYPE="HIDDEN" NAME="Firstname" value="<?= $arResult["PAYER"]["NAME"] ?>">
                                <INPUT TYPE="HIDDEN" NAME="Email" value="<?= $arResult["PAYER"]["EMAIL"] ?>">
                                <INPUT TYPE="HIDDEN" NAME="MobilePhone" value="<?= $arResult["PAYER"]["PHONE"] ?>">

                                <INPUT TYPE="HIDDEN" NAME="URL_RETURN" VALUE="https://eurotrans.by/thank-you/">

                                <INPUT TYPE="SUBMIT" class="btn btn-colored mt-20 mb-10" style="line-height: 40px;"
                                       NAME="Submit"
                                       VALUE="Перейти к оплате">
                            </FORM>

                        <? elseif ($arResult["PAYMENT_METHOD"] == "erip"): ?>

                            <div class="title h3" style="margin-top:40px">Как оплатить через ЕРИП</div>
                            <div class="content-column-content-inner">
                                <blockquote class="caption mt-20"><b>Система "Расчет"(ЕРИП)-&gt; Билеты, Лотереи -&gt;
                                        Транспортные билеты -&gt; Eurotrans.by</b>
                                    <br> Для оплаты необходимо ввести номер вашей брони:
                                    <b><?= $arResult["MAIN_BOOKING_ID"]; ?></b>
                                </blockquote>
                            </div>

                        <? endif; ?>
                    <? else: ?>
                        <? if ($arResult["PAYMENT_METHOD"] == "cash"): ?>

                            <div class="btn btn-gray mt-20 mb-10" style="cursor: default;">
                                <span>Оплата при посадке</span>
                            </div>

                            <div class="title h3" style="margin-top:40px">Как работает бронирование?</div>
                            <div class="content-column-content-inner mt-0">
                                <blockquote class="caption">
                                    <ul class="mt-10">
                                        <li>Подтвердите бронирование<br>
                                            <span style="font-size:12px">С вами свяжется наш оператор за день до отправления до 15:00 по указанным вами контактным данным. </span>
                                        </li>
                                        <li>Пройти сверку паспорта при посадке<br>
                                            <span style="font-size:12px">После посадки, в автобусе будет произведен расчет и выписка проездных документов.</span>
                                        </li>
                                        <li><s>При не подтверждении, бронь будет аннулирована автоматически.</s></li>
                                    </ul>
                                </blockquote>
                            </div>
                        <? endif; ?>
                    <? endif; ?>
                </div>
            <? endif; ?>
        <? endif; ?>
    </div>

    <div>
        <? if ($arResult['IS_AGENT']): ?>
            <a class="aside-list-item text-1" href="/office-agent/">К списку заказов</a>
     <? else: ?>
    <!--        <a class="aside-list-item text-1" href="/">Новое бронирование</a> -->
        <? endif; ?>
    </div>
</div>

<? /*============= dataLayer part ============*/ ?>
<? if (isset($_GET['show_data_layer']) && ($_GET['show_data_layer'] == 'true')): ?>
    <script>

        function removeURLParameter(url, parameter) {
            //prefer to use l.search if you have a location/link object
            let urlparts = url.split('?');
            if (urlparts.length >= 2) {

                let prefix = encodeURIComponent(parameter) + '=';
                let pars = urlparts[1].split(/[&;]/g);

                //reverse iteration as may be destructive
                for (let i = pars.length; i-- > 0;) {
                    //idiom for string.startsWith
                    if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                        pars.splice(i, 1);
                    }
                }

                url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
                return url;
            } else {
                return url;
            }
        }

        // for only one show this script
        let newUrl = removeURLParameter(window.location.href, 'show_data_layer');
        history.pushState(null, null, newUrl);

        let arrDataLayer = JSON.parse('<?= json_encode($arResult['DATA_LAYER']);?>');

        dataLayer.push(arrDataLayer);
    </script>
<? endif; ?>
<script>
    function startTimer() {
        //var my_timer = document.querySelector("#timer span");
        //var time = my_timer.innerHTML;
        //var arr = time.split(":");
        //var h = document.querySelector("#timer span");arr[0];
        var m = document.querySelector("#timer .min").innerText;
        var s = document.querySelector("#timer .sec").innerText;
        if (s == 0) {
            if (m == 0) {

                alert("Время вышло");
                window.location.replace("/");
                return;
            }
            m--;
            if (m < 10) m = "0" + m;
            s = 59;
        }
        else s--;
        if (s < 10) s = "0" + s;
        document.querySelector("#timer .min").innerText = m;
        document.querySelector("#timer .sec").innerText = s;
        if(m == 0 && s == 0 ){
            alert('Время бронирования закончилось')
            window.location.href = "/";
        }
        setTimeout(startTimer, 1000);

    }
    if(document.querySelector('.timer')){
        startTimer();
    }
</script>
<? /*============= end dataLayer part ============*/ ?>
