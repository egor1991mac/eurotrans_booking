<?use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);?>
                </div>

            <?$APPLICATION->IncludeComponent(
	"bitrix:advertising.banner", 
	"new_site_central", 
	array(
		"CACHE_TIME" => "0",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "new_site_central",
		"NOINDEX" => "Y",
		"QUANTITY" => "1",
		"TYPE" => "CENTER_1",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>

            <footer class="footer">
                <div class="footer__main">
                    <div class="container">
                        <div class="row">
                            <div class="footer">
                                    <div class="col-md-5 col-sm-7  pull-left col-xs-12">
                                        <div class="footer__links">
                                            <ul class="list-inline">
                                                <?$APPLICATION->IncludeComponent(
                                                    "bitrix:menu",
                                                    "bottom.menu",
                                                    Array(
                                                        "ALLOW_MULTI_SELECT" => "N",
                                                        "CHILD_MENU_TYPE" => "bottom",
                                                        "DELAY" => "N",
                                                        "MAX_LEVEL" => "1",
                                                        "MENU_CACHE_GET_VARS" => array(""),
                                                        "MENU_CACHE_TIME" => "3600",
                                                        "MENU_CACHE_TYPE" => "N",
                                                        "MENU_CACHE_USE_GROUPS" => "N",
                                                        "ROOT_MENU_TYPE" => "bottom",
                                                        "USE_EXT" => "Y"
                                                    )
                                                );?>
                                                <li><noindex>
                                                        <div class="social-block">
                                                            <?$APPLICATION->IncludeFile("/include/footer_socservices.php", Array(), Array(
                                                                "MODE"      => "html",
                                                                "NAME"      => "Соц. сервисы (подвал сайта)",
                                                            ));?>
                                                        </div>
                                                    </noindex>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4  pull-right hidden-xs">
                                        <div class="cmn-padding_top_10">
                                            <?$APPLICATION->IncludeFile("/include/yandex_metrika.php", Array(), Array(
                                                "MODE"      => "html",
                                                "NAME"      => "Yandex метрика (подвал сайта)",
                                            ));?>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <p><?$APPLICATION->IncludeFile("/include/copyright.php", Array(), Array(
                                                "MODE"      => "html",
                                                "NAME"      => "Copyright",
                                            ));?>
                                            <br><?$APPLICATION->IncludeFile("/include/footer_info.php", Array(), Array(
                                                "MODE"      => "html",
                                                "NAME"      => "Информация (подвал сайта)",
                                            ));?>
                                        </p>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyright">
                    <div class="container">
                                <span class="copyright__info">
                                    <?$APPLICATION->IncludeFile("/include/copyright_info.php", Array(), Array(
                                        "MODE"      => "html",
                                        "NAME"      => "Авторские права",
                                    ));?>
                                </span>
                                <div class="copyright-list">
                                    <?$APPLICATION->IncludeFile("/include/footer_developer.php", Array(), Array(
                                        "MODE"      => "html",
                                        "NAME"      => "Разработчик сайта (подвал сайта)",
                                    ));?>
                                </div>
                    </div>
                </div>
            </footer>
        </div>
		<div style="clear:both;"></div>
		<?$APPLICATION->ShowPanel()?>
     </body>
</html>