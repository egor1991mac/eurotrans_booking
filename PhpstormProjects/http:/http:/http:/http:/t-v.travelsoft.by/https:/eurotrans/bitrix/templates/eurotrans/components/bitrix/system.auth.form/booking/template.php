<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CJSCore::Init();
?>

<? global $USER; ?>

<? if (isset($arResult["ERROR_MESSAGE"]["MESSAGE"]) && !empty($arResult["ERROR_MESSAGE"]["MESSAGE"])): ?>
    <div class="input-wrapper">
        <span class='error-block form-error'><?= $arResult["ERROR_MESSAGE"]["MESSAGE"]; ?></span>
    </div>
<? endif; ?>

<div class="content-column-content">
    <form class="form-block-wrapper js-validate" name="system_auth_form<?= $arResult["RND"] ?>"
          method="post" target="_top"
          id="ajax_auth_form"
          action="<?= $arResult["AUTH_URL"] ?>">

        <? if ($arResult["BACKURL"] <> ''): ?>
            <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
        <? endif ?>
        <?
        foreach ($arResult["POST"] as $key => $value): ?>
            <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
        <? endforeach ?>
        <input type="hidden" name="AUTH_FORM" value="Y"/>
        <input type="hidden" name="TYPE" value="AUTH"/>

        <div class="form-block-section">
            <div class="input-form">
                <? $randString = randString(7) ?>

                <div class="input-item s100">
                    <div class="input-wrapper req">
                        <label for="imbl<?= $randString; ?>">E-mail</label>
                        <input class="input-main" name="USER_LOGIN" type="email"
                               placeholder="example@mail.com"
                               data-validation="email" data-validation-error-msg="Не заполнено"
                               id="imbl<?= $randString; ?>">

                        <script>
                            BX.ready(function () {
                                var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
                                if (loginCookie) {
                                    var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
                                    var loginInput = form.elements["USER_LOGIN"];
                                    loginInput.value = loginCookie;
                                }
                            });
                        </script>

                    </div>
                </div>

                <? $randString = randString(7) ?>
                <div class="input-item s100">
                    <!--пароли короче 6 символов в принципе не принимаем-->
                    <div class="input-wrapper req">
                        <label for="imbl<?= $randString; ?>">Пароль</label>
                        <input class="input-main" type="password" placeholder="Пароль"
                               data-validation="length" data-validation-length="min6"
                               data-validation-error-msg="Не заполнено" id="imbl<?= $randString; ?>"
                               name="USER_PASSWORD">
                    </div>
                </div>
                <div class="input-item s100">
                <!-- ведет на соотв. страницу-->
                    <a class="link-border dotted" href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>"
                       rel="nofollow">
                        <span>Забыли пароль?</span>
                    </a>

                </div>
            </div>

            <? if ($arResult["AUTH_SERVICES"]): ?>

                <div class="text-1"><?= GetMessage("socserv_as_user_form") ?></div>
                <? $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "flat",
                    array(
                        "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                        "SUFFIX" => "form",
                    ), '', array("HIDE_ICONS" => "Y")
                ); ?>

            <? endif ?>

        </div>


        <div class="form-block-footer">
            <button class="btn btn-colored" type="button" onclick="auth.submit()" name="Login">
                <span>Войти</span>
            </button>
        </div>

    </form>
</div>
