<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arCurrentValues */

$arComponentParameters['PARAMETERS']['SEARCH_TICKETS_PAGE'] = array(
    "PARENT" => "BASE",
    "NAME" => "Страница поиска билетов",
    "TYPE" => "STRING"
);

$arComponentParameters['PARAMETERS']['ROUTE_LIST_PAGE'] = array(
    "PARENT" => "BASE",
    "NAME" => "Страница маршрутов",
    "TYPE" => "STRING"
);

$arComponentParameters['PARAMETERS']['REQUEST'] = array(
    "PARENT" => "BASE",
    "NAME" => "Запрос на поиск",
    "TYPE" => "STRING",
    "DEFAULT" => '={$_REQUEST["eurotrans"]}'
);

$arComponentParameters['PARAMETERS']['DEF_DEP_POINT'] = array(
    "PARENT" => "BASE",
    "NAME" => "id точки отправления по-умолчанию",
    "TYPE" => "STRING"
);

$arComponentParameters['PARAMETERS']['DEF_ARR_POINT'] = array(
    "PARENT" => "BASE",
    "NAME" => "id точки прибытия по-умолчанию",
    "TYPE" => "STRING"
);