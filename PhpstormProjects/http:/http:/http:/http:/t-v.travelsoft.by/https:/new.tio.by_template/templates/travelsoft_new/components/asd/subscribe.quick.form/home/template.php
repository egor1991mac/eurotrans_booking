<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (method_exists($this, 'setFrameMode')) {
	$this->setFrameMode(true);
}

if ($arResult['ACTION']['status']=='error') {
	ShowError($arResult['ACTION']['message']);
} elseif ($arResult['ACTION']['status']=='ok') {
	ShowNote($arResult['ACTION']['message']);
}
?>
<div class="widget ts-width-100">
    <div class="form-sidebar-2 bg-grey-2 ts-mt-10 ts-mt-lg-0">
        <h3 class="form-sidebar-2__title"><?=GetMessage('ASD_SUBSCRIBEQUICK_PODPISATQSA_TITLE')?></h3>
        <div id="asd_subscribe_res" style="display: none;"></div>
        <form class="marketing-form form-sidebar-2__form" action="<?= POST_FORM_ACTION_URI?>" method="post" id="asd_subscribe_form">
            <?= bitrix_sessid_post()?>
            <input type="hidden" name="asd_subscribe" value="Y" />
            <input type="hidden" name="charset" value="<?= SITE_CHARSET?>" />
            <input type="hidden" name="site_id" value="<?= SITE_ID?>" />
            <input type="hidden" name="asd_rubrics" value="<?= $arParams['RUBRICS_STR']?>" />
            <input type="hidden" name="asd_format" value="<?= $arParams['FORMAT']?>" />
            <input type="hidden" name="asd_show_rubrics" value="<?= $arParams['SHOW_RUBRICS']?>" />
            <input type="hidden" name="asd_not_confirm" value="<?= $arParams['NOT_CONFIRM']?>" />
            <input type="hidden" name="asd_key" value="<?= md5($arParams['JS_KEY'].$arParams['RUBRICS_STR'].$arParams['SHOW_RUBRICS'].$arParams['NOT_CONFIRM'])?>" />

            <input type="text" class="form-control input-dark form-sidebar-2__input" name="asd_email" placeholder="Ваш e-mail" value="" />
            <button type="submit" name="asd_submit" id="asd_subscribe_submit" class="btn btn-dark form-sidebar-2__btn block-sub-btn" value="<?=GetMessage("ASD_SUBSCRIBEQUICK_PODPISATQSA")?>"><?=GetMessage("ASD_SUBSCRIBEQUICK_PODPISATQSA")?></button>

            <?/*if (isset($arResult['RUBRICS'])):*/?><!--
                <br/>
                <?/*foreach($arResult['RUBRICS'] as $RID => $title):*/?>
                    <input type="checkbox" name="asd_rub[]" id="rub<?/*= $RID*/?>" value="<?/*= $RID*/?>" />
                    <label for="rub<?/*= $RID*/?>"><?/*= $title*/?></label><br/>
                <?/*endforeach;*/?>
            --><?/*endif;*/?>

        </form>
        <div class="form-sidebar-2__info"><?=GetMessage('ASD_SUBSCRIBEQUICK_PODPISATQSA_TEXT')?></div>
    </div>
</div>
