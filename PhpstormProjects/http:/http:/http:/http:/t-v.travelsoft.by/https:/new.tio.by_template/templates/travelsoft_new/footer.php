<? use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__); ?>

<? if (defined("HOME_PAGE") && HOME_PAGE === true): ?>

<? elseif (defined("ERROR_404") && ERROR_404 == "Y"): ?>

<? else: ?>

    </div>

    <? if ($APPLICATION->GetDirProperty("SHOW_RIGHT_SIDEBAR") != "N" && $APPLICATION->GetDirProperty("SHOW_LEFT_SIDEBAR") != "Y"): ?>
        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR . "include/sidebar.php"
        )); ?>
    <? endif ?>

    </div>
    </div>
<? endif ?>

<? $APPLICATION->IncludeComponent(
	"bitrix:advertising.banner", 
	"jssor", 
	array(
		"CACHE_TIME" => "0",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "jssor",
		"NOINDEX" => "Y",
		"QUANTITY" => "1",
		"TYPE" => "CENTER_1",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DEFAULT_TEMPLATE" => "-",
		"SCALE" => "N",
		"CYCLING" => "N",
		"EFFECTS" => array(
		),
		"ANIMATION_DURATION" => "500",
		"WRAP" => "1",
		"ARROW_NAV" => "1",
		"BULLET_NAV" => "2",
		"KEYBOARD" => "N"
	),
	false
); ?>

<footer class="footer">
    <div class="footer__main">
        <div class="ts-wrap">
            <div class="ts-row">
                <div class="footer">
                    <div class="ts-col-md-10 ts-col-sm-14  pull-left ts-col-xs-24">
                        <div class="footer__links">
                            <ul class="list-inline">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "bottom.menu",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "bottom",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "1",
                                        "MENU_CACHE_GET_VARS" => array(""),
                                        "MENU_CACHE_TIME" => "3600",
                                        "MENU_CACHE_TYPE" => "N",
                                        "MENU_CACHE_USE_GROUPS" => "N",
                                        "ROOT_MENU_TYPE" => "bottom",
                                        "USE_EXT" => "Y"
                                    )
                                ); ?>
                                <li>
                                    <noindex>
                                        <div class="social-block">
                                            <? $APPLICATION->IncludeFile("/include/footer_socservices.php", Array(), Array(
                                                "MODE" => "html",
                                                "NAME" => "Соц. сервисы (подвал сайта)",
                                            )); ?>
                                        </div>
                                    </noindex>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="ts-col-md-6 ts-col-sm-8  pull-right hidden-xs">
                        <div class="cmn-padding_top_10">
                            <? $APPLICATION->IncludeFile("/include/yandex_metrika.php", Array(), Array(
                                "MODE" => "html",
                                "NAME" => "Yandex метрика (подвал сайта)",
                            )); ?>
                        </div>
                    </div>

                    <div class="ts-col-sm-24">
                        <p><? $APPLICATION->IncludeFile("/include/copyright.php", Array(), Array(
                                "MODE" => "html",
                                "NAME" => "Copyright",
                            )); ?>
                            <br><? $APPLICATION->IncludeFile("/include/footer_info.php", Array(), Array(
                                "MODE" => "html",
                                "NAME" => "Информация (подвал сайта)",
                            )); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="ts-wrap">
                                <span class="copyright__info">
                                    <? $APPLICATION->IncludeFile("/include/copyright_info.php", Array(), Array(
                                        "MODE" => "html",
                                        "NAME" => "Авторские права",
                                    )); ?>
                                </span>
            <div class="copyright-list">
                <? $APPLICATION->IncludeFile("/include/footer_developer.php", Array(), Array(
                    "MODE" => "html",
                    "NAME" => "Разработчик сайта (подвал сайта)",
                )); ?>
            </div>
        </div>
    </div>
</footer>

<!-- BUTTON BACK TO TOP-->
<!--<a href="#" class="scrollup">
    <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
</a>-->

</div> <!-- l-theme animated-css -->
<div style="clear:both;"></div>
<? $APPLICATION->ShowPanel() ?>
</body>
</html>