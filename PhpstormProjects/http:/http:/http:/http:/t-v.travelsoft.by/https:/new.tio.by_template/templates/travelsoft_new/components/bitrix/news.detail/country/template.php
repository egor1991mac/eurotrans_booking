<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//$this->addExternalJS(SITE_TEMPLATE_PATH."/assets/js/theia-sticky-sidebar.min.js");
$htmlMapID = "route-map";
$scroll = array();
$img = getSrc($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"], array('width' => 1140, 'height' => 550));
?>

<section class="b-post b-post_img-bg b-post_img-bg_type-8 clearfix">
    <?if(!empty($img[0])):?>
        <div class="entry-media">
            <img class="img-responsive" src="<?=$img[0]?>" alt="<?=$arResult["NAME"]?>">
        </div>
    <?endif?>
    <div class="entry-main<?if(empty($img[0])):?> section-block-nophoto<?endif?>">
        <div class="entry-header">
            <div class="entry-meta">
                <?if(in_array(array(1,7),$GLOBALS["USER"]->GetUserGroupArray())):?>
                    <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                        <a class="entry-meta__link"><?=$arResult["SHOW_COUNTER"]?></a>
                    </span>
                <?endif?>
            </div>
            <h1 class="entry-title hov-title">
                <a class="hov-title__inner"><?=$arResult["NAME"]?></a>
            </h1>
        </div>
        <?if(!empty($arResult["PREVIEW_TEXT"])):?>
            <div class="entry-content">
                <p><?=$arResult["~PREVIEW_TEXT"]?></p>
            </div>
        <?endif?>
    </div>
</section>
<div class="col-md-8">

    <ul class="nav nav-tabs nav-tabs_top-header">
        <li class="active"><a href="#desc" data-toggle="tab"><?=GetMessage('DESCRIPTION')?></a></li>
        <?if($arResult["ROUTER"]["RESORTS"]):?>
            <li><a href="<?=$arParams["DETAIL_PAGE"]?>resorts/"><?=GetMessage('RESORTS')?></a></li>
        <?endif?>
        <?if($arResult["ROUTER"]["VISAS"]):?>
            <li><a href="<?=$arParams["DETAIL_PAGE"]?>visa/"><?=GetMessage('VISA')?></a></li>
        <?endif?>
        <?if($arResult["ROUTER"]["TOURS"]):?>
            <li><a href="<?=$arParams["DETAIL_PAGE"]?>tours/"><?=GetMessage('TOURS')?></a></li>
        <?endif?>
        <?if($arResult["ROUTER"]["HOTELS"]):?>
            <li><a href="<?=$arParams["DETAIL_PAGE"]?>hotels/"><?=GetMessage('HOTELS')?></a></li>
        <?endif?>
        <?if(!empty($arResult["ROUTE_INFO"])):?>
            <li><a class="map_tab" rel="nofollow" href="#map" data-toggle="tab"><?=GetMessage('MAP')?></a></li>
        <?endif?>
    </ul>

    <div class="tab-content">
        <div class="tab-pane fade in active" id="desc">

            <div class="l-main-content l-main-content_mrg-right_minus">
                <article class="b-post b-post-full clearfix">
                    <div class="entry-main">

                        <div class="entry-content">

                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["LANG"]["VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["TIME"]["VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["PEOPLE"]["VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["KEY"]["VALUE"])):?>
                                <div class="table-container">
                                    <table class="table table_secondary table-type-2 table-striped typography-last-elem">
                                        <tbody>
                                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["LANG"]["VALUE"])):?>
                                                <tr>
                                                    <td class="td_name"><?=$arResult["DISPLAY_PROPERTIES"]["LANG"]["NAME"]?></td>
                                                    <td><?=$arResult["DISPLAY_PROPERTIES"]["LANG"]["VALUE"]?></td>
                                                </tr>
                                            <?endif?>
                                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["PEOPLE"]["VALUE"])):?>
                                                <tr>
                                                    <td class="td_name"><?=$arResult["DISPLAY_PROPERTIES"]["PEOPLE"]["NAME"]?></td>
                                                    <td><?=$arResult["DISPLAY_PROPERTIES"]["PEOPLE"]["VALUE"]?></td>
                                                </tr>
                                            <?endif?>
                                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["TIME"]["VALUE"])):?>
                                                <tr>
                                                    <td class="td_name"><?=$arResult["DISPLAY_PROPERTIES"]["TIME"]["NAME"]?></td>
                                                    <td><?=$arResult["DISPLAY_PROPERTIES"]["TIME"]["VALUE"]?></td>
                                                </tr>
                                            <?endif?>
                                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["KEY"]["VALUE"])):?>
                                                <tr>
                                                    <td class="td_name"><?=$arResult["DISPLAY_PROPERTIES"]["KEY"]["NAME"]?></td>
                                                    <td><?=$arResult["DISPLAY_PROPERTIES"]["KEY"]["VALUE"]?></td>
                                                </tr>
                                            <?endif?>
                                        </tbody>
                                    </table>
                                </div>
                            <?endif?>

                            <div class="block_desc" id="block_desc">
                                <?$scroll[] = array("id"=>'block_desc',"name"=>GetMessage('DESC'));?>
                                <p><?=$arResult["~DETAIL_TEXT"]?></p>
                                <br>
                            </div>
                            <?if(isset($arResult["SLIDER1"]) && !empty($arResult["SLIDER1"])):?>
                                <div class="block_photo" id="block_photo">
                                    <?$scroll[] = array("id"=>'block_photo',"name"=>GetMessage('PHOTO'));?>
                                    <h3><?=GetMessage('PHOTO')?></h3>
                                    <div class="main-slider slider-pro" id="main-slider1" data-slider-width="78%" data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false" style="margin: 0">
                                        <div class="sp-slides">
                                            <?foreach ($arResult["SLIDER1"] as $k=>$slide):?>
                                                <?$slide_desc = $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["DESCRIPTION"][$k];?>
                                                <div class="sp-slide">
                                                    <img class="sp-image" src="<?=$slide?>" alt="<?=!empty($slide_desc) ? $slide_desc : 'slide-'.$k+1?>"/>
                                                    <?if(!empty($slide_desc)):?>
                                                        <div class="main-slider__info sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-10">
                                                                        <section class="b-post b-post_slider clearfix">
                                                                            <div class="entry-main">
                                                                                <div class="entry-header">
                                                                                    <h2 class="entry-title hov-title"><a class="hov-title__inner"><?=$slide_desc?></a></h2>
                                                                                </div>
                                                                            </div>
                                                                        </section>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?endif?>
                                                </div>
                                            <?endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            <?endif?>

                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["POPULAR_RESORTS"]["VALUE"]["TEXT"])):?>
                                <div class="block_resort" id="block_resort">
                                    <?$scroll[] = array("id"=>'block_resort',"name"=>$arResult["DISPLAY_PROPERTIES"]["POPULAR_RESORTS"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["POPULAR_RESORTS"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["POPULAR_RESORTS"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["KLIMAT"]["VALUE"]["TEXT"])):?>
                                <div class="block_klimat" id="block_klimat">
                                    <?$scroll[] = array("id"=>'block_klimat',"name"=>$arResult["DISPLAY_PROPERTIES"]["KLIMAT"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["KLIMAT"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["KLIMAT"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["KITCHEN"]["VALUE"]["TEXT"])):?>
                                <div class="block_kitchen" id="block_kitchen">
                                    <?$scroll[] = array("id"=>'block_kitchen',"name"=>$arResult["DISPLAY_PROPERTIES"]["KITCHEN"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["KITCHEN"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["KITCHEN"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["TERRITORY"]["VALUE"]["TEXT"])):?>
                                <div class="block_territory" id="block_territory">
                                    <?$scroll[] = array("id"=>'block_territory',"name"=>$arResult["DISPLAY_PROPERTIES"]["TERRITORY"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["TERRITORY"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["TERRITORY"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["BEACHES"]["VALUE"]["TEXT"])):?>
                                <div class="block_beaches" id="block_beaches">
                                    <?$scroll[] = array("id"=>'block_beaches',"name"=>$arResult["DISPLAY_PROPERTIES"]["BEACHES"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["BEACHES"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["BEACHES"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["HOTELS"]["VALUE"]["TEXT"])):?>
                                <div class="block_hotels" id="block_hotels">
                                    <?$scroll[] = array("id"=>'block_hotels',"name"=>$arResult["DISPLAY_PROPERTIES"]["HOTELS"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["HOTELS"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["HOTELS"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["SHOP"]["VALUE"]["TEXT"])):?>
                                <div class="block_shop" id="block_shop">
                                    <?$scroll[] = array("id"=>'block_shop',"name"=>$arResult["DISPLAY_PROPERTIES"]["SHOP"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["SHOP"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["SHOP"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["MONEY"]["VALUE"]["TEXT"])):?>
                                <div class="block_money" id="block_money">
                                    <?$scroll[] = array("id"=>'block_money',"name"=>$arResult["DISPLAY_PROPERTIES"]["MONEY"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["MONEY"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["MONEY"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["SAFETY_OF_TOURISTS"]["VALUE"]["TEXT"])):?>
                                <div class="block_tourists" id="block_tourists">
                                    <?$scroll[] = array("id"=>'block_tourists',"name"=>$arResult["DISPLAY_PROPERTIES"]["SAFETY_OF_TOURISTS"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["SAFETY_OF_TOURISTS"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["SAFETY_OF_TOURISTS"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["TRANSPORT"]["VALUE"]["TEXT"])):?>
                                <div class="block_transport" id="block_transport">
                                    <?$scroll[] = array("id"=>'block_transport',"name"=>$arResult["DISPLAY_PROPERTIES"]["TRANSPORT"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["TRANSPORT"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["TRANSPORT"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["RENT_A_CAR"]["VALUE"]["TEXT"])):?>
                                <div class="block_rentcar" id="block_rentcar">
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["RENT_A_CAR"]["NAME"]?></h3>
                                    <?$scroll[] = array("id"=>'block_rentcar',"name"=>$arResult["DISPLAY_PROPERTIES"]["RENT_A_CAR"]["NAME"]);?>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["RENT_A_CAR"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>

                        </div>


                        <div class="entry-footer">
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["THEME"]["VALUE"])):?>
                                <div class="post-tags"><span class="entry-footer__title"><?=GetMessage('THEME')?></span>
                                    <ul class="list-tags list-tags_grey list-unstyled">
                                        <?if(isset($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"]) && !empty($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"])):?>
                                            <?foreach ($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"] as $theme):?>
                                                <li class="list-tags__item"><a class="list-tags__link btn btn-default" href="<?=$theme["DETAIL_PAGE_URL"]?>"><?=$theme["NAME"]?></a></li>
                                            <?endforeach;?>
                                        <?else:?>
                                            <?if(is_array($arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"])):?>
                                                <?=implode2('</li>, <li class="list-tags__item">',$arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"])?>
                                            <?else:?>
                                                <li class="list-tags__item"><?=$arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"]?></li>
                                            <?endif?>
                                        <?endif?>
                                    </ul>
                                </div>
                            <?endif?>
                            <div class="entry-footer__social">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:main.share",
                                    "main",
                                    Array(
                                        "HANDLERS" => array("facebook","twitter","vk"),
                                        "HIDE" => "N",
                                        "PAGE_TITLE" => $APPLICATION->GetTitle(),
                                        "PAGE_URL" => $_SERVER["REQUEST_URI"],
                                        "SHORTEN_URL_KEY" => "",
                                        "SHORTEN_URL_LOGIN" => ""
                                    )
                                );?>
                            </div>
                        </div>
                    </div>
                </article>
            </div>

        </div>
        <?if(!empty($arResult["ROUTE_INFO"])):?>
            <div class="tab-pane fade" id="map">
                <div class="l-main-content l-main-content_mrg-right_minus">
                    <article class="b-post b-post-full clearfix">
                        <div class="entry-main">

                            <div class="entry-content">

                                <?
                                $this->addExternalJs("https://maps.googleapis.com/maps/api/js?key=AIzaSyBaEtiggot7kJNVdbYyTr9imXxRipJ7jrM");
                                $this->addExternalJS(SITE_TEMPLATE_PATH."/assets/js/jquery-custom-google-map-lib.js");
                                ?>
                                <div style="width: 100%; height: 432px" id="<?= $htmlMapID ?>"></div>

                            </div>

                        </div>
                    </article>
                </div>
            </div>
        <?endif?>

    </div>

</div>
<div class="col-md-4">
    <aside class="l-sidebar l-sidebar_mrg-left l-sidebar_first-section" id="sidebar">
        <?if(!empty($scroll)):?>
            <!--<div class="theiaStickySidebar" style="top:120px !important;">-->
                <div class="widget-content">
                    <ul class="widget-list widget-list_white list list-mark-4">
                        <?foreach($scroll as $s):?>
                            <?if(!empty($s)):?>
                                <li class="widget-list__item">
                                    <a class="widget-list__link anchor_" href="#<?= $s["id"]?>"><?= $s["name"]?></a>
                                </li>
                            <?endif?>
                        <?endforeach?>
                    </ul>
                </div>
           <!-- </div>-->
        <?endif?>
    </aside>
</div>

<script>
    var $block = $("#sidebar");
    var fixedClass = "fixed";
    var active_block = 0;

    var height_content_block = $('.tab-pane.active').height() + 540;

    $('.nav-tabs li a').on("click", function () {

        active_block = $(this).attr('href');
        if(active_block.indexOf('#') >= 0){
            active_block = active_block.replace("#",'',active_block);
            height_content_block = $('#' + active_block).height() + 500;
        }

    });

    $(window).scroll(function(){
        if($(window).scrollTop()>954 && $(window).scrollTop()<height_content_block){
            $block.addClass(fixedClass)
        }else{
            $block.removeClass(fixedClass)
        }
    });



    $('.map_tab').one('click', function () {
        (function (gm) {
            // init map and draw route
            gm.createGoogleMap("<?= $htmlMapID ?>", {center: gm.LatLng(<?= $arResult['ROUTE_INFO'][0]["lat"] ?>, <?= $arResult['ROUTE_INFO'][0]["lng"] ?>), zoom: <?= $arResult['MAP_SCALE'] ?>})
        })(window.GoogleMapFunctionsContainer)
    });

    $( document ).ready(function() {
        $(".entry-content img").each(function () {
                $(this).addClass('img-responsive');
            }
        );
        $(".entry-footer .list-tags li").each(function () {
                if($(this).find('a').hasClass('list-tags__link') === false){
                    $(this).find('a').addClass('list-tags__link btn btn-default');
                }
            }
        );

        $('a.anchor_').on("click", function () {

            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

                if($('.tab-pane.active').attr('id') != 'desc'){

                    $('.nav-tabs li a[href="#desc"]').trigger('click');

                    setTimeout(function () {
                        $('html,body').stop().animate({
                            scrollTop: (target.offset().top - 135) // 70px offset for navbar menu
                        }, 1000);
                    }, 1000);


                } else {
                    $('html,body').stop().animate({
                        scrollTop: (target.offset().top - 135) // 70px offset for navbar menu
                    }, 1000);
                }
                return false;

            }
        });

    });
</script>