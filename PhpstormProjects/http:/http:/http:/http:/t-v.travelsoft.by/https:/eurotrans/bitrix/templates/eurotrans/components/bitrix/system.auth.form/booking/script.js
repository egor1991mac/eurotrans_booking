let auth = {
    submit: function () {
        let form = $('#ajax_auth_form');
        $.post(form.attr('action'), form.serialize()).done(function (data) {
            $('#js_booking_form').html($(data).find('#js_booking_form').html());
        });
    }
};