<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CJSCore::Init();
?>

<? global $USER; ?>

<? if ($arResult["AUTH_SERVICES"]): ?>

    <div class="text-1"><?= GetMessage("socserv_as_user_form") ?></div>
    <? $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "flat",
        array(
            "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
            "SUFFIX" => "form",
        ), '', array("HIDE_ICONS" => "Y")
    ); ?>

<? endif ?>