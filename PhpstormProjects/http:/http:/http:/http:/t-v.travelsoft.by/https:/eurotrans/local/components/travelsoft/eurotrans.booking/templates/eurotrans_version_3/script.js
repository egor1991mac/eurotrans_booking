/**
 * Eurotrans booking of tickets
 *
 * @author dimabresky https://github.com/dimabresky
 * @copyright 2017, travelsoft
 */

BX.ready(function () {

    var form = BX("booking-details");

    // включение спинера на кнопке
    function __spinerEnable(el) {
        BX.addClass(el, "has-animation");
        BX.addClass(el, "animated");
    }

    // выключение спинера на кнопке
    function __spinerDisable(el) {
        BX.removeClass(el, "has-animation");
        BX.removeClass(el, "animated");
    }

    var birthday = BX.findChildren(form, function (el) {

        return typeof el.dataset !== "undefined" && el.dataset.birthday === "true";

    }, true);

    $(document).ready(function() {
        $(birthday).mask('99.99.9999');
    });

    /* Fields for auto fill fields payer. */
    var nameFirstPassenger = BX.findChild(form, {attribute: {name: 'eurotrans[passangers][1][name]'}}, true);
    var lastNameFirstPassenger = BX.findChild(form, {attribute: {name: 'eurotrans[passangers][1][last_name]'}}, true);

    BX.bind(nameFirstPassenger, "bxchange", function () {
        BX.findChild(form, {attribute: {name: 'eurotrans[payer][name]'}}, true).value = nameFirstPassenger.value;
    });

    BX.bind(lastNameFirstPassenger, "bxchange", function () {
        BX.findChild(form, {attribute: {name: 'eurotrans[payer][last_name]'}}, true).value = lastNameFirstPassenger.value;
    });

    //Type document for change status series
    /*var typeDocuments = BX.findChildren(form, function (el) {

        return /^eurotrans\[passangers\]\[(?:\d*)\]\[documentType\]$/.test(el.name);
        
    }, true);*/

/*
    typeDocuments.forEach(function (typeDocument) {

        BX.bind(typeDocument, "bxchange", function (e) {
            var select = $(this);

            var optionSelected = select.find(':selected');
            var groupNumber = select.data('groupNumber');

            var dataSeries = optionSelected.data('series');

            var inputSeries = BX.findChild(form, {attribute: {name: 'eurotrans[passangers][' + groupNumber + '][series]'}}, true);

            if(dataSeries === 1){
                inputSeries.removeAttribute('disabled');
            }
            else {
                inputSeries.setAttribute('disabled', 'disabled');
            }
        });
        BX.fireEvent(BX(typeDocument), 'change');
    });
*/
/*
    typeDocuments.forEach(function (typeDocument) {

        BX.bind(BX(typeDocument), "bxchange", function () {

            var checkbox = $(this);

            var groupNumber = checkbox.data('groupNumber');

            var dataSeries = checkbox.data('series');

            var inputSeries = BX.findChild(form, {attribute: {name: 'eurotrans[passangers][' + groupNumber + '][series]'}}, true);

            if(dataSeries === 1){
                inputSeries.removeAttribute('disabled');
            }
            else {
                inputSeries.setAttribute('disabled', 'disabled');
            }
        });
      //  BX.fireEvent(BX(typeDocument), 'change');
    });*/


    // validator
    BX.bind(form, 'submit', function (e) {

        var form_elements = BX.findChildren(this, {attribute: {"data-need-validation": "yes"}}, true);

        var validators, error_areas, hasErrors = false, errors = [], isChecked = false, inputs,
            errorsArea = BX.findChildren(this, {className: "error-area"}, true);

        __spinerEnable(BX("eurotrans-booking-btn"));

        var i;
        for (i = 0; i < errorsArea.length; i++) {
            errorsArea[i].innerHTML = "";
        }

        for (i = 0; i < form_elements.length; i++) {

            errors = [];
            validators = form_elements[i].dataset.validators.split("|");

            for (var j = 0; j < validators.length; j++) {
                switch (validators[j]) {

                    case "is_empty":

                        if (!form_elements[i].value.length) {

                            errors.push('Поле не может быть пустым');
                        }

                        break;

                    case "is_phone":

                        if (!/^([\+]+)*[0-9\x20\x28\x29\-]{5,20}$/.test(form_elements[i].value)) {

                            errors.push('Введите телефон в указанном формате');
                        }

                        break;

                    case "is_email":


                        if (!/^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/.test(form_elements[i].value)) {
                            errors.push('Введите коректный email');
                        }

                        break;

                    case "custom_select_is_selected":

                        inputs = BX.findChildren(this, {
                            tag: "input",
                            attribute: {name: form_elements[i].dataset.validationFn}
                        }, true);

                        isChecked = false;

                        for (var k = 0; k < inputs.length; k++) {

                            if (inputs[k].checked) {
                                isChecked = true;
                                break;
                            }

                        }

                        if (!isChecked) {
                            errors.push('Поле не выбрано');
                        }

                        break;
                }
            }

            if (errors.length) {
                hasErrors = true;
                BX("error-area-" + form_elements[i].dataset.linkErrorArea).innerHTML = errors.join("<br>");
            }

        }

        if (hasErrors) {
            error_areas = BX.findChildren(this, {className: "error-area"}, true);
            for (i = 0; i < error_areas.length; i++) {
                if (error_areas[i].innerHTML !== "") {
                    var element = error_areas[i].dataset.scrolltoid;
                    BX.scrollToNode(BX(element));
                    e.preventDefault();
                    __spinerDisable(BX("eurotrans-booking-btn"));
                    return;
                }
            }

        }

    });
});


setInterval(function () {

    var json_request = {sessid: BX.bitrix_sessid()};
    BX.ajax.get("/local/components/travelsoft/eurotrans.booking/ajax/check_session.php", json_request, function (json_resp) {
        var resp = JSON.parse(json_resp);
        if (resp.error === 'true') {
            alert(
                'Время бронирования закончилось, пожалуйста начните бронирование билетов сначала!'
            );
            window.location.replace("/");
        }
        if (resp.session === 'ok') {

        }
    });

}, 300000);