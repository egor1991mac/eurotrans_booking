<?

class TravelsoftEurotransEnrollInTheQueue extends CBitrixComponent
{

    protected $request = null;

    function processPostForm()
    {
        $result = false;

        $this->request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

        if(!empty($this->request->getPost($this->arResult["HIDDEN_CHECK_STRING"]))) {

            if (check_bitrix_sessid() && ($this->request->isPost()) && !empty($this->request->getPost('SUBMIT'))) {

                $result = true;

                foreach ($this->arResult['FORM_FIELDS'] as $name => &$field) {

                    $value = $this->request->getPost($name);
                    if (!empty($value)) {
                        $field["VALUE"] = $value;
                    }

                    if ($field["REQUIRED"] && empty($value)) {
                        $field["ERROR"] = "Не заполнено";
                        $result = false;
                    }

                    //тут проверка полей
                    switch ($field["TYPE"]) {
                        case "tel":

                            //$pattern = '/^(\+375(?:29|44|33|25)\d{7})$/';
                            $pattern = '/^(\+\d{11,12})$/';

                            if (preg_match($pattern, $field["VALUE"]) == 0) {

                                $field["ERROR"] = 'Вы ввели номер телефона в неверном формате';
                                $result = false;

                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        return $result;
    }

    private function generateFormFields()
    {
        $result = Array();

        $arFormFields = Array(
            "PHONE" => Array(
                "TYPE" => "tel",
                "REQUIRED" => true,
                "PLACEHOLDER" => "+375291234567",
                "LABEL" => "Телефон +375 29 1234567",
                "WIDTH" => "6",
            ),
            "COMMENTS" => Array(
                "TYPE" => "hidden",
                "REQUIRED" => false,
                "LABEL" => "",
                "WIDTH" => "12",
                "VALUE" => "<a href='https://eurotrans.by" . $_SERVER["REQUEST_URI"] . "'>Маршрут</a>",
            ),
        );

        if (!empty($arFormFields)) {
            foreach ($arFormFields as $name => $field) {
                if (($field["TYPE"] == "text") || ($field["TYPE"] == "tel") || ($field["TYPE"] == "textarea") || ($field["TYPE"] == "hidden")) {

                    $result[$name] = Array(

                        "TYPE" => $field["TYPE"],
                        "REQUIRED" => (!empty($field["REQUIRED"]) && ($field["REQUIRED"] == 1)) ? "required" : "",
                        "PLACEHOLDER" => $field["PLACEHOLDER"],
                        "LABEL" => $field["LABEL"],
                        "WIDTH" => !empty($field["WIDTH"]) ? $field["WIDTH"] : 12,
                        "VALUE" => !empty($field["VALUE"]) ? $field["VALUE"] : ""

                    );

                } else {

                    $this->arResult["ERRORS"]["GLOBALS"] .= "<span style='color: red'>Задан не верный тип поля " . $name . ".</span><br/>";

                }
            }
        }

        return $result;
    }

    private function sendToCRM(){

        $queryFunction = 'crm.lead.add';

        $fields = Array();

        if (!empty($this->arParams["TITLE"])) {
            $fields["TITLE"] = $this->arParams["TITLE"];
        }

        foreach ($this->arResult['FORM_FIELDS'] as $name => $field) {
            if ($name == "PHONE") {

                $fields[$name] = Array('n0' => Array('VALUE' => $field["VALUE"]));

            } else {

                $fields[$name] = $field["VALUE"];

            }
        }

        $from = \travelsoft\eurotrans\Utils::getDeparturePointNameById($this->arParams['REQUEST']['location_from']);
        $to = \travelsoft\eurotrans\Utils::getArrivalPointNameByDepAndArrivId($this->arParams['REQUEST']['location_from'], $this->arParams['REQUEST']['location_to']);

        $fields["UF_CRM_1483795016"] = $from;
        $fields["UF_CRM_1483795308"] = $to;
        $fields["UF_CRM_1484130496"] = $this->arParams['REQUEST']['adults'] + $this->arParams['REQUEST']['children'] + $this->arParams['REQUEST']['social'];
        $fields["UF_CRM_1484130552"] = $this->arParams['REQUEST']['children'];
        $fields["UF_CRM_1483795809"] = !empty($this->arParams['REQUEST']['date_back']);
        $fields["UF_CRM_1524746103"] = $this->arParams['REQUEST']['date_from'];

        if(!empty($this->arParams['REQUEST']['date_back'])) {

            $fields["UF_CRM_1512034491"] = $to;
            $fields["UF_CRM_1512117684"] = $from;
            $fields["UF_CRM_1524746189"] = $this->arParams['REQUEST']['date_back'];
        }

        $queryData = http_build_query(array(
            'fields' => $fields,
            'params' => array("REGISTER_SONET_EVENT" => "Y")
        ));

        $result = $this->crmRequestWithResponse($queryFunction, $queryData);

        if (!empty($result["error_description"])) {

            $this->arResult["ERRORS"][] = $result["error_description"];

        } else {

            $this->arResult['MESSAGE_OK'] = "Спасибо. В ближайшее время мы свяжемся с Вами.";

        }

    }

    public function crmRequestWithResponse($queryFunction, $queryData = "")
    {
        if (!empty($queryFunction)) {

            $queryUrl = CRM_EUROTRANS;
            $queryUrl .= $queryFunction . '/';

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $queryUrl,
                CURLOPT_POSTFIELDS => $queryData,
            ));

            $result = curl_exec($curl);
            curl_close($curl);
            $result = json_decode($result, 1);
            return $result;

        }
        return false;
    }

    private function clearFieldsValues(){

        if(empty($this->arResult["ERRORS"])){

            foreach ($this->arResult["FORM_FIELDS"] as $name => &$field){

                if(isset($_GET[$name])){
                    unset($_GET[$name]);
                }
                if(isset($_POST[$name])){
                    unset($_POST[$name]);
                }

                $field["VALUE"] = "";

            }

        }

    }

    public function executeComponent()
    {
        $this->arResult["FORM_FIELDS"] = $this->generateFormFields();

        $this->arResult["HIDDEN_CHECK_STRING"] = trim($this->arParams["HIDDEN_CHECK_STRING"]);

        $this->arResult['FROM'] = \travelsoft\eurotrans\Utils::getDeparturePointNameById($this->arParams['REQUEST']['location_from']);
        $this->arResult['TO'] = \travelsoft\eurotrans\Utils::getArrivalPointNameByDepAndArrivId($this->arParams['REQUEST']['location_from'], $this->arParams['REQUEST']['location_to']);
        $this->arResult['DATE_FROM'] = $this->arParams['REQUEST']['date_from'];
        
        if(empty($this->arResult["ERRORS"]["GLOBALS"])) {

            if ($this->processPostForm()) {

                //Успешное заполнение всех полей
                $this->sendToCRM();

                $this->clearFieldsValues();

            }
            $this->IncludeComponentTemplate();

        }
        else{

            echo $this->arResult["ERRORS"]["GLOBALS"];

        }
    }
}
