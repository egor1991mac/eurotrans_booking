<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
$arComponentDescription = array(
    'NAME' => "Форма поиска элементов из инфоблока",
    'DESCRIPTION' => "Форма поиска элементов из инфоблока",
    'SORT' => 20,
    'CACHE_PATH' => 'Y',
    'PATH' => array('ID' => 'travelsoft')
);
