<?php

use travelsoft\eurotrans\Settings;

/**
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class TravelsoftEurotransBooking extends CBitrixComponent
{

    public function executeComponent()
    {

        \Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

        try {
            $request = \Bitrix\Main\Context::getCurrent()->getRequest();

            $timestamp = "";
            if (!empty($request->getQuery("ordernumber"))) {
                foreach ($_SESSION[Settings::sessionStoreId()] as $_timestamp => $session) {
                    if (in_array($request->getQuery("ordernumber"), $session["AR_BOOKING_ID"])) {
                        $timestamp = $_timestamp;
                        break;
                    }
                }
            } else if (!empty($_GET["id"])) {
                $timestamp = $_GET["id"];
            }

            if (empty($timestamp)) {
                $timestamp = key($_SESSION[Settings::sessionStoreId()]);
            }

            $this->arResult["BOOKINGS_ID"] = $_SESSION[Settings::sessionStoreId()][$timestamp]["AR_BOOKING_ID"];

            if (!empty($this->arResult["BOOKINGS_ID"])) {
                $this->arResult["MAIN_BOOKING_ID"] = \travelsoft\eurotrans\Utils::getMainBookingId($this->arResult["BOOKINGS_ID"]);
                $this->arResult["INFO_BOOKINGS"] = \travelsoft\eurotrans\Utils::GetInfoTourOnTicketId($this->arResult["BOOKINGS_ID"]);

                if (!empty($this->arResult["INFO_BOOKINGS"])) {

                    $this->arResult["INFO_BOOKINGS"] = \travelsoft\eurotrans\Utils::getPointsNameByID($this->arResult["INFO_BOOKINGS"]);

                    $total_price = 0;
                    $payment_method = '';
                    $payer["NAME"] = '';
                    $payer["FEMALE"] = '';
                    $payer["PHONE"] = '';
                    $payer["EMAIL"] = '';

                    $status_bool = true;
                    $payment_type = 'cash';
                    foreach ($this->arResult["INFO_BOOKINGS"] as &$booking) {

                        $nameSeats = Array();
                        foreach ($booking["arrSeatsId"] as $value) {
                            if (!empty($value["type"]) && ($value["type"] == "WITHOUT_SEATING")) {
                                array_push($nameSeats, "Без номера.");
                            } else {
                                array_push($nameSeats, $value["name"]);
                            }
                        }
                        $booking["implodeSeatsId"] = implode(", ", $nameSeats);

                        $total_price += $booking["price"];
                        $payment_method = $booking["payment_method"];
                        $payer["NAME"] = $booking["name"];
                        $payer["FEMALE"] = $booking["female"];
                        $payer["PHONE"] = $booking["phone"];
                        $payer["EMAIL"] = $booking["email"];

                        $booking["TRIP_INFO"] = travelsoft\eurotrans\Utils::getTripInfo($booking["bus_id"]);

                        if ($booking["status"] != "confirmed") {
                            $status_bool = false;
                        }

                        $payment_type = $booking['payment_type'] ?? $payment_type;
                    }

                    $this->arResult["PAYMENT_TYPE"] = $payment_type;
                    $this->arResult["PAYMENT_METHOD"] = !empty($payment_method) ? $payment_method : "cash";
                    $this->arResult["TOTAL_PRICE"] = $total_price;
                    $this->arResult["STATUS"] = array(
                        "VALUE" => $status_bool,
                        "TEXT" => $status_bool ? "Оплачено" : "Не оплачено"
                    );
                    $this->arResult["SHOW_FORM_BOOK_ONLINE"] = true;

                    foreach ($payer as $value) {
                        if (empty($value)) {
                            $this->arResult["SHOW_FORM_BOOK_ONLINE"] = false;
                        }
                    }

                    if ($this->arResult["SHOW_FORM_BOOK_ONLINE"]) {
                        $this->arResult["PAYER"] = $payer;
                    }
                }
            }

            $this->arResult['DATA_LAYER'] = $this->generateDataLayer();
            $this->arResult["IS_AGENT"] = !empty(\travelsoft\eurotrans\Utils::getApiAccessId()) ? true : false;

            $this->IncludeComponentTemplate();
            //unset($_SESSION[Settings::sessionStoreId()]);

        } catch (\Exception $ex) {

            (new travelsoft\eurotrans\Logger())
                ->write("Component: " . $this->__name . "; Error: " . $ex->getMessage());
            ShowError("Произошла ошибка в работе системы бронирования.");
        }
    }

    private function generateDataLayer()
    {
        $products = [];
        $total_price = 0;

        end($this->arResult['INFO_BOOKINGS']);
        $last_booking_id = key($this->arResult['INFO_BOOKINGS']);
        foreach ($this->arResult['INFO_BOOKINGS'] as $booking_id => $info_booking) {

            $first_key = key($info_booking['TRIP_INFO']['location_arr']);
            end($info_booking['TRIP_INFO']['location_arr']);
            $last_key = key($info_booking['TRIP_INFO']['location_arr']);

            foreach ($info_booking['arrSeatsId'] as $arSeat) {

                $ticket_type = '';
                switch ($this->getTicketsCategory($arSeat['seat_name'])) {
                    case 'adults':
                        $ticket_type = 'Взрослый';
                        break;
                    case 'children':
                        $ticket_type = 'Детский';
                        break;
                    case 'social':
                        $ticket_type = 'Льготный';
                        break;
                }

                if(isset($products[$arSeat['ticket_id']])){
                    $products[$arSeat['ticket_id']]['quantity'] = "" . ($products[$arSeat['ticket_id']]['quantity'] + 1) . "";
                }
                else {
                    $products[$arSeat['ticket_id']] = [
                        'name' => travelsoft\eurotrans\Utils::deleteBrackets($info_booking['departure']) . '-' . travelsoft\eurotrans\Utils::deleteBrackets($info_booking['arrival']) . ' ' . $ticket_type,
                        'id' => $first_key . '_' . $last_key . '_' . $arSeat['ticket_id'],
                        'price' => "" . $arSeat["price_2"] . "",        //Цена билета в рос рублях
                        'category' => travelsoft\eurotrans\Utils::deleteBrackets($info_booking['TRIP_INFO']['location_arr'][$first_key]['name']) . '-' .travelsoft\eurotrans\Utils::deleteBrackets( $info_booking['TRIP_INFO']['location_arr'][$last_key]['name']),    //Название главного маршрута
                        'brand' => $ticket_type,        //Используем для указания категории билета
                        'variant' => $info_booking['booking_datetime']['date'],        //Дата рейса
                        'quantity' => '1',
                        'coupon' => '',
                    ];
                    if((count($this->arResult['INFO_BOOKINGS']) > 1) && ($last_booking_id == $booking_id)) {
                        if ($arSeat['discount'] > 0) {
                            $products[$arSeat['ticket_id']]['coupon'] = 'round_trip';
                            $products[$arSeat['ticket_id']]['price'] = $products[$arSeat['ticket_id']]['price'] * (100 - $arSeat['discount']) / 100;
                        }
                    }
                    else{
                        $products[$arSeat['ticket_id']]['coupon'] = $arSeat['promotional_data_name'];
                        if(!empty($arSeat['amount_rub'])){
                            $products[$arSeat['ticket_id']]['price'] -= $arSeat['amount_rub'];
                        }
                        else if(!empty($arSeat['amount_percent'])){
                            $products[$arSeat['ticket_id']]['price'] = $products[$arSeat['ticket_id']]['price'] * (100 - $arSeat['amount_percent']) / 100;
                        }
                    }
                }
                $total_price += $products[$arSeat['ticket_id']]['price'];
            }
        }
        $products = array_values($products);

        $coupons = [];
        foreach ($products as $product){
            if(!empty($product['coupon'])) {
                if (!in_array($product['coupon'], $coupons)) {
                    array_push($coupons, $product['coupon']);
                }
            }
        }

        $arrDataLayer['ip'] = $_SERVER['REMOTE_ADDR'];
        $arrDataLayer['event'] = 'ecomm';
        $arrDataLayer['ecommerce']['currencyCode'] = 'RUB';

        $arrDataLayer['ecommerce']['purchase']['actionField'] = [
            'id' => $this->arResult['MAIN_BOOKING_ID'],
            'revenue' => "" . $total_price . "",
            'coupon' => implode(' | ', $coupons),
        ];
        $arrDataLayer['ecommerce']['purchase']['products'] = $products;

        return $arrDataLayer;
    }

    private function getTicketsCategory($seat_name)
    {

        $tc = Settings::tickesCategories();

        $vars = array(
            "adults", "children", "social"
        );

        $ticket_type = '';
        foreach ($vars as $var) {

            if (in_array($seat_name, $tc[$var])) {
                $ticket_type = $var;
            }

        }
        return $ticket_type;
    }
}
