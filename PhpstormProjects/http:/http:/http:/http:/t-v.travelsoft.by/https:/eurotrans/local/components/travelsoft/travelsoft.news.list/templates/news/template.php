<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams['IS_AJAXPAGES'] != "Y"):?>
<div class="news-wrapper-inner pages-items">
<?endif;?>
<?
	if($arParams['IS_AJAXPAGES'] == "Y"):
		$this->SetViewTarget("items");
	endif;
?>
<?
	foreach($arResult["ITEMS"] as $arItem):
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

		$path = Base::getEmptyPreviewPath();
		if(isset($arItem["DETAIL_PICTURE"]["SRC"])){
			$path = $arItem["DETAIL_PICTURE"]["SRC"];
		}
		
		$arPhoto = Base::imageResize(array(
				'WIDTH' => 575
				,'HEIGHT' => 540
				,'MODE' => 'inv' // cut, in, inv, width
			)
			, $path
		);
		unset($path);
		
		$isShowAction = false;
		if(strlen($arItem['PROPERTIES']['PROP_TYPE']['VALUE_XML_ID']) > 0){
			$isShowAction = true;
		}

	?>
		<article class="news-item big pages-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<a class="news-item-inner" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
			<div class="news-item-img">
				<div class="card-item-img-inner" style="background-image:url(<?=$arPhoto?>);"></div>
			</div>
			<div class="news-item-content">
			<?if($isShowAction == true):?>
			<div class="small-block <?=$arItem['PROPERTIES']['PROP_TYPE']['VALUE_XML_ID']?>">
				<span><?=$arItem['PROPERTIES']['PROP_TYPE']['VALUE_ENUM']?></span>
			</div>
			<?endif;?>
			<div class="news-item-content-inner">
				<time class="text-sm"><?=strtolower($arItem["DISPLAY_ACTIVE_FROM"])?></time>
				<div class="news-item-name"><?=$arItem["NAME"]?></div>
				<div class="text-2"><?=$arItem["PREVIEW_TEXT"];?></div>
			</div>
			</div>
		</a>
		</article>
<?endforeach;?>
<?
	if($arParams['IS_AJAXPAGES'] == "Y"):
		$this->EndViewTarget();
		$templateData['items'] = $APPLICATION->GetViewContent('items');
	endif;
?>
<?if($arParams['IS_AJAXPAGES']!="Y"):?>
</div>
<?endif;?>
<?
	if($arParams['IS_AJAXPAGES'] == "Y"):
		$this->SetViewTarget("pages");
	endif;
	
	if(intval($arResult['NAV_RESULT']->NavPageNomer) < intval($arResult['NAV_RESULT']->NavPageCount) ):
?>
<a 
	class="loader-pager btn btn-gray col-100 has-animation <?if($arParams['USE_AUTO_AJAXPAGES']=='Y'):?> auto<?endif;?>" 
	rel="nofollow" 
	href="#"
	data-ajaxurl="<?=$arResult['AJAXPAGE_URL']?>"
	data-ajaxpagesid="<?=$arParams['AJAXPAGESID']?>"
	data-navpagenomer="<?=($arResult['NAV_RESULT']->NavPageNomer)?>"
	data-navpagecount="<?=($arResult['NAV_RESULT']->NavPageCount)?>"
	data-navnum="<?=($arResult['NAV_RESULT']->NavNum)?>"
>
	<span>показать ещё новости</span>
</a>
<?
	endif;
	if($arParams['IS_AJAXPAGES']=="Y"):
		$this->EndViewTarget();
		$templateData['pages'] = $APPLICATION->GetViewContent('pages');
	endif;
?>
<?//_pr($arResult["ITEMS"]);?>