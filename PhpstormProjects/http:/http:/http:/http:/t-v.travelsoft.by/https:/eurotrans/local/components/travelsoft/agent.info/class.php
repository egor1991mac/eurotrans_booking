<?php

/*
 * Extantion for news.list component
 */


class  TravelsoftAgentInfoComponent extends CBitrixComponent
{
    private function userDm($data = [])
    {
        $data = (array)$data;
        echo "<pre>" . print_r($data, true) . "</pre>";
    }

    private function getAgentInfo(){

        global $USER;

        $user = CUser::GetByID($USER->GetID());

        $arUserFields = $user->arResult[0];

        $arFields = [
            'ID',
            'LOGIN',
            'NAME',
            'SECOND_NAME',
            'LAST_NAME',
            'EMAIL',
            'PERSONAL_MOBILE',
            'UF_LEGAL_ADDRESS',
            'UF_BANK_DETAIS',
            'UF_CONTRACT_FORM',
            'UF_AGENT_NAME',
        ];

        $arValues = array_intersect_key($arUserFields, array_flip($arFields));

        $arAgentFields = \travelsoft\eurotrans\Utils::getAgentInfo();
        if(!empty($arAgentFields)){
            $arValues['COMMISSION'] = $arAgentFields['commission'];
        }

        $this->arResult['AGENT'] = $arValues;
    }

    public function executeComponent()
    {
        Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

        $this->getAgentInfo();

        $this->includeComponentTemplate();
    }
}