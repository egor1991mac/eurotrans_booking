<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("LOST_AND_FOUND"),
	"DESCRIPTION" => GetMessage("LOST_AND_FOUND_DESC"),
	"ICON" => "/images/include.gif",
	"PATH" => array(
		"ID" => "travelsoft",
	),
);
?>