<?php
$MESS = [
    'TRAVELSOFT_EUROTRANS_ADULTS_CONTENT' => '',
    'TRAVELSOFT_EUROTRANS_CHILDREN_CONTENT' => 'Детский:<br>Дети: до 12 лет включительно;',
    'TRAVELSOFT_EUROTRANS_SOCIAL_CONTENT' => 'Льготный:<br>Студенты: до 25 лет + студенческий;<br>Пассажиры: старше 60 лет;<br>Инвалиды: I, II, III группы;',
];