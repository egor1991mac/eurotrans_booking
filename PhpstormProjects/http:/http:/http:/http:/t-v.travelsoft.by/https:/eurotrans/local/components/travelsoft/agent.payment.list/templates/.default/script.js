function hideBookings() {
    $('#body-agent-payments').html(generateWaitBookingsHtml());
}

function generateWaitBookingsHtml(){
    return `<div class="text-1">Ожидайте...</div>`
}

function triggerItem(element) {
    let trigger = $(element);
    let textCont = trigger.find('span');
    let target = trigger.parent().find('.dropdown-target');
    let item = target.find('.sort-select-item a');

    trigger.toggleClass('active');

    $(document).on('mouseup', function (e) {
        if (!trigger.is(e.target) && trigger.has(e.target).length === 0) {
            trigger.removeClass('active');
        }
    });
}

function isActive(element) {
    return element.active;
}

let currentPage = 1;
let pageCount = 10;
let statusList = [
    {active: true, text: "Смотреть все", value: 'all'},
    {active: false, text: "Оплачены", value: 'confirmed'},
    {active: false, text: "Не оплачены", value: 'cancelled'},
];

let activeStatus = statusList.find(isActive);

function changeStatus(item){
    activeStatus.active = false;
    activeStatus = statusList.find(function(element){
        return element.value === item.dataset.value;
    });
    activeStatus.active = true;
    getPayments();
}

function getPayments() {
    $.ajax({
        type: 'post',
        url: '/local/components/travelsoft/agent.payment.list/ajax/getPayments.php',
        dataType: "json",
        data: {current_page: currentPage, page_count: pageCount, status: activeStatus.value},
        cache: false,
        success: function (data) {
            currentPage = data.current_page;
            $('#body-agent-payments').html(generateAgentPaymentHtml(data));
        },
        beforeSend: function (xhr) {

        }
    });
}

function setPage(page) {
    currentPage = page;
    getPayments();
}

$(document).ready(
    getPayments()
);

function generateAgentPaymentHtml(data) {

    if(data.count === 0){
        return `<div class="content-column-wrap content-small">
            <div class="content-column-content">
                <div class="content-column-content-inner">
                    <p>У вас ещё нет выставленных счетов.</p>
                </div>
            </div>`;
    }
    else {
        return `${(function (data) {

            let html = `<div class="table-navigation">
            <div class="table-navigation-links"></div>
            <div class="table-navigation-nav">
                <div class="table-pagination">
                    ${(function (count_pages, current_page) {

                let html = `<a class="table-pagination-arr left ${current_page === 1 ? 'disabled' : ''}" onclick="setPage(${current_page - 1})">
                                <i class="arr-down">
                                    <svg class="icon icon-drop">
                                        <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                            <div class="table-pagination-inner">`;

                for (let i = 1; i <= count_pages; i++) {
                    html += `<a class="table-pagination-item ${current_page === i ? 'active' : ''}" onclick="setPage(${i})">${i}</a>`;
                }
                html += `</div>
                            <a class="table-pagination-arr right ${current_page === count_pages ? 'disabled' : ''}"  onclick="setPage(${current_page + 1})">
                                <i class="arr-down">
                                    <svg class="icon icon-drop">
                                        <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>`;

                return html;
            })(data.count_pages, data.current_page)}
                </div>
                ${(function () {
                return `<div class="table-collapse">
                        <div class="table-navigation-link has-select js-select-item" onclick="triggerItem(this)">
                            <span>${activeStatus.text}</span>
                            <i class="arr-down">
                                <svg class="icon icon-drop">
                                    <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                            <div class="dropdown-target">
                                <div class="dropdown-inner">
                                    <div class="dropdown-content">
                                        ${(function () {
                    let html = '';
                    statusList.forEach(function (item) {
                        html += `<div class="sort-select-item ${item.active ? 'active' : ''}">
                                                    <a class="active" data-value="${item.value}" data-text="${item.text}" 
                                                        onclick="changeStatus(this)">${item.text}</a>
                                                </div>`;
                    });
                    return html;
                })()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
            })()}
            </div>
        </div>
        <div class="table table-tickets sold">
            <div class="table-head">
                <div class="table-item table-num">
                    <span>№</span>
                </div>
                <div class="table-item">
                    <span>
                        Дата
                        <br>отчёта
                    </span>
                </div>
                <div class="table-item"> 
                    <span>
                        Даты
                        <br>продаж
                    </span>
                </div>
                <div class="table-item">
                    <span>
                        Стоимость
                        <br>билетов
                    </span>
                </div>
                <div class="table-item">
                    <span>
                        Кол-во
                        <br>билетов
                    </span>
                </div>
                <div class="table-item">
                    <span>
                        Комиссия
                    </span>
                </div>
                <div class="table-item">
                    <span>
                        Штраф
                    </span>
                </div>
                <div class="table-item">
                    <span>
                        К выплате
                    </span>
                </div>
                <div class="table-item">
                    <span>
                        Статус
                        <br>оплаты
                    </span>
                </div>
                <div class="table-item table-actions">
                    <span>
                        Скачать
                        <br>платежку
                    </span>
                </div>
            </div>`;

            data.payments.forEach(function (payment) {
                html += `<div class="table-row">
                <div class="table-item table-num">
                    <div class="text-1">${payment.id}</div>
                </div>
                <div class="table-item">
                    <div class="text-1">${payment.invoice_date}</div>
                </div>
                <div class="table-item">
                    <div class="text-1">${payment.start_date} - ${payment.end_date}</div>
                </div>
                <div class="table-item">
                    <div class="text-bold org">${payment.ticket_price} BYN</div>
                </div>
                <div class="table-item">
                    <div class="text-1">${payment.quantity}</div>
                </div>
                <div class="table-item">
                    <div class="text-bold org">${payment.commission} BYN</div>
                </div>
                <div class="table-item">
                    <div class="text-bold org">${payment.fine} BYN</div>
                </div>
                <div class="table-item">
                    <div class="text-bold org">${(parseFloat(payment.ticket_price) - parseFloat(payment.commission) + parseFloat(payment.fine))} BYN</div>
                </div>
                <div class="table-item">
                     ${(function (status) {

                    if (status === 'confirmed') {

                        return `<div class="text-bold yep"></div>`;
                    }
                    else {
                        return `<div class="text-bold nope"></div>`;
                    }
                })(payment.status)}
                </div>
                <div class="table-item table-actions">
                    <a class="btn btn-extrasmall" href="/local/modules/travelsoft.eurotrans.booking/pages/download.agent_payment.php?payment_id=${payment.id}&file_name=${payment.file_link}">
                        <span>Скачать</span>
                    </a>
           
                    ${(function (act_link) {
                    if (act_link !== null) {
                        return `<a class="btn btn-extrasmall" href="/local/modules/travelsoft.eurotrans.booking/pages/download.agent_act.php?payment_id=${payment.id}&file_name=${payment.act_link}">
                                <span>Скачать акт</span>
                            </a>`;
                    }
                    else {
                        return ``;
                    }
                })(payment.act_link)}
                    
                </div>
            </div>`;
            });

            html += `</div>`;

            return html;
        })(data)}`;
    }
}