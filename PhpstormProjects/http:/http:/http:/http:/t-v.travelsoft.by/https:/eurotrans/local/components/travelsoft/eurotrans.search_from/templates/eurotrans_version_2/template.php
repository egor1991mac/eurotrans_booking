<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$this->setFrameMode(true);
?>
<div class="promo-form">
    <form autocomplete="off" class="js-validate input-form" id="main-search-tickets-from" method="get" action="<?= $arParams['SEARCH_TICKETS_PAGE'] ?>">
        <div class="input-item s100">
            <div class="input-wrapper">
                <div class="select-item">
                    <label class="select-label js-condition">
                        <span class="select-main">
                            <input id="round-trip" class="select-real" type="checkbox" value="1" name="eurotrans[round_trip]">
                            <span class="select-checked"></span>
                        </span>
                        <span class="select-text"><?= GetMessage('TRAVELSOFT_EUROTRANS_ROUND_TRIP_TITLE');?></span>
                    </label>
                </div>
            </div>
        </div>
        <div class="input-item s50">
            <div class="input-wrapper">
                <div class="label"><?= GetMessage('TRAVELSOFT_EUROTRANS_LOCATION_FROM_TITLE');?></div>

                <div class="select-check js-select-custom depends-on">
                    <div class="selects" data-placeholder="<?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE')?>">
                        <input type="text" autocomplete="off"
                               data-search="location_from"
                               class="search-input" placeholder="<?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE')?>"/>
                        <i class="arr-down">
                            <svg class="icon icon-drop">
                                <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </div>
                    <div class="dropdown-target">
                        <div class="dropdown-inner">
                            <div class="dropdown-content">
                                <div class="select-u-body select-list">
                                    <? foreach ($arResult['DEPARTURE_POINTS'] as $location): ?>
                                        <label class="option">
                                            <input type="radio" name="eurotrans[location_from]" value="<?= $location['id'] ?>"
                                                   data-name="<?=$location['name']?>">
                                            <span class="location-from-title"><?= $location['name'] ?></span>
                                        </label>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="input-item s50">
            <div class="input-wrapper">
                <div class="label"><?= GetMessage('TRAVELSOFT_EUROTRANS_LOCATION_TO_TITLE')?></div>
                <div class="select-check js-select-custom depends-on">
                    <div class="selects" data-placeholder="<?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE')?>">
                        <input type="text" autocomplete="off"
                               data-search="location_to"
                               class="search-input" placeholder="<?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE')?>">
                        <i class="arr-down">
                            <svg class="icon icon-drop">
                                <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </div>
                    <div class="dropdown-target">
                        <div class="dropdown-inner">
                            <div class="dropdown-content">
                                <div class="select-u-body select-list text-center" id="location_to-box">
                                    <img src="<?= $templateFolder . '/images/preloader.gif' ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="input-item s50">
            <div class="input-wrapper disabled js-condition-target">
                <label><?= GetMessage('TRAVELSOFT_EUROTRANS_DATE_FROM_TITLE')?></label>
                <input id="date-from" name="eurotrans[date_from]" class="input-main datepicker-mtfs" type="text" placeholder="-- <?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE')?> --"  readonly>
                <svg class="icon icon-drop">
                    <use xlink:href="#calendar" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </div>
        </div>
        <div class="input-item s50">
            <div class="input-wrapper disabled js-condition-target">
                <label><?= GetMessage('TRAVELSOFT_EUROTRANS_DATE_TO_TITLE')?></label>
                <input id="date-back" name="eurotrans[date_back]" class="input-main datepicker-mtfs" type="text" placeholder="-- <?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE')?> --"  readonly >
                <svg class="icon icon-drop">
                    <use xlink:href="#calendar" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </div>
        </div>
        <div class="input-item s33">
            <div class="input-wrapper">
                <div class="label"><?= GetMessage('TRAVELSOFT_EUROTRANS_ADULTS_TITLE')?></div>
                <div id="adults-box" class="select-check js-select-custom disabled">
                    <button class="selects" data-placeholder="0">
                        <span class="btn-text"><?= GetMessage('TRAVELSOFT_EUROTRANS_CHOOSE_TITLE')?></span>
                        <i class="arr-down">
                            <svg class="icon icon-drop">
                                <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                    <div class="dropdown-target">
                        <div class="dropdown-inner">
                            <div class="dropdown-content">
                                <div class="select-u-body select-list text-center" >
                                    <?foreach (array(0,1,2,3,4,5,6) as $i):?>
                                        <label class="option">
                                            <input type="radio" name="eurotrans[adults]" value="<?= $i ?>">
                                            <span class="adults-title"><?= $i ?></span>
                                        </label>
                                    <?endforeach?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="input-item s33">
            <div class="input-wrapper">
                <div class="label">
                    <?= GetMessage("TRAVELSOFT_EUROTRANS_CHILDREN_TITLE")?>

                    <div class="roadmap-item-point road-popup" style="padding-left: 0;">
                        <div class="roadmap-item-point-inner dropdown-trigger">
                            <div class="question"></div>
                            <div class="dropdown-target arrow-bottom">
                                <div class="dropdown-inner">
                                    <div class="dropdown-content">
                                        <div class="roadmap-item-description">
                                            <div class="roadmap-item-text">

                                                <?= GetMessage("TRAVELSOFT_EUROTRANS_CHILDREN_CONTENT")?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="children-box" class="select-check js-select-custom disabled">
                    <button class="selects" data-placeholder="0">
                        <span class="btn-text"><?= GetMessage("TRAVELSOFT_EUROTRANS_CHOOSE_TITLE")?></span>
                        <i class="arr-down">
                            <svg class="icon icon-drop">
                                <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                    <div class="dropdown-target">
                        <div class="dropdown-inner">
                            <div class="dropdown-content">
                                <div class="select-u-body select-list text-center" >
                                    <?foreach (array(0,1,2,3,4,5,6) as $i):?>
                                        <label class="option">
                                            <input type="radio" name="eurotrans[children]" value="<?= $i ?>">
                                            <span class="children-title"><?= $i ?></span>
                                        </label>
                                    <?endforeach?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="input-item s33">
            <div class="input-wrapper">
                <div class="label">
                    <?= GetMessage("TRAVELSOFT_EUROTRANS_SOCIAL_TITLE")?>

                    <div class="roadmap-item-point road-popup" style="padding-left: 0;">
                        <div class="roadmap-item-point-inner dropdown-trigger">
                            <div class="question"></div>
                            <div class="dropdown-target arrow-bottom">
                                <div class="dropdown-inner">
                                    <div class="dropdown-content">
                                        <div class="roadmap-item-description">
                                            <div class="roadmap-item-text">

                                                <?= GetMessage("TRAVELSOFT_EUROTRANS_SOCIAL_CONTENT")?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="social-box" class="select-check js-select-custom disabled">
                    <button class="selects" data-placeholder="0">
                        <span class="btn-text"><?= GetMessage("TRAVELSOFT_EUROTRANS_CHOOSE_TITLE")?></span>
                        <i class="arr-down">
                            <svg class="icon icon-drop">
                                <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                    <div class="dropdown-target">
                        <div class="dropdown-inner">
                            <div class="dropdown-content">
                                <div class="select-u-body select-list text-center" >
                                    <?foreach (array(0,1,2,3,4,5,6) as $i):?>
                                        <label class="option">
                                            <input type="radio" name="eurotrans[social]" value="<?= $i ?>">
                                            <span class="social-title"><?= $i ?></span>
                                        </label>
                                    <?endforeach?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-footer">
            <button class="btn btn-colored" type="sumbit">
                <span><?= GetMessage('TRAVELSOFT_EUROTRANS_BTN_TITLE');?></span>
            </button>
            <a class="link-border" href="<?= $arParams['ROUTE_LIST_PAGE'] ?>">
                <span><?= GetMessage('TRAVELSOFT_EUROTRANS_ALL_TRIPS_TITLE');?></span>
            </a>
        </div>
    </form>
</div>

<script>

    if (typeof window.eurotrans !== 'object') { window.eurotrans = {}; }

    window.eurotrans.storage = {
        sessid: "<?= bitrix_sessid() ?>",
        dates: <? if (!empty($arResult['DATES'])) {
        echo json_encode($arResult['DATES']);
    } else {
        echo "{}";
    } ?>,
        locationsTo: <? if (!empty($arResult['ARRIVAL_POINTS'])) {
        echo json_encode($arResult['ARRIVAL_POINTS']);
    } else {
        echo "{}";
    } ?>,
        request: <? if (!empty($arParams['REQUEST'])) {
        echo json_encode($arParams['REQUEST']);
    } else {
        echo "{}";
    } ?>,
        urls: {
            locationsTo: "<?= $this->__component->__path ?>/ajax/arrivalpoints.php",
            dates: "<?= $this->__component->__path ?>/ajax/dates.php"
        }
    };

</script>