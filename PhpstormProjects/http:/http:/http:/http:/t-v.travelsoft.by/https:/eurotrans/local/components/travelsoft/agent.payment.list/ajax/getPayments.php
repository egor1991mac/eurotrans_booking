<?php
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PUBLIC_AJAX_MODE", true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');
Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

$data = $_POST;
$payments = travelsoft\eurotrans\Utils::getAgentPayments($data);

travelsoft\eurotrans\Utils::jsonResponse(json_encode($payments));