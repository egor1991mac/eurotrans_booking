<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalCss(SITE_TEMPLATE_PATH."/assets/plugins/slider-pro/slider-pro.css");
$this->addExternalJS(SITE_TEMPLATE_PATH."/assets/plugins/slider-pro/jquery.sliderPro.min.js");

if ($arResult["PROPERTIES"]["WIDE_ARTICLE_DESIGN"]["VALUE"] === "Y") {
	$APPLICATION->AddViewContent("additional_class", "ts-bg-gray");

}

$img = getSrc($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["VALUE"], array('width' => 1140, 'height' => 550));
if(empty($img[0])){
    $img = getSrc($arResult["DISPLAY_PROPERTIES"]["PHOTO3"]["VALUE"], array('width' => 1140, 'height' => 550));
}
?><section id="ts-section-new-article">
<article class="ts-col-24">
    <?if(!empty($img[0])):?>
        <div class="ts-row header-article" style="background: url(<?=$img[0]?>); background-size:cover;" alt="<?=$arResult["NAME"]?>" >
            <div class="ts-col-24 ts-height-100 header-text ts-align-items__flex-end ts-p-5 ts-p-sm-10">
                <div class="ts-row">
                    <div class="ts-d-none ts-d-sm-flex ts-px-2 info ts-flex-direction__column ts-flex-direction-sm__row">
                        <div class="ts-d-block  ts-p-2 ts-mr-sm-1 ts-mb-1 ts-mb-sm-0 ts-justify-content__center">
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["AUTHOR"]["VALUE"]) && isset($arResult["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"]) && !empty($arResult["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"])):?>
                                <?=$arResult["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"]?>

                            <?endif?>
                        </div>
                        <div class="ts-d-block ts-p-2 ts-mr-sm-1 ts-mb-1 ts-mb-sm-0  ts-justify-content__center">
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["THEME"]["VALUE"])):?>
                                <?=strip_tags($arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"])?>
                            <?endif?>
                        </div>
                        <div class="ts-d-block ts-p-2 ts-justify-content__center">
                            <?=$arResult["DISPLAY_ACTIVE_FROM"]?>
                        </div>
                    </div>
                <div class="ts-col-24">
                <h1><?=$arResult["NAME"]?></h1>
                </div>
                    <div class="ts-col-24">
                <small><?=$arResult["~PREVIEW_TEXT"]?></small>
                    </div>
                </div>
            </div>

        </div>
    <?endif?>
    

<div class="ts-content ts-col-24  ts-mx-auto">
  
                <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DESC1"]["VALUE"]["TEXT"]) || !empty($arResult["DISPLAY_PROPERTIES"]["DESC2"]["VALUE"]["TEXT"]) || !empty($arResult["DISPLAY_PROPERTIES"]["DESC3"]["VALUE"]["TEXT"])):?>
                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DESC1"]["VALUE"]["TEXT"])):?>
                        <?=$arResult["DISPLAY_PROPERTIES"]["DESC1"]["DISPLAY_VALUE"]?>
                        
                        <?if(isset($arResult["SLIDER1"]) && !empty($arResult["SLIDER1"])):?>
                            <div class="main-slider slider-pro" id="main-slider1" data-slider-width="78%" data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false">
                                <div class="sp-slides">
                                    <?foreach ($arResult["SLIDER1"] as $k=>$slide):?>
                                        <?$slide_desc = $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["DESCRIPTION"][$k];?>
                                        <!-- Slide -->
                                        <div class="sp-slide">
                                            <img class="sp-image" src="<?=$slide?>" alt="<?=!empty($slide_desc) ? $slide_desc : 'slide-'.$k+1?>"/>
                                            <?if(!empty($slide_desc)):?>
                                                <div class="main-slider__info sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <section class="b-post b-post_slider clearfix">
                                                                    <div class="entry-main">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title hov-title"><a class="hov-title__inner"><?=$slide_desc?></a></h2>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?endif?>
                                        </div>
                                    <?endforeach;?>
                                </div>
                            </div>
                        <?endif?>
                    <?endif?>
                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DESC2"]["VALUE"]["TEXT"])):?>
                        
                            <?=$arResult["DISPLAY_PROPERTIES"]["DESC2"]["DISPLAY_VALUE"]?>
                      
                        <?if(isset($arResult["SLIDER2"]) && !empty($arResult["SLIDER2"])):?>
                            <div class="main-slider slider-pro" id="main-slider2" data-slider-width="78%" data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false">
                                <div class="sp-slides">
                                    <?foreach ($arResult["SLIDER2"] as $k=>$slide):?>
                                        <?$slide_desc = $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO2"]["DESCRIPTION"][$k];?>
                                        <!-- Slide -->
                                        <div class="sp-slide">
                                            <img class="sp-image" src="<?=$slide?>" alt="<?=!empty($slide_desc) ? $slide_desc : 'slide-'.$k+1?>"/>
                                            <?if(!empty($slide_desc)):?>
                                                <div class="main-slider__info sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <section class="b-post b-post_slider clearfix">
                                                                    <div class="entry-main">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title hov-title"><a class="hov-title__inner"><?=$slide_desc?></a></h2>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?endif?>
                                        </div>
                                    <?endforeach;?>
                                </div>
                            </div>
                        <?endif?>
                    <?endif?>
                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["DESC3"]["VALUE"]["TEXT"])):?>
                       
                          <?=$arResult["DISPLAY_PROPERTIES"]["DESC3"]["DISPLAY_VALUE"]?>
                        
                    <?endif?>
                <?else:?>
                  
                        <?=$arResult["~DETAIL_TEXT"]?>
                    
                        <?if(isset($arResult["SLIDER1"]) && !empty($arResult["SLIDER1"])):?>
                            <div class="main-slider slider-pro" id="main-slider1" data-slider-width="78%" data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false" style="margin: 0">
                                <div class="sp-slides">
                                    <?foreach ($arResult["SLIDER1"] as $k=>$slide):?>
                                        <?$slide_desc = $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["DESCRIPTION"][$k];?>
                                        <!-- Slide -->
                                        <div class="sp-slide">
                                            <img class="sp-image" src="<?=$slide?>" alt="<?=!empty($slide_desc) ? $slide_desc : 'slide-'.$k+1?>"/>
                                            <?if(!empty($slide_desc)):?>
                                                <div class="main-slider__info sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <section class="b-post b-post_slider clearfix">
                                                                    <div class="entry-main">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title hov-title"><a class="hov-title__inner"><?=$slide_desc?></a></h2>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?endif?>
                                        </div>
                                    <?endforeach;?>
                                </div>
                            </div>
                        <?endif?>

                <?endif?>

            

        <!-- end .post-->
    </div>
</div>
</article>
</section>
<!--
<div class="col-md-12">
<div class="entry-footer ts-bg-white ts-custom-conatiner">
                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["THEME"]["VALUE"])):?>
                        <div class="post-tags ts-d-flex ts-mx-2"><span class="entry-footer__title"><?=GetMessage('THEME')?></span>
                            <ul class="list-tags list-tags_grey list-unstyled ts-mx-1">
                                <?if(isset($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"]) && !empty($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"])):?>
                                    <?foreach ($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"] as $theme):?>
                                        <li class="list-tags__item"><a class="list-tags__link btn btn-default" href="<?=$theme["DETAIL_PAGE_URL"]?>"><?=$theme["NAME"]?></a></li>
                                    <?endforeach;?>
                                <?else:?>
                                    <?if(is_array($arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"])):?>
                                        <?=implode2('</li>, <li class="list-tags__item">',$arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"])?>
                                    <?else:?>
                                        <li class="list-tags__item"><?=$arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"]?></li>
                                    <?endif?>
                                <?endif?>
                            </ul>
                        </div>
                    <?endif?>
                    <div class="entry-footer__social ts-d-flex ts-mx-2">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.share",
                            "main",
                            Array(
                                "HANDLERS" => array("facebook","twitter","vk"),
                                "HIDE" => "N",
                                "PAGE_TITLE" => $APPLICATION->GetTitle(),
                                "PAGE_URL" => $_SERVER["REQUEST_URI"],
                                "SHORTEN_URL_KEY" => "",
                                "SHORTEN_URL_LOGIN" => ""
                            )
                        );?>
                    </div>
                </div> -->
<script>
    $( document ).ready(function() {
       
        $(".entry-footer .list-tags li").each(function () {
                if($(this).find('a').hasClass('list-tags__link') === false){
                    $(this).find('a').addClass('list-tags__link btn btn-default');
                }
            }
        );
    });
</script>