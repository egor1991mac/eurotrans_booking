/**
 * Eurotrans booking of tickets
 *
 * @author dimabresky https://github.com/dimabresky
 * @copyright 2017, travelsoft
 */




BX.ready(function () {

    var form = BX("booking-details");

    // включение спинера на кнопке
    function __spinerEnable(el) {
        BX.addClass(el, "has-animation");
        BX.addClass(el, "animated");
    }

    // выключение спинера на кнопке
    function __spinerDisable(el) {
        BX.removeClass(el, "has-animation");
        BX.removeClass(el, "animated");
    }

    var birthday = BX.findChildren(form, function (el) {

        return typeof el.dataset !== "undefined" && el.dataset.birthday === "true";

    }, true);

    $(document).ready(function () {
        $(birthday).mask('99.99.9999');
    });

    /* Fields for auto fill fields payer. */
    var nameFirstPassenger = BX.findChild(form, {attribute: {name: 'eurotrans[passangers][1][name]'}}, true);
    var lastNameFirstPassenger = BX.findChild(form, {attribute: {name: 'eurotrans[passangers][1][last_name]'}}, true);

    BX.bind(nameFirstPassenger, "bxchange", function () {
        BX.findChild(form, {attribute: {name: 'eurotrans[payer][name]'}}, true).value = nameFirstPassenger.value;
    });

    BX.bind(lastNameFirstPassenger, "bxchange", function () {
        BX.findChild(form, {attribute: {name: 'eurotrans[payer][last_name]'}}, true).value = lastNameFirstPassenger.value;
    });

    //Type document for change status series
    /*var typeDocuments = BX.findChildren(form, function (el) {

        return /^eurotrans\[passangers\]\[(?:\d*)\]\[documentType\]$/.test(el.name);
        
    }, true);*/

    /*
        typeDocuments.forEach(function (typeDocument) {

            BX.bind(typeDocument, "bxchange", function (e) {
                var select = $(this);

                var optionSelected = select.find(':selected');
                var groupNumber = select.data('groupNumber');

                var dataSeries = optionSelected.data('series');

                var inputSeries = BX.findChild(form, {attribute: {name: 'eurotrans[passangers][' + groupNumber + '][series]'}}, true);

                if(dataSeries === 1){
                    inputSeries.removeAttribute('disabled');
                }
                else {
                    inputSeries.setAttribute('disabled', 'disabled');
                }
            });
            BX.fireEvent(BX(typeDocument), 'change');
        });
    */
    /*
        typeDocuments.forEach(function (typeDocument) {

            BX.bind(BX(typeDocument), "bxchange", function () {

                var checkbox = $(this);

                var groupNumber = checkbox.data('groupNumber');

                var dataSeries = checkbox.data('series');

                var inputSeries = BX.findChild(form, {attribute: {name: 'eurotrans[passangers][' + groupNumber + '][series]'}}, true);

                if(dataSeries === 1){
                    inputSeries.removeAttribute('disabled');
                }
                else {
                    inputSeries.setAttribute('disabled', 'disabled');
                }
            });
          //  BX.fireEvent(BX(typeDocument), 'change');
        });*/

    // validator
    BX.bind(form, 'submit', function (e) {

        var form_elements = BX.findChildren(this, {attribute: {"data-need-validation": "yes"}}, true);

        var validators, error_areas, hasErrors = false, errors = [], isChecked = false, inputs,
            errorsArea = BX.findChildren(this, {className: "error-area"}, true);

        __spinerEnable(BX("eurotrans-booking-btn"));

        var i;
        for (i = 0; i < errorsArea.length; i++) {
            errorsArea[i].innerHTML = "";
        }

        for (i = 0; i < form_elements.length; i++) {

            errors = [];
            validators = form_elements[i].dataset.validators.split("|");

            for (var j = 0; j < validators.length; j++) {
                switch (validators[j]) {

                    case "is_empty":

                        if (!form_elements[i].value.length) {

                            errors.push('Поле не может быть пустым');
                        }

                        break;

                    case "is_phone":

                        if (!/^([\+]+)*[0-9\x20\x28\x29\-]{5,20}$/.test(form_elements[i].value)) {

                            errors.push('Введите телефон в указанном формате');
                        }

                        break;

                    case "is_email":


                        if (!/^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/.test(form_elements[i].value)) {
                            errors.push('Введите коректный email');
                        }

                        break;

                    case "custom_select_is_selected":

                        inputs = BX.findChildren(this, {
                            tag: "input",
                            attribute: {name: form_elements[i].dataset.validationFn}
                        }, true);

                        isChecked = false;

                        for (var k = 0; k < inputs.length; k++) {

                            if (inputs[k].checked) {
                                isChecked = true;
                                break;
                            }

                        }

                        if (!isChecked) {
                            errors.push('Поле не выбрано');
                        }

                        break;
                }
            }

            if (errors.length) {
                hasErrors = true;
                BX("error-area-" + form_elements[i].dataset.linkErrorArea).innerHTML = errors.join("<br>");
            }

        }

        if (hasErrors) {
            error_areas = BX.findChildren(this, {className: "error-area"}, true);
            for (i = 0; i < error_areas.length; i++) {
                if (error_areas[i].innerHTML !== "") {
                    var element = error_areas[i].dataset.scrolltoid;
                    BX.scrollToNode(BX(element));
                    e.preventDefault();
                    __spinerDisable(BX("eurotrans-booking-btn"));
                    return;
                }
            }

        }

        let birthDays = BX.findChildren(form, function (el) {
            return /^eurotrans\[passangers\]\[(?:\d*)\]\[birthday\]$/.test(el.name);
        }, true);
        let isPresentAdults = false;
        birthDays.forEach(function (element) {
            let value = $(element).val();
            let parts = value.split('.');
            let birthday = new Date(parts[2], parts[1] - 1, parts[0]);
            let year = birthday.getFullYear();
            let month = birthday.getMonth();
            let day = birthday.getDate();
            let date = new Date(year + 12, month, day);

            if (date <= new Date()) {
                isPresentAdults = true;
            }
        });

        if (!isPresentAdults) {
            e.preventDefault();
            __spinerDisable(BX("eurotrans-booking-btn"));
            showPopupWithoutAdults();
        }
    });
});

function closePopupWithoutAdults() {
    $('#popup-without-adults').remove();
}

function getSearchParameters() {
    let prmstr = window.location.search.substr(1);
    return prmstr !== null && prmstr !== "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray(prmstr) {
    let params = {};
    let prmarr = prmstr.split("&");
    for (let i = 0; i < prmarr.length; i++) {
        let tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}

let params = getSearchParameters();

$(document).ready(function () {

    //Проверка на хитрость
    $('#input-payer-phone').on('change', function () {

        if (!userGroups.includes("9")) {

            let json_request = {sessid: BX.bitrix_sessid(), phone: this.value, timestamp: params.id};

            BX.ajax.get("/local/components/travelsoft/eurotrans.booking/ajax/check_on_booking.php", json_request, function (json_resp) {
                let resp = JSON.parse(json_resp);

                if (resp.exist === true) {
                    let text = `У Вас есть  активное бронирование, следующее бронирование будет доступно после оплаты брони на ${resp.booking_datetime}.
                    <a href="${resp.link}">Пожалуйста, оплатите.</a>`;

                    showPopup(text);

                    if (!userGroups.includes("8")) {
                        disableBooking();
                    }
                }
            });
        }
    });
});

function disableBooking() {
    $('#booking-details').on('keyup keypress', function (e) {
        let keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    }).on('submit', function () {
        return false;
    });

    $('#eurotrans-booking-btn-submit').attr("disabled", "disabled").addClass('disabled');
}

function showPopup(text) {
    let html = `<div class="modal-layout active" id="popup-original">
                    <div class="modal-container" style="padding: 2.125rem 2.125rem; margin: 5rem 0; box-shadow: 0 0 0 10000px #00000070; max-height: 70%; overflow-y: auto;">
                        <div class="modal-container-header">
                            <i class="small-link pop-close" onclick="closePopup('#popup-original')"></i>
                        </div>
                        <div class="modal-container-content">
                            <div class="text-sm" style="color: black">${text}</div>
                        </div>
                    </div>
                </div>`;

    $('body').append(html);
}

function closePopup(id) {
    $(id).remove();
}

function showPopupWithoutAdults() {

    let html = `<div class="modal-layout active" id="popup-without-adults">
                    <div class="modal-container" style="padding: 2.125rem 2.125rem; margin: 5rem 0; box-shadow: 0 0 0 10000px #00000070; max-height: 70%; overflow-y: auto;">
                        <div class="modal-container-header">
                            <div class="text-medium">Несовершеннолетние (до 12 лет) не могут отправляться</div>
                            <i class="small-link pop-close" onclick="closePopupWithoutAdults()"></i>
                        </div>
                        <div class="modal-container-content">
                            <div class="text-sm" style="color: black">
                                В  соответствии со статьей 12, Закона Республики Беларусь от 20 сентября  2009 г. № 49-3, граждане, не достигшие четырнадцати лет, если они не  приобрели дееспособность в полном объеме в результате заключения брака  или объявления полностью дееспособными, могут выезжать из Республики  Беларусь по своим документам для выезда из Республики Беларусь и въезда в  Республику Беларусь:
                                
                                В сопровождении обоих законных  представителей при предъявлении законными представителями  несовершеннолетнего сотруднику органа пограничной службы документов,  подтверждающих статус законных представителей несовершеннолетнего;
                                В  сопровождении одного из законных представителей при предъявлении  законным представителем несовершеннолетнего сотруднику органа  пограничной службы документа, подтверждающего статус законного  представителя несовершеннолетнего;
                                Без сопровождения  законных представителей при предъявлении сопровождающим  несовершеннолетнего лицом, достигшим совершеннолетия, сотруднику органа  пограничной службы письменного согласия обоих законных представителей.
                                В  случае отсутствия одного из законных представителей или невозможности  получения его согласия несовершеннолетний может выезжать из Республики  Беларусь по своему документу для выезда из Республики Беларусь и въезда в  Республику Беларусь без сопровождения законных представителей с  письменного согласия одного законного представителя при предъявлении  несовершеннолетним или сопровождающим его лицом сотруднику органа  пограничной службы одного из следующих документов:
                                
                                — копии  решения суда о возможности выезда из Республики Беларусь  несовершеннолетнего без согласия другого законного представителя;
                                — копии решения суда о признании другого законного представителя недееспособным;
                                — копии решения суда о лишении другого законного представителя родительских прав;
                                ссправки о нахождении в розыске другого законного представителя, выданной органом, ведущим уголовный процесс;
                                —  брачного договора или Соглашения о детях, в которых предусмотрена  возможность выезда из Республики Беларусь несовершеннолетнего без  согласия другого законного представителя, либо их копии;
                                — свидетельства о смерти другого законного представителя или его копии;
                                — копии решения суда об объявлении другого законного представителя умершим;
                                — копии решения суда о признании другого законного представителя безвестно отсутствующим;
                                —  справки органа, регистрирующего акты гражданского состояния, о том, что  запись об отце ребенка в книге записей актов о рождении произведена на  основании заявления матери, не состоящей в браке, в соответствии со  статьей 55 Кодекса Республики Беларусь о браке и семье, или ее копии.
                            </div>
                        </div>
                    </div>
                </div>`;

    $('body').append(html);
}


setInterval(function () {

    var json_request = {sessid: BX.bitrix_sessid()};
    BX.ajax.get("/local/components/travelsoft/eurotrans.booking/ajax/check_session.php", json_request, function (json_resp) {
        var resp = JSON.parse(json_resp);
        if (resp.error === 'true') {
            alert(
                'Время бронирования закончилось, пожалуйста начните бронирование билетов сначала!'
            );
            window.location.replace("/");
        }
        if (resp.session === 'ok') {

        }
    });

}, 300000);

function validator(node){
    switch (node.value) {

        case "is_empty":
}
$(document).ready(function()
{
    setTimeout(function() {
        $("html,body").animate({scrollTop: 400}, 1000);
    }, 100);

    var tabs = document.querySelectorAll('.ts-tabs__nav-item');



    var next_toggle = document.querySelectorAll('[data-key="next__tab"]');

    [].slice.call(next_toggle).forEach(function(item){
        item.onclick = function (e) {
            e.preventDefault();
            var inputForm = document.querySelectorAll('.ts-tabs input');
            console.log(inputForm);
        }
    })


});
