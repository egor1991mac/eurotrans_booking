<?

class CallPhoneForm extends CBitrixComponent
{

    protected $request = null;

    function processPostForm()
    {
        if ($this->request->isPost() &&
            $this->request->getPost('SUBMIT') !== ""
        ) {

            foreach ($this->arResult['FORM']['ROUTE'] as $NAME => &$arField) {

                if ($arField["type"] == "datetime") {

                    $date = $this->request->getPost($NAME);
                    $hour = $this->request->getPost($NAME . '-HOUR');
                    $minute = $this->request->getPost($NAME . '-MINUTE');

                    if ($arField['isRequired']) {

                        if ($date == "") {
                            $error = getMessage("TS_CP_EMPTY_FIELD", array("#FIELD#" => $arField["formLabel"]));
                            $this->arResult['ERRORS'][] = $error;
                        }

                        if ($hour == "") {
                            $error = getMessage("TS_CP_EMPTY_FIELD", array("#FIELD#" => $arField["formLabel"]));
                            $this->arResult['ERRORS'][] = $error;
                        }

                        if ($minute == "") {
                            $error = getMessage("TS_CP_EMPTY_FIELD", array("#FIELD#" => $arField["formLabel"]));
                            $this->arResult['ERRORS'][] = $error;
                        }

                    }
                    if (!empty($date) && !empty($hour) && !empty($minute)) {
                        $arField['value'] = Array($date . ' ' . $hour . ':' . $minute . ':00');
                    }

                } elseif ($arField["type"] == "boolean") {

                    if ($this->request->getPost($NAME) == "") {
                        $arField['value'] = "0";
                    } else {
                        $arField['value'] = "1";
                    }

                }
                else {
                    $value = $this->request->getPost($NAME);

                    if ($arField['isRequired']) {

                        if ($value == "") {
                            $error = getMessage("TS_CP_EMPTY_FIELD", array("#FIELD#" => $arField["formLabel"]));
                            $this->arResult['ERRORS'][] = $error;
                        }
                    }

                    $arField['value'] = $value;
                }
            }

            foreach ($this->arResult['FORM']['APPLICATION_FORM'] as $NAME => &$arField) {
                if($arField['type'] == 'file'){
                    $file = $this->request->getFile($NAME);
                    $value = $file;
                }
                else {
                    $value = $this->request->getPost($NAME);
                }
                if ($arField['isRequired']) {

                    if ($value == "") {
                        $error = getMessage("TS_CP_EMPTY_FIELD", array("#FIELD#" => $arField["formLabel"]));
                        $this->arResult['ERRORS'][] = $error;
                    }

                }
                if (($arField["type"] == "tel") || ($arField["type"] == "email")) {
                    $arField['value'] = Array('n0' => Array('VALUE' => $value));
                } else {
                    $arField['value'] = $value;
                }
            }
            if (!$this->arResult['ERRORS'])
                return true;
        }
        return false;
    }

    public function crmRequestWithResponse($queryFunction, $queryData = "")
    {
        if (!empty($queryFunction)) {
            $queryUrl = CRM_EUROTRANS;
            $queryUrl .= $queryFunction . '/';

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $queryUrl,
                CURLOPT_POSTFIELDS => $queryData,
            ));

            $result = curl_exec($curl);
            curl_close($curl);
            $result = json_decode($result, 1);
            return $result;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getFields(){
        $dataFields = array();
        if($this->arParams["FORM_ID"] == "BUS_RENTAL") {
            $dataFields = Array
            (
                "ROUTE" => Array
                (
                    "UF_CRM_1483795016" => Array
                    (
                        "type" => "string",
                        "isRequired" => "1",
                        "formLabel" => "Откуда:",
                    ),
                    "UF_CRM_1483795308" => Array
                    (
                        "type" => "string",
                        "isRequired" => "1",
                        "formLabel" => "Куда:",
                    ),
                    "UF_CRM_1483795426" => Array
                    (
                        "type" => "datetime",
                        "isRequired" => "1",
                        "formLabel" => "Дата выезда:",
                        "formLabelTime" => "Время выезда:",
                    ),
                    "UF_CRM_1484130496" => Array
                    (
                        "type" => "string",
                        "isRequired" => "1",
                        "formLabel" => "Общее кол-во пассажиров:",
                    ),
                    "UF_CRM_1483795809" => Array
                    (
                        "type" => "boolean",
                        "isRequired" => "1",
                        "formLabel" => "Маршрут Туда-Обратно:",
                    ),
                    "UF_CRM_1512372157" => Array
                    (
                        "type" => "datetime",
                        "isRequired" => "",
                        "formLabel" => "Дата выезда обратно:",
                        "formLabelTime" => "Время выезда обратно:",
                        "hidden" => "1",
                    )
                ),
                "APPLICATION_FORM" => Array(
                    "NAME" => Array(
                        "type" => "text",
                        "formLabel" => "Имя:",
                        "placeholder" => "Ваше имя",
                        "isRequired" => "1",
                    ),
                    "PHONE" => Array(
                        "type" => "tel",
                        "formLabel" => "Телефон:",
                        "placeholder" => "Ваш телефон",
                        "isRequired" => "1",
                    ),
                    "EMAIL" => Array(
                        "type" => "email",
                        "formLabel" => "E-mail:",
                        "placeholder" => "Ваш e-mail",
                        "isRequired" => "1",
                    ),
                    "COMMENTS" => Array(
                        "type" => "textarea",
                        "formLabel" => "Комментарий:",
                        "placeholder" => "Для точного расчёта, опишите программу поездки или опишите специальные требования для поездки.",
                        "isRequired" => "",
                    ),
                    "UF_CRM_1531227575" => array(
                        "type" => 'file',
                        "formLabel" => "Прикрепить файл программы поездки:",
                        "isRequired" => "",
                    )
                )
            );
        }
        else if($this->arParams["FORM_ID"] == "TRANSFERS") {
            $dataFields = Array
            (
                "ROUTE" => Array
                (
                    "UF_CRM_1483795016" => Array
                    (
                        "type" => "enumeration",
                        "isRequired" => "1",
                        "formLabel" => "Откуда:",
                        "items" => Array
                        (
                            "0" => "Бобруйск ( Белая Церковь)",
                            "1" => "Борисов (ул. Гагарина, возле танка)",
                            "2" => "Витебск (Ж/Д вокзал, напротив центрального входа)",
                            "3" => "Жодино (пр-т Ленина, автовокзал)",
                            "4" => "Минск ( станция Дружная)",
                            "5" => "Могилёв (площадь Орджоникидзе)",
                            "6" => "Орша ул.Ленина21 (школа искусств)",
                            "7" => "Осиповичи (102км а/д Минск-Гомель АЗС№18)",
                        )
                    ),
                    "UF_CRM_1483795308" => Array
                    (
                        "type" => "enumeration",
                        "isRequired" => "1",
                        "formLabel" => "Куда:",
                        "items" => Array
                        (
                            "0" => "Москва (Внуково)",
                            "1" => "Москва (Домодедово)",
                            "2" => "Москва (Шереметьево)",
                            "3" => "Киев (Жуляны)",
                            "4" => "Киев (Борисполь)",
                        )
                    ),
                    "UF_CRM_1512372719" => Array
                    (
                        "type" => "datetime",
                        "isRequired" => "1",
                        "formLabel" => "Дата вылета:",
                        "formLabelTime" => "Время вылета:",
                    ),
                    "UF_CRM_1484130496" => Array
                    (
                        "type" => "enumeration",
                        "isRequired" => "1",
                        "formLabel" => "Кол-во взрослых:",
                        "items" => Array
                        (
                            "0" => "1",
                            "1" => "2",
                            "2" => "3",
                            "3" => "4",
                            "4" => "5",
                            "5" => "6",
                            "6" => "7",
                            "7" => "8",
                        )
                    ),
                    "UF_CRM_1484130552" => Array
                    (
                        "type" => "enumeration",
                        "isRequired" => "",
                        "formLabel" => "Кол-во детей до 16 лет:",
                        "items" => Array
                        (
                            "0" => "0",
                            "1" => "1",
                            "2" => "2",
                            "3" => "3",
                            "4" => "4",
                            "5" => "5",
                            "6" => "6",
                            "7" => "7",
                            "8" => "8",
                        )
                    ),
                    "UF_CRM_1483795809" => Array
                    (
                        "type" => "boolean",
                        "isRequired" => "1",
                        "formLabel" => "Маршрут Туда-Обратно:",
                    ),
                    "UF_CRM_1512034491" => Array
                    (
                        "type" => "enumeration",
                        "isRequired" => "",
                        "formLabel" => "Откуда обратно:",
                        "items" => Array
                        (
                            "0" => "Москва (Внуково)",
                            "1" => "Москва (Домодедово)",
                            "2" => "Москва (Шереметьево)",
                            "3" => "Киев (Жуляны)",
                            "4" => "Киев (Борисполь)",
                        ),
                        "hidden" => "1",
                    ),
                    "UF_CRM_1512117684" => Array
                    (
                        "type" => "enumeration",
                        "isRequired" => "",
                        "formLabel" => "Куда обратно:",
                        "items" => Array
                        (
                            "0" => "Бобруйск ( Белая Церковь)",
                            "1" => "Борисов (ул. Гагарина, возле танка)",
                            "2" => "Витебск (Ж/Д вокзал, напротив центрального входа)",
                            "3" => "Жодино (пр-т Ленина, автовокзал)",
                            "4" => "Минск ( станция Дружная)",
                            "5" => "Могилёв (площадь Орджоникидзе)",
                            "6" => "Орша ул.Ленина21 (школа искусств)",
                            "7" => "Осиповичи (102км а/д Минск-Гомель АЗС№18)",
                        ),

                        "hidden" => "1",
                    ),
                    "UF_CRM_1512372750" => Array
                    (
                        "type" => "datetime",
                        "isRequired" => "",
                        "formLabel" => "Дата прилёта:",
                        "formLabelTime" => "Время прилёта:",
                        "hidden" => "1",
                    )
                ),
                "APPLICATION_FORM" => Array
                (
                    "NAME" => Array
                    (
                        "type" => "text",
                        "formLabel" => "Имя:",
                        "placeholder" => "Ваше имя",
                        "isRequired" => "1",
                    ),
                    "PHONE" => Array
                    (
                        "type" => "tel",
                        "formLabel" => "Телефон:",
                        "placeholder" => "Ваш телефон",
                        "isRequired" => "1",
                    ),
                    "EMAIL" => Array
                    (
                        "type" => "email",
                        "formLabel" => "E-mail:",
                        "placeholder" => "Ваш e-mail",
                        "isRequired" => "",
                    ),
                    "COMMENTS" => Array
                    (
                        "type" => "textarea",
                        "formLabel" => "Комментарий:",
                        "isRequired" => "",
                    )
                )
            );
        }
        return $dataFields;
    }

    /**
     * Выполнение компонента
     */
    public function executeComponent()
    {
        $this->request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

        $this->arResult['FORM'] = $this->getFields();
        if ($this->processPostForm()) {

            $queryFunction = 'crm.lead.add';

            $fields = array();
            if (!empty($this->arParams["TITLE"])) {
                $fields["TITLE"] = $this->arParams["TITLE"];
            }

            foreach ($this->arResult['FORM']["APPLICATION_FORM"] as $k => $value) {
                if($value['type'] == 'file'){
                    $fields[$k] = [
                        'fileData' => [$value["value"]['name'], base64_encode(file_get_contents($value["value"]['tmp_name']))]
                    ];
                }
                else {
                    $fields[$k] = $value["value"];
                }
            }

            foreach ($this->arResult['FORM']["ROUTE"] as $k => $value) {
                $fields[$k] = $value["value"];
            }

            $queryData = http_build_query(array(
                'fields' => $fields,
                'params' => array("REGISTER_SONET_EVENT" => "Y")
            ));

            $result = $this->crmRequestWithResponse($queryFunction, $queryData);

            if (!empty($result["error_description"])) {
                $this->arResult['ERRORS'][] = $result["error_description"];
            } else {
                LocalRedirect($_SERVER["SCRIPT_NAME"] . "?message=ok");
            }
        }
        $this->includeComponentTemplate();
    }
}