<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/** @var array $arCurrentValues */

$arComponentParameters['PARAMETERS']['FORM_FIELDS'] = array(
    "PARENT" => "BASE",
    "NAME" => "Массив полей",
    "TYPE" => "STRING",
    "MULTIPLE" => "N",
    "ADDITIONAL_VALUES" => "N",
    "DEFAULT" => '={$arFormFields}',
);
$arComponentParameters['PARAMETERS']['CRM_EUROTRANS'] = array(
    "PARENT" => "BASE",
    "NAME" => "URL для вызова REST",
    "TYPE" => "STRING",
    "MULTIPLE" => "N",
    "ADDITIONAL_VALUES" => "N",
    "DEFAULT" => 'https://24.eurotrans.by/rest/12998/binjgjbc5yf2phrd/',
);
$arComponentParameters['PARAMETERS']['HIDDEN_CHECK_STRING'] = array(
    "PARENT" => "BASE",
    "NAME" => "Hidden input для проверки формы",
    "TYPE" => "STRING",
    "MULTIPLE" => "N",
    "ADDITIONAL_VALUES" => "N",
    "DEFAULT" => 'my_test_id_string',
);
