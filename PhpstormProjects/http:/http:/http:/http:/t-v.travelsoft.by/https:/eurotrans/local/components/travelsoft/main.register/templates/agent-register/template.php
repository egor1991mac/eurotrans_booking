<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

use travelsoft\Main\Config\ExtOptions;

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<?// ExtOptions::changeLocally("main", "new_user_registration_email_confirmation", 'N');?>

<? $this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery.mask.js"); ?>
<section class="aside-section">
    <div class="content-column-wrap">
        <div class="content-column-content">
            <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform" id = "reg_user_form" enctype="multipart/form-data">
                <input name='REGISTER[LOGIN]' value='<?=$arResult["VALUES"]["LOGIN"]?>' type='hidden'>
                <input name='UF_IS_AGENT' value='true' type='hidden'>
                <div class="form-block-section">
                    <div class="title h3">
                        Регистрация агентства
                        <?if ($arResult["ERRORS"]["0"]):?>
                            <div class="input-wrapper" style="margin-top: 30px;">
                                <?echo getRegError($arResult["ERRORS"]["0"], GetMessage("0"));?>
                            </div>
                        <?endif;?>
                    </div>
                    <div class="input-form">
                        <div class="input-item s50">
                            <div class="input-wrapper req">
                                <label for="imp7">Наименование агента</label>
                                <input id="imp7" class="input-main" type="text" name="<?=$arResult["USER_PROPERTIES"]["DATA"]["UF_AGENT_NAME"]['FIELD_NAME']?>"
                                       placeholder="Наименование агента" value="<?=$_POST[$arResult["USER_PROPERTIES"]["DATA"]["UF_AGENT_NAME"]['FIELD_NAME']]?>">
                            </div>
                        </div>

                        <div class="input-item s50">
                            <div class="input-wrapper req">
                                <label for="imp4"><?=GetMessage("REGISTER_FIELD_EMAIL")?></label>
                                <input class="input-main" name="REGISTER[EMAIL]" type="email" placeholder="example@mail.com" id="imp4" value="<?=$arResult["VALUES"]["EMAIL"]?>">
                                <?if ($arResult["ERRORS"]["LOGIN"]) {
                                    $ERROR = $arResult["ERRORS"]["LOGIN"];
                                }
                                if ($arResult["ERRORS"]["EMAIL"]) {
                                    $ERROR = $arResult["ERRORS"]["EMAIL"];
                                }
                                if (!empty($ERROR)) {
                                    echo getRegError($ERROR, GetMessage("REGISTER_FIELD_EMAIL"));
                                } ?>
                            </div>
                        </div>

                        <div class="input-item s100">
                            <div class="input-wrapper req">
                                <label for="imp7">Юридический адрес</label>
                                <input id="imp7" class="input-main" type="text" name="<?=$arResult["USER_PROPERTIES"]["DATA"]["UF_LEGAL_ADDRESS"]['FIELD_NAME']?>"
                                       placeholder="Юридический адрес" value="<?=$_POST[$arResult["USER_PROPERTIES"]["DATA"]["UF_LEGAL_ADDRESS"]['FIELD_NAME']]?>">
                            </div>
                        </div>

                        <div class="input-item s100">
                            <div class="input-wrapper req">
                                <label for="imp7">Почтовый адрес</label>
                                <input id="imp7" class="input-main" type="text" name="<?=$arResult["USER_PROPERTIES"]["DATA"]["UF_MAILING_ADDRESS"]['FIELD_NAME']?>"
                                       placeholder="Почтовый адрес" value="<?=$_POST[$arResult["USER_PROPERTIES"]["DATA"]["UF_MAILING_ADDRESS"]['FIELD_NAME']]?>">
                            </div>
                        </div>

                        <div class="input-item s50">
                            <div class="input-wrapper req">
                                <label for="imp2"><?=GetMessage("REGISTER_FIELD_NAME")?></label>
                                <input class="input-main" type="text" name="REGISTER[NAME]" placeholder="<?=GetMessage("REGISTER_FIELD_NAME")?>" id="imp2" value="<?=$arResult["VALUES"]["NAME"]?>">
                                <?
                                if ($arResult["ERRORS"]["NAME"]) {
                                    echo getRegError($arResult["ERRORS"]["NAME"], GetMessage("REGISTER_FIELD_NAME"));
                                }
                                ?>
                            </div>
                        </div>

                        <div class="input-item s50">
                            <div class="input-wrapper req">
                                <label for="imp1"><?=GetMessage("REGISTER_FIELD_LAST_NAME")?></label>
                                <input class="input-main" name="REGISTER[LAST_NAME]" type="text" placeholder="<?=GetMessage("REGISTER_FIELD_LAST_NAME")?>" id="imp1" value="<?=$arResult["VALUES"]["LAST_NAME"]?>">
                                <?
                                if ($arResult["ERRORS"]["LAST_NAME"]) {
                                    echo getRegError($arResult["ERRORS"]["LAST_NAME"], GetMessage("REGISTER_FIELD_LAST_NAME"));
                                }
                                ?>
                            </div>
                        </div>

                        <div class="input-item s50">
                            <div class="input-wrapper">
                                <label for="imp2"><?=GetMessage("REGISTER_FIELD_SECOND_NAME")?></label>
                                <input class="input-main" type="text" name="REGISTER[SECOND_NAME]" placeholder="<?=GetMessage("REGISTER_FIELD_SECOND_NAME")?>" id="imp3" value="<?=$arResult["VALUES"]["SECOND_NAME"]?>">
                                <?
                                if ($arResult["ERRORS"]["SECOND_NAME"]) {
                                    echo getRegError($arResult["ERRORS"]["SECOND_NAME"], GetMessage("REGISTER_FIELD_SECOND_NAME"));
                                }
                                ?>
                            </div>
                        </div>

                        <div class="input-item s50">
                            <div class="input-wrapper req">
                                <label for="imp3"><?= GetMessage("REGISTER_FIELD_PERSONAL_MOBILE")?></label>
                                <input class="input-main" name="REGISTER[PERSONAL_MOBILE]" type="tel" placeholder="<?= GetMessage("REGISTER_FIELD_PERSONAL_MOBILE")?>" id="imp3" value="<?=$arResult["VALUES"]["PERSONAL_MOBILE"]?>">
                                <?
                                if ($arResult["ERRORS"]["PERSONAL_MOBILE"]) {
                                    echo getRegError($arResult["ERRORS"]["PERSONAL_MOBILE"], GetMessage("REGISTER_FIELD_PERSONAL_MOBILE"));
                                }
                                ?>
                            </div>
                        </div>

                        <div class="input-item s100">
                            <div class="input-wrapper req">
                                <label for="imp8">Банковские реквезиты</label>
                                <input id="imp8" class="input-main" type="text" name="<?=$arResult["USER_PROPERTIES"]["DATA"]["UF_BANK_DETAIS"]['FIELD_NAME']?>"
                                       placeholder="Банковские реквезиты" value="<?=$_POST[$arResult["USER_PROPERTIES"]["DATA"]["UF_BANK_DETAIS"]['FIELD_NAME']]?>">
                            </div>
                        </div>

                        <div class="input-item s50">
                            <div class="input-wrapper req">
                                <label for="imp8">Должность подписывающего договор</label>
                                <input id="imp8" class="input-main" type="text" name="<?=$arResult["USER_PROPERTIES"]["DATA"]["UF_SIGNER_POSITION"]['FIELD_NAME']?>"
                                       placeholder="Должность подписывающего договор" value="<?=$_POST[$arResult["USER_PROPERTIES"]["DATA"]["UF_SIGNER_POSITION"]['FIELD_NAME']]?>">
                            </div>
                        </div>

                        <div class="input-item s50">
                            <div class="input-wrapper req">
                                <label for="imp8">Инициалы подписывающего договор</label>
                                <input id="imp8" class="input-main" type="text" name="<?=$arResult["USER_PROPERTIES"]["DATA"]["UF_SIGNER_INITIALS"]['FIELD_NAME']?>"
                                       placeholder="Инициалы подписывающего договор" value="<?=$_POST[$arResult["USER_PROPERTIES"]["DATA"]["UF_SIGNER_INITIALS"]['FIELD_NAME']]?>">
                            </div>
                        </div>
                        <div class="input-item s50">
                            <div class="input-wrapper req">
                                <label for="imp5"><?=GetMessage("REGISTER_FIELD_PASSWORD")?></label>
                                <input class="input-main" name="REGISTER[PASSWORD]" type="password" placeholder="<?=GetMessage("REGISTER_FIELD_PASSWORD")?>" id="imp5"  value="<?=$arResult["VALUES"]["PASSWORD"]?>">
                                <?
                                if ($arResult["ERRORS"]["PASSWORD"]) {
                                    echo getRegError($arResult["ERRORS"]["PASSWORD"], GetMessage("REGISTER_FIELD_PASSWORD"));
                                }
                                ?>
                            </div>
                        </div>
                        <div class="input-item s50">
                            <div class="input-wrapper req">
                                <label for="imp6"><?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD")?></label>
                                <input class="input-main" name="REGISTER[CONFIRM_PASSWORD]" type="password" placeholder="<?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD")?>" id="imp6" value="<?=$arResult["VALUES"]["CONFIRM_PASSWORD"]?>">
                                <?
                                if ($arResult["ERRORS"]["CONFIRM_PASSWORD"]) {
                                    echo getRegError($arResult["ERRORS"]["CONFIRM_PASSWORD"], GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD"));
                                }
                                ?>
                            </div>
                        </div>
                        <!--
                        <div class="input-item s100">
                            <div class="input-wrapper">
                                <div class="select-item">
                                    <label class="select-label js-condition">
                                        <span class="select-main">
                                          <input class="select-real" type="checkbox" value="1" name="two-way-variant">
                                          <span class="select-checked"></span>
                                        </span>
                                        <span class="select-text">Получать уведомление о бронированиях, акция, отказах и т.д.</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        -->
                    </div>
                </div>
                <div class="form-block-footer">
                    <div class="form-footer-notify">
                        <div class="text-sm">Поля для обязательного заполнения</div>
                    </div>
                    <input type="submit" style="padding: .9375rem 1.375rem;" class="btn btn-colored" name="register_submit_button" value="<?= GetMessage("AUTH_REGISTER") ?>"/>
                </div>
            </form>
        </div>
        <div class="content-column-right">
            <div class="caption-content">
                <span>Жить необязательно. Путешествовать — необходимо.</span>
                <i>Уильям Берроуза</i>
            </div>
        </div>
    </div>
</section>

<script>
    if (typeof jQuery === "function") {
        jQuery("#reg_user_form").submit(function () {
            jQuery("input[name='REGISTER[LOGIN]'").val(jQuery("input[name='REGISTER[EMAIL]']").val());
            return true;
        });
    }
</script>

<script>
    $(document).ready(function() {
        let birthday = BX.findChildren(BX("reg_user_form"), function (el) {

            return typeof el.dataset !== "undefined" && el.dataset.birthday === "true";

        }, true);

        $(birthday).mask('99.99.9999');
    });
</script>

<?
// вывод ошибок при регистрации поставщика
function getRegError ($mess, $replace) {
    return "<span class='help-block form-error'>" . str_replace("#FIELD_NAME#", "&quot;".$replace."&quot;", $mess) . "</span>";
}
?>