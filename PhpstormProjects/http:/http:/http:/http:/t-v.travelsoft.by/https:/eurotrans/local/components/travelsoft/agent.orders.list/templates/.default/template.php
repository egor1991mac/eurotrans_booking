<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="aside-section">
    <div class="content-column-wrap content-small">
        <div class="content-column-content">
            <div class="content-column-content-inner">
                <h2>Проданные Вами билеты</h2>
                <p> Ваше агентство продаёт билеты через компьютерную систему, позволяющую выписывать билет на маршрут
                    eurotrans.by в режиме on-line в Вашем офисе, и получает комиссионный процент от стоимости
                    билета.</p>
            </div>
        </div>
        <div class="content-column-right">
            <a class="border-card-link" href="/office-agent/manual/">
                <div class="border-card small percent">
                    <div class="border-card-inner"
                         style="background-image:url(/bitrix/templates/eurotrans/img/cards/percent.jpg);">
                        <? if (!empty($arResult['AGENT']['COMMISSION'])): ?>
                            <div class="border-card-name">
                                <div class="text-sm">Ваш комиссионный процент от стоимости билета</div>
                                <span>
                                    <?= $arResult['AGENT']['COMMISSION']; ?>%
                                </span>
                            </div>
                        <? endif; ?>
                        <div class="border-card-content bonus-content">
                            <div class="link-border">
                                <!--<span>Расчитать комиссию</span>-->
                                <span>Инструкция</span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="table-wrapper">

        <div id="search-agent-bookings">
            <div class="table-wrapper">
                <div class="form-block-section lilform-wrap mt-20">
                    <div class="title h3 mb-20">Поиск по заказам</div>
                    <div class="input-form">
                        <div class="input-item s33 mb-10">
                            <div class="input-wrapper">
                                <label>Фамилия</label>
                                <input v-model="surname" class="input-main" type="text" placeholder="Фамилия"/>
                            </div>
                        </div>
                        <div class="input-item s33 mb-10">
                            <div class="input-wrapper">
                                <label>Телефон</label>
                                <input v-model="phone" class="input-main" type="text" placeholder="Телефон"/>
                            </div>
                        </div>
                        <div class="input-item s33 mb-10">
                            <div class="input-wrapper">
                                <label>Номер заказа</label>
                                <input v-model="number" class="input-main" type="text" placeholder="Номер заказа"/>
                            </div>
                        </div>
                        <div class="input-item s25 mb-10">
                            <div class="input-wrapper">
                                <label>Дата рейса от</label>
                                <input v-model="date_journey_from" class="input-main" type="date">
                            </div>
                        </div>
                        <div class="input-item s25 mb-10">
                            <div class="input-wrapper">
                                <label>Дата рейса до</label>
                                <input v-model="date_journey_to" class="input-main" type="date">
                            </div>
                        </div>
                        <div class="input-item s25 mb-10">
                            <div class="input-wrapper">
                                <label>Дата продажи от</label>
                                <input v-model="date_created_from" class="input-main" type="date">
                            </div>
                        </div>
                        <div class="input-item s25 mb-10">
                            <div class="input-wrapper">
                                <label>Дата продажи до</label>
                                <input v-model="date_created_to" class="input-main" type="date">
                            </div>
                        </div>
                        <div class="input-item s33 mb-10">
                            <div class="input-wrapper">
                                <div class="label">Откуда</div>
                                <select v-model="location_from" class="input-main">
                                    <option disabled value="">Выберите</option>
                                    <option v-for="location_from in locations_from" :key="location_from.id"
                                            :value="`${location_from.id}`">{{location_from.name}}
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="input-item s33 mb-10">
                            <div class="input-wrapper">
                                <div class="label">Куда</div>
                                <select v-model="location_to" class="input-main">
                                    <option disabled value="">Выберите</option>
                                    <option v-for="location_to in locations_to" :key="location_to.id"
                                            :value="`${location_to.id}`">{{location_to.name}}
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="input-item s33 mb-10">
                            <div class="input-wrapper">
                                <div class="label">&nbsp;</div>
                                <button class="btn btn-colored small" @click="downloadInXLS">
                                    <span>Выгрузить поиск в XLS</span>
                                </button>
                            </div>
                        </div>
                        <div class="input-item s100 mb-0">
                            <div class="text-1 text-sm">
                                Поиск работает от 3 символов. Если хотите сбросить поиск, то удалите содержимое поиска.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-navigation">
                    <div class="table-navigation-links">
                    </div>
                    <div class="table-navigation-nav">
                        <div class="table-pagination">
                            <a class="table-pagination-arr left" :class="activeDecPage ? 'disabled' : ''"
                               @click="decPage()">
                                <i class="arr-down">
                                    <svg class="icon icon-drop">
                                        <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                            <div class="table-pagination-inner">
                                <a class="table-pagination-item"
                                   :class="current_page === page ? 'active' : ''"
                                   v-for="page in pages" :key="page"
                                   v-if="isVisiblePage(page)"
                                   @click="setPage(page)">
                                    {{page}}
                                </a>
                            </div>
                            <a class="table-pagination-arr right" :class="activeIncPage ? 'disabled' : ''"
                               @click="incPage()">
                                <i class="arr-down">
                                    <svg class="icon icon-drop">
                                        <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                        <div class="table-collapse">
                            <div class="table-navigation-link has-select js-select-item" @change="triggerItem(this)">
                                <span>{{activeStatus.text}}</span>
                                <i class="arr-down">
                                    <svg class="icon icon-drop">
                                        <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <div class="dropdown-target">
                                    <div class="dropdown-inner">
                                        <div class="dropdown-content">
                                            <div v-for="(item, index) in statusList" :key="index"
                                                 class="sort-select-item">
                                                <a class="active" @click="changeStatus(item)">{{item.text}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table table-tickets sold">
                    <div class="table-head">
                        <div class="table-item table-num">
                            <span>№</span>
                        </div>
                        <div class="table-item">
                            <span>Пассажир ФИО</span>
                        </div>
                        <div class="table-item">
                            <span>Дата продажи</span>
                        </div>
                        <div class="table-item">
                            <span>Дата рейса</span>
                        </div>
                        <div class="table-item">
                            <span>Рейс</span>
                        </div>
                        <div class="table-item">
                            <span>Статус</span>
                        </div>
                        <div class="table-item">
                            <span>Итого</span>
                        </div>
                        <div class="table-item">
                            <span>Комиссия</span>
                        </div>
                        <div class="table-item">
                            <span>Штраф</span>
                        </div>
                        <div class="table-item table-actions">
                            <span>Действия</span>
                        </div>
                    </div>

                    <div class="table-row" v-for="booking in bookings" :key="booking.id">
                        <div class="table-item table-num">
                            <div class="text-1">
                                <nobr>{{booking.id}}</nobr>
                            </div>
                        </div>
                        <div class="table-item">
                            <div class="text-1" v-for="(seat, index) in booking.arrSeatsId" :key="index">
                                {{seat.last_name}} {{seat.first_name}} {{seat.patronymic}}<br/><br/>
                            </div>
                            <div class="text-1">
                                {{booking.phone}}
                                {{booking.phone_2 || ''}}
                            </div>
                        </div>
                        <div class="table-item">
                            <div class="text-1">
                                {{booking.created}}
                            </div>
                        </div>

                        <div class="table-item">
                            <div class="text-1">
                                {{booking.booking_datetime.date}}
                                {{booking.booking_datetime.time}}
                            </div>
                        </div>

                        <div class="table-item">
                            <div class="text-1">
                                {{booking.departure}} → {{booking.arrival}}
                            </div>
                        </div>
                        <div class="table-item dropdown-table-item">
                            <div v-if="booking.modified_date === ''">
                                <div class="text-bold yep"
                                     v-if="booking.status === 'confirmedOperator' || booking.status === 'confirmed'"></div>
                                <div class="text-bold check" v-else-if="booking.status === 'pending'"></div>
                                <div class="text-bold nope"
                                     v-else-if="booking.status === 'cancelled' || booking.status === 'canceledClient'"></div>
                            </div>
                            <div v-else>
                                <div class="dropdown-trigger"
                                     v-if="booking.status === 'confirmedOperator' || booking.status === 'confirmed'">
                                    <div class="text-bold yep"></div>
                                    <div class="dropdown-target arrow-bottom">
                                        <div class="dropdown-inner">
                                            <div class="dropdown-content">
                                                <div class="text-sm">
                                                    {{booking.modified_date}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-trigger" v-else-if="booking.status === 'pending'">
                                    <div class="text-bold check"></div>
                                    <div class="dropdown-target arrow-bottom">
                                        <div class="dropdown-inner">
                                            <div class="dropdown-content">
                                                <div class="text-sm">
                                                    {{booking.modified_date}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-trigger"
                                     v-else-if="booking.status === 'cancelled' || booking.status === 'canceledClient'">
                                    <div class="text-bold nope"></div>
                                    <div class="dropdown-target arrow-bottom">
                                        <div class="dropdown-inner">
                                            <div class="dropdown-content">
                                                <div class="text-sm">
                                                    {{booking.modified_date}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-item">
                            <div class="text-bold">
                                <nobr>
                                    {{booking.price}}
                                    <span>
                                        {{booking.currency}}
                                    </span>
                                </nobr>
                            </div>
                        </div>
                        <div class="table-item">
                            <div class="text-bold org">
                                <nobr>
                                    {{booking.commission}}
                                    <span>
                                        {{booking.currency}}
                                    </span>
                                </nobr>
                            </div>
                        </div>
                        <div class="table-item">
                            <div class="text-bold org">
                                <nobr v-if="booking.agency_fines !== ''">
                                    {{booking.agency_fines}}
                                    <span>
                                        {{booking.currency}}
                                    </span>
                                </nobr>
                            </div>
                        </div>
                        <div class="table-item table-actions">
                            <div v-if="booking.status === 'confirmedOperator' || booking.status === 'confirmed'">
                                <a class="btn btn-extrasmall orange" @click="downloadAll(booking.data.print_urls)">
                                    <span>Билеты</span>
                                </a>

                                <a class="link-border gray" @click="cancelBooking(booking.id)">
                                    <span>Отменить</span>
                                </a>
                            </div>

                            <div v-else-if="booking.status === 'pending'">
                                <a class="link-border gray" @click="cancelBooking(booking.id)">
                                    <span>Отменить</span>
                                </a>

                                <a class="btn btn-extrasmall orange" @click="confirmBooking(booking.id)">
                                    <span>Оплатить</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<script>
    let searchAgentBookingsApp = new Vue({
        el: '#search-agent-bookings',
        data: {
            surname: '',
            phone: '',
            number: '',
            date_journey_from: '',
            date_journey_to: '',
            date_created_from: '',
            date_created_to: '',
            location_from: '',
            location_to: '',

            locations_from: [],
            locations_to: [],
            current_page: 1,
            count_pages: 0,
            count_per_page: 15,
            statusList: [
                {active: true, text: "Смотреть все", value: 'all'},
                {active: false, text: "Оплачены", value: 'confirmed'},
                {active: false, text: "Брони", value: 'pending'},
                {active: false, text: "Отменены", value: 'cancelled'},
            ],
            bookings: []
        },
        computed: {
            pages() {
                return Array.from({length: this.count_pages}, (v, k) => k + 1);
            },
            activeStatus() {
                return this.statusList.find(function (element) {
                    return element.active;
                });
            },
            activeDecPage() {
                return this.current_page === 1;
            },
            activeIncPage() {
                return this.count_pages === 0 || this.current_page === this.count_pages;
            },
        },
        created() {
            this.getBookings();
            this.getLocationsFrom();
        },
        methods: {
            isVisiblePage(page){
                if(page === 1){
                    return true;
                }
                if(page === this.count_pages){
                    return true;
                }
                if(page < (this.current_page + 5) && page > (this.current_page - 5)){
                    return true;
                }
                return false;
            },
            getBookings() {
                let _this = this;

                $.ajax({
                    type: 'post',
                    url: '/local/components/travelsoft/agent.orders.list/ajax/getBookings.php',
                    dataType: "json",
                    data: {
                        current_page: this.current_page,
                        page_count: this.count_per_page,
                        status: this.activeStatus.value,
                        surname: this.surname,
                        phone: this.phone,
                        number: this.number,
                        date_journey_from : this.date_journey_from,
                        date_journey_to : this.date_journey_to,
                        date_created_from : this.date_created_from,
                        date_created_to : this.date_created_to,
                        location_from : this.location_from,
                        location_to : this.location_to,
                    },
                    cache: false,
                    success: function (data) {
                        _this.count_pages = data.count_pages;
                        _this.current_page = data.current_page;
                        _this.bookings = data.bookings;
                    },
                    beforeSend: function (xhr) {

                    }
                });
            },
            getLocationsFrom() {
                let _this = this;
                $.ajax({
                    type: 'post',
                    url: '/local/components/travelsoft/agent.orders.list/ajax/getLocationsFrom.php',
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        _this.locations_from = data;
                    },
                });
            },
            getArrivalPoints() {
                let _this = this;
                $.ajax({
                    type: 'post',
                    url: '/local/components/travelsoft/agent.orders.list/ajax/getArrivalPoints.php',
                    dataType: "json",
                    data: {
                        departure: _this.location_from
                    },
                    cache: false,
                    success: function (data) {
                        _this.locations_to = data;
                    },
                });
            },
            decPage() {
                this.current_page--;
                this.getBookings();
            },
            setPage(page) {
                this.current_page = page;
                this.getBookings();
            },
            incPage() {
                this.current_page++;
                this.getBookings();
            },
            triggerItem(element) {
                let trigger = $(element);
                let textCont = trigger.find('span');
                let target = trigger.parent().find('.dropdown-target');
                let item = target.find('.sort-select-item a');

                trigger.toggleClass('active');

                $(document).on('mouseup', function (e) {
                    if (!trigger.is(e.target) && trigger.has(e.target).length === 0) {
                        trigger.removeClass('active');
                    }
                });
            },
            changeStatus(item) {
                this.activeStatus.active = false;
                activeStatus = this.statusList.find(function (element) {
                    return element.value === item.value;
                });
                activeStatus.active = true;
                this.getBookings();
            },
            downloadAll(print_urls) {

                let arUrls = JSON.parse(print_urls);

                let link = document.createElement('a');

                link.setAttribute('download', null);
                link.style.display = 'none';
                document.body.appendChild(link);

                arUrls.forEach(function (item) {
                    link.setAttribute('href', item);
                    link.click();
                });

                document.body.removeChild(link);
            },
            cancelBooking(id) {
                let _this = this;
                showPopupCancelBooking(id)
                    .then(data => {
                        $.ajax({
                            type: 'post',
                            url: '/local/components/travelsoft/agent.orders.list/ajax/cancelBooking.php',
                            dataType: "json",
                            data: {id: id},
                            success: function (data) {

                            },
                            beforeSend: function (xhr) {

                            },
                            complete: function () {
                                _this.getBookings();
                            }
                        });
                    })
                    .catch(data => {
                        //console.log('not cancel booking');
                    })
                    .finally(() => {
                        $('body').find('#payback').remove();
                    })
                ;
            },
            confirmBooking(id) {
                let _this = this;
                $.ajax({
                    type: 'post',
                    url: '/local/components/travelsoft/agent.orders.list/ajax/confirmBooking.php',
                    dataType: "json",
                    data: {id: id},
                    success: function (data) {
                    },
                    beforeSend: function (xhr) {
                    },
                    complete: function () {
                        _this.getBookings();
                    }
                });
            },
            downloadInXLS(){
                const params = jQuery.param({
                    status: this.activeStatus.value,
                    surname: this.surname,
                    phone: this.phone,
                    number: this.number,
                    date_journey_from : this.date_journey_from,
                    date_journey_to : this.date_journey_to,
                    date_created_from : this.date_created_from,
                    date_created_to : this.date_created_to,
                    location_from : this.location_from,
                    location_to : this.location_to,
                });

                let link = document.createElement('a');
                link.href = '/local/components/travelsoft/agent.orders.list/ajax/downloadXLS.php?' + params;
                link.click();
            }
        },
        watch: {
            surname: {
                handler(val) {
                    if (val.length === 0 || val.length > 2) {
                        this.getBookings();
                    }
                },
                deep: true
            },
            phone: {
                handler(val) {
                    if (val.length === 0 || val.length > 2) {
                        this.getBookings();
                    }
                },
                deep: true
            },
            number: {
                handler(val) {
                    if (val.length === 0 || val.length > 2) {
                        this.getBookings();
                    }
                },
                deep: true
            },
            location_from: {
                handler() {
                    this.location_to = '';
                    this.getBookings();
                    this.getArrivalPoints();
                },
                deep: true
            },
            location_to: {
                handler() {
                    this.getBookings();
                },
                deep: true
            },
            date_journey_from: {
                handler() {
                    this.getBookings();
                },
                deep: true
            },
            date_journey_to: {
                handler() {
                    this.getBookings();
                },
                deep: true
            },
            date_created_from: {
                handler() {
                    this.getBookings();
                },
                deep: true
            },
            date_created_to: {
                handler() {
                    this.getBookings();
                },
                deep: true
            },
        }
    });
</script>
