<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
$arComponentDescription = array(
    'NAME' => "Список броней агентов",
    'DESCRIPTION' => "Список броней агентов eurotrans.by",
    'SORT' => 30,
    'CACHE_PATH' => 'Y',
    'PATH' => array('ID' => 'travelsoft')
);
