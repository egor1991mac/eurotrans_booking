<?
return array (
  'ts-after-text-img' => 
  array (
    'tag' => 'p',
    'title' => 'Текст под картинкой',
  ),
	'ts-blockoute' =>
	array (
		'tag' => 'blockquote',
		'title' => 'blockquote'
	),
	'ts-section-new-article-content' =>
		array(
			'tag'=>'div',
			'title'=>'Блок инфо'
		),
	'ts-section-new-article-info-heading' =>
		array(
			'tag'=>'div',
			'title'=>'Блок инфо заголовок'
		),
	'ts-section-new-article-info-list' =>
		array(
			'tag'=>'ul',
			'title'=>'Блок инфо список'
		),
);
?>