<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$this->setFrameMode(false);

/**
 * @param array $seats
 * @param bool $without_seating
 * @return string
 */
function __getSeatsString (array $seats = null, $without_seating = false) {

    if($without_seating == 'true'){

        return "Без номера.";

    }
    else {

        $seats__ = \travelsoft\eurotrans\Utils::getSeatsArray($seats);

        return implode(", ", !empty($seats__) ? $seats__ : array("-"));

    }
}

// ссылки на предыдущие страницы
$this->SetViewTarget('route_link');
if (strlen($arResult["previous_links"]["routes"])) {
    echo $arResult["previous_links"]["routes"];
} else {
    echo "/routes/";
}
$this->EndViewTarget();

$this->SetViewTarget('tickets_link');
if (strlen($arResult["previous_links"]["tickets"])) {
    echo $arResult["previous_links"]["tickets"];
} else {
    echo "/event-registration/";
}
$this->EndViewTarget();
/*if ($_SESSION["__eurotrans_booking_is_ok"]) {?>
    <div class="booking-block-inner">
        <div class="booking-block-head">
            <div class="title h2">
                <span>Бронирование билетов прошло успешно. Для детальной информации пройдите в личный кабинет.</span>
            </div>
        </div>
    </div>
<?unset($_SESSION["__eurotrans_booking_is_ok"]); return;}
*/
/*
if (!empty($arResult["ERRORS"])) {
    ?>

    <div class="booking-block-inner">
        <div class="booking-block-head">
            <div class="title h2">
                <span>Необходимо выбрать рейс.</span>
            </div>
        </div>
    </div>

    <?
    return;
}*/
$POST = $_POST["eurotrans"];
?>
<div class="booking-block-inner">
    <?/*<div class="booking-block-head">
        <div class="title h2">
            <span><?= $arResult['DEPARTURE_POINT_NAME'] ?></span> →
            <span><?= $arResult['ARRIVAL_POINT_NAME'] ?></span>
        </div>
    </div>*/?>
    <div id="tickets-block">
        <div class="input-item s100">
            <div class="title h3">
               Информация о выбранной поездке:
            </div>
        </div>
        <div class="input-item s50 feedback-item-reply">
            <?/*<div class="title h3">
                <span class="bold"><?= $arResult['DEPARTURE_POINT_NAME'] ?></span> →
                <span class="bold"><?= $arResult['ARRIVAL_POINT_NAME'] ?></span>
            </div>*/?>
            <?
            $k = "trip";
            $total_price = 0;
            ?>
            <?
            if ($arResult["details"][$k]["transfer"]) {
                $transfer = $arResult["details"][$k]["transfer"];
                ?>

                <ul class="tickets-info">
                    <li class="bold">Трансфер</li>
                    <li>Дата поездки: <span class="bold date"><?= $transfer["departure_date"] ?></span></li>
                    <li>Отправление из: <span class="bold location-from"><?= $transfer["info"]["location_arr"][$arResult["location_from"]]["name"] ?> в <?= $transfer["departure_time"] ?></span></li>
                    <li>Прибытие в: <span class="bold location-to"><?= $transfer["info"]["location_arr"][$transfer["location_to_id"]]["name"] ?> в <?= $transfer["arrival_time"] ?></span></li>
                    <li>Рейс: <span class="bold trip"><?= current($transfer["info"]["location_arr"])["name"] . " - " . $transfer["info"]["location_arr"][$transfer["location_to_id"]]["name"] ?>  </span></li>
                    <li>Место:<?= __getSeatsString($transfer["seats"], $arResult["without_seating"]); ?></li>
                    <li>Стоимость: <span class="price bold"><?= $transfer["price"] . " " . $transfer["currency"] ?></span></li>
                    <? $total_price += $transfer["price"] ?>
                </ul>
            <? } ?>

            <? $way = $arResult["details"][$k]["way"] ?>

            <ul class="tickets-info">
                <? if ($arResult["details"][$k]["transfer"]): ?>
                    <li class="bold">Основной рейс</li>
                <? endif ?>
                <li>Дата поездки: <span class="bold date"><?= $arResult["date_from"] ?></span></li>
                <li>Отправление из: <span class="bold location-from"><?= $way["info"]["location_arr"][$way["location_from_id"]]["name"] ?> в <?= $way["departure_time"] ?></span></li>
                <li>Прибытие в: <span class="bold location-to"><?= $way["info"]["location_arr"][$way["location_to_id"]]["name"] ?> в <?= $way["arrival_time"] ?></span></li>
                <li>Рейс: <span class="bold trip"><?= current($way["info"]["location_arr"])["name"] . " - " . $way["info"]["location_arr"][$way["location_to_id"]]["name"] ?>  </span></li>
                <li>Место: <span class="seats bold"><?= __getSeatsString($way["seats"], $arResult["without_seating"]); ?></span></li>
                <li>Тип билетов: <? /*<span class="price bold"><?= $way["price"] . " " . $way["currency"] ?></span>*/ ?>
                    <span class="price bold"><br/>
                        <? $count_tickets_types = 0; ?>
                        <? foreach ($way["tickets_category_count"] as $ID => $ticket): ?>
                            <? if ($ticket["count"] > 0): ?>
                                <? if ($count_tickets_types == 0): ?>
                                    Взрослый:
                                <? elseif ($count_tickets_types == 1): ?>
                                    Детский:
                                <? elseif ($count_tickets_types == 2): ?>
                                    Старческий:
                                <? endif; ?>
                                <?= $ticket["amount"] . " " . $way["currency"] ?> x<?= $ticket["count"] ?><br/>
                            <? endif; ?>
                            <?$count_tickets_types++;?>
                        <? endforeach; ?>
                    </span>
                </li>
                <? $total_price += $way["price"] ?>
            </ul>

        </div>
        <?
        $k = "return_trip";
        if ($arResult["details"][$k]):
            ?>
            <div class="input-item s50 feedback-item-reply">
                <?/*<div class="title h3">
                    <span class="bold"><?= $arResult['ARRIVAL_POINT_NAME'] ?></span> →
                    <span class="bold"><?= $arResult['DEPARTURE_POINT_NAME'] ?></span>
                </div>*/?>

                <? $way = $arResult["details"][$k]["way"];?>

                <ul class="tickets-info">
                    <? if ($arResult["details"][$k]["transfer"]): ?>
                        <li class="bold">Основной рейс</li>
                    <? endif ?>
                    <li>Дата: <span class="bold date"><?= $arResult["date_back"] ?></span></li>
                    <li>Отправление из: <span class="bold location-from"><?= $way["info"]["location_arr"][$way["location_from_id"]]["name"] ?> в <?= $way["departure_time"] ?></span></li>
                    <li>Прибытие в: <span class="bold location-to"><?= $way["info"]["location_arr"][$way["location_to_id"]]["name"] ?> в <?= $way["arrival_time"] ?></span></li>
                    <li>Рейс: <span class="bold trip"><?= current($way["info"]["location_arr"])["name"] . " - " . $way["info"]["location_arr"][$way["location_to_id"]]["name"] ?>  </span></li>
                    <li>Место: <span class="seats bold"><?= __getSeatsString($way["seats"], $arResult["without_seating"]); ?></span></li>
                    <li>Тип билетов: <? /*<span class="price bold"><?= $way["price"] . " " . $way["currency"] ?></span>*/ ?>
                        <span class="price bold"><br/>
                            <? $count_tickets_types = 0; ?>
                            <? foreach ($way["tickets_category_count"] as $ID => $ticket): ?>
                                <? if ($ticket["count"] > 0): ?>
                                    <? if ($count_tickets_types == 0): ?>
                                        Взрослый:
                                    <? elseif ($count_tickets_types == 1): ?>
                                        Детский:
                                    <? elseif ($count_tickets_types == 2): ?>
                                        Старческий:
                                    <? endif; ?>
                                    <?= $ticket["amount"] . " " . $way["currency"] ?> x<?= $ticket["count"] ?><br/>
                                <? endif; ?>
                                <?$count_tickets_types++;?>
                            <? endforeach; ?>
                    </span>
                    </li>
                    <? $total_price += $way["price"] ?>
                </ul>

                <?
                if ($arResult["details"][$k]["transfer"]) {
                    $transfer = $arResult["details"][$k]["transfer"];
                    ?>
                    <ul class="tickets-info">
                        <li class="bold">Трансфер</li>
                        <li>Дата: <span class="bold date"><?= $transfer["departure_date"] ?></span></li>
                        <li>Отправление из: <span class="bold location-from"><?= $transfer["info"]["location_arr"][$transfer["location_from_id"]]["name"] ?> в <?= $transfer["departure_time"] ?></span></li>
                        <li>Прибытие в: <span class="bold location-to"><?= $transfer["info"]["location_arr"][$arResult["location_from"]]["name"] ?> в <?= $transfer["arrival_time"] ?></span></li>
                        <li>Рейс: <span class="bold trip"><?= current($transfer["info"]["location_arr"])["name"] . " - " . $transfer["info"]["location_arr"][$transfer["location_to_id"]]["name"] ?>  </span></li>
                        <li>Место: <span class="seats bold"><?= __getSeatsString($transfer["seats"], $arResult["without_seating"]); ?></span></li>
                        <li>Стоимость: <span class="price bold"><?= $transfer["price"] . " " . $transfer["currency"] ?></span></li>
                        <? $total_price += $transfer["price"] ?>
                    </ul>
                <? } ?>

            </div>
        <? endif ?>
    </div>
    <div id="total-price-block">
        <div class="title h3 bold">Общая стоимость: <span class="total-price"><?= $total_price . ' ' . travelsoft\eurotrans\Settings::CURRENT_CURRENCY ?></span></div>
    </div>
    <form action="<?= POST_FORM_ACTION_URI ?>" id="booking-details" method="POST">
        <?= bitrix_sessid_post()?>
        <div class="passangers-block form-block-section">
            <? $people = $arResult["adults"] + $arResult["children"] + $arResult["social"] ?>

            <? for ($i = 1; $i <= $people; $i++): ?>
                <div class="input-item s50">
                    <div class="title h4">
                        <span id="passanger-title-<?= $i?>" class="passanger-title bold">Пассажир №<?= $i ?></span>
                    </div>
                    <div class="input-form">
                        <div class="input-item s100">
                            <div class="input-wrapper">
                                <label class="f-s">Имя<span style="color:red">*</span></label>
                                <? $rstr = randString(7) ?>
                                <div data-scroll-to-id="passanger-title-<?= $i?>" id="error-area-<?= $rstr ?>" class="error-area"></div>
                                <input required data-link-error-area="<?= $rstr ?>" data-need-validation="yes" data-validators="is_empty" data-validation-f="Имя" class="input-main" type="text" name="eurotrans[passangers][<?= $i ?>][name]" value="<?= htmlspecialchars($POST["passangers"][$i]["name"]) ?>">
                            </div>
                        </div>
                        <div class="input-item s100">
                            <div class="input-wrapper">
                                <label class="f-s">Фамилия<span style="color:red">*</span></label>
                                <? $rstr = randString(7) ?>
                                <div id="error-area-<?= $rstr ?>" class="error-area"></div>
                                <input required data-link-error-area="<?= $rstr ?>" data-need-validation="yes" data-validators="is_empty" data-validation-f="Фамилия" class="input-main" type="text" name="eurotrans[passangers][<?= $i ?>][last_name]" value="<?= htmlspecialchars($POST["passangers"][$i]["name"]) ?>">
                            </div>
                        </div>
                        <div class="input-item s100">
                            <div class="input-wrapper">
                                <? if (!empty($arResult['ERRORS']['PHONE'])): ?>
                                    <span class="select-text" style="color: red; padding-left: 0px;">
                                        <?= $arResult['ERRORS']['PHONE'][$i]; ?>
                                    </span>
                                <? endif; ?>
                                <label class="f-s">Телефон(формат +375291234567) <?if($i == "1"):?><span style="color:red">*</span><?endif;?></label>
                                <? $rstr = randString(7) ?>
                                <div id="error-area-<?= $rstr ?>" class="error-area"></div>
                                <? /* <div class="phone-prefix">+375</div> */ ?><input  <?if($i == "1"):?>required<?endif;?> data-link-error-area="<?= $rstr ?>" data-need-validation="yes" data-validators="is_empty|is_phone" data-validation-f="Телефон" class="input-main" type="text" name="eurotrans[passangers][<?= $i ?>][phone]" value="<?= htmlspecialchars($POST["passangers"][$i]["phone"]) ?>">
                            </div>
                        </div>
                        <div class="input-item s100">
                            <div class="input-wrapper">
                                <? if (!empty($arResult['ERRORS']['PHONE_ADDITIONAL'])): ?>
                                    <span class="select-text" style="color: red; padding-left: 0px;">
                                        <?= $arResult['ERRORS']['PHONE_ADDITIONAL'][$i]; ?>
                                    </span>
                                <? endif; ?>
                                <label class="f-s">Второй телефон(формат +375291234567) </label>
                                <? $rstr = randString(7) ?>
                                <div id="error-area-<?= $rstr ?>" class="error-area"></div>
                                <? /* <div class="phone-prefix">+375</div> */ ?>
                                <input data-link-error-area="<?= $rstr ?>" data-need-validation="yes" data-validators="is_empty|is_phone" data-validation-f="Телефон дополнтельный" class="input-main" type="text" name="eurotrans[passangers][<?= $i ?>][phone_additional]" value="<?= htmlspecialchars($POST["passangers"][$i]["phone_additional"]) ?>">
                            </div>
                        </div>
                        <div class="input-item s100">
                            <div class="input-wrapper">
                                <label data- class="f-s">Гражданство</label>
                                <? $rstr = randString(7) ?>
                                <div id="error-area-<?= $rstr ?>" class="error-area"></div>
                                <div id="social-box" class="select-check js-select-custom">
                                    <button class="selects" data-placeholder="">
                                        <span class="btn-text"></span>
                                        <i class="arr-down">
                                            <svg class="icon icon-drop">
                                            <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </button>
                                    <div class="dropdown-target">
                                        <div class="dropdown-inner">
                                            <div class="dropdown-content">
                                                <div data-need-validation="yes" data-link-error-area="<?= $rstr ?>" data-validators="custom_select_is_selected" data-validation-fn="eurotrans[passangers][<?= $i ?>][citizensip]" class="select-u-body select-list text-center" >
                                                    <label class="option">
                                                        <input checked type="radio" name="eurotrans[passangers][<?= $i ?>][citizensip]" value="21">
                                                        <span class="social-title">Беларусь</span>
                                                    </label>
                                                    <label class="option">
                                                        <input type="radio" name="eurotrans[passangers][<?= $i ?>][citizensip]" value="183">
                                                        <span class="social-title">РФ</span>
                                                    </label>
                                                    <?/*<label class="option">
                                                        <input type="radio" name="eurotrans[passangers][<?= $i ?>][citizensip]" value="233">
                                                        <span class="social-title">Украина</span>
                                                    </label>*/?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input-wrapper">
                            <div class="select-item">
                                <label class="select-label js-condition">
                                    <span class="select-main">
                                        <input data-passanger="<?= $i ?>" class="is-payer select-real" type="radio" value="1" name="is_payer">
                                        <span class="select-checked"></span>
                                    </span>
                                    <span class="select-text">Является плательщиком</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            <? endfor ?>

        </div>

        <div class="payer-block form-block-section">
            <div class="input-item s50">
                <div class="title h4">
                    <span id="payer-title" class="passanger-title bold">Плательщик</span>
                </div>
                <div class="input-form">
                    <div class="input-item s100">
                        <div class="input-wrapper">
                            <?if(!empty($arResult['ERRORS']['PAYER']['NAME'])):?>
                                <span class="select-text" style="color: red; padding-left: 0px;">
                                    <?= $arResult['ERRORS']['PAYER']['NAME']; ?>
                                </span>
                            <?endif;?>
                            <label class="f-s">Имя<span style="color:red">*</span></label>
                            <? $rstr = randString(7) ?>
                            <div data-scroll-to-id="payer-title" id="error-area-<?= $rstr ?>" class="error-area"></div>
                            <input required data-link-error-area="<?= $rstr ?>" data-need-validation="yes" data-validators="is_empty" data-validation-f="Имя" class="input-main" type="text" name="eurotrans[payer][name]" value="<?= htmlspecialchars($POST["payer"]["name"]) ?>">
                        </div>
                    </div>
                    <div class="input-item s100">
                        <div class="input-wrapper">
                            <?if(!empty($arResult['ERRORS']['PAYER']['NAME'])):?>
                                <span class="select-text" style="color: red; padding-left: 0px;">
                                    <?= $arResult['ERRORS']['PAYER']['NAME']; ?>
                                </span>
                            <?endif;?>
                            <label class="f-s">Фамилия<span style="color:red">*</span></label>
                            <? $rstr = randString(7) ?>
                            <div id="error-area-<?= $rstr ?>" class="error-area"></div>
                            <input required data-link-error-area="<?= $rstr ?>" data-need-validation="yes" data-validators="is_empty" data-validation-f="Фамилия" class="input-main" type="text" name="eurotrans[payer][last_name]" value="<?= htmlspecialchars($POST["payer"]["name"]) ?>">
                        </div>
                    </div>
                    <div class="input-item s100">
                        <div class="input-wrapper">
                            <?if(!empty($arResult['ERRORS']['PAYER']['PHONE'])):?>
                                <span class="select-text" style="color: red; padding-left: 0px;">
                                    <?= $arResult['ERRORS']['PAYER']['PHONE']; ?>
                                </span>
                            <?endif;?>
                            <label class="f-s">Телефон<span style="color:red">*</span></label>
                            <? $rstr = randString(7) ?>
                            <div id="error-area-<?= $rstr ?>" class="error-area"></div>
                            <? /* <div class="phone-prefix">+375</div> */ ?>
                            <input required data-link-error-area="<?= $rstr ?>" data-need-validation="yes" data-validators="is_empty|is_phone" data-validation-f="Телефон" class="input-main" type="text" name="eurotrans[payer][phone]" value="<?= htmlspecialchars($POST["payer"]["phone"]) ?>">
                        </div>
                    </div>
                    <div class="input-item s100">
                        <div class="input-wrapper">
                            <label class="f-s">Email<span style="color:red">*</span></label>
                            <? $rstr = randString(7) ?>
                            <div id="error-area-<?= $rstr ?>" class="error-area"></div>
                            <input required data-link-error-area="<?= $rstr ?>" data-need-validation="yes" data-validators="is_email" data-validation-f="Email" class="input-main" type="email" name="eurotrans[payer][email]" value="<?= htmlspecialchars($POST["payer"]["email"]) ?>">
                        </div>
                    </div>
                    <div class="input-item s100">
                        <?if(!empty($arResult["payment_methods"])):?>
                        <div class="input-wrapper">
                            <label class="f-s bold">Оплата: </label>
                            <?foreach ($arResult["payment_methods"] as $payment_method):?>
                                <label class="f-s"><?=$payment_method["label"]?></label>
                                <input name="eurotrans[payer][payment_method]" value="<?=$payment_method["name"]?>" <?if($payment_method["selected"]):?>checked<?endif;?> type="radio">
                            <?endforeach;?>
                        </div>
                        <? endif;?>
                        <?/*
                        <div class="input-wrapper">
                            <label class="f-s bold">Оплата: </label>
                            <label class="f-s">Наличными</label>
                            <input name="eurotrans[payer][payment_method]" value="cash" checked type="radio">
                            <label class="f-s">Online</label>
                            <input name="eurotrans[payer][payment_method]" value="creditcard" type="radio">
                        </div>
                        */?>
                    </div>
                    <div style="color: #aaabad; font-size:12px">
                    <span style="color: red">*</span> - поля для обязательного заполнения
                    </div>
                    <div class="input-wrapper">
                        <div class="select-item">
                            <label class="select-label js-condition">
                                <span class="select-main">
                                    <input required class="is-payer select-real" type="checkbox" name="agree_with_rules">
                                    <span class="select-checked"></span>
                                </span>
                                <span class="select-text" style="display: inline-block; white-space: normal">
                                    Согласен с
                                    <a href="/customers/pravila-perevozki/" style="display: inline-block;" target="_blank">правилами перевозки пассажиров</a>,
                                    <a href="/customers/baggage_rules/" style="display: inline-block;" target="_blank">правилами провоза багажа</a> и
                                    <a href="/customers/offer/" style="display: inline-block;" target="_blank">условиями оферты</a>.
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?/*<div class="input-item s50">
            <div class="input-wrapper">
                <label class="f-s">Введите слово с картинки</label>
                <input type="hidden" name="captcha_sid" value="<?= $arResult["captcha_sid"]; ?>" />
                <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["captcha_sid"]; ?>" alt="CAPTCHA" />
                <? $rstr = randString(7) ?>
                <div id="error-area-<?= $rstr ?>" class="error-area"></div>
                <input data-link-error-area="<?= $rstr ?>" data-need-validation="yes" data-validators="is_empty" class="input-main" type="text" name="captcha_word">
            </div>
        </div>*/?>
        <div id="booking-btn-block">
            <span id="eurotrans-booking-btn" style="display: inline-block;">
                <input type="submit" name="eurotrans[booking]" value="Бронировать" class="btn btn-colored">
            </span>
        </div>
    </form>
</div>
