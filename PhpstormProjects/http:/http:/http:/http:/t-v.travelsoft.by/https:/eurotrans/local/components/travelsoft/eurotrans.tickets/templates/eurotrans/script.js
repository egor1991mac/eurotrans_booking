/**
 * Eurotrans booking of tickets
 *
 * @author dimabresky https://github.com/dimabresky
 * @copyright 2017, travelsoft
 */

BX.ready(function () {

    var bookrequest = {};

    var buses = {};

    var route_titles = {};

    var previousLinks = {};

    // сетка по рассадке в автобусе
    var seatsGrid = {

        __grid: {},

        createRow: function (group, bus_id, cols_cnt) {
            if (typeof this.__grid[group] !== "object") {
                this.__grid[group] = {};
            }

            this.__grid[group][bus_id] = [];
            for (var i = 0; i < cols_cnt; i++) {
                this.__grid[group][bus_id].push(null);
            }


        },

        reset: function (group, bus_id) {
            for (var i = 0; i < this.__grid[group][bus_id].length; i++) {
                this.__grid[group][bus_id][i] = null;
            }
        },

        add: function (group, bus_id, value) {

            for (var i = 0; i < this.__grid[group][bus_id].length; i++) {

                if (!this.__grid[group][bus_id][i]) {
                    this.__grid[group][bus_id][i] = value;
                    return i;
                }
            }

            return null;

        },

        remove: function (group, bus_id, col) {
            this.__grid[group][bus_id][col] = null;
        }

    };

    // отображение кнопки бронирования
    function __view_bf(bf_id) {

        return `${`<div class="table-item"></div>`.repeat(4)}
                        <div class="table-item">
                                <a data-book-details='${JSON.stringify(bookrequest)}' class="btn btn-colored small js-book-link" id="${bf_id}"><span>Бронировать</span></a>
                        </div>`;

    }

    // отображение карты автобуса
    function __view_map(data) {

        return `
            <div class="bus-map-container bsMapHolder pjBsSeatsContainer" style="height: ${data.avail_arr.bus_type_arr.base64_seats_map_size[1] + 20}px">
            <img class="bus-map-img" src="${data.avail_arr.bus_type_arr.base64_seats_map}" alt=""/>
            
            ${(function (seat_arr, booked_seat_arr, holding_seats_arr, bus_id, group) {
            
            var html = ``;
            var seat, isAv = false;

            for (var i = 0; i < seat_arr.length; i++) {

                seat = seat_arr[i];
                isAv = typeof booked_seat_arr[seat.id] === "string";

                if(isAv === false) {
    
                    isAv = typeof holding_seats_arr[seat.id] === "string";
                    if(isAv === true) {
                        //console.log(seat);
                        //значит место холдировано
                    }
    
                }
                
                html += `<span data-group="${group}" data-col="null" data-bus-id="${bus_id}" data-seat-id="${BX.util.strip_tags(seat.id)}" data-seat="${BX.util.strip_tags(seat.name)}" ${isAv ? '' : `onclick="eurotrans.seat_choose_process(this)"`} rel="hi_${BX.util.strip_tags(seat.id)}" 
                                        class="rect empty${isAv ? ' bs-booked' : ' bs-available'}" 
                                        data-name= "${BX.util.strip_tags(seat.name)}" 
                                        style= "width: ${BX.util.strip_tags(seat.width)}px; 
                                        height: ${BX.util.strip_tags(seat.height)}px; 
                                        left: ${BX.util.strip_tags(seat.left)}px; 
                                        top: ${BX.util.strip_tags(seat.top)}px;
                                        background-color: ${isAv ? '#f32a2a' : 'green'};
                                        line-height: ${BX.util.strip_tags(seat.height)}px">
                                            <span
                                                class="bsInnerRect" data-name="hi_${BX.util.strip_tags(seat.id)}">${BX.util.strip_tags(seat.name)}</span></span>`;
            }
            return html;

        })(data.seat_arr, data.avail_arr.booked_seat_arr, data.avail_arr.holding_seats_arr, data.bus_id, data.group)}
            
            </div>
            <div data-group="${data.group}" data-bus-id="${data.bus_id}" class="bus-title" onclick="eurotrans.resetAlSeats(this)" style="display: none">
                <div class="ajaxCountSeatsSelected"></div>
                <a>Выбрать другое место(а)</a>
            </div>
            <div class="leftColor">
                <span class="bs-available" style="background-color: green"></span> - Свободно
            </div>
            <div class="leftColor">
                <span class="bs-available" style="background-color: #ff8400"></span> - Выбрано
            </div>
            <div class="leftColor">
                <span class="bs-available" style="background-color: #f32a2a"></span> - Занято
            </div>
        `;
    }

    // подсчет общего количества элементов
    function __people_cnt() {

        var cnt = 0;
        var adults = Number(bookrequest.adults);
        var children = Number(bookrequest.children);
        var social = Number(bookrequest.social);

        if (!isNaN(adults)) {
            cnt += adults;
        }

        if (!isNaN(children)) {
            cnt += children;
        }

        if (!isNaN(social)) {
            cnt += social;
        }

        return cnt;
    }

    // включение спинера на кнопке
    function __spinerEnable(el) {
        BX.addClass(el, "has-animation");
        BX.addClass(el, "animated");
    }

    // выключение спинера на кнопке
    function __spinerDisable(el) {
        BX.removeClass(el, "has-animation");
        BX.removeClass(el, "animated");
    }

    // валидация выбранных мест перед отправкой на страницу бронирования
    function __seats_validation(e) {

        var not_choosed = false;
        var self = this;
        var data_for_check_seats = {};
        e.preventDefault();

        __spinerEnable(this.btn);
        for (var bus_id in seatsGrid.__grid[this.group]) {
            for (var i = 0; i < __people_cnt(); i++) {
                if (seatsGrid.__grid[this.group][bus_id][i] === null) {
                    not_choosed = true;
                    break;
                }
            }
            for (var prop in buses[this.group]) {
                for (var prop_ in buses[this.group][prop]) {
                    if (buses[this.group][prop][prop_] && buses[this.group][prop][prop_].bus_id === bus_id) {
                        buses[this.group][prop][prop_].seats = seatsGrid.__grid[this.group][bus_id];
                        data_for_check_seats[bus_id] = {
                            group: this.group,
                            bus_id: buses[this.group][prop][prop_].bus_id,
                            pickup_id: buses[this.group][prop][prop_].location_from_id,
                            return_id: buses[this.group][prop][prop_].location_to_id,
                            departure_time: buses[this.group][prop][prop_].departure_time,
                            departure_date: buses[this.group][prop][prop_].departure_date,
                            arrival_date: buses[this.group][prop][prop_].arrival_date,
                            arrival_time: buses[this.group][prop][prop_].arrival_time,
                            seats: buses[this.group][prop][prop_].seats
                        };
                    }
                }
            }

            if (not_choosed) {
                alert('Не выбраны места для рассадки');
                __spinerDisable(this.btn);

                return;
            }
        }

        __checkSeatsAvalible(data_for_check_seats)
            .then(function () {

                var bookDetails = JSON.parse(self.btn.dataset.bookDetails);
                var bookRequest = {eurotrans: bookDetails, sessid: BX.bitrix_sessid()};
                bookRequest.eurotrans.details = buses[self.group];
                bookRequest.eurotrans.previous_links = previousLinks;
                bookRequest.eurotrans.without_seating = self.without_seating;
                BX.ajax.get("/local/components/travelsoft/eurotrans.tickets/ajax/save_booking_request.php", bookRequest, function (json_resp) {
                    var resp = JSON.parse(json_resp);
                    console.log(resp);
                    if (resp.error) {
                        alert("Возникла ошибка при попытке сохранить данные по брониванию. Пожалуйста, перезагрузите страницу и повторите процедуру бронирования. Спасибо.");
                        __spinerDisable(self.btn);
                        return;
                    }

                    if(resp.time){
                        window.location = "?redirect&time=" + resp.time;
                    }
                    else {
                        window.location = "?redirect";
                    }
                });
            })
            .catch(function (d) {

                var message_data = [];
                // обработка занятых мест или ошибки
                if (typeof d.error === "boolean") {
                    alert("Произошла ошибка. Пожалуйста, перезагрузите страницу и повторите попытку бронирования. Спасибо.");
                } else if (d.not_avail) {

                    for (bus_id in d.not_avail) {

                        if (d.not_avail.hasOwnProperty(bus_id)) {
                            continue;
                        }
                        message_data.push(`Место ${d.not_avail[bus_id].join(',')} по рейсу ${route_titles[bus_id]} занято.`);
                    }

                    message_data.push(`Пожалуйста, выберите другие места. Спасибо.`);
                    alert(message_data.join("\n"));
                }

                __spinerDisable(self.btn);
            });
    }

    // проверка мест на доступность
    function __checkSeatsAvalible(parameters) {

        var data = {
            details: parameters,
            sessid: BX.bitrix_sessid()
        };

        var seat, not_avail = {};
        return new Promise(function (resolve, reject) {

            BX.ajax.get("/local/components/travelsoft/eurotrans.tickets/ajax/get_booked_seats.php", data, function (json_resp) {
                var resp = JSON.parse(json_resp);

                if (resp.error) {

                    reject({error: true});
                } else {

                    for (var bus_id in resp.data) {
                        if (resp.data.hasOwnProperty(bus_id) &&
                            parameters.hasOwnProperty(bus_id)) {

                            for (var seat_id in resp.data[bus_id]) {

                                if (parameters[bus_id].seats.hasOwnProperty(seat_id)) {

                                    // убираем возможность выбра на карте автобуса
                                    seat = document.querySelector(`span[data-seat-id='${seat_id}']`);
                                    seat.style["background-color"] = "red";
                                    seat.onclick = null;

                                    // очищаем сетку размещений
                                    seatsGrid.reset(parameters[bus_id].group, bus_id);
                                    if (!not_avail.hasOwnProperty(bus_id)) {
                                        not_avail[bus_id] = [];
                                    }
                                    not_avail[bus_id].push(parameters[bus_id].seats.seat);
                                }
                            }
                        }
                    }

                    if (not_avail.length) {
                        reject({not_avail: not_avail});
                    } else {
                        resolve();
                    }
                }
            });
        });

    }

    // namespace
    if (!window.eurotrans) {
        window.eurotrans = {};
    }

    /**
     * Загрузка карт автобусов
     * @param {String} btn_id
     * @param {Object} parameters
     * @returns {undefined}
     */
    window.eurotrans.loadBusMap = function (btn_id, parameters) {

        var data = {
            bus_id: parameters[0].BUS_ID,
            pickup_id: parameters[0].LOCATION_FROM_ID,
            return_id: parameters[0].LOCATION_TO_ID,
            departure_time: parameters[0].DEPARTURE_TIME,
            departure_date: parameters[0].DEPARTURE_DATE,
            arrival_date: parameters[0].ARRIVAL_DATE,
            arrival_time: parameters[0].ARRIVAL_TIME,
            sessid: BX.bitrix_sessid()
        };

        var bx_btn = BX(btn_id);

        __spinerEnable(bx_btn);

        BX.ajax.get("/local/components/travelsoft/eurotrans.tickets/ajax/loadmap.php", data, function (json_d) {

            var d = JSON.parse(json_d);

            d.end = typeof parameters[1] !== 'object';

            if (!d.error && typeof d.data.avail_arr.bus_type_arr.base64_seats_map === "string") {

                d.data.group = parameters[0].GROUP;
                d.data.bf_id = "booking-request-" + d.data.group;
                d.data.bus_id = data.bus_id;
                d.data.route_title = `${d.data.location_from}-${d.data.location_to}`;
                route_titles[data.bus_id] = d.data.route_title;
                BX(parameters[0].HTML_ID).innerHTML = __view_map(d.data);
                seatsGrid.createRow(d.data.group, data.bus_id, __people_cnt());
                if (d.end) {
                    BX.insertAfter(BX.create("div", {
                        html: __view_bf(d.data.bf_id),
                        props: {className: "table-row"}
                    }), BX(parameters[0].HTML_ID));
                    BX.bind(BX(d.data.bf_id), 'click', BX.delegate(__seats_validation, {
                        btn: BX(d.data.bf_id),
                        group: d.data.group,
                        without_seating: false
                    }));
                }
            }
            else{

                d.data.group = parameters[0].GROUP;
                d.data.bf_id = "booking-request-" + d.data.group;
                d.data.bus_id = data.bus_id;
                d.data.route_title = `${d.data.location_from}-${d.data.location_to}`;
                seatsGrid.createRow(d.data.group, data.bus_id, __people_cnt());

                BX(parameters[0].HTML_ID).innerHTML = generateHtmlForBusWithoutSeats(d.data);

                if (d.end) {
                    BX.insertAfter(BX.create("div", {
                        html: __view_bf(d.data.bf_id),
                        props: {className: "table-row"}
                    }), BX(parameters[0].HTML_ID));
                    BX.bind(BX(d.data.bf_id), 'click', BX.delegate(__seats_validation, {
                        btn: BX(d.data.bf_id),
                        group: d.data.group,
                        without_seating: true
                    }));
                }
            }

            if (!d.end) {

                window.eurotrans.loadBusMap(btn_id, (function () {
                    var _p = [];
                    for (var i = 1; i < parameters.length; i++) {
                        _p.push(parameters[i]);
                    }
                    return _p;
                })(parameters));
            } else {
                __spinerDisable(bx_btn);
            }
        });

        bx_btn.href = "javascript:void(0)";
    };

    function generateHtmlForBusWithoutSeats(data){
        var people = 0, oneSeat, bookingSeats = [];
        var peopleCount = __people_cnt();
        var isAv = false;
        var html = '';

        for(var i = 0; i < data.seat_arr.length; i++){

            oneSeat = data.seat_arr[i];
            isAv = typeof data.avail_arr.booked_seat_arr[oneSeat.id] === "string";
            if(!isAv){
                isAv = typeof data.avail_arr.holding_seats_arr[oneSeat.id] === "string";
                if(!isAv) {
                    people++;
                    bookingSeats.push(oneSeat);
                }
            }
            if(people === peopleCount){
                break;
            }
        }

        html += '<div class="content-column-content"><div class="content-column-content-inner"><p>';
        html += '<span style="color: #2e2e2e;">На данном рейсе не предусмотрен выбор места в автобусе. Система производит рассадку в автоматическом режиме.</span><br/>';

        let countSeats = 0;
        for (let field in data.avail_arr.booked_seat_arr) {
            countSeats++;
        }

        countSeats = data.seat_arr.length - countSeats;

        html += '<span style="color: #2e2e2e;"> Свободных мест: ' + countSeats + '</span>';

        if(people < peopleCount){
            html += '<span style="color: #2e2e2e;">Не хватает свободных мест.</span>';
        }
        else {

            bookingSeats.forEach(
                function (item) {

                    let bus_id = data.bus_id;
                    let group = data.group;

                    let seat = BX.util.strip_tags(item.name);
                    let seat_id = BX.util.strip_tags(item.id);

                    let value = {};
                    value[seat_id] = seat;

                    seatsGrid.add(group, bus_id, value);
                }
            );
        }

        html += '</p></div></div>';

        return html;
    }

    /**
     * @param {Object} request
     * @returns {undefined}
     */
    window.eurotrans.loadRequest = function (request) {
        bookrequest = request;
    };

    /**
     * @param {Object} links
     * @returns {undefined}
     */
    window.eurotrans.loadPreviousLinks = function (links) {
        previousLinks = links;
    };

    /**
     * Загрузка параметров рейсов
     * @param {Object} parameters
     * @returns {undefined}
     */
    window.eurotrans.loadBuses = function (parameters) {

        var __buses = {};

        if (bookrequest.round_trip) {

            __buses = {
                trip: {
                    transfer: null,
                    way: null
                },
                return_trip: {
                    way: null,
                    transfer: null
                }
            };

            for (var i = 0; i < parameters.length; i++) {

                if (parameters[i].IS_RETURN) {

                    if (parameters[i].IS_TRANSFER) {
                        __buses.return_trip.transfer = {
                            tickets_category_count: parameters[i].TICKETS_CATEGORY_COUNT,
                            is_transfer: parameters[i].IS_TRANSFER,
                            is_return: parameters[i].IS_RETURN,
                            departure_date: parameters[i].DEPARTURE_DATE,
                            departure_time: parameters[i].DEPARTURE_TIME,
                            arrival_date: parameters[i].ARRIVAL_DATE,
                            arrival_time: parameters[i].ARRIVAL_TIME,
                            location_from_id: parameters[i].LOCATION_FROM_ID,
                            location_to_id: parameters[i].LOCATION_TO_ID,
                            bus_id: parameters[i].BUS_ID,
                            price: parameters[i].PRICE,
                            //price_without_discount: parameters[i].PRICE_WITHOUT_DISCOUNT,
                            currency: parameters[i].CURRENCY,
                            seats: null
                        };
                    } else {
                        __buses.return_trip.way = {
                            tickets_category_count: parameters[i].TICKETS_CATEGORY_COUNT,
                            is_transfer: parameters[i].IS_TRANSFER,
                            is_return: parameters[i].IS_RETURN,
                            departure_date: parameters[i].DEPARTURE_DATE,
                            departure_time: parameters[i].DEPARTURE_TIME,
                            arrival_date: parameters[i].ARRIVAL_DATE,
                            arrival_time: parameters[i].ARRIVAL_TIME,
                            location_from_id: parameters[i].LOCATION_FROM_ID,
                            location_to_id: parameters[i].LOCATION_TO_ID,
                            bus_id: parameters[i].BUS_ID,
                            price: parameters[i].PRICE,
                            price_without_discount: parameters[i].PRICE_WITHOUT_DISCOUNT,
                            price_2: parameters[i].PRICE_2,
                            price_without_discount_2: parameters[i].PRICE_WITHOUT_DISCOUNT_2,
                            currency: parameters[i].CURRENCY,
                            seats: null
                        };
                    }

                } else {

                    if (parameters[i].IS_TRANSFER) {
                        __buses.trip.transfer = {
                            tickets_category_count: parameters[i].TICKETS_CATEGORY_COUNT,
                            is_transfer: parameters[i].IS_TRANSFER,
                            is_return: parameters[i].IS_RETURN,
                            departure_date: parameters[i].DEPARTURE_DATE,
                            departure_time: parameters[i].DEPARTURE_TIME,
                            arrival_date: parameters[i].ARRIVAL_DATE,
                            arrival_time: parameters[i].ARRIVAL_TIME,
                            location_from_id: parameters[i].LOCATION_FROM_ID,
                            location_to_id: parameters[i].LOCATION_TO_ID,
                            bus_id: parameters[i].BUS_ID,
                            price: parameters[i].PRICE,
                            //price_without_discount: parameters[i].PRICE_WITHOUT_DISCOUNT,
                            currency: parameters[i].CURRENCY,
                            seats: null
                        };
                    } else {
                        __buses.trip.way = {
                            tickets_category_count: parameters[i].TICKETS_CATEGORY_COUNT,
                            is_transfer: parameters[i].IS_TRANSFER,
                            is_return: parameters[i].IS_RETURN,
                            departure_date: parameters[i].DEPARTURE_DATE,
                            departure_time: parameters[i].DEPARTURE_TIME,
                            arrival_date: parameters[i].ARRIVAL_DATE,
                            arrival_time: parameters[i].ARRIVAL_TIME,
                            location_from_id: parameters[i].LOCATION_FROM_ID,
                            location_to_id: parameters[i].LOCATION_TO_ID,
                            bus_id: parameters[i].BUS_ID,
                            price: parameters[i].PRICE,
                            price_without_discount: parameters[i].PRICE_WITHOUT_DISCOUNT,
                            price_2: parameters[i].PRICE_2,
                            price_without_discount_2: parameters[i].PRICE_WITHOUT_DISCOUNT_2,
                            currency: parameters[i].CURRENCY,
                            seats: null
                        };
                    }
                }
            }

        } else {

            __buses = {
                trip: {
                    transfer: null,
                    way: null
                }
            };

            for (var i = 0; i < parameters.length; i++) {
                if (parameters[i].IS_TRANSFER) {
                    __buses.trip.transfer = {
                        tickets_category_count: parameters[i].TICKETS_CATEGORY_COUNT,
                        is_transfer: parameters[i].IS_TRANSFER,
                        is_return: parameters[i].IS_RETURN,
                        departure_date: parameters[i].DEPARTURE_DATE,
                        departure_time: parameters[i].DEPARTURE_TIME,
                        arrival_date: parameters[i].ARRIVAL_DATE,
                        arrival_time: parameters[i].ARRIVAL_TIME,
                        location_from_id: parameters[i].LOCATION_FROM_ID,
                        location_to_id: parameters[i].LOCATION_TO_ID,
                        bus_id: parameters[i].BUS_ID,
                        price: parameters[i].PRICE,
                        currency: parameters[i].CURRENCY,
                        seats: null
                    };
                } else {
                    __buses.trip.way = {
                        tickets_category_count: parameters[i].TICKETS_CATEGORY_COUNT,
                        is_transfer: parameters[i].IS_TRANSFER,
                        is_return: parameters[i].IS_RETURN,
                        departure_date: parameters[i].DEPARTURE_DATE,
                        departure_time: parameters[i].DEPARTURE_TIME,
                        arrival_date: parameters[i].ARRIVAL_DATE,
                        arrival_time: parameters[i].ARRIVAL_TIME,
                        location_from_id: parameters[i].LOCATION_FROM_ID,
                        location_to_id: parameters[i].LOCATION_TO_ID,
                        bus_id: parameters[i].BUS_ID,
                        price: parameters[i].PRICE,
                        price_without_discount: parameters[i].PRICE_WITHOUT_DISCOUNT,
                        price_2: parameters[i].PRICE_2,
                        price_without_discount_2: parameters[i].PRICE_WITHOUT_DISCOUNT_2,
                        currency: parameters[i].CURRENCY,
                        seats: null
                    };
                }
            }
        }

        buses[parameters[0].GROUP] = __buses;
    };

    // обработка процесса выбора мест

    var arraySelectSeats = [];

    window.eurotrans.seat_choose_process = function (ceil) {

        var seat = ceil.dataset.seat;
        var seat_id = ceil.dataset.seatId;
        var value = {};
        var bus_id = ceil.dataset.busId;
        var group = ceil.dataset.group;
        var ceils;

        value[seat_id] = seat;

        if(typeof arraySelectSeats[group + '_' + bus_id] === 'undefined'){
            arraySelectSeats[group + '_' + bus_id] = [];
        }

        if (ceil.dataset.col === "null") {
            ceil.dataset.col = String(seatsGrid.add(group, bus_id, value));

            if (ceil.dataset.col !== "null") {

                ceil.style["background-color"] = "#ff8400";

                arraySelectSeats[group + '_' + bus_id].push(seat);

            } else {
                seatsGrid.reset(group, bus_id);
                ceils = document.querySelectorAll("span[data-bus-id='" + bus_id + "']");
                for (var i = 0; i < ceils.length; i++) {

                    if (ceils[i].dataset.col !== "null") {
                        ceils[i].dataset.col = "null";
                        ceils[i].style["background-color"] = "green";
                    }
                }
                arraySelectSeats[group + '_' + bus_id] = [];
                arraySelectSeats[group + '_' + bus_id].push(seat);

                ceil.dataset.col = String(seatsGrid.add(group, bus_id, value));
                ceil.style["background-color"] = "#ff8400";
            }

        } else {

            var indexSeat = arraySelectSeats[group + '_' + bus_id].indexOf(seat);
            arraySelectSeats[group + '_' + bus_id].splice(indexSeat, 1);

            seatsGrid.remove(group, bus_id, ceil.dataset.col);
            ceil.dataset.col = "null";
            ceil.style["background-color"] = "green";
        }
        appendNumberSeats(group, bus_id);
    };

    var appendNumberSeats = function (group, bus_id) {

        var ajaxContainer = $("div[data-bus-id=" + bus_id + "][data-group=" + group  + "]");
        var seats = ajaxContainer.find('.ajaxCountSeatsSelected');
        if(arraySelectSeats[group + '_' + bus_id].length !== 0){
            seats.html("Выбранное место(а): ");
            arraySelectSeats[group + '_' + bus_id].forEach(function (item, index, array) {
                if(index !== 0){
                    seats.append(', ');
                }
                seats.append(item);
            });

            ajaxContainer.slideDown();
        }
        else {
            
            ajaxContainer.slideUp();

        }

    };

    window.eurotrans.resetAlSeats = function (ceil) {
        var bus_id = ceil.dataset.busId;
        var group = ceil.dataset.group;
        var ceils;

        seatsGrid.reset(group, bus_id);
        ceils = document.querySelectorAll("span[data-bus-id='" + bus_id + "']");
        for (var i = 0; i < ceils.length; i++) {

            if (ceils[i].dataset.col !== "null") {
                ceils[i].dataset.col = "null";
                ceils[i].style["background-color"] = "green";
            }
        }

        arraySelectSeats[group + '_' + bus_id] = [];
        appendNumberSeats(group, bus_id);
    };

});