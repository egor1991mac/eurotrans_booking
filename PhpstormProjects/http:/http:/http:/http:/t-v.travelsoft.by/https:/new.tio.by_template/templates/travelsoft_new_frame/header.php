<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$oAsset = Bitrix\Main\Page\Asset::getInstance();
CJSCore::Init();
?><!doctype html>
<html lang="<?= LANGUAGE_ID ?>">
    <head>
        <? $APPLICATION->ShowHead() ?>
        <title><?= $APPLICATION->ShowTitle() ?></title>
        <meta charset="utf-8"/>
        <meta http-equiv="x-ua-compatible" content="ie=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="shortcut icon" type="image/png" href="/favicon.ico">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
        <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/assets/css/master.css"); ?>
        <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/assets/css/tscustom.css"); ?>
		<? $oAsset->addCss(SITE_TEMPLATE_PATH . "/assets/css/react-form.css"); ?>
 	 	<? $oAsset->addCss(SITE_TEMPLATE_PATH . "/assets/css/ts-style.css"); ?>
		<? $oAsset->addCss("https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css"); ?>
        <?/*
        <!--[if lt IE 9 ]>
        <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/separate-js/html5shiv-3.7.2.min.js" type="text/javascript"></script>
        <![endif]-->
        */?>
        <? $oAsset->addJs("https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js") ?>
		<? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/js/react-form.js") ?>
		<? $oAsset->addJs("https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js") ?>
		<? $oAsset->addJs(SITE_TEMPLATE_PATH . "/assets/js/ts-new-article.js") ?>

    </head>
    <body>
        <div class="animated-css" data-header="sticky" data-header-top="200" data-canvas="container">
            <div class="wrap-fixed-menu" id="fixedMenu">
                <nav class="fullscreen-center-menu">
                    <div class="menu-main-menu-container">
                        <div class="slidebar-nav-menu">
                            <nav>
                                <div id="dl-menu" class="dl-menuwrapper">
                                    <ul class="dl-menu nav navbar-nav">
                                        <?$APPLICATION->IncludeComponent(
                                            "bitrix:menu",
                                            "mobile.top.menu",
                                            Array(
                                                "ALLOW_MULTI_SELECT" => "N",
                                                "CHILD_MENU_TYPE" => "left",
                                                "DELAY" => "N",
                                                "MAX_LEVEL" => "2",
                                                "MENU_CACHE_GET_VARS" => array(""),
                                                "MENU_CACHE_TIME" => "3600",
                                                "MENU_CACHE_TYPE" => "N",
                                                "MENU_CACHE_USE_GROUPS" => "N",
                                                "ROOT_MENU_TYPE" => "top",
                                                "USE_EXT" => "Y"
                                            )
                                        );?>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </nav>
                <button type="button" class="fullmenu-close"><i class="fa fa-times"></i></button>
            </div>

            <header class="header header-boxed-width header-logo-black header-navbar-white header-topbarbox-1-left header-topbarbox-2-right header-navibox-1-left header-navibox-3-right header-navibox-4-right navbar-fixed-top">
                <div class="container container-boxed-width">
                    <div class="container">
                        <div class="header-navibox-2">
                            <ul class="yamm main-menu nav navbar-nav">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "top.menu",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "left",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "2",
                                        "MENU_CACHE_GET_VARS" => array(""),
                                        "MENU_CACHE_TIME" => "3600",
                                        "MENU_CACHE_TYPE" => "N",
                                        "MENU_CACHE_USE_GROUPS" => "N",
                                        "ROOT_MENU_TYPE" => "top",
                                        "USE_EXT" => "Y"
                                    )
                                );?>
                            </ul>
                        </div>
                        <div class="header-navibox-1">
                            <a class="navbar-brand scroll" <?if(defined("HOME_PAGE") && HOME_PAGE === true):?><?else:?>href="/"<?endif;?>>
                                <img class="normal-logo img-responsive" src="<?=SITE_TEMPLATE_PATH?>/assets/media/general/logo.png" alt="logo"/>
                                <img class="scroll-logo img-responsive hidden-xs" src="<?=SITE_TEMPLATE_PATH?>/assets/media/general/logo_small.png" alt="logo"/>
                            </a>
                        </div>
                        <div class="header-navibox-5">
                            <button class="menu-mobile-button visible-xs-block js-toggle-screen toggle-menu-button"><i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i></button>
                        </div>
                    </div>
                </div>
            </header>
                <h1 style="display: none"><?=$APPLICATION->ShowTitle(false)?></h1>
                <div class="section-area-frame">