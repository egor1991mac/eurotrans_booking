<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<? /*============= dataLayer part ============*/ ?>
<? if (!empty($arResult['DATA_LAYER'])): ?>
    <script>
        let arrDataLayer = JSON.parse('<?= json_encode($arResult['DATA_LAYER']);?>');
        
        dataLayer.push(arrDataLayer);
    </script>
<? endif; ?>
<? /*============= end dataLayer part ============*/ ?>