/**
 * Eurotrans booking of tickets
 * 
 * @author dimabresky https://github.com/dimabresky
 * @copyright 2017, travelsoft
 */

BX.ready(function () {

    var form = BX("booking-details");

    var isPayerCheckboxes = BX.findChildren(form, {attribute: {name: "is_payer"}}, true);
    
    // включение спинера на кнопке
    function __spinerEnable(el) {
        BX.addClass(el, "has-animation");
        BX.addClass(el, "animated");
    }

    // выключение спинера на кнопке
    function __spinerDisable(el) {
        BX.removeClass(el, "has-animation");
        BX.removeClass(el, "animated");
    }
    
    for (var i = 0; i < isPayerCheckboxes.length; i++) {
        BX.bind(isPayerCheckboxes[i], "click", function () {

            var passanger = this.dataset.passanger;
            if (this.checked) {
                
                BX.findChild(form, {attribute: {name: `eurotrans[payer][name]`}}, true).value = BX.findChild(form, {attribute: {name: `eurotrans[passangers][${passanger}][name]`}}, true).value;
                BX.findChild(form, {attribute: {name: `eurotrans[payer][last_name]`}}, true).value = BX.findChild(form, {attribute: {name: `eurotrans[passangers][${passanger}][last_name]`}}, true).value;
                BX.findChild(form, {attribute: {name: `eurotrans[payer][phone]`}}, true).value = BX.findChild(form, {attribute: {name: `eurotrans[passangers][${passanger}][phone]`}}, true).value;
            }
        });
    }

    // validator
    BX.bind(form, 'submit', function (e) {

        var form_elements = BX.findChildren(this, {attribute: {"data-need-validation": "yes"}}, true);

        var validators, error_areas, hasErrors = false, errors = [], isChecked = false, inputs, errorsArea = BX.findChildren(this, {className: "error-area"}, true);
        
        __spinerEnable(BX("eurotrans-booking-btn"));
        
        for (var i = 0; i < errorsArea.length; i++) {
            errorsArea[i].innerHTML = "";
        }

        for (var i = 0; i < form_elements.length; i++) {

            errors = [];
            validators = form_elements[i].dataset.validators.split("|");

            for (var j = 0; j < validators.length; j++) {
                switch (validators[j]) {

                    case "is_empty":

                        if (!form_elements[i].value.length) {

                            errors.push(`Поле не может быть пустым`);
                        }

                        break;

                    case "is_phone":

                        if (!/^([\+]+)*[0-9\x20\x28\x29\-]{5,20}$/.test(form_elements[i].value)) {

                            errors.push(`Введите телефон в указанном формате`);
                        }

                        break;

                    case "is_email":


                        if (!/^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/.test(form_elements[i].value)) {
                            errors.push(`Введите коректный email`);
                        }

                        break;

                    case "custom_select_is_selected":

                        inputs = BX.findChildren(this, {tag: "input", attribute: {name: form_elements[i].dataset.validationFn}}, true);

                        isChecked = false;

                        for (var k = 0; k < inputs.length; k++) {

                            if (inputs[k].checked) {
                                isChecked = true;
                                break;
                            }

                        }

                        if (!isChecked) {
                            errors.push(`Поле не выбрано`);
                        }

                        break;
                }
            }

            if (errors.length) {
                hasErrors = true;
                BX("error-area-" + form_elements[i].dataset.linkErrorArea).innerHTML = errors.join("<br>");
            }

        }

        if (hasErrors) {
            error_areas = BX.findChildren(this, {className: "error-area"}, true);
            for (var i = 0; i < error_areas.length; i++) {
                if (error_areas[i].innerHTML !== "") {
                    BX.scrollToNode(BX(error_areas[i].dataset.scrollToId));
                    e.preventDefault();
                    __spinerDisable(BX("eurotrans-booking-btn"));
                    return;
                }
            }
            
        }

    });



});