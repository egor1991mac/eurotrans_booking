<?php

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PUBLIC_AJAX_MODE", true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

Bitrix\Main\Loader::includeModule("travelsoft.eurotrans.booking");

try {

    $_request = $_REQUEST["eurotrans"];
    if (
        empty($_request) ||
        !$_request['location_from'] ||
        !$_request['location_to'] ||
        !$_request['date_from'] ||
        ($_request['round_trip'] == 1 && !$_request['date_back']) ||
        (
            $_request['adults'] <= 0 &&
            $_request['children'] <= 0 &&
            $_request['social'] <= 0
        )
        ||
        !$_request["details"]
    ) {
        throw new Exception("");
    }
    $response = \travelsoft\eurotrans\Utils::lockSeats($_request);
    if($response['error']){
        $resp["error"] = true;
    }
    else {
        $timestamp = time();

        if(!empty($_request['promocode'])){

            $_request = \travelsoft\eurotrans\Utils::applyPromocode($_request);

        }

        $_SESSION[travelsoft\eurotrans\Settings::sessionStoreId()][$timestamp] = $_request;

        $resp["error"] = false;
        $resp["time"] = $timestamp;

    }
    travelsoft\eurotrans\Utils::jsonResponse(json_encode($resp));


} catch (Exception $ex) {

    travelsoft\eurotrans\Utils::jsonResponse(json_encode(array("error" => true)));
}

