<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$features = array();
$features_db = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=> 37),false, false, Array("ID", "NAME"));
while($ar_fields = $features_db->GetNext())
{
    $features[$ar_fields["ID"]] = $ar_fields["NAME"];
}

if(!empty($features)) {
    foreach ($arResult as $key => $arItem) {

        if (!empty($arItem["PROPERTIES"]["FEATURES_LIST"]["VALUE"])) {

            foreach ($arItem["PROPERTIES"]["FEATURES_LIST"]["VALUE"] as $feature) {

                $arResult[$key]["PROPERTIES"]["FEATURES_LIST"]["DISPLAY_VALUE"][] = $features[$feature];

            }

        }

    }
}