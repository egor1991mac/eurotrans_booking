<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (!EMPTY($arResult['ERRORS'])): ?>
    <div class="content-column-content-inner">
        <p style="color: red;">
            <? foreach ($arResult["ERRORS"] as $error) {
                echo $error . '<br/>';
            } ?>
        </p>
    </div>
<? endif ?>
<? if (!EMPTY($_GET["message"]) && ($_GET["message"] == 'ok')): ?>
    <div class="content-column-content-inner">
        <p style="color: green;">
            Спасибо. В ближайшее время мы свяжемся с Вами.
        </p>
    </div>
<? endif ?>
<form class="form-block-wrapper js-validate" method="POST" action="<?= POST_FORM_ACTION_URI ?>"
      enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <div class="form-block-section">
        <div class="title h3">Маршрут</div>
        <div class="input-form">
            <? foreach ($arResult['FORM']["ROUTE"] as $NAME => $arItem): ?>
                <? if ($arItem["type"] == "enumeration"): ?>
                    <div class="input-item s50 <? if ($arItem["hidden"]): ?> hideRound forSearchHide<? endif; ?>"
                         id="<?= $NAME ?>">
                        <div class="input-wrapper <? if ($arItem["isRequired"] == true): ?>req<? endif; ?>">
                            <div class="label"><?= $arItem["formLabel"] ?></div>
                            <div class="select-check js-select-custom">
                                <button class="selects" data-placeholder="Выберите">
                                    <span class="btn-text">Выберите</span>
                                    <i class="arr-down">
                                        <svg class="icon icon-drop">
                                            <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </button>
                                <div class="dropdown-target">
                                    <div class="dropdown-inner">
                                        <div class="dropdown-content">
                                            <div class="select-u-body select-list">
                                                <? foreach ($arItem["items"] as $id => $item): ?>
                                                    <label class="option">
                                                        <input type="radio" name="<?= $NAME ?>"
                                                               value="<?= $item ?>"
                                                               data-validation="required"
                                                               <? if ($_POST[$NAME] == $item): ?>checked<? endif; ?>>
                                                        <span><?= $item ?></span>
                                                    </label>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? elseif ($arItem["type"] == "datetime"): ?>
                    <div class="input-item s50 <? if ($arItem["hidden"]): ?> hideRound forSearchHide<? endif; ?>">
                        <div class="input-wrapper <? if ($arItem["isRequired"]): ?>req<? endif; ?>">
                            <label><?= $arItem["formLabel"] ?></label>
                            <input class="input-main datepicker" type="text"
                                   placeholder="-- Дата --"
                                   data-validation="required" name="<?= $NAME ?>"
                                   data-validation-error-msg="Не выбрано" readonly
                                   value="<?= htmlspecialchars($_POST[$NAME]); ?>">
                            <svg class="icon icon-drop">
                                <use xlink:href="#calendar"
                                     xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="input-item s50 <? if ($arItem["hidden"]): ?> hideRound forSearchHide<? endif; ?>">
                        <div class="input-wrapper double <? if ($arItem["isRequired"]): ?>req<? endif; ?>">
                            <div class="label"><?= $arItem["formLabelTime"] ?></div>
                            <div class="select-check js-select-custom">
                                <button class="selects" data-placeholder="Часы">
                                    <span class="btn-text">Часы</span>
                                    <i class="arr-down">
                                        <svg class="icon icon-drop">
                                            <use xlink:href="#strelka"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </button>
                                <div class="dropdown-target">
                                    <div class="dropdown-inner">
                                        <div class="dropdown-content">
                                            <div class="select-u-body select-list">
                                                <? foreach (Array("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23") as $hour): ?>
                                                    <label class="option">
                                                        <input type="radio" name="<?= $NAME ?>-HOUR"
                                                               value="<?= $hour ?>"
                                                               data-validation="required"
                                                               <? if ($_POST[$NAME . "-HOUR"] == $hour): ?>checked<? endif; ?>>
                                                        <span>
                                                                <?= $hour ?> ч
                                                            </span>
                                                    </label>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="select-check js-select-custom">
                                <button class="selects" data-placeholder="Минуты">
                                    <span class="btn-text">Минуты</span>
                                    <i class="arr-down">
                                        <svg class="icon icon-drop">
                                            <use xlink:href="#strelka"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </button>
                                <div class="dropdown-target">
                                    <div class="dropdown-inner">
                                        <div class="dropdown-content">
                                            <div class="select-u-body select-list">
                                                <? foreach (Array("00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55") as $minute): ?>
                                                    <label class="option">
                                                        <input type="radio" name="<?= $NAME ?>-MINUTE"
                                                               value="<?= $minute ?>"
                                                               data-validation="required"
                                                               <? if ($_POST[$NAME . "-MINUTE"] == $minute): ?>checked<? endif; ?>>
                                                        <span>
                                                                <?= $minute ?> мин
                                                            </span>
                                                    </label>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                <? elseif ($arItem["type"] == "boolean"): ?>
                    <div class="input-item s100 <? if ($arItem["hidden"]): ?> hideRound forSearchHide<? endif; ?>">
                        <div class="input-wrapper">
                            <div class="select-item">
                                <label class="select-label">
                                        <span class="select-main">
                                            <input class="select-real" type="checkbox" onclick="showRoundTrip()"
                                                   name="<?= $NAME ?>">
                                            <span class="select-checked"></span>
                                        </span>
                                    <span class="select-text"><?= $arItem["formLabel"] ?></span>
                                </label>
                            </div>
                        </div>
                    </div>
                <? elseif ($arItem["type"] == "string"): ?>
                    <div class="input-item s50 <? if ($arItem["hidden"]): ?> hideRound forSearchHide<? endif; ?>">
                        <div class="input-wrapper <? if ($arItem["isRequired"] == true): ?>req<? endif; ?>">
                            <label><?= $arItem["formLabel"] ?></label>
                            <input class="input-main error" type="text"
                                   name="<?= $NAME ?>"
                                   data-validation="required"
                                   data-validation-length="min2"
                                   data-validation-error-msg="Не заполнено"
                                   value="<?= htmlspecialchars($_POST[$NAME]); ?>">
                        </div>
                    </div>
                <? endif; ?>
            <? endforeach; ?>
        </div>
    </div>
    <div class="form-block-section">
        <div class="title h3">Заявку оформил</div>
        <div class="input-form">
            <? foreach ($arResult['FORM']["APPLICATION_FORM"] as $NAME => $arItem): ?>
                <? if ($arItem['type'] == 'textarea'): ?>
                    <div class="input-item s100">
                        <div class="input-wrapper">
                            <label for="text1"><?= $arItem["formLabel"] ?></label>
                            <textarea class="input-main" name="<?= $NAME ?>" rows="5" id="text1"
                                      placeholder="<?= $arItem['placeholder'] ?>"><?= htmlspecialchars($_POST[$NAME]); ?></textarea>
                        </div>
                    </div>

                <? elseif ($arItem['type'] == 'file'): ?>

                    <div class="input-item s100">
                        <div class="input-wrapper <? if ($arItem["isRequired"] == true): ?>req<? endif; ?>">
                            <label><?= $arItem["formLabel"] ?></label>
                            <input
                                   type="file"
                                   name="<?= $NAME ?>"/>
                        </div>
                    </div>

                <? else: ?>

                    <div class="input-item s33">
                        <div class="input-wrapper <? if ($arItem["isRequired"] == true): ?>req<? endif; ?>">
                            <label><?= $arItem["formLabel"] ?></label>
                            <input class="input-main"
                                   type="<?= $arItem["type"] ?>"
                                   placeholder="<?= $arItem["placeholder"] ?>"
                                   name="<?= $NAME ?>"
                                   data-validation-error-msg="Не заполнено"
                                   value="<?= htmlspecialchars($_POST[$NAME]); ?>">
                        </div>
                    </div>
                <? endif; ?>
            <? endforeach; ?>
        </div>
    </div>
    <div class="form-block-footer">
        <div class="form-footer-notify">
            <div class="text-sm">Поля для обязательного заполнения</div>
        </div>
        <div class="input-form">
            <div class="input-item s33" style="float: right">
                <button name="SUBMIT" value="submit" class="btn btn-colored blue">
                    <span>Отправить заявку</span>
                </button>
            </div>
        </div>
    </div>
</form>
<style>
    .hideRound {
        display: none;
    }
</style>
<script>
    function showRoundTrip() {
        let hideRound = $('.forSearchHide');
        hideRound.toggleClass('hideRound');

        let selectFrom = $('#UF_CRM_1483795016').find("div.select-list");
        let inputFrom = selectFrom.find('input[name=UF_CRM_1483795016]');
        inputFrom.each(function () {
            if ($(this).prop('checked')) {
                let backFrom = $("#UF_CRM_1512117684");
                backFrom.find('input[value="' + $(this).val() + '"]').prop('checked', true);
                backFrom.find('div.select-list').trigger('reinit');
            }
        });

        let selectTo = $('#UF_CRM_1483795308').find("div.select-list");
        let inputTo = selectTo.find('input[name=UF_CRM_1483795308]');
        inputTo.each(function () {
            if ($(this).prop('checked')) {
                let toFrom = $("#UF_CRM_1512034491");
                toFrom.find('input[value="' + $(this).val() + '"]').prop('checked', true);
                toFrom.find('div.select-list').trigger('reinit');
            }
        });
    }
</script>