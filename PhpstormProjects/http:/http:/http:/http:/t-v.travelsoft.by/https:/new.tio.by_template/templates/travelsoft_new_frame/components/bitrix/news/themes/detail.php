<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
if (!empty($arResult['VARIABLES']['ELEMENT_CODE'])) {
    $objFindTools = new CIBlockFindTools();
    $elementID = $objFindTools->GetElementID(false, $arResult['VARIABLES']['ELEMENT_CODE'], false, false, array("IBLOCK_ID" => $arParams['IBLOCK_ID']));

    if (!empty($elementID)) {
        $APPLICATION->IncludeComponent(
            "travelsoft:travelsoft.news.list",
            "articles_",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AFP_123" => array(),
                "AFP_228" => array(),
                "AFP_286" => array(),
                "AFP_290" => "",
                "AFP_291" => "",
                "AFP_292" => "",
                "AFP_295" => "",
                "AFP_296" => "",
                "AFP_297" => "",
                "AFP_298" => "",
                "AFP_5" => array(),
                "AFP_6" => array(),
                "AFP_67" => array(),
                "AFP_68" => array(),
                "AFP_7" => array(
                    0 => $elementID,
                ),
                "AFP_ID" => "",
                "AFP_MAX_2" => "",
                "AFP_MAX_259" => "",
                "AFP_MAX_287" => "",
                "AFP_MAX_288" => "",
                "AFP_MAX_3" => "",
                "AFP_MAX_56" => "",
                "AFP_MAX_57" => "",
                "AFP_MAX_58" => "",
                "AFP_MIN_2" => "",
                "AFP_MIN_259" => "",
                "AFP_MIN_287" => "",
                "AFP_MIN_288" => "",
                "AFP_MIN_3" => "",
                "AFP_MIN_56" => "",
                "AFP_MIN_57" => "",
                "AFP_MIN_58" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "N",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "2",
                "IBLOCK_TYPE" => "news",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "N",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "20",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "main",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(0 => "PHOTO3", 1 => "MORE_PHOTO", 2 => "AUTHOR", 4 => "",),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC",
                "COMPONENT_TEMPLATE" => "articles_",
                "TEXT_TITLE" => "",
                "TEXT_DESCRIPTION" => "",
                "DESCRIPTION_LINK" => ""
            ),
            false
        );
    }
}
?>
