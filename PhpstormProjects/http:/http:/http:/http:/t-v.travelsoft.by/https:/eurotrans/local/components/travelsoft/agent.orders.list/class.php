<?php


class  TravelsoftAgentOrdersListComponent extends CBitrixComponent
{
    public function getCommission()
    {
        $agent = \travelsoft\eurotrans\Utils::getAgentInfo();

        $this->arResult['AGENT'] = $this->arrayKeysUpper($agent);
    }

    function arrayKeysUpper($arr){
        return array_map(function ($item) {
            if (is_array($item)) {
                $item = self::arrayKeysUpper($item);
            }

            return $item;
        }, array_change_key_case($arr, CASE_UPPER));
    }

    public function executeComponent()
    {
        $this->getCommission();

        $this->includeComponentTemplate();
    }
}