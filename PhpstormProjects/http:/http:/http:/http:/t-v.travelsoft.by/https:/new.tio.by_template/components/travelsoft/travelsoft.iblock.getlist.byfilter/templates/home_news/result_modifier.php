<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult as $key=>$arItem){

    if(strlen($arItem["ACTIVE_FROM"])>0)
        $arResult[$key]["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));
    else
        $arResult[$key]["DISPLAY_ACTIVE_FROM"] = "";

}