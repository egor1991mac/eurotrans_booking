(function (window) {

    'use strict';

    var $ = window.jQuery;

    var document = window.document;

    var _cache = new window.Cache();

    /**
     * Хранилище параметров для формы поиска билетов
     * @type Object
     */
    var storage = {

        _storage: {},

        /**
         * Получение значения параметра хранилища
         * @param {String} parameter
         * @returns {mixed}
         */
        getParameter: function (parameter) {

            var value = null;

            if (typeof this._storage[parameter] !== 'undefined') {
                value = this._storage[parameter];
            }

            return value;

        },

        /**
         * Получение массива дат для подсветки в календаре
         * @param {Number} location_from
         * @param {Number} location_to
         * @returns {Array}
         */
        getDates: function (location_from, location_to) {

            var dates = storage.getParameter("dates");

            var result = [];

            try {

                if ($.isArray(dates[location_from][location_to])) {
                    result = dates[location_from][location_to];
                }

            } catch (e) {
                console.log('empty dates');
            }

            return result;
        },

        /**
         * Сохранение дат
         * @param {Object} dates
         * @returns {undefined}
         */
        addDates: function (dates) {

            var lf, lt;

            for (lf in dates) {

                if (typeof this._storage.dates[lf] !== 'object') {
                    this._storage.dates[lf] = {};
                }

                for (lt in dates[lf]) {

                    if (typeof this._storage.dates[lf][lt] !== 'object') {
                        this._storage.dates[lf][lt] = {};
                    }

                    this._storage.dates[lf][lt] = dates[lf][lt];

                }
            }

        },

        /**
         * Сохраняет точки прибытия для точке отправления
         * @param {String} dep_point
         * @param {Array} locations
         * @returns {undefined}
         */
        addLocationsTo: function (dep_point, locations) {
            this._storage.locationsTo[dep_point] = locations;
        },

        /**
         * Возвращает точки прибытия по точке отправления
         * @param {String} dep_point
         * @returns {Array}
         */
        getLocationsTo: function (dep_point) {

            var result = [];

            if ($.isArray(this._storage.locationsTo[dep_point])) {
                result = this._storage.locationsTo[dep_point];
            }

            return result;
        },

        setAdults: function(adults){
            if(this.getParameter('request') === null) {
                this._storage.request = {};
            }

            this._storage.request.adults = adults;
        },

        setChildren: function(children){
            if(this.getParameter('request') === null) {
                this._storage.request = {};
            }

            this._storage.request.children = children;
        },

        setSocial: function(social){
            if(this.getParameter('request') === null) {
                this._storage.request = {};
            }

            this._storage.request.social = social;
        },
    };

    var utilites = {

        /**
         * Отрисовка пунктов прибытия для выбора в форме
         * @param {Array} points
         * @returns {undefined}
         */
        renderArrivalPoints: function (points) {

            var i, len = points.length;
            var tpl = '<label class="option"> \
                            <input type="radio" name="eurotrans[location_to]" value="#id#" data-name="#name#"> \
                            <span class="location-to-title">#name#</span> \
                        </label>';
            var html = '';
            var _html = '';


            for (i = 0; i < len; i++) {

                _html = tpl;

                while (/#id#/.test(_html)) {
                    _html = _html.replace("#id#", Number(points[i].id));
                }

                while (/#name#/.test(_html)) {
                    _html = _html.replace("#name#", points[i].name.replace(/<[^>]+>/gi, ''));
                }

                html += _html;

            }

            document.getElementById("location_to-box").innerHTML = html;
        },

        /**
         * Проверка селекта
         * @param {$} form
         * @returns {undefined}
         */
        checkForSelect: function (form) {

            if (form.find('.select-check').length) {
                var wrap = form.find('.select-check');

                wrap.each(function () {
                    var _ = $(this),
                        option = _.find('.option.has-error');
                    if (option.length) {
                        _.addClass('error');

                    } else {
                        _.removeClass('error');
                    }
                });
                wrap.hasClass('error') ? false : true
            }
        },

        /**
         * Инициализация работы селектов
         * @returns {undefined}
         */
        initCustomSelectList: function () {

            var _items = $('.js-select-custom');

            $.each(_items, function () {

                var _select = $(this),
                    _button = _select.find('.selects'),
                    _list = _select.find('.select-list');


                _select.on('reinit', function () {

                    var _active = _list.find('input:checked');

                    if ($(this).hasClass('depends-on')) {

                        var wrapperSelect = '.input-item';

                        var item = $(this).closest(wrapperSelect);

                        if (!item.length) {

                            wrapperSelect = '.input-wrapper';

                            item = $(this).closest(wrapperSelect);
                        }

                        if (_active.length) {

                            var next = item.nextAll(wrapperSelect).find('.depends-on');

                            next.removeClass('disabled').find('input').prop('checked', false);

                            next.trigger('reinit');

                        } else {

                            var next = item.nextAll(wrapperSelect).find('.depends-on');

                            next.addClass('disabled').find('input').prop('checked', false);

                            next.trigger('reinit');
                        }
                    }
                    if (_active.length) {

                        if ($(this).hasClass('price-total')) {

                            _button.children('.search-input').addClass('active').val(_active.siblings('.elem-price').html()).change().parent().addClass('is-checked')

                        } else {

                            _button.children('.search-input').addClass('active').val('' + _active.siblings('span').text() + '').change().parent().addClass('is-checked')

                        }

                    } else {

                        _button.children('.search-input').removeClass('active').val('').change().parent().removeClass('is-checked');
                    }

                    _select.trigger('ShOnSelect');

                });

                _button.off('click').on('click', function () {

                    _button.parent().toggleClass('active').siblings().removeClass('active');
                    return (false);
                });

                _select.off('click').on('click', 'label', function () {

                    var _label = $(this),
                        _input = _label.find('input');

                    _input.prop('checked', true);

                    _select.trigger('reinit');

                    _button.parent().removeClass('active');

                });

                _select.trigger('reinit');

                _select.addClass('cs-active');

                $(document).on('mouseup', function (e) {

                    if (!_select.is(e.target)
                        && _select.has(e.target).length === 0) {

                        _select.removeClass('active');
                    }
                });
            });
        },

        /**
         * Инициализация календаря
         * @param {$} item
         * @returns {$}
         */
        initDatepicker: function (item, options) {

            var defOptions = {
                changeMonth: false,
                changeYear: false,
                dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                monthNamesShort: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                dateFormat: 'dd.mm.yy',
                showOn: "focus"
            };

            item.datepicker($.extend({}, defOptions, options));
            return item.datepicker('widget');
        },

        /**
         * Сброс datepicker'a
         * @returns {undefined}
         */
        destroyDates: function () {

            _cache.dfBox.addClass('disabled');
            _cache.dbBox.addClass('disabled');
            _cache.df.datepicker('destroy');
            _cache.df.val("");
            _cache.db.datepicker('destroy');
            _cache.db.val("");
        },

        /**
         * Включение блоков выбора количества человек
         * @returns {undefined}
         */
        unDisablePeopleBlock: function () {

            _cache.adBox.removeClass('disabled').addClass('cs-active');
            _cache.chBox.removeClass('disabled').addClass('cs-active');
            _cache.socialBox.removeClass('disabled').addClass('cs-active');
        },

        /**
         * Выключение блоков выбора количества человек
         * @returns {undefined}
         */
        disablePeopleBlock: function () {

            _cache.adBox.find('input[name="eurotrans[adults]"]:checked').prop("checked", false);
            _cache.adBox.addClass('disabled').removeClass('cs-active');
            _cache.adBtn.children('.btn-text').removeClass('active').text(_cache.adBtn.data('placeholder')).parent().removeClass('is-checked');
            _cache.chBox.find('input[name="eurotrans[children]"]:checked').prop("checked", false);
            _cache.chBox.addClass('disabled').removeClass('cs-active');
            _cache.chBtn.children('.btn-text').removeClass('active').text(_cache.chBtn.data('placeholder')).parent().removeClass('is-checked');
            _cache.socialBox.find('input[name="eurotrans[social]"]:checked').prop("checked", false);
            _cache.socialBox.addClass('disabled').removeClass('cs-active');
            _cache.socialBtn.children('.btn-text').removeClass('active').text(_cache.socialBtn.data('placeholder')).parent().removeClass('is-checked');

        },

        /**
         * @param {Array} arrivalpoints
         * @returns {undefined}
         */
        __chooseLocationFrom: function (arrivalpoints) {

            $('.location-to-title').each(function () {
                $(this).off('click');
            });

            utilites.renderArrivalPoints(arrivalpoints);

            $('.location-to-title').on('click', function () {

                var input = $(this).prev().get(0);

                var location_from = $('input[name="eurotrans[location_from]"]:checked').val();

                if (input.value > 0 && location_from > 0) {

                    if (storage.getDates(location_from, input.value).length) {

                        utilites.initDateFromDatepickerWithHighlightDates(location_from, input.value);
                        return;
                    }

                    // else
                    $.ajax({
                        type: "POST",
                        data: {sessid: storage.getParameter("sessid"), location_from: location_from, location_to: input.value},
                        url: storage.getParameter("urls").dates,
                        dataType: "json",
                        success: function (dates) {

                            if (typeof dates.error_message !== "undefined") {
                                alert(dates.error_message);
                                return;
                            }

                            storage.addDates(dates);

                            utilites.chooseLocationTo(location_from, input.value);

                        }
                    });

                } else {

                    utilites.destroyDates();
                }
            });

            utilites.destroyDates();
            utilites.disablePeopleBlock();
        },

        /**
         * Выбор точки отправления
         * @param {array} arrivalpoints
         * @returns {undefined}
         */
        chooseLocationFrom: function (arrivalpoints) {

            utilites.chooseLocationFrom = utilites.__chooseLocationFrom;

            utilites.__chooseLocationFrom(arrivalpoints);

            // имитация выбора точки прибытия, если данный параметр есть в запросе
            setTimeout(function () {
                if (typeof storage.getParameter("request").location_to === 'string' || typeof storage.getParameter("request").location_to === 'number') {

                    $('input[name="eurotrans[location_to]"][value="' + storage.getParameter("request").location_to + '"]')
                        .parent()
                        .find('.location-to-title')
                        .trigger('click');
                }
            }, 50);


        },

        /**
         * Выбор точки отпраления
         * @param {type} location_from
         * @param {type} location_to
         * @returns {undefined}
         */
        chooseLocationTo: function (location_from, location_to) {

            utilites.chooseLocationTo = function (lf, lt) {

                utilites.initDateFromDatepickerWithHighlightDates(lf, lt);
            };

            utilites.initDateFromDatepickerWithHighlightDates(location_from, location_to);

        },

        /**
         * Возвращает значение параметра формы
         * @param {String} name
         * @returns {String}
         */
        getFormParameterValue: function (name) {

            var i, fdata = $('#main-search-tickets-from').serializeArray();

            for (i = 0; i < fdata.length; i++) {

                if (fdata[i].name === name) {

                    return fdata[i].value.toString();
                }

            }

            return '';

        },

        /**
         * Подстветка даты
         * @param {Date} date
         * @param {Number} location_from
         * @param {Number} location_to
         * @returns {Array}
         */
        highlightDay: function (date, location_from, location_to) {

            if ($.inArray(date.format('yyyy-mm-dd'), storage.getDates(location_from, location_to)) !== -1) {

                return [true, 'highlight'];
            }
            return [false, ''];
        },

        /**
         * Инициализация блока количества людей из запроса
         * @returns {undefined}
         */
        initPeopleBlockFromRequest: function () {

            var peopleCnt;
            var peopleCntArea = $("#people-cnt");

            // Для формы с выпадающим блоком по количеству человек
            if (peopleCntArea.length) {

                peopleCnt = Number($('input[name="eurotrans[adults]"]').val()) + Number($('input[name="eurotrans[children]"]').val()) +
                    Number($('input[name="eurotrans[social]"]').val());

                if (peopleCnt > 0) {
                    peopleCntArea.addClass('active').text(peopleCnt);
                }
                return;
            }

            if (typeof storage.getParameter('request').adults === 'string') {
                $('input[name="eurotrans[adults]"][value="' + storage.getParameter("request").adults + '"]')
                    .parent()
                    .find('.adults-title')
                    .trigger('click');
            }

            if (typeof storage.getParameter('request').children === 'string') {
                $('input[name="eurotrans[children]"][value="' + storage.getParameter("request").children + '"]')
                    .parent()
                    .find('.children-title')
                    .trigger('click');
            }

            if (typeof storage.getParameter('request').social === 'string') {
                $('input[name="eurotrans[social]"][value="' + storage.getParameter("request").social + '"]')
                    .parent()
                    .find('.social-title')
                    .trigger('click');
            }

        },

        /**
         * Создание календаря для даты отправления с подсветкой дат
         * @param {Number} location_from
         * @param {Number} location_to
         * @returns {undefined}
         */
        initDateFromDatepickerWithHighlightDates: function (location_from, location_to) {

            let currentSelection = {};

            $(_cache.db).click(function(){
                currentSelection = _cache.db;
            });
            $(_cache.df).click(function(){
                currentSelection = _cache.df;
            });

            let widget = null;

            _cache.dfBox.removeClass('disabled');
            _cache.df.datepicker("destroy");
            widget = utilites.initDatepicker(_cache.df, {
                beforeShowDay: function (date) {
                    return utilites.highlightDay(date, location_from, location_to);
                },

                onSelect: function (dateText) {

                    var widget = null;

                    if (_cache.roundTrip.prop("checked")) {

                        _cache.dbBox.removeClass('disabled');
                        _cache.db.datepicker("destroy");
                        widget = utilites.initDatepicker(_cache.db, {
                            minDate: dateText,
                            beforeShowDay: function (date) {
                                return utilites.highlightDay(date, location_to, location_from);
                            },
                            onSelect: function () {

                                utilites.unDisablePeopleBlock();
                            }
                        });

                        // имитация выбора даты отправления обратно, если данный параметр есть в запросе
                        setTimeout(function () {
                            if (typeof storage.getParameter("request").date_back === 'string') {

                                _cache.db.datepicker('setDate', storage.getParameter("request").date_back);
                                widget.find(".ui-datepicker-current-day").trigger("click");
                                utilites.initPeopleBlockFromRequest();
                            }
                        }, 50);

                    } else {

                        utilites.unDisablePeopleBlock();

                        setTimeout(function () {
                            utilites.initPeopleBlockFromRequest();
                        }, 50);
                    }
                }
            });

            widget.on('click', '.btn-today', function(){
                if(currentSelection === _cache.df) {
                    _cache.df.datepicker('setDate', new Date());
                }
                else if(currentSelection === _cache.db) {
                    _cache.db.datepicker('setDate', new Date());
                }

                widget.find(".ui-datepicker-current-day").trigger("click");
            });

            widget.on('click', '.btn-tomorrow', function(){
                let tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                if(currentSelection === _cache.df) {
                    _cache.df.datepicker('setDate', tomorrow);
                }
                else if(currentSelection === _cache.db) {
                    _cache.db.datepicker('setDate', tomorrow);
                }
                widget.find(".ui-datepicker-current-day").trigger("click");
            });

            // имитация выбора точки прибытия, если данный параметр есть в запросе
            setTimeout(function () {
                if (typeof storage.getParameter("request").date_from === 'string') {

                    _cache.df.datepicker('setDate', storage.getParameter("request").date_from);
                    widget.find(".ui-datepicker-current-day").trigger("click");
                }
            }, 50);
        },

        /**
         * Запуск установки значений формы из запроса
         * @returns {undefined}
         */
        initFormValuesFromRequest: function () {

            var lfInput = $('input[name="eurotrans[location_from]"][value="' + storage.getParameter("request").location_from + '"]');

            if (storage.getParameter("request").round_trip === '1') {

                $('.select-checked').trigger('click');
            }

            if (typeof storage.getParameter("request").location_from === 'string' || typeof storage.getParameter("request").location_from === 'number') {

                lfInput.prop('checked', true);
                lfInput
                    .parent()
                    .find('.location-from-title')
                    .trigger('click');
            }

        }
    };

    $(document).ready(function () {

        storage._storage = window.eurotrans.storage;

        // инициализация получения элементов DOM из кеша
        // jquery DOMNode даты отправления
        _cache.getter("df", function () {

            var cacheElement = _cache.get("df");

            if (typeof cacheElement === "object") {
                return cacheElement;
            }

            _cache.set("df", $('input[name="eurotrans[date_from]"]'));
            return _cache.get("df");

        });

        // инициализация получения элементов DOM из кеша
        // jquery DOMNode контейнера даты отправления
        _cache.getter("dfBox", function () {

            var cacheElement = _cache.get("dfBox");

            if (typeof cacheElement === "object") {
                return cacheElement;
            }

            _cache.set("dfBox", _cache.df.closest('.js-condition-target'));
            return _cache.get("dfBox");

        });

        // инициализация получения элементов DOM из кеша
        // jquery DOMNode даты отправления обратно
        _cache.getter("db", function () {

            var cacheElement = _cache.get("db");

            if (typeof cacheElement === "object") {
                return cacheElement;
            }

            _cache.set("db", $('input[name="eurotrans[date_back]"]'));
            return _cache.get("db");

        });

        // инициализация получения элементов DOM из кеша
        // jquery DOMNode контейнера даты отправления обратно
        _cache.getter("dbBox", function () {

            var cacheElement = _cache.get("dbBox");

            if (typeof cacheElement === "object") {
                return cacheElement;
            }

            _cache.set("dbBox", _cache.db.closest('.js-condition-target'));
            return _cache.get("dbBox");

        });

        // инициализация получения элементов DOM из кеша
        // jquery DOMNode контейнера количества взрослых
        _cache.getter("adBox", function () {

            var cacheElement = _cache.get("adBox");

            if (typeof cacheElement === "object") {
                return cacheElement;
            }

            _cache.set("adBox", $('input[name="eurotrans[adults]"]').closest('#adults-box'));
            return _cache.get("adBox");

        });

        // инициализация получения элементов DOM из кеша
        // jquery DOMNode button количества взрослых
        _cache.getter("adBtn", function () {

            var cacheElement = _cache.get("adBtn");

            if (typeof cacheElement === "object") {
                return cacheElement;
            }

            _cache.set("adBtn", _cache.adBox.children("button"));
            return _cache.get("adBtn");

        });

        // инициализация получения элементов DOM из кеша
        // jquery DOMNode контейнера количества детей
        _cache.getter("chBox", function () {

            var cacheElement = _cache.get("chBox");

            if (typeof cacheElement === "object") {
                return cacheElement;
            }

            _cache.set("chBox", $('input[name="eurotrans[children]"]').closest('#children-box'));
            return _cache.get("chBox");

        });

        // инициализация получения элементов DOM из кеша
        // jquery DOMNode button количества детей
        _cache.getter("chBtn", function () {

            var cacheElement = _cache.get("chBtn");

            if (typeof cacheElement === "object") {
                return cacheElement;
            }

            _cache.set("chBtn", _cache.chBox.children("button"));
            return _cache.get("chBtn");

        });

        // инициализация получения элементов DOM из кеша
        // jquery DOMNode контейнера количества социальных билетов
        _cache.getter("socialBox", function () {

            var cacheElement = _cache.get("socialBox");

            if (typeof cacheElement === "object") {
                return cacheElement;
            }

            _cache.set("socialBox", $('input[name="eurotrans[social]"]').closest('#social-box'));
            return _cache.get("socialBox");

        });

        // инициализация получения элементов DOM из кеша
        // jquery DOMNode button количества социальных билетов
        _cache.getter("socialBtn", function () {

            var cacheElement = _cache.get("socialBtn");

            if (typeof cacheElement === "object") {
                return cacheElement;
            }

            _cache.set("socialBtn", _cache.socialBox.children("button"));
            return _cache.get("socialBtn");

        });

        // инициализация получения элементов DOM из кеша
        // jquery DOMNode переключателя "туда и обратно"
        _cache.getter("roundTrip", function () {

            var cacheElement = _cache.get("roundTrip");

            if (typeof cacheElement === "object") {
                return cacheElement;
            }

            _cache.set("roundTrip", $('#round-trip'));
            return _cache.get("roundTrip");

        });

        utilites.initCustomSelectList();

        // обработка выбора точки отправления
        $('.location-from-title').on('click', function () {

            var input = $(this).prev().get(0);

            if (input.value > 0) {

                if (typeof storage.getParameter("locationsTo")[input.value] === 'object') {

                    utilites.chooseLocationFrom(storage.getParameter("locationsTo")[input.value]);
                    return;
                }

                // else
                $.ajax({
                    type: "POST",
                    data: {sessid: storage.getParameter("sessid"), location_from: input.value},
                    url: storage.getParameter("urls").locationsTo,
                    dataType: "json",
                    success: function (arrivalpoints) {

                        if (typeof arrivalpoints.error_message !== "undefined") {
                            alert(arrivalpoints.error_message);
                            return;
                        }

                        if ($.isArray(arrivalpoints) && arrivalpoints.length > 0) {

                            storage.addLocationsTo(input.value, arrivalpoints);
                            utilites.chooseLocationFrom(storage.getParameter("locationsTo")[input.value]);
                        }

                    }
                });
            }
        });

        // обработка инпута "туда и обратно"
        _cache.roundTrip.on('click', function () {

            var location_from = $("input[name='eurotrans[location_from]']:checked").val();

            var location_to = $("input[name='eurotrans[location_to]']:checked").val();

            var df_len = _cache.df.val().length;

            if ($(this).prop("checked") && location_from > 0 && location_to > 0 && df_len) {

                _cache.dbBox.removeClass('disabled');
                _cache.db.datepicker("destroy");
                utilites.initDatepicker(_cache.db, {
                    minDate: _cache.df.val(),
                    beforeShowDay: function (date) {
                        return utilites.highlightDay(date, location_to, location_from);
                    }
                });

            } else {

                _cache.dbBox.addClass('disabled');
                _cache.db.datepicker("destroy");
                _cache.db.val("");
            }
        });

        // plus/minus
        $('.plus, .minus').each(function () {

            var $this = $(this);

            $this.on('click', function (e) {

                e.stopPropagation();

                var input = $($this.data("link-selector"));

                var currentVal = Number(input.val());

                var nextVal = $this.hasClass('plus') ? currentVal + 1 : currentVal - 1;

                if (nextVal < 0) {
                    nextVal = 0;
                }

                input.val(nextVal);

                $("#people-cnt").addClass('active').text(
                    Number($('input[name="eurotrans[adults]"]').val()) +
                    Number($('input[name="eurotrans[children]"]').val()) +
                    Number($('input[name="eurotrans[social]"]').val())
                );

            });
        });

        // validation form
        $('#main-search-tickets-from').on('submit', function () {

            var errors = [];

            if (!utilites.getFormParameterValue('eurotrans[location_from]').length) {
                errors.push('Укажите точку отправления');
            }

            if (!utilites.getFormParameterValue('eurotrans[location_to]').length) {
                errors.push('Укажите точку прибытия');
            }

            if (!utilites.getFormParameterValue('eurotrans[date_from]').length) {
                errors.push('Укажите дату отправления');
            }

            if (
                utilites.getFormParameterValue('eurotrans[round_trip]') === '1' &&
                !utilites.getFormParameterValue('eurotrans[date_back]').length
            ) {
                errors.push('Укажите дату отправления обратно');
            }

            if (
                !Number(utilites.getFormParameterValue('eurotrans[adults]')) &&
                !Number(utilites.getFormParameterValue('eurotrans[children]')) &&
                !Number(utilites.getFormParameterValue('eurotrans[social]'))
            ) {
                errors.push('Укажите количество человек');
            }

            if (errors.length) {
                alert(errors.join('\n'));
                return false;
            }

            return true;
        });

        utilites.initFormValuesFromRequest();

        // подсказка
        $('.hint').each(function () {

            $(this).webuiPopover({
                trigger: 'hover',
                width: 200,

            });

        });

        $('input[name="eurotrans[adults]"][value="1"]')
            .parent()
            .find('.adults-title')
            .trigger('click');

        storage.setAdults('1');
    });

    $(document).ready(function () {

        $('input[name="eurotrans[adults]"]').on('click', function () {
            storage.setAdults(this.value)
        });

        $('input[name="eurotrans[children]"]').on('click', function () {
            storage.setChildren(this.value)
        });

        $('input[name="eurotrans[social]"]').on('click', function () {
            storage.setSocial(this.value)
        });
    });

    let translit = [
        { russian: 'а', english: 'f'},
        { russian: 'б', english: ','},
        { russian: 'в', english: 'd'},
        { russian: 'г', english: 'u'},
        { russian: 'д', english: 'l'},
        { russian: 'е', english: 't'},
        { russian: 'ё', english: '`'},
        { russian: 'ж', english: ';'},
        { russian: 'з', english: 'p'},
        { russian: 'и', english: 'b'},
        { russian: 'й', english: 'q'},
        { russian: 'к', english: 'r'},
        { russian: 'л', english: 'k'},
        { russian: 'м', english: 'v'},
        { russian: 'н', english: 'y'},
        { russian: 'о', english: 'j'},
        { russian: 'п', english: 'g'},
        { russian: 'р', english: 'h'},
        { russian: 'с', english: 'c'},
        { russian: 'т', english: 'n'},
        { russian: 'у', english: 'e'},
        { russian: 'ф', english: 'a'},
        { russian: 'х', english: '['},
        { russian: 'ц', english: 'w'},
        { russian: 'ч', english: 'x'},
        { russian: 'ш', english: 'i'},
        { russian: 'щ', english: 'o'},
        { russian: 'ы', english: 's'},
        { russian: 'э', english: '\''},
        { russian: 'ю', english: '.'},
        { russian: 'я', english: 'z'}
    ];

    $(document).ready(function () {
        let searchInputs = $('.search-input');
        searchInputs.each(function () {
            let searchInput = $(this);
            searchInput.bind("propertychange change input", function () {

                let $this = $(this),
                    searchString = $this.val().toLowerCase(),
                    searchOn = $this.data('search');

                let translitSearchString = searchString;
                translit.forEach(function (item) {
                    translitSearchString = translitSearchString.replace(item.english, item.russian);
                });

                let items = $('input[name="eurotrans[' + searchOn + ']"]');

                let countHideItems = 0;

                items.each(function () {
                    let item = $(this);
                    let name = item.data('name');
                    let option = item.closest('.option');

                    if((searchString === '') || (name.toLowerCase().indexOf(searchString) !== -1) || (name.toLowerCase().indexOf(translitSearchString) !== -1)){
                        option.show();
                    }
                    else{
                        option.hide();
                        countHideItems++;
                    }
                });

                $this.next('.select-u-body').find('#empty-'+searchOn).remove();
                if(countHideItems === items.length){
                    let emptyHtml = '<label class="option" id="empty-'+searchOn+'">' +
                        '<span class="location-from-title">Ничего не найдено.</span>' +
                        '</label>';
                    $this.next('.select-u-body').append(emptyHtml);
                }

            });

            searchInput.click(function () {
                searchInput.val('').change();
            });
        });

        $("#swapper").click(function () {

            let location_from = $("input[name='eurotrans[location_from]']:checked").val();
            let location_to = $("input[name='eurotrans[location_to]']:checked").val();

            let lfInput = $('input[name="eurotrans[location_from]"][value="' + location_to + '"]');

            lfInput.prop('checked', true);
            lfInput
                .parent()
                .find('.location-from-title')
                .trigger('click');

            setTimeout(function() {
                let ltInput = $('input[name="eurotrans[location_to]"][value="' + location_from + '"]');

                ltInput.prop('checked', true);
                ltInput
                    .parent()
                    .find('.location-to-title')
                    .trigger('click');

            }, 100);
        });

        $('.arrow-calendar-left').click(function(){
            let input = $(this).siblings('.datepicker-mtfs');
            let newValue = decrementDay(input.val());

            input.val(newValue);
        });

        $('.arrow-calendar-right').click(function(){
            let input = $(this).siblings('.datepicker-mtfs');
            let newValue = incrementDay(input.val());

            input.val(newValue);
        });
    });

    let incrementDay = function (params) {
        if(params === ''){
            return params;
        }

        let parts = params.split('.');
        let date = new Date(parts[2], parts[1] - 1, parts[0]);

        let newDate = (new Date(date.getTime() + 60 * 60 * 24 * 1000));

        let mm = newDate.getMonth() + 1; // getMonth() is zero-based
        let dd = newDate.getDate();

        return [(dd > 9 ? '' : '0') + dd, (mm > 9 ? '' : '0') + mm, newDate.getFullYear()].join('.');
    };

    let decrementDay = function (params) {
        if(params === ''){
            return params;
        }

        let parts = params.split('.');
        let date = new Date(parts[2], parts[1] - 1, parts[0]);

        let now = (new Date()).setHours(0,0,0,0);

        if(date.getTime() === now){
            return params;
        }

        let newDate = (new Date(date.getTime() - 60 * 60 * 24 * 1000));

        let mm = newDate.getMonth() + 1; // getMonth() is zero-based
        let dd = newDate.getDate();

        return [(dd > 9 ? '' : '0') + dd, (mm > 9 ? '' : '0') + mm, newDate.getFullYear()].join('.');
    };

})(window);
