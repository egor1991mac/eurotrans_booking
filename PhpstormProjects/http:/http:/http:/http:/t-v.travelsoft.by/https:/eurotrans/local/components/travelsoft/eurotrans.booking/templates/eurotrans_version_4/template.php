<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$this->setFrameMode(false);

/**
 * @param array $seats
 * @param bool $without_seating
 * @return string
 */
function __getSeatsString(array $seats = null, $without_seating = false)
{
    if ($without_seating == 'true') {

        return "Без номера.";

    } else {

        $seats__ = \travelsoft\eurotrans\Utils::getSeatsArray($seats);

        return implode(", ", !empty($seats__) ? $seats__ : array("-"));

    }
}

// ссылки на предыдущие страницы
$this->SetViewTarget('route_link');
if (strlen($arResult["previous_links"]["routes"])) {
    echo $arResult["previous_links"]["routes"];
} else {
    echo "/routes/";
}
$this->EndViewTarget();

$this->SetViewTarget('tickets_link');
if (strlen($arResult["previous_links"]["tickets"])) {
    echo $arResult["previous_links"]["tickets"];
} else {
    echo "/event-registration/";
}
$this->EndViewTarget();
?>

<? $this->addExternalJs(SITE_TEMPLATE_PATH . "/js/jquery.mask.js"); ?>

<? if (!empty($arResult["ERRORS"]["BOOKING"])): ?>

    <div class="booking-block-inner">
        <div id="tickets-block">
            <div class="input-item s100">
                <div class="title h3">
                    <?= $arResult["ERRORS"]["BOOKING"]; ?>
                </div>
            </div>
        </div>
    </div>
    <? return; ?>

<? endif; ?>

<div class="booking-block-inner">
    <div id="tickets-block">
        <div class="input-item s100">
            <div class="title h3">
                Информация о выбранной поездке:
            </div>
        </div>

        <?
        $keys = [
            "trip",
            "return_trip"
        ];
        $total_price = 0;
        ?>
        <? foreach ($keys as $k): ?>
            <? if (!empty($arResult["details"][$k]["way"])): ?>
                <div class="input-item s50 feedback-item-reply">
                    <? if (!empty($arResult["details"][$k]["transfer"])) {
                        $transfer = $arResult["details"][$k]["transfer"]; ?>

                        <ul class="tickets-info">
                            <li class="bold">Трансфер</li>
                            <li>Дата поездки: <span class="bold date"><?= $transfer["departure_date"] ?></span></li>
                            <li>Отправление из: <span
                                        class="bold location-from"><?= $transfer["info"]["location_arr"][$arResult["location_from"]]["name"] ?>
                                    в <?= $transfer["departure_time"] ?></span></li>
                            <li>Прибытие в: <span
                                        class="bold location-to"><?= $transfer["info"]["location_arr"][$transfer["location_to_id"]]["name"] ?>
                                    в <?= $transfer["arrival_time"] ?></span></li>
                            <li>Рейс: <span
                                        class="bold trip"><?= current($transfer["info"]["location_arr"])["name"] . " - " . $transfer["info"]["location_arr"][$transfer["location_to_id"]]["name"] ?>  </span>
                            </li>
                            <li>
                                Место:<?= __getSeatsString($transfer["seats"], $arResult["without_seating"]); ?></li>
                            <li>Стоимость: <span
                                        class="price bold"><?= $transfer["price"] . " " . $transfer["currency"] ?></span>
                            </li>
                            <? $total_price += $transfer["price"] ?>
                        </ul>
                    <? } ?>

                    <? $way = $arResult["details"][$k]["way"] ?>

                    <ul class="tickets-info">
                        <? if ($arResult["details"][$k]["transfer"]): ?>
                            <li class="bold">Основной рейс</li>
                        <? endif ?>
                        <li>Дата поездки: <span
                                    class="bold date"><?= $arResult['details'][$k]['way']['departure_date']; ?></span>
                        </li>
                        <li>Отправление из:
                            <span class="bold location-from">
                                    <?= $way["info"]["location_arr"][$way["location_from_id"]]["name"] ?>
                                в <?= $way["departure_time"] ?>
                                </span>
                        </li>
                        <li>Прибытие в:
                            <span class="bold location-to">
                                    <?= $way["info"]["location_arr"][$way["location_to_id"]]["name"] ?>
                                в <?= $way["arrival_time"] ?>
                                </span>
                        </li>
                        <li>Рейс:
                            <span class="bold trip">
                                    <?= current($way["info"]["location_arr"])["name"] . " - " . $way["info"]["location_arr"][$way["location_to_id"]]["name"] ?>
                                </span>
                        </li>
                        <li>Место:
                            <span class="seats bold">
                                    <?= __getSeatsString($way["seats"], $arResult["without_seating"]); ?>
                                </span>
                        </li>
                        <li>Тип билетов:
                            <? foreach ($way["tickets_category_count"] as $ID => $ticket): ?>
                                <? // if ($ticket["count"] > 0): ?>
                                <span class="price bold"><br/>
                                    <? switch ($ticket['category']):
                                        case "adults":
                                            echo "Взрослый:";
                                            break;
                                        case "children":
                                            echo "Детский:";
                                            break;
                                        case "social":
                                            echo "Льготный:";
                                            break;
                                    endswitch; ?>
                                        </span>

                                <?= $ticket["price"] . " " . $way["currency"] ?> x<?= $ticket["count"] ?>
                                <? if ($ticket["price"] != $ticket["amount"]): ?>
                                    <br/>
                                    <span class="orange">
                                                Скидка
                                        <? if (!empty($ticket['discount_for'])): ?>
                                            (за <?= implode(', ', $ticket['discount_for']); ?>)
                                        <? endif; ?>
                                        :
                                            </span>
                                    <span class="without-discount-price">
                                                <?= ($ticket["price"] - $ticket["amount"]) . " " . $way["currency"]; ?>
                                        x<?= $ticket["count"] ?>
                                            </span>
                                    <br/>
                                    <span class="with-discount-label">
                                                Стоимость с учётом скидки:
                                            </span>
                                    <span class="with-discount-price">
                                                <?= $ticket["amount"] . " " . $way["currency"] . " x " . $ticket["count"] ?>
                                            </span>
                                <? endif; ?>

                                <? if ($ticket['category'] !== 'adults'): ?>
                                    <div class="roadmap-item-point road-popup">
                                        <div class="roadmap-item-point-inner dropdown-trigger">
                                            <div class="question"></div>
                                            <div class="dropdown-target arrow-bottom">
                                                <div class="dropdown-inner">
                                                    <div class="dropdown-content">
                                                        <div class="roadmap-item-description">
                                                            <div class="roadmap-item-text">
                                                                <?= GetMessage('TRAVELSOFT_EUROTRANS_' . strtoupper($ticket['category']) . '_CONTENT') ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>

                                <? // endif; ?>
                            <? endforeach; ?>
                        </li>
                        <? $total_price += $way["price"]; ?>
                    </ul>
                </div>
            <? endif; ?>
        <? endforeach; ?>
    </div>

    <div id="total-price-block">
        <div class="title h3 bold">
            Общая стоимость:
            <span class="total-price">
                <?= $total_price . ' ' . travelsoft\eurotrans\Settings::CURRENT_CURRENCY ?>
            </span>
        </div>
    </div>

    <form action="<?= POST_FORM_ACTION_URI ?>" id="booking-details" method="POST">
        <?= bitrix_sessid_post() ?>

        <div class="passangers-block form-block-section">
            <? $people = $arResult["adults"] + $arResult["children"] + $arResult["social"] ?>

            <? for ($i = 1; $i <= $people; $i++): ?>
                <div class="input-item s50">
                    <div class="title h4">
                        <span id="passanger-title-<?= $i ?>" class="passanger-title bold">Пассажир №<?= $i ?></span>
                        <? if ($i <= $arResult["adults"]): ?>
                            <span class="passanger-title">(Взрослый)</span>
                        <? elseif ($i > $arResult["adults"] && $i <= $arResult["adults"] + $arResult["children"]): ?>
                            <span class="passanger-title">(Детский)</span>
                        <? elseif ($i > $arResult["adults"] + $arResult["children"] && $i <= $people): ?>
                            <span class="passanger-title">(Льготный)</span>
                        <? endif ?>
                    </div>
                    <div class="input-form">

                        <div class="input-item s33">

                            <? $rstr = randString(7) ?>
                            <div class="input-wrapper req">

                                <label for="input-<?= $rstr ?>" class="f-s">
                                    Фамилия
                                </label>

                                <div id="error-area-<?= $rstr ?>" class="error-area"
                                     data-scrolltoid="input-<?= $rstr ?>"></div>
                                <input data-link-error-area="<?= $rstr ?>"
                                       data-need-validation="yes"
                                       data-validators="is_empty"
                                       data-validation-f="Фамилия"
                                       id="input-<?= $rstr ?>"
                                       class="input-main"
                                       name="eurotrans[passangers][<?= $i ?>][last_name]"
                                       value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["last_name"]) ?>">
                            </div>
                        </div>
                        <div class="input-item s33">

                            <? $rstr = randString(7) ?>
                            <div class="input-wrapper req">

                                <label for="input-<?= $rstr ?>" class="f-s">
                                    Имя
                                </label>

                                <div id="error-area-<?= $rstr ?>" class="error-area"
                                     data-scrolltoid="input-<?= $rstr ?>"></div>

                                <input data-link-error-area="<?= $rstr ?>"
                                       data-need-validation="yes"
                                       data-validators="is_empty"
                                       data-validation-f="Имя"
                                       id="input-<?= $rstr ?>"
                                       class="input-main"
                                       name="eurotrans[passangers][<?= $i ?>][name]"
                                       value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["name"]) ?>">
                            </div>

                        </div>
                        <div class="input-item s33">

                            <? $rstr = randString(7) ?>
                            <div class="input-wrapper req">

                                <label for="input-<?= $rstr ?>" class="f-s">
                                    Отчество
                                </label>

                                <div id="error-area-<?= $rstr ?>" class="error-area"
                                     data-scrolltoid="input-<?= $rstr ?>"></div>
                                <input data-link-error-area="<?= $rstr ?>"
                                       data-need-validation="yes"
                                       data-validators="is_empty"
                                       data-validation-f="Отчество"
                                       id="input-<?= $rstr ?>"
                                       class="input-main"
                                       name="eurotrans[passangers][<?= $i ?>][patronymic]"
                                       value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["patronymic"]) ?>">
                            </div>
                        </div>

                        <div class="input-item s50">

                            <? $rstr = randString(7) ?>
                            <div class="input-wrapper req">

                                <label for="input-<?= $rstr ?>" class="f-s">
                                    Документ
                                </label>

                                <div id="error-area-<?= $rstr ?>" class="error-area"
                                     data-scrolltoid="input-<?= $rstr ?>"></div>
                                <div id="social-box" class="select-check js-select-custom">
                                    <button class="selects" data-placeholder="">
                                        <span class="btn-text"></span>
                                        <i class="arr-down">
                                            <svg class="icon icon-drop">
                                                <use xlink:href="#strelka"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </button>
                                    <div class="dropdown-target">
                                        <div class="dropdown-inner">
                                            <div class="dropdown-content">
                                                <div data-need-validation="yes"
                                                     data-link-error-area="<?= $rstr ?>"
                                                     data-validators="custom_select_is_selected"
                                                     data-validation-fn="eurotrans[passangers][<?= $i ?>][documentType]"
                                                     class="select-u-body select-list text-center">
                                                    <? foreach ($arResult["DOCUMENT_TYPE"] as $document_type): ?>
                                                        <label class="option">
                                                            <input
                                                                    data-series="<?= $document_type["show_series"] ?>"
                                                                    data-group-number="<?= $i; ?>"
                                                                <? if (!empty($_POST["eurotrans"]["passangers"][$i]["documentType"])): ?>
                                                                    <? if (htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["documentType"]) == $document_type["id"]): ?>
                                                                        checked
                                                                    <? endif; ?>
                                                                <? else: ?>
                                                                    <? if ($document_type["id"] == 4): ?>
                                                                        checked
                                                                    <? endif; ?>
                                                                <? endif; ?>
                                                                    type="radio"
                                                                    name="eurotrans[passangers][<?= $i ?>][documentType]"
                                                                    value="<?= $document_type["id"] ?>">
                                                            <span class="social-title"><?= $document_type["name"] ?></span>
                                                        </label>
                                                    <? endforeach; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="input-item s20">

                            <? $rstr = randString(7) ?>
                            <div class="input-wrapper req">

                                <label for="input-<?= $rstr ?>" class="f-s">
                                    Серия
                                </label>

                                <div id="error-area-<?= $rstr ?>" class="error-area" data-scrolltoid="payer-title">
                                    <? if (!empty($arResult['ERRORS']['SERIES'])): ?>
                                        <?= $arResult['ERRORS']['SERIES'][$i]; ?>
                                    <? endif; ?>
                                </div>
                                <input data-link-error-area="<?= $rstr ?>"
                                       data-need-validation="yes"
                                       data-validators="is_empty"
                                       data-validation-f="Серия"
                                       id="input-<?= $rstr ?>"
                                       class="input-main"
                                       name="eurotrans[passangers][<?= $i ?>][series]"
                                       value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["series"]) ?>">
                            </div>
                        </div>

                        <div class="input-item s30">

                            <? $rstr = randString(7) ?>
                            <div class="input-wrapper req">

                                <label for="input-<?= $rstr ?>" class="f-s">
                                    Номер
                                </label>

                                <div id="error-area-<?= $rstr ?>" class="error-area" data-scrolltoid="payer-title">
                                    <? if (!empty($arResult['ERRORS']['DOCUMENT_NUMBER'])): ?>
                                        <?= $arResult['ERRORS']['DOCUMENT_NUMBER'][$i]; ?>
                                    <? endif; ?>
                                </div>
                                <input data-link-error-area="<?= $rstr ?>"
                                       data-need-validation="yes"
                                       data-validators="is_empty"
                                       data-validation-f="Номер"
                                       id="input-<?= $rstr ?>"
                                       class="input-main"
                                       name="eurotrans[passangers][<?= $i ?>][document_number]"
                                       value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["document_number"]) ?>">
                            </div>
                        </div>

                        <div class="input-item s50">

                            <? $rstr = randString(7) ?>
                            <div class="input-wrapper req">

                                <label for="input-<?= $rstr ?>" class="f-s">
                                    Дата рождения
                                </label>

                                <div id="error-area-<?= $rstr ?>" class="error-area"
                                     data-scrolltoid="input-<?= $rstr ?>">
                                    <? if (!empty($arResult['ERRORS']['BIRTHDAY'])): ?>
                                        <?= $arResult['ERRORS']['BIRTHDAY'][$i]; ?>
                                    <? endif; ?>
                                </div>
                                <input data-link-error-area="<?= $rstr ?>"
                                       data-need-validation="yes"
                                       data-validators="is_empty"
                                       data-validation-f="Дата рождения"
                                       data-birthday="true"
                                       id="input-<?= $rstr ?>"
                                       class="input-main"
                                       placeholder="01.01.1970"
                                       name="eurotrans[passangers][<?= $i ?>][birthday]"
                                       value="<?= htmlspecialchars($_POST["eurotrans"]["passangers"][$i]["birthday"]) ?>">
                            </div>
                        </div>

                        <? if (!empty($arResult["CITIZENSHIP"])): ?>
                            <div class="input-item s50">

                                <? $rstr = randString(7) ?>
                                <div class="input-wrapper req">

                                    <label for="input-<?= $rstr ?>" class="f-s">
                                        Гражданство
                                    </label>

                                    <div id="error-area-<?= $rstr ?>" class="error-area"
                                         data-scrolltoid="input-<?= $rstr ?>"></div>
                                    <div id="social-box" class="select-check js-select-custom">
                                        <button class="selects" data-placeholder="">
                                            <span class="btn-text"></span>
                                            <i class="arr-down">
                                                <svg class="icon icon-drop">
                                                    <use xlink:href="#strelka"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </button>
                                        <div class="dropdown-target">
                                            <div class="dropdown-inner">
                                                <div class="dropdown-content">
                                                    <div data-need-validation="yes"
                                                         data-link-error-area="<?= $rstr ?>"
                                                         data-validators="custom_select_is_selected"
                                                         data-validation-fn="eurotrans[passangers][<?= $i ?>][citizensip]"
                                                         class="select-u-body select-list text-center">
                                                        <? foreach ($arResult["CITIZENSHIP"] as $citizenship): ?>
                                                            <label class="option">
                                                                <input
                                                                    <? if ($citizenship["id"] == 21): ?>checked<? endif; ?>
                                                                    type="radio"
                                                                    name="eurotrans[passangers][<?= $i ?>][citizensip]"
                                                                    value="<?= $citizenship["id"]; ?>">
                                                                <span class="social-title"><?= $citizenship["country_title"]; ?></span>
                                                            </label>
                                                        <? endforeach; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            <? endfor ?>

        </div>

        <div class="payer-block form-block-section">
            <div class="input-item s50">
                <div class="title h4">
                    <span id="payer-title" class="passanger-title bold">Плательщик</span>
                </div>
                <div class="input-form">
                    <div class="input-item s50">

                        <? $rstr = randString(7) ?>
                        <div class="input-wrapper req">

                            <label for="input-<?= $rstr ?>" class="f-s">
                                Фамилия
                            </label>

                            <div id="error-area-<?= $rstr ?>" class="error-area" data-scrolltoid="payer-title">
                                <? if (!empty($arResult['ERRORS']['PAYER']['NAME'])): ?>
                                    <?= $arResult['ERRORS']['PAYER']['NAME']; ?>
                                <? endif; ?>
                            </div>
                            <input data-link-error-area="<?= $rstr ?>"
                                   data-need-validation="yes"
                                   data-validators="is_empty"
                                   data-validation-f="Фамилия"
                                   id="input-<?= $rstr ?>"
                                   class="input-main"
                                   name="eurotrans[payer][last_name]"
                                   placeholder="Фамилия"
                                   value="<?= htmlspecialchars($_POST["eurotrans"]["payer"]["last_name"]) ?>">
                        </div>
                    </div>
                    <div class="input-item s50">

                        <? $rstr = randString(7) ?>
                        <div class="input-wrapper req">

                            <label for="input-<?= $rstr ?>" class="f-s">
                                Имя
                            </label>

                            <div id="error-area-<?= $rstr ?>" class="error-area" data-scrolltoid="payer-title">
                                <? if (!empty($arResult['ERRORS']['PAYER']['NAME'])): ?>
                                    <?= $arResult['ERRORS']['PAYER']['NAME']; ?>
                                <? endif; ?>
                            </div>
                            <input data-link-error-area="<?= $rstr ?>"
                                   data-need-validation="yes"
                                   data-validators="is_empty"
                                   data-validation-f="Имя"
                                   id="input-<?= $rstr ?>"
                                   class="input-main"
                                   name="eurotrans[payer][name]"
                                   placeholder="Имя"
                                   value="<?= htmlspecialchars($_POST["eurotrans"]["payer"]["name"]) ?>">
                        </div>
                    </div>
                    <div class="input-item s50">

                        <div class="input-wrapper req">

                            <label for="input-payer-phone" class="f-s">
                                Телефон
                            </label>

                            <div id="error-area-payer-phone" class="error-area" data-scrolltoid="payer-title">
                                <? if (!empty($arResult['ERRORS']['PAYER']['PHONE'])): ?>
                                    <?= $arResult['ERRORS']['PAYER']['PHONE']; ?>
                                <? endif; ?>
                            </div>
                            <input data-link-error-area="payer-phone"
                                   data-need-validation="yes"
                                   data-validators="is_empty|is_phone"
                                   data-validation-f="Телефон"
                                   id="input-payer-phone"
                                   class="input-main"
                                   name="eurotrans[payer][phone]"
                                   placeholder="+375291234567"
                                   value="<?= htmlspecialchars($_POST["eurotrans"]["payer"]["phone"]) ?>">
                        </div>
                    </div>
                    <div class="input-item s50">

                        <? $rstr = randString(7) ?>
                        <div class="input-wrapper">

                            <label for="input-<?= $rstr ?>" class="f-s">
                                Второй телефон
                            </label>

                            <div id="error-area-<?= $rstr ?>" class="error-area" data-scrolltoid="payer-title">
                                <? if (!empty($arResult['ERRORS']['PAYER']['PHONE_ADDITIONAL'])): ?>
                                    <?= $arResult['ERRORS']['PAYER']['PHONE_ADDITIONAL']; ?>
                                <? endif; ?>
                            </div>
                            <input id="input-<?= $rstr ?>"
                                   class="input-main"
                                   name="eurotrans[payer][phone_2]"
                                   placeholder="+375291234567"
                                   value="<?= htmlspecialchars($_POST["eurotrans"]["payer"]["phone_2"]) ?>">
                        </div>
                    </div>
                    <? if ($arResult['IS_AGENT']): ?>

                        <input type="hidden"
                               name="eurotrans[payer][email]"
                               value="<?= htmlspecialchars($_POST["eurotrans"]["payer"]["email"]) ?>">

                    <? else: ?>

                        <div class="input-item s100">

                            <? $rstr = randString(7) ?>
                            <div class="input-wrapper req">

                                <label for="input-<?= $rstr ?>" class="f-s">
                                    Email
                                </label>

                                <div id="error-area-<?= $rstr ?>" class="error-area"
                                     data-scrolltoid="payer-title"></div>
                                <input data-link-error-area="<?= $rstr ?>"
                                       data-need-validation="yes"
                                       data-validators="is_email"
                                       data-validation-f="Email"
                                       id="input-<?= $rstr ?>"
                                       class="input-main"
                                       type="email"
                                       name="eurotrans[payer][email]"
                                       value="<?= htmlspecialchars($_POST["eurotrans"]["payer"]["email"]) ?>">
                            </div>
                        </div>
                    <? endif; ?>

                    <div class="input-item s100">

                        <? $rstr = randString(7) ?>
                        <div class="input-wrapper">

                            <label for="input-<?= $rstr ?>" class="f-s">
                                Примечание
                            </label>
                            <textarea id="input-<?= $rstr ?>"
                                      class="input-main"
                                      name="eurotrans[payer][comment]"><?= htmlspecialchars($_POST["eurotrans"]["payer"]["comment"]) ?></textarea>
                        </div>
                    </div>

                    <div class="input-item s100">

                        <? if ($arResult['IS_AGENT']): ?>
                            <? if ($arResult['AGENT_ALLOW_BOOK']): ?>

                                <div class="input-wrapper">
                                    <label class="f-s bold">Оплата: </label>

                                    <label for="payment-method-cashless" class="f-s">
                                        Безналичная оплата
                                    </label>

                                    <input name="eurotrans[payer][payment_method]"
                                           value="cashless"
                                           id="payment-method-cashless"
                                           checked
                                           type="radio">

                                    <label for="payment-method-cash" class="f-s">
                                        Оплата клиентом в автобусе
                                    </label>

                                    <input name="eurotrans[payer][payment_method]"
                                           value="cash"
                                           id="payment-method-cash"
                                           type="radio">
                                </div>

                            <? else: ?>

                                <input type="hidden" name="eurotrans[payer][payment_method]" value="cashless"/>

                            <? endif; ?>
                        <? else: ?>
                            <? if (!empty($arResult["payment_methods"])): ?>

                                <div class="input-wrapper">
                                    <label class="f-s bold">Оплата: </label>
                                    <? foreach ($arResult["payment_methods"] as $k => $payment_method): ?>

                                        <label for="payment-method-<?= $k ?>" class="f-s">
                                            <?= $payment_method["label"] ?>
                                        </label>

                                        <input name="eurotrans[payer][payment_method]"
                                               value="<?= $payment_method["name"] ?>"
                                               id="payment-method-<?= $k ?>"
                                               <? if ($payment_method["selected"]): ?>checked<? endif; ?>
                                               type="radio">

                                    <? endforeach; ?>
                                </div>

                            <? endif; ?>
                        <? endif; ?>

                    </div>

                    <div style="color: #aaabad; font-size:12px">
                        <span style="color: red">*</span> - поля для обязательного заполнения
                    </div>
                    <div class="input-wrapper">
                        <div class="select-item">

                            <label class="select-label js-condition">
                                <span class="select-main">
                                    <input required
                                           class="is-payer select-real"
                                           type="checkbox"
                                           name="agree_with_rules">
                                    <span class="select-checked"></span>
                                </span>
                                <span class="select-text" style="display: inline-block; white-space: normal">
                                    Согласен с
                                    <a href="/customers/pravila-perevozki/" style="display: inline-block;"
                                       target="_blank">правилами перевозки пассажиров</a>,
                                    <a href="/customers/baggage_rules/" style="display: inline-block;" target="_blank">правилами провоза багажа</a> и
                                    <a href="/customers/offer/" style="display: inline-block;" target="_blank">условиями оферты</a>.
                                </span>
                            </label>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="booking-btn-block">
             <span id="eurotrans-booking-btn" style="display: inline-block;">
                <input type="submit"
                       id="eurotrans-booking-btn-submit"
                       name="eurotrans[booking]"
                       value="Бронировать"
                       class="btn btn-colored">
             </span>
        </div>
    </form>
</div>
<script>
    generatePopup(document.documentElement.clientHeight, document.documentElement.clientWidth, "<?= $arResult["previous_links"]["tickets"]; ?>");
</script>

<?
global $USER;
if (($USER->IsAdmin() && ($USER->GetLogin() == "ts@ts.com")) || (isset($_GET['user']) && ($_GET['user'] == 'admin'))) {
    ?>
    <script>
        let form = $('#booking-details');
        form.find("input[name='eurotrans[passangers][1][series]']").val(111111);
        form.find("input[name='eurotrans[passangers][1][document_number]']").val(111111111111);
        form.find("input[name='eurotrans[passangers][1][name]']").val("test_111111111111");
        form.find("input[name='eurotrans[passangers][1][last_name]']").val("test_female_111111111111");
        form.find("input[name='eurotrans[passangers][1][patronymic]']").val("test_patronymic_111111111111");
        form.find("input[name='eurotrans[passangers][1][birthday]']").val("25.08.2000");
        form.find("input[name='eurotrans[passangers][1][phone]']").val("+375291111111");

        form.find("input[name='eurotrans[passangers][2][series]']").val(222222);
        form.find("input[name='eurotrans[passangers][2][document_number]']").val(222222222222);
        form.find("input[name='eurotrans[passangers][2][name]']").val("test_222222222222");
        form.find("input[name='eurotrans[passangers][2][last_name]']").val("test_female_222222222222");
        form.find("input[name='eurotrans[passangers][2][patronymic]']").val("test_patronymic_222222222222");
        form.find("input[name='eurotrans[passangers][2][birthday]']").val("02.02.2002");

        form.find("input[name='eurotrans[passangers][3][series]']").val(333333);
        form.find("input[name='eurotrans[passangers][3][document_number]']").val(333333333333);
        form.find("input[name='eurotrans[passangers][3][name]']").val("test_333333333333");
        form.find("input[name='eurotrans[passangers][3][last_name]']").val("test_female_333333333333");
        form.find("input[name='eurotrans[passangers][3][patronymic]']").val("test_patronymic_333333333333");
        form.find("input[name='eurotrans[passangers][3][birthday]']").val("03.03.2003");

        form.find("input[name='eurotrans[passangers][4][series]']").val(444444);
        form.find("input[name='eurotrans[passangers][4][document_number]']").val(444444444444);
        form.find("input[name='eurotrans[passangers][4][name]']").val("test_444444444444");
        form.find("input[name='eurotrans[passangers][4][last_name]']").val("test_female_444444444444");
        form.find("input[name='eurotrans[passangers][4][patronymic]']").val("test_patronymic_444444444444");
        form.find("input[name='eurotrans[passangers][4][birthday]']").val("04.04.2004");

        form.find("input[name='eurotrans[payer][name]']").val("test_111111111111");
        form.find("input[name='eurotrans[payer][last_name]']").val("test_female_111111111111");
        form.find("input[name='eurotrans[payer][phone]']").val("+375290000000");
        form.find("input[name='eurotrans[payer][email]']").val("s.kudryavtsev@travelsoft.by");
        form.find("input[name='agree_with_rules']").prop('checked', true);
    </script>
    <?
} ?>

<? /*============= dataLayer part ============*/ ?>
<script>
    let arrDataLayer = JSON.parse('<?= json_encode($arResult['DATA_LAYER']);?>');
    dataLayer.push(arrDataLayer);
</script>
<? /*============= end dataLayer part ============*/ ?>
<?
global $USER;
?>
<script>
    let userGroups = <?= json_encode($USER->GetUserGroupArray())?>;
</script>