<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="aside-section">
    <div class="content-column-wrap">
        <div class="content-column-content">
            <div class="content-column-content-inner">
                <h2>Ваши зазказы</h2>
            </div>
        </div>
    </div>
    <div class="table-wrapper">

        <div id="user-bookings">
            <div class="table-navigation">
                <div class="table-navigation-links">
                </div>
                <div class="table-navigation-nav">
                    <div class="table-pagination">
                        <a class="table-pagination-arr left" :class="activeDecPage ? 'disabled' : ''"
                           @click="decPage()">
                            <i class="arr-down">
                                <svg class="icon icon-drop">
                                    <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </a>
                        <div class="table-pagination-inner">
                            <a class="table-pagination-item"
                               :class="current_page === page ? 'active' : ''"
                               v-for="page in pages" :key="page"
                               @click="setPage(page)">
                                {{page}}
                            </a>
                        </div>
                        <a class="table-pagination-arr right" :class="activeIncPage ? 'disabled' : ''"
                           @click="incPage()">
                            <i class="arr-down">
                                <svg class="icon icon-drop">
                                    <use xlink:href="#strelka" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="table table-tickets sold">
                <div class="table-head">
                    <div class="table-item table-num">
                        <span>№</span>
                    </div>
                    <div class="table-item">
                        <span>ФИО пассажиров</span>
                    </div>
                    <div class="table-item">
                        <span>Дата продажи</span>
                    </div>
                    <div class="table-item">
                        <span>Дата рейса</span>
                    </div>
                    <div class="table-item">
                        <span>Дата рейса обратно</span>
                    </div>
                    <div class="table-item">
                        <span>Рейс</span>
                    </div>
                    <div class="table-item">
                        <span>Статус</span>
                    </div>
                    <div class="table-item">
                        <span>Цена</span>
                    </div>
                    <div class="table-item table-actions">
                        <span>Действия</span>
                    </div>
                </div>

                <div class="table-row" v-for="(arr_booking, index) in bookings" :key="arr_booking.data.order_id">

                    <div class="table-item table-num">
                        <div class="text-1">
                            <nobr>{{arr_booking.data.order_id}}</nobr>
                        </div>
                    </div>

                    <div class="table-item">
                        <div class="text-1" v-for="booking in arr_booking.bookings" :key="booking.id">
                            <p v-for="seat in booking.arrSeatsId" :key="seat.seat_id">
                                <a @click="seat.active = !seat.active">
                                    {{seat.last_name}} {{seat.first_name}}
                                </a>

                                <detail-passenger-popup v-if="seat.active" :arr_booking="arr_booking" :booking="booking" :seat="seat"
                                                        @seat-updated="onSeatUpdate(booking, seat, $event)"></detail-passenger-popup>
                            </p>
                        </div>
                    </div>

                    <div class="table-item table-num">
                        <div class="text-1">
                            <nobr>{{arr_booking.data.created_date}}</nobr>
                            <nobr>{{arr_booking.data.created_time}}</nobr>
                        </div>
                    </div>

                    <div class="table-item" v-for="booking in arr_booking.bookings" :key="booking.id">
                        <div class="text-1">
                            {{booking.booking_datetime.date}}
                        </div>

                        <div class="text-1">
                            <nobr>
                                {{booking.booking_datetime.time}} -
                                {{booking.stop_datetime.time}}
                            </nobr>
                        </div>
                    </div>

                    <div class="table-item" v-if="arr_booking.bookings.length < 2">
                        <div class="text-1">
                            -
                        </div>
                    </div>

                    <div class="table-item">
                        <div class="text-1">
                            {{arr_booking.bookings[0].departure}} → {{arr_booking.bookings[0].arrival}}
                        </div>
                    </div>

                    <div class="table-item dropdown-table-item">
                        <div>
                            <div class="dropdown-trigger"
                                 v-if="arr_booking.data.status === 'confirmedOperator' || arr_booking.data.status === 'confirmed'">
                                <div class="text-bold yep"></div>
                                <div class="dropdown-target arrow-bottom">
                                    <div class="dropdown-inner">
                                        <div class="dropdown-content">
                                            <div class="text-sm">
                                                Оплачено
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-trigger"
                                 v-else-if="arr_booking.data.status === 'pending'">
                                <div class="text-bold check"></div>
                                <div class="dropdown-target arrow-bottom">
                                    <div class="dropdown-inner">
                                        <div class="dropdown-content">
                                            <div class="text-sm">
                                                Ожидается оплата
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-trigger"
                                 v-else-if="arr_booking.data.status === 'cancelled' || arr_booking.data.status === 'canceledClient'">
                                <div class="text-bold nope"></div>
                                <div class="dropdown-target arrow-bottom">
                                    <div class="dropdown-inner">
                                        <div class="dropdown-content">
                                            <div class="text-sm">
                                                Отменено
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-item">
                        <div class="text-bold">
                            <nobr>
                                {{arr_booking.data.price}}
                                <span>
                                    {{arr_booking.data.currency}}
                                </span>
                            </nobr>
                        </div>
                    </div>
                    <div class="table-item table-actions">
                        <div v-if="arr_booking.data.status === 'confirmedOperator' || arr_booking.data.status === 'confirmed'">
                            <a class="btn btn-extrasmall orange" @click="downloadAll(arr_booking)">
                                <span>Билеты</span>
                            </a>
                        </div>

                        <div v-if="arr_booking.data.status == 'pending'">
                            <FORM ACTION="https://pay169.paysec.by/pay/order.cfm" METHOD="POST">
                                <INPUT TYPE="HIDDEN" NAME="Merchant_ID" VALUE="488693">
                                <INPUT TYPE="HIDDEN" NAME="OrderNumber" :VALUE="`${arr_booking.data.order_id}`">
                                <INPUT TYPE="HIDDEN" NAME="Language" VALUE="RU">
                                <INPUT TYPE="HIDDEN" NAME="OrderAmount" :VALUE="`${arr_booking.data.price}`">
                                <INPUT TYPE="HIDDEN" NAME="OrderCurrency"
                                       VALUE="<?= travelsoft\eurotrans\Settings::CURRENT_CURRENCY ?>">

                                <INPUT TYPE="HIDDEN" NAME="Lastname" :value="`${arr_booking.data.last_name}`">
                                <INPUT TYPE="HIDDEN" NAME="Firstname" :value="`${arr_booking.data.name}`">
                                <INPUT TYPE="HIDDEN" NAME="Email" :value="`${arr_booking.data.email}`">
                                <INPUT TYPE="HIDDEN" NAME="MobilePhone" :value="`${arr_booking.data.phone}`">

                                <INPUT TYPE="HIDDEN" NAME="URL_RETURN"
                                       VALUE="https://eurotrans.by/personal-account/">

                                <button type="submit" class="btn btn-extrasmall orange" NAME="Submit">
                                    <span>Оплатить</span>
                                </button>
                            </FORM>
                        </div>

                        <div v-if="arr_booking.data.status != 'cancelled' && arr_booking.data.status != 'canceledClient'">
                            <a class="link-border gray" @click="cancelBooking(arr_booking.data.order_id)">
                                <span>Отказаться</span>
                            </a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>


<script>
    new Vue({
        el: '#user-bookings',
        data: {
            current_page: 1,
            count_pages: 0,
            count_per_page: 15,
            bookings: []
        },
        computed: {
            pages() {
                return Array.from({length: this.count_pages}, (v, k) => k + 1);
            },
            activeDecPage() {
                return this.current_page === 1;
            },
            activeIncPage() {
                return this.current_page === this.count_pages;
            },
        },
        created() {
            this.getBookings();
        },
        methods: {
            downloadAll(arr_booking){
                let urls = [];
                arr_booking.bookings.forEach((booking) => {
                    booking.arrSeatsId.forEach((seat) => {
                        urls.push(seat.url);
                    });
                });


                let link = document.createElement('a');

                link.setAttribute('download', null);
                link.style.display = 'none';
                document.body.appendChild(link);

                urls.forEach(function (item) {
                    link.setAttribute('href', item);
                    link.click();
                });

                document.body.removeChild(link);
            },
            onSeatUpdate(booking, oldSeat, newSeat) {
                Vue.set(booking.arrSeatsId, booking.arrSeatsId.indexOf(oldSeat), newSeat);
            },
            getBookings() {
                let _this = this;

                $.ajax({
                    type: 'post',
                    url: '/local/components/travelsoft/orders.list/ajax/getBookings.php',
                    dataType: "json",
                    data: {
                        current_page: this.current_page,
                        page_count: this.count_per_page,
                    },
                    cache: false,
                    success: function (data) {
                        _this.count_pages = data.count_pages;
                        _this.current_page = data.current_page;

                        data.bookings.forEach((booking) => {
                            booking.bookings.forEach((arrBooking) => {
                                arrBooking.arrSeatsId.forEach((seat) => {
                                    seat.active = false;
                                })
                            })
                        });

                        _this.bookings = data.bookings;
                    },
                    beforeSend: function (xhr) {

                    }
                });
            },
            decPage() {
                this.current_page--;
                this.getBookings();
            },
            setPage(page) {
                this.current_page = page;
                this.getBookings();
            },
            incPage() {
                this.current_page++;
                this.getBookings();
            },
            cancelBooking(id) {
                let _this = this;
                showPopupCancelBooking()
                    .then(() => {
                        $.ajax({
                            type: 'post',
                            url: '/local/components/travelsoft/orders.list/ajax/cancelBooking.php',
                            dataType: "json",
                            data: {id: id},
                            success: function (data) {
                                console.log(data);
                            },
                            beforeSend: function (xhr) {

                            },
                            complete: function () {
                                _this.getBookings();
                            }
                        });
                    })
                    .catch(() => {
                        //console.log('not cancel booking');
                    })
                    .finally(() => {
                        $('body').find('#payback').remove();
                    })
                ;
            },
        },
    });

    Vue.component('DetailPassengerPopup', {
        props: {
            seat: {
                type: Object,
                required: true
            },
            booking: {
                type: Object,
                required: true
            },
            arr_booking: {
                type: Object,
                required: true
            },
        },
        data: {
            popupSeat: {},
        },
        created() {
            this.popupSeat = JSON.parse(JSON.stringify(this.seat));
        },
        methods: {
            clickOutsideEvent(event) {
                this.popupSeat.active = false;
                this.$emit('seat-updated', this.popupSeat);
            }
        },
        template: `<div class="modal-layout js-popup-container active modal-passenger" data-modal="details">
        <div class="modal-container" v-click-outside="clickOutsideEvent">
          <div class="modal-container-header">
            <div class="table-navigation">
              <div class="table-navigation-links">
                <a class="table-navigation-link" :href="seat.url" v-if="arr_booking.data.status === 'confirmedOperator' || arr_booking.data.status == 'confirmed'">
                  <i>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 15.8 13.8" style="enable-background:new 0 0 15.8 13.8;" xml:space="preserve">
                      <style type="text/css">.sttable0{fill:#A8AFBD;}</style>
                      <path class="sttable0" d="M11.9,11.8C11.8,11.8,11.8,11.8,11.9,11.8c-0.2,0-0.3-0.1-0.4-0.3c0-0.2,0.1-0.3,0.3-0.4								c1.9-0.2,3.3-1.9,3.3-3.7c0-2.1-1.7-3.7-3.7-3.7c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3-0.1-0.3-0.2c-0.6-1.7-2.2-2.8-4-2.8								c-2.3,0-4.2,1.9-4.2,4.2c0,0.2,0,0.4,0.1,0.6c0,0.2-0.1,0.3-0.2,0.4C1.4,6.2,0.7,7.3,0.7,8.4c0,1.5,1.2,2.7,2.7,2.7h0.6								c0.2,0,0.3,0.1,0.3,0.3c0,0.2-0.1,0.3-0.3,0.3H3.3C1.5,11.8,0,10.3,0,8.4c0-1.4,0.8-2.6,2-3.1C2,5.2,2,5,2,4.9C2,2.2,4.2,0,6.9,0								c2,0,3.8,1.2,4.5,3c0,0,0,0,0,0c2.4,0,4.4,2,4.4,4.4C15.8,9.6,14,11.5,11.9,11.8z M6.1,11.3l1.4,1.4V7.6c0-0.2,0.1-0.3,0.3-0.3								s0.3,0.1,0.3,0.3v5.1l1.4-1.4c0.1-0.1,0.3-0.1,0.5,0c0.1,0.1,0.1,0.3,0,0.5l-2,2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0								c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1-0.1l-2-2c-0.1-0.1-0.1-0.3,0-0.5C5.8,11.1,6,11.1,6.1,11.3z"></path>
                    </svg>
                  </i>
                  <span>Скачать</span>
                </a>
              </div>
            </div>
            <i class="small-link pop-close closePopup" @click="clickOutsideEvent"></i>
          </div>
          <div class="modal-container-content">
            <div class="person-profile-wrapper">
              <div class="person-profile-about">
                <div class="person-profile-about-inner">
                  <div class="person-profile-elem s33">
                    <div class="text-sm">Пассажир
                      <br>ФИО
                    </div>
                    <div class="text-1">
                      {{seat.last_name}} {{seat.first_name}} {{seat.patronymic}}
                    </div>
                  </div>
                  <div class="person-profile-elem s33">
                    <div class="text-sm">Дата продажи</div>
                    <div class="text-1">
                      {{arr_booking.data.created_date}} {{arr_booking.data.created_time}}
                    </div>
                  </div>
                  <div class="person-profile-elem s33">
                    <div class="text-sm">Плательщик</div>
                    <div class="text-1">
                      {{booking.female}} {{booking.name}}
                    </div>
                  </div>
                  <div class="person-profile-elem s33">
                    <div class="text-sm">Стоимость</div>
                    <div class="text-1">
                      {{booking.price}}
                    </div>
                  </div>
                  <div class="person-profile-elem s33">
                    <div class="text-sm">Тип билета</div>
                    <div class="text-1">
                      {{seat.seat_name}}
                    </div>
                  </div>
                  <div class="person-profile-elem s33">
                    <div class="text-sm">№ места</div>
                    <div class="text-1">
                      {{seat.name}}
                    </div>
                  </div>
                  <div class="person-profile-elem s33">
                    <div class="text-sm">Перевозчик</div>
                    <div class="text-1">
                      {{arr_booking.data.carrier}}
                    </div>
                  </div>
                  <div class="person-profile-elem s33" v-if="booking.platform_from !== ''">
                    <div class="text-sm">Отправление</div>
                    <div class="text-1">
                      {{booking.platform_from}}
                    </div>
                  </div>
                  <div class="person-profile-elem s33" v-if="booking.platform_to !== ''">
                    <div class="text-sm">Прибытие</div>
                    <div class="text-1">
                      {{booking.platform_to}}
                    </div>
                  </div>
                </div>
              </div>
              <div class="person-profile-reys">
                <div class="person-profile-reys-inner">
                  <div class="person-profile-elem s100">
                    <div class="text-sm">Маршрут</div>
                    <div class="text-1">
                      {{booking.departure}} &rarr; {{booking.arrival}}
                    </div>
                  </div>
                  <div class="person-profile-elem s100">
                    <div class="text-sm">Время отправления</div>
                    <div class="text-1">
                      {{booking.booking_datetime.date}} {{booking.booking_datetime.time}}
                    </div>
                  </div>
                  <div class="person-profile-elem s100">
                    <div class="text-sm">Время прибытия</div>
                    <div class="text-1">
                      {{booking.stop_datetime.date}} {{booking.stop_datetime.time}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>`
    });


    Vue.directive('click-outside', {
        bind: function (el, binding, vnode) {
            this.event = function (event) {
                if (!(el == event.target || el.contains(event.target))) {
                    vnode.context[binding.expression](event);
                }
            };
            document.body.addEventListener('click', this.event)
        },
        unbind: function (el) {
            document.body.removeEventListener('click', this.event)
        },
    });


</script>
