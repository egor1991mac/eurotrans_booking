<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult as $key=>$arItem){

    if(!empty($arItem["PROPERTIES"]["AUTHOR"]["VALUE"])){

        $rsUser = CUser::GetByID($arItem["PROPERTIES"]["AUTHOR"]["VALUE"]);
        $arUser = $rsUser->Fetch();
        $arResult[$key]["PROPERTIES"]["AUTHOR"]["DESC"] = $arUser["NAME"]." ".$arUser["LAST_NAME"];

    }

}